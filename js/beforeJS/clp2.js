/*******************************************************************************
 * CLP.JS Version: 1.0.1
 ******************************************************************************/
// TEST
if (typeof String.prototype.startsWith != 'function') {
String.prototype.startsWith = function(str) {
    return this.indexOf(str) == 0;
};
}

// Triggers the AutoAdd function based on if checkbox exists
   function triggerAutoAdd() {
    var checkBox = $('.autoAdd_checkBox').html();
var cookieVal = getCookie('tt');
var checkmark = $('.autoAdd_checkMark').html();

// Check for checkbox
   if (checkBox) {

// runs when you check the box
            $('.autoAdd_1').click(function() {
if ($(this).prop("checked") == true) {
if (typeof AUTOADD_SLOT_OVERRIDE != 'undefined') {
addAutoAddSlot(AUTOADD_SLOT_OVERRIDE);
} else {
  addAutoAddSlot("2");
  }
$('.autoAdd_1').attr('checked', 'true');
} else {
  addAutoAddSlot("-1");
      $('.autoAdd_1').attr('checked', false);
}
});

// Then check for Cookie onload
   var currentSlotValue = getCurrentSlot();
if (currentSlotValue !== null) {
// console.log(currentSlotValue)
if (currentSlotValue == '-1') {
    $('.autoAdd_1').attr('checked', false);
} else {
      $('.autoAdd_1').attr('checked', 'true');
}

}
// If no Cookie
   else {
// uncheck the box, theres no cookie
// console.log("no cookie here");
$('.autoAdd_1').attr('checked', false);
}
}
else {
// There's no checkbox on the page.
}


if (checkmark){

// Buy Button
            $(checkmark).parent().next('.btn_renew').children('a').click(function(){
addAutoAddSlot("-1");
    $('.autoAdd_1').attr('checked', false);
});

//Learn more
            $(checkmark).parent().parent().next().children('a').click(function(){
addAutoAddSlot("-1");
    $('.autoAdd_1').attr('checked', false);
});
}
else {}
}

function onReady_entitlement() {
    var numberOfPCs = $data('IUC');
    var pcValueSplit = numberOfPCs.split("-");
    var pcValue = pcValueSplit[1];

$("*[class*='entitlement']:contains('{SEAT_COUNT}')").text(function() {
    return $(this).text().replace("{SEAT_COUNT}", pcValue);
});
}

function addAutoAddSlot(slotNo) {
    var slotNoKey = "SLOTNO";
    var currentTTCookieString = getCookie('tt');
    var updatedString;

if (currentTTCookieString != null) {
    var splitCurrentTTCookieString = currentTTCookieString.split('|');
    for (i = 0; i < splitCurrentTTCookieString.length; i++) {
if (splitCurrentTTCookieString[i].indexOf('SLOTNO') != -1) {
    splitCurrentTTCookieString.splice(i, 1, slotNoKey + ":"
    + slotNo);
    var updatedString = splitCurrentTTCookieString.join("|");
    break;
}
}
if (updatedString) {
setCookie('tt', updatedString, 3);
} else {
      slotAddedString = currentTTCookieString.concat('|').concat(
          slotNoKey).concat(':').concat(slotNo);
  setCookie('tt', slotAddedString, 3);
  }
} else {
      slotAddedString = slotNoKey + ":" + slotNo;
  setCookie('tt', slotAddedString, 3);
  }
}

function getCurrentSlot() {
    var slotNoKey = "SLOTNO";
    var currentTTCookieString = getCookie('tt');
    var updatedString;

if (currentTTCookieString != null) {
    var splitCurrentTTCookieString = currentTTCookieString.split('|');
    for (i = 0; i < splitCurrentTTCookieString.length; i++) {
if (splitCurrentTTCookieString[i].indexOf('SLOTNO') != -1) {
    var splitSlot = splitCurrentTTCookieString[i].split(':');
    return splitSlot[1];
    break;
}
}
}
return null;
}

// work with select drop down
   var selectObj = {
getSlotUpdate : function(parent) {
    var dropdownOverrideValuesArray = jQuery.trim($('#dropdownOverrideValues').html()).split(/;/);
var productId = parent.attr('data-product-id');
var slot = null,
           overrideRule = '^' + productId;
// override rule lists dropdown as 1, 2, 3, 4 ...
		for ( var i = 1; i <= $(parent).find('select').length; i++) {
    overrideRule += ':' + $(parent).find('select[data-dropdown-id="' + i + '"]').val();
}
jQuery.each(dropdownOverrideValuesArray, function(j, rule) {
if (rule.match(new RegExp(overrideRule, 'gi'))) {
    slot = rule.replace(/.*:/gi, '');
    return slot;
}
});
return slot;
},
getValuePostfix : function(valDefault) {
    var valDefaultArray = valDefault.split('_');
    return '_' + valDefaultArray[1];
},
update : function(parent) {
    var slot = selectObj.getSlotUpdate(parent);
    if (slot == null)
return;

// update price
   var price = $('span.salePrice[data-slot="' + slot + '"]').html();
$(parent).find('.priceCurrent').html(price);

// update buy button
		$(parent).find('.bttnBuyItNow a').attr('data-slot', slot);
var valueDefault = $(parent).find('.bttnBuyItNow a').attr('value')
var valuePostfix = selectObj.getValuePostfix(valueDefault);
$(parent).find('.bttnBuyItNow a').attr('value', slot + valuePostfix);
}
};
var radioObj = {
getSlot : function(thiz) {
    return $(':radio[name=' + ele + ']').filter(":checked").val();
}
};
// function to help with radio buttons
   var checkLink = {
getSlt : function(thiz) {
// what element are we talking about
   var ele = $(thiz).attr('class');
var classes = ele.split(" ");
ele = classes[0];

// get the corresponding input boxes
   var val = $(':radio[name=' + ele + ']').filter(":checked").val(); // if we have radio buttons
// var val = radioObj.getSlot(thiz); // replace the above statement

                                        if (val == null) {
// if we have dropdown lists
   var parent = $(thiz).parent().parent();
var slotUpdate = selectObj.getSlotUpdate(parent);
var valDefault = $('.' + ele).filter('[data-uid="NoRadioBuyButton"]').attr('value');
if (slotUpdate == null) {
    val = valDefault;
} else {
      val = slotUpdate + selectObj.getValuePostfix(valDefault);
  }
}
if (val == null) { // If it is a straight forward add-to-cart
                      val = $('.' + ele).filter('[data-uid="NoRadioBuyButton"]').attr('value');
}

if (val == null) {
    console.log('Need page slotting fix. ele:', ele, 'val:', val);
}

return val;
},

prodDisplay : function(thiz) {
// get the more info link
   var slot = this.getSlt(thiz);
var ref = $('.m' + slot).attr('href');
window.open(ref,'','scrollbars=yes,location=no,menuBar=no,resizable=no,status=no,toolbar=no,width=850,height=550,left=200,top=200');
},

addBtn : function(thiz) {
    var slot = this.getSlt(thiz);
// go get the correct href
   var ref = $('.a' + slot).attr('href');
// set the href
		$(thiz).attr('href', ref);
}
};

/*******************************************************************************
 * Slotting helpers
 ******************************************************************************/

function getOverride(override_str, is_dual, is_str_value) {
var ret = {
'val' : {},
'isDualOverride' : false,
'overrideDetected' : false
};
var i;
for (i = 1; i <= 18; ++i) {
    ret.val[i] = i;
}

var values = override_str.split(";");
if (values.length == 0 || // no override specified
(is_dual && is_str_value)) // Cannot be both dual and string override
                              return ret; // Assume no override

                                             jQuery.each(values, function() { // NOTE: Don't use "$." -- it confuses
// the velocity parser
   var parts = this.split(":");
parts[0] = parseInt(jQuery.trim(parts[0]));
parts[1] = jQuery.trim(parts[1]);
if (!is_str_value)
parts[1] = parseInt(parts[1]);
if ((parts[0] != parts[1]) && !isNaN(parts[0])
&& (is_str_value || !isNaN(parts[1]))) {
    ret.val[parts[0]] = parts[1];
    if (is_dual)
ret.val[parts[1]] = parts[0];
ret.overrideDetected = true;
}
});
return ret;
}

function readDualOverride(override_str) {
var ret = {
'val' : {
1 : 1,
2 : 2,
3 : 3,
4 : 4,
5 : 5,
6 : 6
},
'isDualOverride' : true,
'overrideDetected' : false
};
var values = override_str.split(";");
if (values.length == 0)
return ret; // Nothing to do

               jQuery.each(values, function() { // NOTE: Don't use "$." -- it confuses the velocity parser
var parts = this.split(":");
parts[0] = jQuery.trim(parts[0]);
parts[1] = jQuery.trim(parts[1]);
var partsInt = [ parseInt(parts[0]), parseInt(parts[1]) ];
if ((partsInt[0] != partsInt[1]) && !isNaN(partsInt[0]) && !isNaN(partsInt[1])) {
    ret.val[partsInt[0]] = partsInt[1];
    ret.val[partsInt[1]] = partsInt[0];
    ret.overrideDetected = true;
}
});
return ret;
}

// will change if we use onewaySlotOverride
   function overrideDataSlots(selector, overrides) {
$(selector).each(function() {
    var slotValue = $(this).attr('data-slot');
if (slotValue != null) {
    var slotValueInt = parseInt(slotValue);
if (!isNaN(slotValueInt) && overrides.val.hasOwnProperty(slotValueInt)) {
    $(this).attr('data-slot', overrides.val[slotValueInt]);
}
}
});
}

// will change if we use onewaySlotOverride
   function overrideDualDataSlots(selector, overrides) {
// swap the slots if necessary
	$(selector).each(function() {
    var slots = $(this).attr('data-slot');
slots = slots.split(":");
var s;
for (s in slots) {
    if (slots.hasOwnProperty(s))
slots[s] = parseInt(slots[s]);
}

for (s in slots) {
    if (slots.hasOwnProperty(s))
slots[s] = overrides.val[slots[s]];
}
var i = 0;
var slots_str = "";
for (s in slots) {
if (slots.hasOwnProperty(s)) {
    slots_str += slots[i];
if (i != (slots.length - 1)) {
    slots_str += ":";
}
i++;
}
}

$(this).attr('data-slot', slots_str);
});
}

function overrideValues(selector, overrides) {
$(selector).each(function() {
    var value = $(this).attr('value');
if (value != null) {
    var parts = value.split("_"); // NOTE: Assuming that value is always format X_Y (eg., 18_13)
var originalSlotInt = parseInt(parts[0]);
if (!isNaN(originalSlotInt) && overrides.val.hasOwnProperty(originalSlotInt)) {
    $(this).attr('value',overrides.val[originalSlotInt] + '_'+ parts[1]);
}
}
});
}

// will change if we use onewaySlotOverride
   function overrideClassName(selector, classPrefix, overrides) {
$(selector).each(function() {
    var classesString = $(this).attr('class');
var classes = classesString.split(' '); // NOTE: Assuming that value is always format X_Y (eg., 18_13)
var newClassesString = '';
var re = new RegExp("^" + classPrefix + "\\d+_\\d+$");
for ( var i in classes) {
if (classes.hasOwnProperty(i)) {
if (classes[i].match(re)) {
    var sub = classes[i].split('_');
    var originalSlot = sub[0].substring(1);
if (overrides.val.hasOwnProperty(originalSlot)) {
    newClassesString += classPrefix + overrides.val[originalSlot] + '_' + sub[1] + ' ';
} else {
      newClassesString += classes[i] + ' ';
  }
} else {
      newClassesString += classes[i] + ' ';
  }
}
}
$(this).attr('class', newClassesString);
});
}

// may change if we use onewaySlotOverride
   function overrideEverything(overrides) {
    var i;
// don't use jQuery.each() -- gives a 'String' array
for (i = 0; i < g_slotOverride.slotOverrideSelectors.length; ++i) {
overrideDataSlots(g_slotOverride.slotOverrideSelectors[i], overrides);
}

    for (i = 0; i < g_slotOverride.dualSlotOverrideSelectors.length; ++i) {
overrideDualDataSlots(g_slotOverride.dualSlotOverrideSelectors[i],
overrides);
}

    for (i = 0; i < g_slotOverride.valueOverrideSelectors.length; ++i) {
overrideValues(g_slotOverride.valueOverrideSelectors[i], overrides);
}

    for (i = 0; i < g_slotOverride.classOverrideSelectors.length; i += 2) {
overrideClassName(g_slotOverride.classOverrideSelectors[i], g_slotOverride.classOverrideSelectors[i + 1], overrides);
}
}

/*******************************************************************************
 * For TnT
 ******************************************************************************/

var g_slotOverride = {};
g_slotOverride.slotOverrideSelectors = [
// eBizSlot override handling
'[data-uid="DisplayName"]', '[data-uid="AddToCart"]', '[data-uid="MoreInfo"]',
'[data-uid="SalePrice"]', '[data-uid="ListPrice"]','[data-uid="SavePrice"]', '[data-uid="SavePercent"]',

// MSRP Override handling
'.mo_salePrice[data-uid="MO_SalePrice"]',
'.mo_listPrice[data-uid="MO_ListPrice"]',
'.mo_savePercent[data-uid="MO_SavePercent"]',
'.mo_savePrice[data-uid="MO_SavePrice"]',
'.mo_msrpOverride[data-uid="MO_ListPrice"]'
];
g_slotOverride.dualSlotOverrideSelectors = [ '.mo_savePercentMsg','.mo_savePriceMsg' ]; // MSRP Override handling
                                                                                           g_slotOverride.valueOverrideSelectors = [ '.SObuyNowChanger','[data-uid="NoRadioBuyButton"]' ];
g_slotOverride.classOverrideSelectors = [ '[data-uid="AddToCart"]', 'a','[data-uid="MoreInfo"]', 'm' ];
g_slotOverride.backend_overrides = {};
g_slotOverride.frontend_overrides = {};

function onReady_correctForEBizSlotOverrides() {
    var override = readDualOverride($('#slotOverrideValues').text());
    g_slotOverride.backend_overrides = override;
    if (!override.overrideDetected)
return; // Nothing to do

overrideEverything(g_slotOverride.backend_overrides);
}

/*******************************************************************************
 * Check for dual or one way slot override
 ******************************************************************************/
function onReady_checkForSlotOverrides() {
if ($('#slotOverrideValues').text().replace(/^\s+|\s+$/, '').length != 0) {
onReady_computeSavings();
onReady_correctForEBizSlotOverrides();
} else if ($('#onewaySlotOverrideValues').text().replace(/^\s+|\s+$/, '').length != 0) {
onReady_applySlotOverrides();
} else {
  onReady_computeSavings();
  }
}

/*******************************************************************************
 * Oneway slot override functions
 ******************************************************************************/

var g_clp = {};
g_clp.currency = {};
g_clp.slot_overrides = {};
g_clp.msrp_overrides = {};
g_clp.parent_id_filters = [ "MO_Hack", "MetaInfoPanel" ];
g_clp.value_override_selectors = [ '.SObuyNowChanger', '[data-uid="NoRadioBuyButton"]' ];
g_clp.slot_override_selectors = [ '[data-uid="MO_ListPrice"]','[data-uid="MO_SavePercent"]' ];

g_clp.savePercentValues = [];
g_clp.savePriceValues = [];
function getSlotOverride(override_str) {
var ret = {
'val' : {},
'isDualOverride' : false,
'overrideDetected' : false
};
var i;
for (i = 1; i <= 18; ++i) {
    ret.val[i] = i;
}

var values = override_str.split(";");
if (values.length == 0)
return ret; // Assume no override

               jQuery.each(values, function() { // NOTE: Don't use "$." -- it confuses the velocity parser
var parts = this.split(":");
parts[0] = parseInt(jQuery.trim(parts[0]));
parts[1] = parseInt(jQuery.trim(parts[1]));
if (!isNaN(parts[0]) && !isNaN(parts[1])) {
    ret.val[parts[0]] = parts[1];
    ret.overrideDetected = true;
}
});
return ret;
}

function getMsrpOverride(override_str) {
var ret = {
'val' : {},
'isDualOverride' : false,
'overrideDetected' : false
};
var i;
for (i = 1; i <= 18; ++i) {
    ret.val[i] = new Price("0.0", g_clp.currency);
}

var values = override_str.split(";");
if (values.length == 0)
return ret; // Assume no override

               jQuery.each(values, function() { // NOTE: Don't use "$." -- it confuses the velocity parser
var parts = this.split(":");
parts[0] = parseInt(jQuery.trim(parts[0]));
parts[1] = jQuery.trim(parts[1]);
if (!isNaN(parts[0]) && parts[1] != "" && parts[1] != "0" && parts[1] != "d") {
    parts[1] = parseFloat(parts[1]);
if (!isNaN(parts[1])) {
    var priceString = $('.mo_msrpOverride[data-uid="MO_ListPrice"][data-slot="'+ parts[0] + '"]').text();
ret.val[parts[0]] = new Price(priceString, g_clp.currency);
ret.overrideDetected = true;
}
}
});
return ret;
}

function isParentMatch(thiz, parent_ids) {
    var parent_id = $(thiz).parent().attr("id");
if (parent_id != null) {
    var idx = jQuery.inArray(parent_id, parent_ids);
if (idx >= 0) {
    return true;
}
}

return false;
}

function getSlot(thiz, attribute) {
    var slot = $(thiz).attr(attribute);
if (slot == null || slot == "")
return null;

if (slot.indexOf(":") != -1) {
    var slots = slot.split(":");
    slot = [];
    var s;
for (s in slots) {
if (slots.hasOwnProperty(s)) {
    slot.push(parseInt(slots[s]));
}
}
} else {
      slot = parseInt(slot);
  }
return slot;
}

function overrideDataSlot(dest_selector, src_selector, overrides, parent_id_filters) {
$(dest_selector).each(function() {
if (!isParentMatch(this, parent_id_filters)) {
    var slot = getSlot(this, 'data-slot-orig');

if (slot == null) {
    slot = getSlot(this, 'data-slot');
}

var newSlot = overrides.val[slot];

if (src_selector != "") {
    var newVal = $(src_selector + '[data-slot="'+ overrides.val[slot] + '"]').text();
$(this).text(newVal);
}
$(this).attr('data-slot-orig', slot);
$(this).attr('data-slot', overrides.val[slot]);
}
});
}

function overrideDualDataSlot(dest_selector, overrides, parent_id_filters) {
$(dest_selector).each(function() {
if (!isParentMatch(this, parent_id_filters)) {
    var slot = getSlot(this, 'data-slot-orig');
if (slot == null) {
    slot = getSlot(this, 'data-slot');
}
$(this).attr('data-slot-orig', slot[0] + ":" + slot[1]);
$(this).attr('data-slot',overrides.val[slot[0]] + ":" + overrides.val[slot[1]]);
}
});
}

function overrideValueAttribs(selector, overrides, parent_id_filters) {
$(selector).each(function() {
if (!isParentMatch(this, parent_id_filters)) {
    var value = $(this).attr('data-value-orig');
if (value == null || value == "") {
    value = $(this).attr('value');
}
if (value != null) {
    var parts = value.split("_"); // NOTE: Assuming that value is always format X_Y (eg., 18_13)
var originalSlotInt = parseInt(parts[0]);
if (!isNaN(originalSlotInt) && overrides.val.hasOwnProperty(originalSlotInt)) {
    $(this).attr('data-value-orig', originalSlotInt + '_' + parts[1]);
$(this).attr('value',overrides.val[originalSlotInt] + '_'+ parts[1]);
}
}
}
});
}

function computeSavings(slot_overrides, msrp_overrides) {
    var slot;
    for (slot = 1; slot <= 18; ++slot) {
g_clp.savePercentValues[slot] = 0;
    g_clp.savePriceValues[slot] = new Price("0.00", g_clp.currency);
}

// Compute the savings percent and savings price per slot.
// Also save it in MO_Hack <div> for debugging.
/*
 * $('.mo_msrpOverride').each(function(){ var slot = getSlot(this,
 * 'data-slot'); var newSlot = slot_overrides.val[slot]; var priceString =
 * $('.mo_salePrice[data-uid="MO_SalePrice"][data-slot="' + newSlot +
 * '"]').text(); var salePrice = new Price(priceString, g_currency); var
 * msrp = new Price($(this).text(), g_currency);
 *
 * var savePercent = ((msrp.val-salePrice.val)/msrp.val) * 100; savePercent =
 * roundNumber(savePercent, 0, false); g_clp.savePercentValues[slot] =
 * savePercent; var savePrice = msrp.val-salePrice.val; savePrice =
 * roundNumber(savePrice, 2, false); var savePriceStruct = new
 * Price(savePrice.toString(), g_currency); g_clp.savePriceValues[slot] =
 * savePriceStruct;
 * //$('.mo_savePercent[data-uid="MO_SavePercent"][data-slot="' + newSlot +
 * '"]').text(savePercent + "%");
 * //$('.mo_savePrice[data-uid="MO_SavePrice"][data-slot="' + newSlot +
 * '"]').text(savePriceStruct.toString());
 * $('.mo_listPrice[data-uid="MO_ListPrice"][data-slot-orig="' + slot +
 * '"]').each(function(){ if(! isParentMatch(this, g_clp.parent_id_filters)) {
 * $(this).text(msrp.toString()); } });
 * //$('[data-uid="ListPrice"][data-slot="' + newSlot +
 * '"]').text(msrp.toString()); });
 */

for (slot = 1; slot <= 18; ++slot) {
var newSlot = slot_overrides.val[slot];
    var priceString = $('.mo_salePrice[data-uid="MO_SalePrice"][data-slot="' + newSlot+ '"]').text();
var salePrice = new Price(priceString, g_clp.currency);
var msrp = msrp_overrides.val[slot];

var savePercent = ((msrp.val - salePrice.val) / msrp.val) * 100;
savePercent = roundNumber(savePercent, 0, false);
g_clp.savePercentValues[slot] = savePercent;

var savePrice = msrp.val - salePrice.val;
savePrice = roundNumber(savePrice, 2, false);
var savePriceVal = Math.max(0, savePrice);
var savePriceStruct = new Price(savePriceVal.toString(), g_clp.currency);
g_clp.savePriceValues[slot] = savePriceStruct;

$('.mo_listPrice[data-uid="MO_ListPrice"][data-slot-orig="'+ slot + '"]').each(function() {
if (!isParentMatch(this, g_clp.parent_id_filters)) {
    $(this).text(msrp.toString());
}
});
}

$('.mo_savePercentMsg').each(function() {
    var slot = getSlot(this, 'data-slot-orig');
if (slot == null) {
    slot = getSlot(this, 'data-slot');
}
var savePercent = Math.max(0, g_clp.savePercentValues[slot]);
$(this).text(savePercent + "%");
});

$('.mo_savePriceMsg').each(function() {
    var slot = getSlot(this, 'data-slot-orig');
if (slot == null) {
    slot = getSlot(this, 'data-slot');
}
$(this).text(g_clp.savePriceValues[slot].toString());
});
}

function onReady_applySlotOverrides() {
    g_clp.slot_overrides = getSlotOverride($('#onewaySlotOverrideValues').text());
    g_clp.msrp_overrides = getMsrpOverride($('.mo_msrpOverrideValues').text());

overrideDataSlot('[data-uid="SalePrice"]', "#MetaInfoPanel .salePrice", g_clp.slot_overrides, g_clp.parent_id_filters);

var i;
for (i = 0; i < g_clp.slot_override_selectors.length; ++i) {
overrideDataSlot(g_clp.slot_override_selectors[i], "", g_clp.slot_overrides, g_clp.parent_id_filters);
}

// overrideValueAttribs(".SObuyNowChanger", g_clp.slot_overrides,
// g_clp.parent_id_filters);

var j;
for (j = 0; j < g_clp.value_override_selectors.length; ++j) {
overrideValueAttribs(g_clp.value_override_selectors[j], g_clp.slot_overrides, g_clp.parent_id_filters);
}

computeSavings(g_clp.slot_overrides, g_clp.msrp_overrides)
}

/*******************************************************************************
 * For MSRP via Content Hack
 ******************************************************************************/

function roundNumber(num, dec, nearest) {
    var bias = nearest ? 0.5 : 0.0;
    var result = Math.round(num * Math.pow(10, dec) + bias) / Math.pow(10, dec);
    return result;
}

function Currency(priceString) {
    var priceMatches = priceString.match(/[0-9].*[0-9]/g);
    var currencyPosition = priceString.match(/[^0-9\.,\s]/);
if (priceMatches == null || currencyPosition == null) {
// we should not be here
   this.symbol = "$";
this.precedes = true;
this.useComma = false;
} else {
      var pricePosition = priceString.match(/[0-9]/);
      var currencyPrecedes = true;
if (currencyPosition && pricePosition && currencyPosition.index > pricePosition.index) {
    currencyPrecedes = false;
}
var currencyVal = priceString.replace(/[0-9].*[0-9]/g, "");
currencyVal = currencyVal.replace(/\s*/g, "");

this.symbol = (currencyVal == "") ? "$" : currencyVal;
this.precedes = currencyPrecedes;
this.useComma = (priceMatches[0].match(/\./) == null && priceMatches[0].match(/,/) != null); // currency symbol can have a comma as well
}
}

Currency.prototype.toString = function() {
    return this.symbol.toString();
};

function Price(priceString, currency) {
    var priceMatches = priceString.match(/[0-9].*[0-9]/g);
if (priceString.match(/^[\s]*$/) || (priceMatches == null)) {
    this.val = 0;
} else {
      var price = priceMatches[0];
if (currency.useComma) {
    price = price.replace(",", ".");
}
price = price.replace(/\s/, "");
price = price.replace(/[^0-9\.]/, "");
price = parseFloat(price);

if(price.toString().indexOf(",") !== -1 || price.toString().indexOf(".") !== -1) {
    price = price.toFixed(2);
}

this.val = price;
}
this.currency = currency;
}

Price.prototype.toString = function() {
    var priceString = this.val.toString();
if (this.currency.useComma) {
    priceString = priceString.replace(".", ",");
}
if (this.currency.precedes) {
    return this.currency + " " + priceString;
}

return priceString + " " + this.currency;
};

var g_currency = {};
function onReady_computeCurrency() {
// get the currency.  Will use the SalePrice from slot 3 as not all the CLPs have salePrice for all 18 slots.  Slot 3 will
// have SalePrice as it's for incoming product.
var priceString = $('.mo_salePrice[data-uid="MO_SalePrice"][data-slot="3"]').text();
g_currency = new Currency(priceString);
g_clp.currency = new Currency(priceString);
$('.mo_currency[data-uid="MO_Currency"]').text(g_currency);
}

function onReady_computeSavings() {
    var savePercentValues = [];
    var savePriceValues = [];

    var i;
    for (i = 0; i <= 18; ++i) {
savePercentValues.push(0);
    savePriceValues.push(0);
}

// Compute the savings percent and savings price per slot.
// Also save it in MO_Hack <div> for debugging.
$('.mo_msrpOverride').each(function() {
var slot = $(this).attr('data-slot');
    slot = parseInt(slot);
    var priceString = $('.mo_salePrice[data-uid="MO_SalePrice"][data-slot="'+ slot + '"]').text();
var salePrice = new Price(priceString, g_currency);
var msrp = new Price($(this).text(), g_currency);

var savePercent = ((msrp.val - salePrice.val) / msrp.val) * 100;
savePercent = roundNumber(savePercent, 0, false);
savePercentValues[slot] = savePercent;
var savePrice = msrp.val - salePrice.val;
savePrice = roundNumber(savePrice, 2, false);
savePriceValues[slot] = new Price(savePrice.toString(), g_currency);
$('.mo_savePercent[data-uid="MO_SavePercent"][data-slot="'+ slot + '"]').text(savePercent + "%");
$('.mo_savePrice[data-uid="MO_SavePrice"][data-slot="'+ slot + '"]').text(savePriceValues[slot].toString());
if (savePriceValues[slot].val < msrp.val) {
    $('.mo_listPrice[data-uid="MO_ListPrice"][data-slot="'+ slot + '"]').text(msrp.toString());
} else {
      $('.mo_listPrice[data-uid="MO_ListPrice"][data-slot="'+ slot + '"]').text("&nbsp;");
}
$('[data-uid="ListPrice"][data-slot="' + slot + '"]').text(msrp.toString());
});

$('.mo_savePercentMsg').each(function() {
    var slots = $(this).attr('data-slot');
slots = slots.split(":");
var s;
for (s in slots) {
    if (slots.hasOwnProperty(s))
slots[s] = parseInt(slots[s]);
}

var savePercent = 0;
for (s in slots) {
if (slots.hasOwnProperty(s)) {
    savePercent = Math.max(savePercent, savePercentValues[slots[s]]);
}
}
$(this).text(savePercent + "%");
});

$('.mo_savePriceMsg').each(function() {
    var slots = $(this).attr('data-slot');
slots = slots.split(":");
var s;
for (s in slots) {
    if (slots.hasOwnProperty(s))
slots[s] = parseInt(slots[s]);
}

var savePriceVal = 0;
for (s in slots) {
if (slots.hasOwnProperty(s)) {
    savePriceVal = Math.max(savePriceVal, savePriceValues[slots[s]].val);
}
}
var savePrice = new Price(savePriceVal.toString(), g_currency);
$(this).text(savePrice.toString());
});

$(".mo_monthlyPrice").each(function() {
// When prices has not been uploaded to QA server (preview or
// dev11)
if (($(this).text().indexOf('msrp') > -1) || ($(this).text().indexOf('YY.YY') > -1)) {
    $(this).text("$mo");
} else {
// Gets current price
   var getPrice = new Price($(this).text(), g_currency);

// Monthly pricing calculation
   var monthPrice = roundNumber(getPrice.val / 12, 2, false);
if (monthPrice % 1 > 0) {
// Ensures 2 decimal points displayed if not a whole
// number
   monthPrice = monthPrice.toFixed(2);
}
monthPrice = monthPrice.toString();

// Rebuild pricing string
   if (getPrice.currency.useComma) {
    monthPrice = monthPrice.replace(".", ",");
}
if (getPrice.currency.precedes) {
    $(this).text(getPrice.currency + " " + monthPrice);
} else {
      $(this).text(monthPrice + " " + getPrice.currency);
}
}
});
}

/*******************************************************************************
 * For D1 CLPs
 ******************************************************************************/

var OptionsTableHidden = false;

function toggleOptionsTableFeatures() {
    var link = $('#divCompareLink');
if (OptionsTableHidden) {
    $('.featureRow').css('display', '');
link.addClass('divCompareactive');
link.removeClass('divCompareinactive');
link.parent().addClass('tdBorderBottom');
OptionsTableHidden = false;
} else {
      $('.featureRow').css('display', 'none');
link.removeClass('divCompareactive');
link.addClass('divCompareinactive');
link.parent().removeClass('tdBorderBottom');
OptionsTableHidden = true;
}
}

/*******************************************************************************
 * For D2 CLPs
 ******************************************************************************/

var onReady_d2KeyFeatureTab = function() {
$('#keyFeatureTab').click(function() {
togglekeyFeature();
});
};

function togglekeyFeature() {
    var layer = $("#keyFeatureLayer");
    var content = $('.keyFeatureContent');
if (layer.hasClass('showing')) {
$(".keyFeatureContent").animate({ top : '0px'}, 1200, function() {
    layer.removeClass('showing');
});
} else {
$(".keyFeatureContent").animate({ top : '-300px'}, 1200, function() {
    layer.addClass('showing');
});
}
}

var onReady_d2ShowToolTipOnHover = function() {
    $('.jHoverToolTip').hover(showToolTip, hideTooltip);
};
function showToolTip() {
// Element with jToolTip class needs to be next sibling to element with
// jHoverToolTip
	$(this).next('.jToolTip').show();
}
function hideTooltip() {
    $(this).next('.jToolTip').hide();
}

var onReady_d2SlideDownOnHover = function() {
    $('.jHoverExpand').hover(slideDown, slideUp)
};
function slideDown() {
    $("#slide_" + this.id).slideDown(200);
}
function slideUp() {
    $("#slide_" + this.id).slideUp(200);
}

var onReady_d2HoverImageReplace = function() {
    $('.jHoverImage').hover(hoverOnImage, hoverOffImage)
};

var img_url;
var elem;

function hoverOnImage() {
// Gets id and src of img.
	elem = $("img[id*=" + this.id + "]").attr("id");
var src = $("#" + elem).attr("src");
img_url = src;

// Forms new src of img.
	var index = src.lastIndexOf(".");
var ext = src.substring(index, src.length);
var src2 = src.substring(0, index);
var new_src = src2 + "_ro" + ext;

// Replace src with new src.
   $("#" + elem).attr("src", new_src);
}
function hoverOffImage() {
    $("#" + elem).attr("src", img_url);
}

/*******************************************************************************
 * For D3 CLPs
 ******************************************************************************/

var onReady_d3HelpInterstitialRC = function() {
/*
 * $('#helpInterstitial_RC').sym_interstitial({ ajax:
 * '/estore/rc/help?action=90', modal: true, position: { trigger: "Help",
 * window: "helpInterstitial_RC", arrowPos1: "right", arrowPos2: "top" },
 * trigger: 'a.helpTrigger' });
 */

$('#helpInterstitial_RC').sym_interstitial({
ajax : Sym.popupVars.rcHelp,
modal : true,
position : {
    trigger : "Help",
    window : "helpInterstitial_RC",
arrowPos1 : "right",
arrowPos2 : "top"
},
trigger : 'a.helpTrigger'
});
};

/*******************************************************************************
 * For CLP API
 ******************************************************************************/

ClpApi.onReady.make_callbacks_stage = function(stage) {
    var fcn;
for (fcn in this.registrants) {
if (this.registrants.hasOwnProperty(fcn) && this.registrants[fcn][0] === stage) { // Make sure that the order within the same class is maintained
                                                                                     this.registrants[fcn][1]();
}
}
};

ClpApi.onReady.make_callbacks = function() {
    this.make_callbacks_stage("ComputeData");
    this.make_callbacks_stage("CorrectSlots_Backend");
    this.make_callbacks_stage("SetupPage");
    this.make_callbacks_stage("SetupTnt");
    this.make_callbacks_stage("CorrectSlots_Frontend");
};

/*
 * Example Usage ClpApi.onReady.register("CorrectSlots_Frontend", function() {
 * ClpApi.priceTest("1:18;2:2;3:5;4:3;5:1;6:8", "1:129.88; 2:d; 3:32.55; 4:d;
 * 5:54.33; 6:d"); });
 */
ClpApi.priceTest = function(onewayOverride, msrp_override) {
    g_clp.slot_overrides = getSlotOverride(onewayOverride);
    g_clp.msrp_overrides = getMsrpOverride(msrp_override);

    var parent_id_filters = [ "MO_Hack", "MetaInfoPanel" ];
overrideDataSlot('[data-uid="SalePrice"]', "#MetaInfoPanel .salePrice", g_clp.slot_overrides, parent_id_filters);
overrideDataSlot('[data-uid="MO_ListPrice"]', "", g_clp.slot_overrides, parent_id_filters);
overrideValueAttribs(".SObuyNowChanger", g_clp.slot_overrides, parent_id_filters);
overrideDualDataSlot('[data-uid="MO_SavePercent"]', g_clp.slot_overrides, parent_id_filters);
overrideDualDataSlot('[data-uid="MO_SavePrice"]', g_clp.slot_overrides, parent_id_filters);

computeSavings(g_clp.slot_overrides, g_clp.msrp_overrides);
};
/*******************************************************************************
 * Personalization Replace and SymCookie Obj
 ******************************************************************************/

/**
* replace (object)
* Validates if a JSON is a valid JSON and then replaces DCR keys on current page
* with keys from the JSON file
*/
var replace = {
/**
 * hasExecuted
 * Returns false if the replace.validate() did not execute
*/
hasExecuted : false,
    /**
     * isEmpty
     * @param {Object} JSON
     * @return {Boolean}   Returns true if data is not empty else returns false
    */
isEmpty : function(data) {
for ( var i in data) {
    return true;
}
return false;
},
/**
 * validate
 * @param {Object} JSON    JSON data that needs to be validated
 * Checks to see if JSON is not undefined, not empty, not null and is an object
 * then it stringifies the JSON and test it through a regular expression
*/
validate : function(data) {
try {
if ((typeof data !== 'undefined') && (this.isEmpty(data) != false) && (data != '') && (data != null) && (typeof data.valueOf() == "object")) {
// var file = JSON.stringify( data );
// if (/^[\],:{}\s]*$/.test(file.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))){
    this.replaceContent(data);
    this.hasExecuted = true;
// }
}
} catch (e) {
}
},
/**
 * replaceContent
 * @param {Object} JSON
 * Loops through the JSON and replaces html that has matching data-key attribute
*/
replaceContent : function(data) {
    var imgPath = "//buy-static.norton.com/estore/images"; // beginning of image urls to append
                                                              var buyButton = $('.estore_button').sym_button(); // setting the Sym.Button controller into variable
                                                                                                                   var buyButtonController = buyButton.controllers(); // accessing Sym.Button controllers

// loop through the JSON
   for ( var i in data) {
    $('*[data-key="' + i + '"]').html(data[i]); // replace values

// if JSON contains an image, replace the current src with new JSON src
if (data[i].indexOf('.png') != -1 || data[i].indexOf('.gif') != -1 || data[i].indexOf('.jpeg') != -1) {
// Buy/Renew/Ugrade buttons are binded back to Sym.Button controller's rollover event
if ((typeof ($('*[data-key="' + i + '"]').data('key')) !== 'undefined') && (($('*[data-key="' + i + '"]').data('key').indexOf('.BuyNowButton') != -1) || ($('*[data-key="' + i + '"]').data('key').indexOf('.RenewButton') != -1))) {
$(buyButtonController).each(function() {
    this.setButtonSrc(imgPath + data[i])
});
} else {
$('*[data-key="' + i + '"]').attr('src', function() {
    return imgPath + data[i];
});
}
}
//if a Key contains hide, hide it from the DOM
else if( (typeof($('*[data-key="'+i+'"]').data('key')) !== 'undefined') && ($('*[data-key="'+i+'"]').data('key').indexOf('.Hide') != -1) && (data[i] == 'Y') ){
    $('*[data-key="'+i+'"]').hide();
}
// if there is a onewayslotoverride value then replace with new values
   else if ( (typeof($('*[data-key="'+i+'"]').data('key')) !== 'undefined') && ($('*[data-key="'+i+'"]').data('key').indexOf('.onewaySlotOverrideValues') != -1) && (data[i] !== '')){
onReady_applySlotOverrides();
}
// if msrpOverrideValues is not empty then replace with new values
   else if ( (typeof($('*[data-key="'+i+'"]').data('key')) !== 'undefined') && ($('*[data-key="'+i+'"]').data('key').indexOf('.msrpOverrideValues') != -1) && (data[i] !== '')){
    personalizationMsrpOverride.override(data[i]);
    g_clp.msrp_overrides = personalizationMsrpOverride.getMsrpOverride($('#mo_msrpOverrideValues').text());
computeSavings(g_clp.slot_overrides, g_clp.msrp_overrides);
}
}
},
log : function(msg) {
    window.ruleLog = window.ruleLog || [];
    ruleLog.push(msg);
if (typeof console !== "undefined"&& typeof console.log !== "undefined") {
    console.log(msg);
}
}
};
/**
 * symCookie (object)
 * gets, sets and updates a cookie
*/
var symCookie = {
/**
 * getCookie
 * @param {String} The cookie's name
 * @return {String}   Returns the cookie
*/
getCookie : function(c_name){
try{
    var name=c_name+"=";
    var cookies   =  document.cookie.split(';');

    var cookieHex;
for(var i=0;i < cookies.length;i++){
    var c=cookies[i];
while(c.charAt(0)==' '){
    c=c.substring(1,c.length)
};
if(c.indexOf(name)==0){
    cookieHex =  unescape(c.substring(name.length,c.length));

}
}

var cookieString = hexToString(cookieHex);

return cookieString;

} catch(e){}
},
/**
 * setCookie
 * @param {String} The cookie's name
 * @param {Number} The cookie's value encoded to Hex
 * @param {Number} Date of the cookie being set
 * Sets the cookie with the name, value and date
*/
setCookie : function(name, value, days){
try{
    value = encodeToHex(value);
    var expires = "" ;
    var path = "/";
    var domain = "norton.com";
if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    expires = "; expires="+date.toGMTString();
}

var cookie_string = name + "=" + escape ( value ) + expires;
if( path )
cookie_string += "; path=" + escape ( path );
if ( domain )
cookie_string += "; domain=" + escape ( domain );

document.cookie = cookie_string;

}catch(e){}
},
/**
 * hexToString
 * @param {String} Hexidecimal
 * @return {String} Returns the converted hex to a string value if data is not null
 * else it will return null
*/
hexToString : function(data){
if(data !=null){
var b16_digits = '0123456789abcdef';
    var b16_map = new Array();
    for (var i=0; i<256; i++) {
    b16_map[b16_digits.charAt(i >> 4) + b16_digits.charAt(i & 15)] = String.fromCharCode(i);
}
if (!data.match(/^[a-f0-9]*$/i)) return false;// return false if input data is not a valid Hex string

                                                 if (data.length % 2) data = '0'+data;

var result = new Array();
var j=0;
for (var i=0; i<data.length; i+=2) {
    result[j++] = b16_map[data.substr(i,2)];
}

return result.join('');
}

return null;
},
/**
 * encodeToHex
 * @param {String}
 * @return {String} Returns the a hexidecimal value
*/
encodeToHex : function(str){
    var text2='';
    for (i=0;i<str.length;i++)
text2 += str.charCodeAt(i).toString(16);

return text2;
},
/**
 * execute
 * @param {String}
 * @param {String}
 * @param {String}
*/
execute : function(c_name, cid, eid){
    var exdays= 14;
    var cookieString = getCookie(c_name);

if(cookieString != null){
//hexadicmal convertor required to convert cookie
var finalCookie = updateCookie(cookieString,cid,eid);
setCookie(c_name,finalCookie,exdays);
}
    else{
    var new_Cookie = 'CID'.concat(':').concat(cid).concat('|').concat('EID').concat(':').concat(eid);
setCookie(c_name,unescape(new_Cookie),exdays);
}
},
/**
 * updateCookie
 * @param {String} cookieString
 * @param {String} cid
 * @param {String} eid
 * @return {String} Returns string with cookie name along with cid and eid
*/
updateCookie : function(cookieString, cid, eid){

//cid:123,234|eid:aaa,bbb
var cidString = "";
    var eidString = "";
    var cidKey = "";
    var eidKey = "";
    var cidValue = "";
    var eidValue = "";
    var strings = cookieString.split("|");
    var finalString = "";
    var cookieName = "tt";

if(strings != null){
// if order of cid and eid changed
var temp1 = strings[0];
    var temp2 = strings[1];

if(temp1.indexOf('CID') == -1  && temp1.indexOf('cid') == -1) {
    eidString = temp1;
    cidString = temp2;
}

if(temp1.indexOf('EID') == -1  && temp1.indexOf('eid') == -1) {
    cidString = temp1;
    eidString = temp2;
}
}

// CID
if(cidString != null){
    var cidStringToken = cidString.split(":");
    cidKey = cidStringToken[0]; //cid
cidValue = cidStringToken[1]; //123,234

if(cidValue !=null){
cidValue = cidValue.concat(',').concat(cid);
}
}
else{
    cidKey = "CID";
    cidValue = cid;
}

//EID
if(eidString != null){
    var eidStringToken = eidString.split(":");
    eidKey = eidStringToken[0]; //eid
eidValue = eidStringToken[1]; //aaa,bbb
if(eidValue !=null){
eidValue = eidValue.concat(',').concat(eid);
}
}
else{
    eidKey = "EID";
    eidValue = eid;
}

finalString = cidKey.concat(':').concat(cidValue).concat('|').concat(eidKey).concat(':').concat(eidValue);

return finalString;
},
/**
 * appendToCookie
 * @param {String} c_name  cookie name
 * @param {String} c_value  string to append to c_name
 * Returns string with cookie name along with cid and eid
*/
appendToCookie : function(c_name, c_value){
    var exdays= 60;
    var cookieString = getCookie(c_name);

if( (cookieString != null) && (cookieString != '') && (typeof cookieString !== 'undefined') && (cookieString.indexOf(c_value) == -1) ){
    var newCookieString = cookieString.concat('|').concat(c_value);
setCookie(c_name, newCookieString, exdays);
}
}
};

/**
 * personalizationMsrpOverride (object)
 * Returns msrpOverride
*/
var personalizationMsrpOverride = {
/**
 * override
 * @param {String} override_value  string value to override existing value
*/
override : function(override_value){
    var msrpValues = override_value.split(';');
for (var i in msrpValues){
    $('.mo_listPrice[data-slot-orig="'+msrpValues[i].split(':')[0]+'"]').text(g_currency.symbol + $('.mo_msrpOverride[data-slot="'+msrpValues[i].split(':')[1]+'"]').text());
}
},
getMsrpOverride : function(override_value){
var ret = {
'val' : {},
'isDualOverride' : false,
'overrideDetected' : false
};
var i;
for(i=1;i<=18; ++i) {
    ret.val[i] = new Price("0.0", g_clp.currency);
}

var values = override_value.split(";");
if(values.length == 0)
return ret; // Assume no override

               jQuery.each(values, function(){  // NOTE: Don't use "$." -- it confuses the velocity parser
var parts = this.split(":");
parts[0] = parseInt(jQuery.trim(parts[0]));
parts[1] = jQuery.trim(parts[1]);
if(!isNaN(parts[0]) && parts[1] != "" && parts[1] != "0" && parts[1] != "d") {
    parts[1] = parseFloat(parts[1]);
if(!isNaN(parts[1])) {

var priceString = $('.mo_msrpOverride[data-uid="MO_ListPrice"][data-slot="' + parts[1] + '"]').text();
    ret.val[parts[0]] = new Price(priceString, g_clp.currency);

    ret.overrideDetected = true;
}
}
});
return ret;
}
};
/*******************************************************************************
 * Do on document ready
 ******************************************************************************/

ClpApi.onReady.register("ComputeData", onReady_computeCurrency);
ClpApi.onReady.register("CorrectSlots_Backend", onReady_checkForSlotOverrides);

/*
 * ClpApi.onReady.register("ComputeData", onReady_computeSavings);
 * ClpApi.onReady.register("CorrectSlots_Backend",
 * onReady_correctForEBizSlotOverrides);
 */
/*
 * ClpApi.onReady.register("ComputeData", onReady_computeCurrency);
 * ClpApi.onReady.register("CorrectSlots_Backend", onReady_applySlotOverrides);
 */

// TODO: Get the design and version from a hidden div and call this only if
// required
   ClpApi.onReady.register("SetupPage", onReady_d2KeyFeatureTab);
ClpApi.onReady.register("SetupPage", onReady_d3HelpInterstitialRC);
ClpApi.onReady.register("SetupPage", onReady_d2ShowToolTipOnHover);
ClpApi.onReady.register("SetupPage", onReady_d2SlideDownOnHover);
ClpApi.onReady.register("SetupPage", onReady_d2HoverImageReplace);

// Trigger AutoAdd
   ClpApi.onReady.register("SetupPage", triggerAutoAdd);

// Entitlement
   ClpApi.onReady.register("SetupPage", onReady_entitlement);

// Date Format
   ClpApi.onReady.register("SetupPage", clpLocalDateFormat);

/*******************************************************************************
 * EU date format fix (EMEA)
 ******************************************************************************/


function clpLocalDateFormat() {

    var expiryDateVar;

if(window.location.href.indexOf("d60v1_2p4s_n3603_default") > -1 )  {
    expiryDateVar = $(".expiryDate");
} else if(window.location.href.indexOf("d6v1_1p2s_n3601_default") > -1) {
    expiryDateVar = $("#date");
} else if(window.location.href.indexOf("d61v1_1p2s_n3601_default") > -1 || window.location.href.indexOf("d61v1_1p2s_nis1_default") > -1) {
    expiryDateVar = $(".expireMessage");
}

if(expiryDateVar !== undefined && expiryDateVar !== null) {
formatRegionDate(expiryDateVar);
}
}

function formatRegionDate(expiryDateVar) {
    var country_date_format = window.country;
    var europDateArr = ["fr","no","pl","tr"];
    var withDotArr =  ["de","at","ch","cz","fi"];
    var dateSegments;
if(expiryDateVar.length > 0 && expiryDateVar[0]) {
if($.inArray(country_date_format, europDateArr) != -1) {
    dateSegments = expiryDateVar.html().replace(',','').split(' ');
    expiryDateVar.text(dateSegments[1] + ' ' + dateSegments[0] + ' ' + dateSegments[2]);
} else if ($.inArray(country_date_format, withDotArr) != -1) {
    dateSegments = expiryDateVar.html().replace(',','').split(' ');
    expiryDateVar.text(dateSegments[1] + '. ' + dateSegments[0] + ' ' + dateSegments[2]);
}
}
}
