/*! jQuery v1.7.1 jquery.com | jquery.org/license */
(function(a,b){function cy(a){return f.isWindow(a)?a:a.nodeType===9?a.defaultView||a.parentWindow:!1}function cv(a){if(!ck[a]){var b=c.body,d=f("<"+a+">").appendTo(b),e=d.css("display");d.remove();if(e==="none"||e===""){cl||(cl=c.createElement("iframe"),cl.frameBorder=cl.width=cl.height=0),b.appendChild(cl);if(!cm||!cl.createElement)cm=(cl.contentWindow||cl.contentDocument).document,cm.write((c.compatMode==="CSS1Compat"?"<!doctype html>":"")+"<html><body>"),cm.close();d=cm.createElement(a),cm.body.appendChild(d),e=f.css(d,"display"),b.removeChild(cl)}ck[a]=e}return ck[a]}function cu(a,b){var c={};f.each(cq.concat.apply([],cq.slice(0,b)),function(){c[this]=a});return c}function ct(){cr=b}function cs(){setTimeout(ct,0);return cr=f.now()}function cj(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}function ci(){try{return new a.XMLHttpRequest}catch(b){}}function cc(a,c){a.dataFilter&&(c=a.dataFilter(c,a.dataType));var d=a.dataTypes,e={},g,h,i=d.length,j,k=d[0],l,m,n,o,p;for(g=1;g<i;g++){if(g===1)for(h in a.converters)typeof h=="string"&&(e[h.toLowerCase()]=a.converters[h]);l=k,k=d[g];if(k==="*")k=l;else if(l!=="*"&&l!==k){m=l+" "+k,n=e[m]||e["* "+k];if(!n){p=b;for(o in e){j=o.split(" ");if(j[0]===l||j[0]==="*"){p=e[j[1]+" "+k];if(p){o=e[o],o===!0?n=p:p===!0&&(n=o);break}}}}!n&&!p&&f.error("No conversion from "+m.replace(" "," to ")),n!==!0&&(c=n?n(c):p(o(c)))}}return c}function cb(a,c,d){var e=a.contents,f=a.dataTypes,g=a.responseFields,h,i,j,k;for(i in g)i in d&&(c[g[i]]=d[i]);while(f[0]==="*")f.shift(),h===b&&(h=a.mimeType||c.getResponseHeader("content-type"));if(h)for(i in e)if(e[i]&&e[i].test(h)){f.unshift(i);break}if(f[0]in d)j=f[0];else{for(i in d){if(!f[0]||a.converters[i+" "+f[0]]){j=i;break}k||(k=i)}j=j||k}if(j){j!==f[0]&&f.unshift(j);return d[j]}}function ca(a,b,c,d){if(f.isArray(b))f.each(b,function(b,e){c||bE.test(a)?d(a,e):ca(a+"["+(typeof e=="object"||f.isArray(e)?b:"")+"]",e,c,d)});else if(!c&&b!=null&&typeof b=="object")for(var e in b)ca(a+"["+e+"]",b[e],c,d);else d(a,b)}function b_(a,c){var d,e,g=f.ajaxSettings.flatOptions||{};for(d in c)c[d]!==b&&((g[d]?a:e||(e={}))[d]=c[d]);e&&f.extend(!0,a,e)}function b$(a,c,d,e,f,g){f=f||c.dataTypes[0],g=g||{},g[f]=!0;var h=a[f],i=0,j=h?h.length:0,k=a===bT,l;for(;i<j&&(k||!l);i++)l=h[i](c,d,e),typeof l=="string"&&(!k||g[l]?l=b:(c.dataTypes.unshift(l),l=b$(a,c,d,e,l,g)));(k||!l)&&!g["*"]&&(l=b$(a,c,d,e,"*",g));return l}function bZ(a){return function(b,c){typeof b!="string"&&(c=b,b="*");if(f.isFunction(c)){var d=b.toLowerCase().split(bP),e=0,g=d.length,h,i,j;for(;e<g;e++)h=d[e],j=/^\+/.test(h),j&&(h=h.substr(1)||"*"),i=a[h]=a[h]||[],i[j?"unshift":"push"](c)}}}function bC(a,b,c){var d=b==="width"?a.offsetWidth:a.offsetHeight,e=b==="width"?bx:by,g=0,h=e.length;if(d>0){if(c!=="border")for(;g<h;g++)c||(d-=parseFloat(f.css(a,"padding"+e[g]))||0),c==="margin"?d+=parseFloat(f.css(a,c+e[g]))||0:d-=parseFloat(f.css(a,"border"+e[g]+"Width"))||0;return d+"px"}d=bz(a,b,b);if(d<0||d==null)d=a.style[b]||0;d=parseFloat(d)||0;if(c)for(;g<h;g++)d+=parseFloat(f.css(a,"padding"+e[g]))||0,c!=="padding"&&(d+=parseFloat(f.css(a,"border"+e[g]+"Width"))||0),c==="margin"&&(d+=parseFloat(f.css(a,c+e[g]))||0);return d+"px"}function bp(a,b){b.src?f.ajax({url:b.src,async:!1,dataType:"script"}):f.globalEval((b.text||b.textContent||b.innerHTML||"").replace(bf,"/*$0*/")),b.parentNode&&b.parentNode.removeChild(b)}function bo(a){var b=c.createElement("div");bh.appendChild(b),b.innerHTML=a.outerHTML;return b.firstChild}function bn(a){var b=(a.nodeName||"").toLowerCase();b==="input"?bm(a):b!=="script"&&typeof a.getElementsByTagName!="undefined"&&f.grep(a.getElementsByTagName("input"),bm)}function bm(a){if(a.type==="checkbox"||a.type==="radio")a.defaultChecked=a.checked}function bl(a){return typeof a.getElementsByTagName!="undefined"?a.getElementsByTagName("*"):typeof a.querySelectorAll!="undefined"?a.querySelectorAll("*"):[]}function bk(a,b){var c;if(b.nodeType===1){b.clearAttributes&&b.clearAttributes(),b.mergeAttributes&&b.mergeAttributes(a),c=b.nodeName.toLowerCase();if(c==="object")b.outerHTML=a.outerHTML;else if(c!=="input"||a.type!=="checkbox"&&a.type!=="radio"){if(c==="option")b.selected=a.defaultSelected;else if(c==="input"||c==="textarea")b.defaultValue=a.defaultValue}else a.checked&&(b.defaultChecked=b.checked=a.checked),b.value!==a.value&&(b.value=a.value);b.removeAttribute(f.expando)}}function bj(a,b){if(b.nodeType===1&&!!f.hasData(a)){var c,d,e,g=f._data(a),h=f._data(b,g),i=g.events;if(i){delete h.handle,h.events={};for(c in i)for(d=0,e=i[c].length;d<e;d++)f.event.add(b,c+(i[c][d].namespace?".":"")+i[c][d].namespace,i[c][d],i[c][d].data)}h.data&&(h.data=f.extend({},h.data))}}function bi(a,b){return f.nodeName(a,"table")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function U(a){var b=V.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}function T(a,b,c){b=b||0;if(f.isFunction(b))return f.grep(a,function(a,d){var e=!!b.call(a,d,a);return e===c});if(b.nodeType)return f.grep(a,function(a,d){return a===b===c});if(typeof b=="string"){var d=f.grep(a,function(a){return a.nodeType===1});if(O.test(b))return f.filter(b,d,!c);b=f.filter(b,d)}return f.grep(a,function(a,d){return f.inArray(a,b)>=0===c})}function S(a){return!a||!a.parentNode||a.parentNode.nodeType===11}function K(){return!0}function J(){return!1}function n(a,b,c){var d=b+"defer",e=b+"queue",g=b+"mark",h=f._data(a,d);h&&(c==="queue"||!f._data(a,e))&&(c==="mark"||!f._data(a,g))&&setTimeout(function(){!f._data(a,e)&&!f._data(a,g)&&(f.removeData(a,d,!0),h.fire())},0)}function m(a){for(var b in a){if(b==="data"&&f.isEmptyObject(a[b]))continue;if(b!=="toJSON")return!1}return!0}function l(a,c,d){if(d===b&&a.nodeType===1){var e="data-"+c.replace(k,"-$1").toLowerCase();d=a.getAttribute(e);if(typeof d=="string"){try{d=d==="true"?!0:d==="false"?!1:d==="null"?null:f.isNumeric(d)?parseFloat(d):j.test(d)?f.parseJSON(d):d}catch(g){}f.data(a,c,d)}else d=b}return d}function h(a){var b=g[a]={},c,d;a=a.split(/\s+/);for(c=0,d=a.length;c<d;c++)b[a[c]]=!0;return b}var c=a.document,d=a.navigator,e=a.location,f=function(){function J(){if(!e.isReady){try{c.documentElement.doScroll("left")}catch(a){setTimeout(J,1);return}e.ready()}}var e=function(a,b){return new e.fn.init(a,b,h)},f=a.jQuery,g=a.$,h,i=/^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,j=/\S/,k=/^\s+/,l=/\s+$/,m=/^<(\w+)\s*\/?>(?:<\/\1>)?$/,n=/^[\],:{}\s]*$/,o=/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,p=/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,q=/(?:^|:|,)(?:\s*\[)+/g,r=/(webkit)[ \/]([\w.]+)/,s=/(opera)(?:.*version)?[ \/]([\w.]+)/,t=/(msie) ([\w.]+)/,u=/(mozilla)(?:.*? rv:([\w.]+))?/,v=/-([a-z]|[0-9])/ig,w=/^-ms-/,x=function(a,b){return(b+"").toUpperCase()},y=d.userAgent,z,A,B,C=Object.prototype.toString,D=Object.prototype.hasOwnProperty,E=Array.prototype.push,F=Array.prototype.slice,G=String.prototype.trim,H=Array.prototype.indexOf,I={};e.fn=e.prototype={constructor:e,init:function(a,d,f){var g,h,j,k;if(!a)return this;if(a.nodeType){this.context=this[0]=a,this.length=1;return this}if(a==="body"&&!d&&c.body){this.context=c,this[0]=c.body,this.selector=a,this.length=1;return this}if(typeof a=="string"){a.charAt(0)!=="<"||a.charAt(a.length-1)!==">"||a.length<3?g=i.exec(a):g=[null,a,null];if(g&&(g[1]||!d)){if(g[1]){d=d instanceof e?d[0]:d,k=d?d.ownerDocument||d:c,j=m.exec(a),j?e.isPlainObject(d)?(a=[c.createElement(j[1])],e.fn.attr.call(a,d,!0)):a=[k.createElement(j[1])]:(j=e.buildFragment([g[1]],[k]),a=(j.cacheable?e.clone(j.fragment):j.fragment).childNodes);return e.merge(this,a)}h=c.getElementById(g[2]);if(h&&h.parentNode){if(h.id!==g[2])return f.find(a);this.length=1,this[0]=h}this.context=c,this.selector=a;return this}return!d||d.jquery?(d||f).find(a):this.constructor(d).find(a)}if(e.isFunction(a))return f.ready(a);a.selector!==b&&(this.selector=a.selector,this.context=a.context);return e.makeArray(a,this)},selector:"",jquery:"1.7.1",length:0,size:function(){return this.length},toArray:function(){return F.call(this,0)},get:function(a){return a==null?this.toArray():a<0?this[this.length+a]:this[a]},pushStack:function(a,b,c){var d=this.constructor();e.isArray(a)?E.apply(d,a):e.merge(d,a),d.prevObject=this,d.context=this.context,b==="find"?d.selector=this.selector+(this.selector?" ":"")+c:b&&(d.selector=this.selector+"."+b+"("+c+")");return d},each:function(a,b){return e.each(this,a,b)},ready:function(a){e.bindReady(),A.add(a);return this},eq:function(a){a=+a;return a===-1?this.slice(a):this.slice(a,a+1)},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},slice:function(){return this.pushStack(F.apply(this,arguments),"slice",F.call(arguments).join(","))},map:function(a){return this.pushStack(e.map(this,function(b,c){return a.call(b,c,b)}))},end:function(){return this.prevObject||this.constructor(null)},push:E,sort:[].sort,splice:[].splice},e.fn.init.prototype=e.fn,e.extend=e.fn.extend=function(){var a,c,d,f,g,h,i=arguments[0]||{},j=1,k=arguments.length,l=!1;typeof i=="boolean"&&(l=i,i=arguments[1]||{},j=2),typeof i!="object"&&!e.isFunction(i)&&(i={}),k===j&&(i=this,--j);for(;j<k;j++)if((a=arguments[j])!=null)for(c in a){d=i[c],f=a[c];if(i===f)continue;l&&f&&(e.isPlainObject(f)||(g=e.isArray(f)))?(g?(g=!1,h=d&&e.isArray(d)?d:[]):h=d&&e.isPlainObject(d)?d:{},i[c]=e.extend(l,h,f)):f!==b&&(i[c]=f)}return i},e.extend({noConflict:function(b){a.$===e&&(a.$=g),b&&a.jQuery===e&&(a.jQuery=f);return e},isReady:!1,readyWait:1,holdReady:function(a){a?e.readyWait++:e.ready(!0)},ready:function(a){if(a===!0&&!--e.readyWait||a!==!0&&!e.isReady){if(!c.body)return setTimeout(e.ready,1);e.isReady=!0;if(a!==!0&&--e.readyWait>0)return;A.fireWith(c,[e]),e.fn.trigger&&e(c).trigger("ready").off("ready")}},bindReady:function(){if(!A){A=e.Callbacks("once memory");if(c.readyState==="complete")return setTimeout(e.ready,1);if(c.addEventListener)c.addEventListener("DOMContentLoaded",B,!1),a.addEventListener("load",e.ready,!1);else if(c.attachEvent){c.attachEvent("onreadystatechange",B),a.attachEvent("onload",e.ready);var b=!1;try{b=a.frameElement==null}catch(d){}c.documentElement.doScroll&&b&&J()}}},isFunction:function(a){return e.type(a)==="function"},isArray:Array.isArray||function(a){return e.type(a)==="array"},isWindow:function(a){return a&&typeof a=="object"&&"setInterval"in a},isNumeric:function(a){return!isNaN(parseFloat(a))&&isFinite(a)},type:function(a){return a==null?String(a):I[C.call(a)]||"object"},isPlainObject:function(a){if(!a||e.type(a)!=="object"||a.nodeType||e.isWindow(a))return!1;try{if(a.constructor&&!D.call(a,"constructor")&&!D.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}var d;for(d in a);return d===b||D.call(a,d)},isEmptyObject:function(a){for(var b in a)return!1;return!0},error:function(a){throw new Error(a)},parseJSON:function(b){if(typeof b!="string"||!b)return null;b=e.trim(b);if(a.JSON&&a.JSON.parse)return a.JSON.parse(b);if(n.test(b.replace(o,"@").replace(p,"]").replace(q,"")))return(new Function("return "+b))();e.error("Invalid JSON: "+b)},parseXML:function(c){var d,f;try{a.DOMParser?(f=new DOMParser,d=f.parseFromString(c,"text/xml")):(d=new ActiveXObject("Microsoft.XMLDOM"),d.async="false",d.loadXML(c))}catch(g){d=b}(!d||!d.documentElement||d.getElementsByTagName("parsererror").length)&&e.error("Invalid XML: "+c);return d},noop:function(){},globalEval:function(b){b&&j.test(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(w,"ms-").replace(v,x)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toUpperCase()===b.toUpperCase()},each:function(a,c,d){var f,g=0,h=a.length,i=h===b||e.isFunction(a);if(d){if(i){for(f in a)if(c.apply(a[f],d)===!1)break}else for(;g<h;)if(c.apply(a[g++],d)===!1)break}else if(i){for(f in a)if(c.call(a[f],f,a[f])===!1)break}else for(;g<h;)if(c.call(a[g],g,a[g++])===!1)break;return a},trim:G?function(a){return a==null?"":G.call(a)}:function(a){return a==null?"":(a+"").replace(k,"").replace(l,"")},makeArray:function(a,b){var c=b||[];if(a!=null){var d=e.type(a);a.length==null||d==="string"||d==="function"||d==="regexp"||e.isWindow(a)?E.call(c,a):e.merge(c,a)}return c},inArray:function(a,b,c){var d;if(b){if(H)return H.call(b,a,c);d=b.length,c=c?c<0?Math.max(0,d+c):c:0;for(;c<d;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,c){var d=a.length,e=0;if(typeof c.length=="number")for(var f=c.length;e<f;e++)a[d++]=c[e];else while(c[e]!==b)a[d++]=c[e++];a.length=d;return a},grep:function(a,b,c){var d=[],e;c=!!c;for(var f=0,g=a.length;f<g;f++)e=!!b(a[f],f),c!==e&&d.push(a[f]);return d},map:function(a,c,d){var f,g,h=[],i=0,j=a.length,k=a instanceof e||j!==b&&typeof j=="number"&&(j>0&&a[0]&&a[j-1]||j===0||e.isArray(a));if(k)for(;i<j;i++)f=c(a[i],i,d),f!=null&&(h[h.length]=f);else for(g in a)f=c(a[g],g,d),f!=null&&(h[h.length]=f);return h.concat.apply([],h)},guid:1,proxy:function(a,c){if(typeof c=="string"){var d=a[c];c=a,a=d}if(!e.isFunction(a))return b;var f=F.call(arguments,2),g=function(){return a.apply(c,f.concat(F.call(arguments)))};g.guid=a.guid=a.guid||g.guid||e.guid++;return g},access:function(a,c,d,f,g,h){var i=a.length;if(typeof c=="object"){for(var j in c)e.access(a,j,c[j],f,g,d);return a}if(d!==b){f=!h&&f&&e.isFunction(d);for(var k=0;k<i;k++)g(a[k],c,f?d.call(a[k],k,g(a[k],c)):d,h);return a}return i?g(a[0],c):b},now:function(){return(new Date).getTime()},uaMatch:function(a){a=a.toLowerCase();var b=r.exec(a)||s.exec(a)||t.exec(a)||a.indexOf("compatible")<0&&u.exec(a)||[];return{browser:b[1]||"",version:b[2]||"0"}},sub:function(){function a(b,c){return new a.fn.init(b,c)}e.extend(!0,a,this),a.superclass=this,a.fn=a.prototype=this(),a.fn.constructor=a,a.sub=this.sub,a.fn.init=function(d,f){f&&f instanceof e&&!(f instanceof a)&&(f=a(f));return e.fn.init.call(this,d,f,b)},a.fn.init.prototype=a.fn;var b=a(c);return a},browser:{}}),e.each("Boolean Number String Function Array Date RegExp Object".split(" "),function(a,b){I["[object "+b+"]"]=b.toLowerCase()}),z=e.uaMatch(y),z.browser&&(e.browser[z.browser]=!0,e.browser.version=z.version),e.browser.webkit&&(e.browser.safari=!0),j.test(" ")&&(k=/^[\s\xA0]+/,l=/[\s\xA0]+$/),h=e(c),c.addEventListener?B=function(){c.removeEventListener("DOMContentLoaded",B,!1),e.ready()}:c.attachEvent&&(B=function(){c.readyState==="complete"&&(c.detachEvent("onreadystatechange",B),e.ready())});return e}(),g={};f.Callbacks=function(a){a=a?g[a]||h(a):{};var c=[],d=[],e,i,j,k,l,m=function(b){var d,e,g,h,i;for(d=0,e=b.length;d<e;d++)g=b[d],h=f.type(g),h==="array"?m(g):h==="function"&&(!a.unique||!o.has(g))&&c.push(g)},n=function(b,f){f=f||[],e=!a.memory||[b,f],i=!0,l=j||0,j=0,k=c.length;for(;c&&l<k;l++)if(c[l].apply(b,f)===!1&&a.stopOnFalse){e=!0;break}i=!1,c&&(a.once?e===!0?o.disable():c=[]:d&&d.length&&(e=d.shift(),o.fireWith(e[0],e[1])))},o={add:function(){if(c){var a=c.length;m(arguments),i?k=c.length:e&&e!==!0&&(j=a,n(e[0],e[1]))}return this},remove:function(){if(c){var b=arguments,d=0,e=b.length;for(;d<e;d++)for(var f=0;f<c.length;f++)if(b[d]===c[f]){i&&f<=k&&(k--,f<=l&&l--),c.splice(f--,1);if(a.unique)break}}return this},has:function(a){if(c){var b=0,d=c.length;for(;b<d;b++)if(a===c[b])return!0}return!1},empty:function(){c=[];return this},disable:function(){c=d=e=b;return this},disabled:function(){return!c},lock:function(){d=b,(!e||e===!0)&&o.disable();return this},locked:function(){return!d},fireWith:function(b,c){d&&(i?a.once||d.push([b,c]):(!a.once||!e)&&n(b,c));return this},fire:function(){o.fireWith(this,arguments);return this},fired:function(){return!!e}};return o};var i=[].slice;f.extend({Deferred:function(a){var b=f.Callbacks("once memory"),c=f.Callbacks("once memory"),d=f.Callbacks("memory"),e="pending",g={resolve:b,reject:c,notify:d},h={done:b.add,fail:c.add,progress:d.add,state:function(){return e},isResolved:b.fired,isRejected:c.fired,then:function(a,b,c){i.done(a).fail(b).progress(c);return this},always:function(){i.done.apply(i,arguments).fail.apply(i,arguments);return this},pipe:function(a,b,c){return f.Deferred(function(d){f.each({done:[a,"resolve"],fail:[b,"reject"],progress:[c,"notify"]},function(a,b){var c=b[0],e=b[1],g;f.isFunction(c)?i[a](function(){g=c.apply(this,arguments),g&&f.isFunction(g.promise)?g.promise().then(d.resolve,d.reject,d.notify):d[e+"With"](this===i?d:this,[g])}):i[a](d[e])})}).promise()},promise:function(a){if(a==null)a=h;else for(var b in h)a[b]=h[b];return a}},i=h.promise({}),j;for(j in g)i[j]=g[j].fire,i[j+"With"]=g[j].fireWith;i.done(function(){e="resolved"},c.disable,d.lock).fail(function(){e="rejected"},b.disable,d.lock),a&&a.call(i,i);return i},when:function(a){function m(a){return function(b){e[a]=arguments.length>1?i.call(arguments,0):b,j.notifyWith(k,e)}}function l(a){return function(c){b[a]=arguments.length>1?i.call(arguments,0):c,--g||j.resolveWith(j,b)}}var b=i.call(arguments,0),c=0,d=b.length,e=Array(d),g=d,h=d,j=d<=1&&a&&f.isFunction(a.promise)?a:f.Deferred(),k=j.promise();if(d>1){for(;c<d;c++)b[c]&&b[c].promise&&f.isFunction(b[c].promise)?b[c].promise().then(l(c),j.reject,m(c)):--g;g||j.resolveWith(j,b)}else j!==a&&j.resolveWith(j,d?[a]:[]);return k}}),f.support=function(){var b,d,e,g,h,i,j,k,l,m,n,o,p,q=c.createElement("div"),r=c.documentElement;q.setAttribute("className","t"),q.innerHTML="   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>",d=q.getElementsByTagName("*"),e=q.getElementsByTagName("a")[0];if(!d||!d.length||!e)return{};g=c.createElement("select"),h=g.appendChild(c.createElement("option")),i=q.getElementsByTagName("input")[0],b={leadingWhitespace:q.firstChild.nodeType===3,tbody:!q.getElementsByTagName("tbody").length,htmlSerialize:!!q.getElementsByTagName("link").length,style:/top/.test(e.getAttribute("style")),hrefNormalized:e.getAttribute("href")==="/a",opacity:/^0.55/.test(e.style.opacity),cssFloat:!!e.style.cssFloat,checkOn:i.value==="on",optSelected:h.selected,getSetAttribute:q.className!=="t",enctype:!!c.createElement("form").enctype,html5Clone:c.createElement("nav").cloneNode(!0).outerHTML!=="<:nav></:nav>",submitBubbles:!0,changeBubbles:!0,focusinBubbles:!1,deleteExpando:!0,noCloneEvent:!0,inlineBlockNeedsLayout:!1,shrinkWrapBlocks:!1,reliableMarginRight:!0},i.checked=!0,b.noCloneChecked=i.cloneNode(!0).checked,g.disabled=!0,b.optDisabled=!h.disabled;try{delete q.test}catch(s){b.deleteExpando=!1}!q.addEventListener&&q.attachEvent&&q.fireEvent&&(q.attachEvent("onclick",function(){b.noCloneEvent=!1}),q.cloneNode(!0).fireEvent("onclick")),i=c.createElement("input"),i.value="t",i.setAttribute("type","radio"),b.radioValue=i.value==="t",i.setAttribute("checked","checked"),q.appendChild(i),k=c.createDocumentFragment(),k.appendChild(q.lastChild),b.checkClone=k.cloneNode(!0).cloneNode(!0).lastChild.checked,b.appendChecked=i.checked,k.removeChild(i),k.appendChild(q),q.innerHTML="",a.getComputedStyle&&(j=c.createElement("div"),j.style.width="0",j.style.marginRight="0",q.style.width="2px",q.appendChild(j),b.reliableMarginRight=(parseInt((a.getComputedStyle(j,null)||{marginRight:0}).marginRight,10)||0)===0);if(q.attachEvent)for(o in{submit:1,change:1,focusin:1})n="on"+o,p=n in q,p||(q.setAttribute(n,"return;"),p=typeof q[n]=="function"),b[o+"Bubbles"]=p;k.removeChild(q),k=g=h=j=q=i=null,f(function(){var a,d,e,g,h,i,j,k,m,n,o,r=c.getElementsByTagName("body")[0];!r||(j=1,k="position:absolute;top:0;left:0;width:1px;height:1px;margin:0;",m="visibility:hidden;border:0;",n="style='"+k+"border:5px solid #000;padding:0;'",o="<div "+n+"><div></div></div>"+"<table "+n+" cellpadding='0' cellspacing='0'>"+"<tr><td></td></tr></table>",a=c.createElement("div"),a.style.cssText=m+"width:0;height:0;position:static;top:0;margin-top:"+j+"px",r.insertBefore(a,r.firstChild),q=c.createElement("div"),a.appendChild(q),q.innerHTML="<table><tr><td style='padding:0;border:0;display:none'></td><td>t</td></tr></table>",l=q.getElementsByTagName("td"),p=l[0].offsetHeight===0,l[0].style.display="",l[1].style.display="none",b.reliableHiddenOffsets=p&&l[0].offsetHeight===0,q.innerHTML="",q.style.width=q.style.paddingLeft="1px",f.boxModel=b.boxModel=q.offsetWidth===2,typeof q.style.zoom!="undefined"&&(q.style.display="inline",q.style.zoom=1,b.inlineBlockNeedsLayout=q.offsetWidth===2,q.style.display="",q.innerHTML="<div style='width:4px;'></div>",b.shrinkWrapBlocks=q.offsetWidth!==2),q.style.cssText=k+m,q.innerHTML=o,d=q.firstChild,e=d.firstChild,h=d.nextSibling.firstChild.firstChild,i={doesNotAddBorder:e.offsetTop!==5,doesAddBorderForTableAndCells:h.offsetTop===5},e.style.position="fixed",e.style.top="20px",i.fixedPosition=e.offsetTop===20||e.offsetTop===15,e.style.position=e.style.top="",d.style.overflow="hidden",d.style.position="relative",i.subtractsBorderForOverflowNotVisible=e.offsetTop===-5,i.doesNotIncludeMarginInBodyOffset=r.offsetTop!==j,r.removeChild(a),q=a=null,f.extend(b,i))});return b}();var j=/^(?:\{.*\}|\[.*\])$/,k=/([A-Z])/g;f.extend({cache:{},uuid:0,expando:"jQuery"+(f.fn.jquery+Math.random()).replace(/\D/g,""),noData:{embed:!0,object:"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",applet:!0},hasData:function(a){a=a.nodeType?f.cache[a[f.expando]]:a[f.expando];return!!a&&!m(a)},data:function(a,c,d,e){if(!!f.acceptData(a)){var g,h,i,j=f.expando,k=typeof c=="string",l=a.nodeType,m=l?f.cache:a,n=l?a[j]:a[j]&&j,o=c==="events";if((!n||!m[n]||!o&&!e&&!m[n].data)&&k&&d===b)return;n||(l?a[j]=n=++f.uuid:n=j),m[n]||(m[n]={},l||(m[n].toJSON=f.noop));if(typeof c=="object"||typeof c=="function")e?m[n]=f.extend(m[n],c):m[n].data=f.extend(m[n].data,c);g=h=m[n],e||(h.data||(h.data={}),h=h.data),d!==b&&(h[f.camelCase(c)]=d);if(o&&!h[c])return g.events;k?(i=h[c],i==null&&(i=h[f.camelCase(c)])):i=h;return i}},removeData:function(a,b,c){if(!!f.acceptData(a)){var d,e,g,h=f.expando,i=a.nodeType,j=i?f.cache:a,k=i?a[h]:h;if(!j[k])return;if(b){d=c?j[k]:j[k].data;if(d){f.isArray(b)||(b in d?b=[b]:(b=f.camelCase(b),b in d?b=[b]:b=b.split(" ")));for(e=0,g=b.length;e<g;e++)delete d[b[e]];if(!(c?m:f.isEmptyObject)(d))return}}if(!c){delete j[k].data;if(!m(j[k]))return}f.support.deleteExpando||!j.setInterval?delete j[k]:j[k]=null,i&&(f.support.deleteExpando?delete a[h]:a.removeAttribute?a.removeAttribute(h):a[h]=null)}},_data:function(a,b,c){return f.data(a,b,c,!0)},acceptData:function(a){if(a.nodeName){var b=f.noData[a.nodeName.toLowerCase()];if(b)return b!==!0&&a.getAttribute("classid")===b}return!0}}),f.fn.extend({data:function(a,c){var d,e,g,h=null;if(typeof a=="undefined"){if(this.length){h=f.data(this[0]);if(this[0].nodeType===1&&!f._data(this[0],"parsedAttrs")){e=this[0].attributes;for(var i=0,j=e.length;i<j;i++)g=e[i].name,g.indexOf("data-")===0&&(g=f.camelCase(g.substring(5)),l(this[0],g,h[g]));f._data(this[0],"parsedAttrs",!0)}}return h}if(typeof a=="object")return this.each(function(){f.data(this,a)});d=a.split("."),d[1]=d[1]?"."+d[1]:"";if(c===b){h=this.triggerHandler("getData"+d[1]+"!",[d[0]]),h===b&&this.length&&(h=f.data(this[0],a),h=l(this[0],a,h));return h===b&&d[1]?this.data(d[0]):h}return this.each(function(){var b=f(this),e=[d[0],c];b.triggerHandler("setData"+d[1]+"!",e),f.data(this,a,c),b.triggerHandler("changeData"+d[1]+"!",e)})},removeData:function(a){return this.each(function(){f.removeData(this,a)})}}),f.extend({_mark:function(a,b){a&&(b=(b||"fx")+"mark",f._data(a,b,(f._data(a,b)||0)+1))},_unmark:function(a,b,c){a!==!0&&(c=b,b=a,a=!1);if(b){c=c||"fx";var d=c+"mark",e=a?0:(f._data(b,d)||1)-1;e?f._data(b,d,e):(f.removeData(b,d,!0),n(b,c,"mark"))}},queue:function(a,b,c){var d;if(a){b=(b||"fx")+"queue",d=f._data(a,b),c&&(!d||f.isArray(c)?d=f._data(a,b,f.makeArray(c)):d.push(c));return d||[]}},dequeue:function(a,b){b=b||"fx";var c=f.queue(a,b),d=c.shift(),e={};d==="inprogress"&&(d=c.shift()),d&&(b==="fx"&&c.unshift("inprogress"),f._data(a,b+".run",e),d.call(a,function(){f.dequeue(a,b)},e)),c.length||(f.removeData(a,b+"queue "+b+".run",!0),n(a,b,"queue"))}}),f.fn.extend({queue:function(a,c){typeof a!="string"&&(c=a,a="fx");if(c===b)return f.queue(this[0],a);return this.each(function(){var b=f.queue(this,a,c);a==="fx"&&b[0]!=="inprogress"&&f.dequeue(this,a)})},dequeue:function(a){return this.each(function(){f.dequeue(this,a)})},delay:function(a,b){a=f.fx?f.fx.speeds[a]||a:a,b=b||"fx";return this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,c){function m(){--h||d.resolveWith(e,[e])}typeof a!="string"&&(c=a,a=b),a=a||"fx";var d=f.Deferred(),e=this,g=e.length,h=1,i=a+"defer",j=a+"queue",k=a+"mark",l;while(g--)if(l=f.data(e[g],i,b,!0)||(f.data(e[g],j,b,!0)||f.data(e[g],k,b,!0))&&f.data(e[g],i,f.Callbacks("once memory"),!0))h++,l.add(m);m();return d.promise()}});var o=/[\n\t\r]/g,p=/\s+/,q=/\r/g,r=/^(?:button|input)$/i,s=/^(?:button|input|object|select|textarea)$/i,t=/^a(?:rea)?$/i,u=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,v=f.support.getSetAttribute,w,x,y;f.fn.extend({attr:function(a,b){return f.access(this,a,b,!0,f.attr)},removeAttr:function(a){return this.each(function(){f.removeAttr(this,a)})},prop:function(a,b){return f.access(this,a,b,!0,f.prop)},removeProp:function(a){a=f.propFix[a]||a;return this.each(function(){try{this[a]=b,delete this[a]}catch(c){}})},addClass:function(a){var b,c,d,e,g,h,i;if(f.isFunction(a))return this.each(function(b){f(this).addClass(a.call(this,b,this.className))});if(a&&typeof a=="string"){b=a.split(p);for(c=0,d=this.length;c<d;c++){e=this[c];if(e.nodeType===1)if(!e.className&&b.length===1)e.className=a;else{g=" "+e.className+" ";for(h=0,i=b.length;h<i;h++)~g.indexOf(" "+b[h]+" ")||(g+=b[h]+" ");e.className=f.trim(g)}}}return this},removeClass:function(a){var c,d,e,g,h,i,j;if(f.isFunction(a))return this.each(function(b){f(this).removeClass(a.call(this,b,this.className))});if(a&&typeof a=="string"||a===b){c=(a||"").split(p);for(d=0,e=this.length;d<e;d++){g=this[d];if(g.nodeType===1&&g.className)if(a){h=(" "+g.className+" ").replace(o," ");for(i=0,j=c.length;i<j;i++)h=h.replace(" "+c[i]+" "," ");g.className=f.trim(h)}else g.className=""}}return this},toggleClass:function(a,b){var c=typeof a,d=typeof b=="boolean";if(f.isFunction(a))return this.each(function(c){f(this).toggleClass(a.call(this,c,this.className,b),b)});return this.each(function(){if(c==="string"){var e,g=0,h=f(this),i=b,j=a.split(p);while(e=j[g++])i=d?i:!h.hasClass(e),h[i?"addClass":"removeClass"](e)}else if(c==="undefined"||c==="boolean")this.className&&f._data(this,"__className__",this.className),this.className=this.className||a===!1?"":f._data(this,"__className__")||""})},hasClass:function(a){var b=" "+a+" ",c=0,d=this.length;for(;c<d;c++)if(this[c].nodeType===1&&(" "+this[c].className+" ").replace(o," ").indexOf(b)>-1)return!0;return!1},val:function(a){var c,d,e,g=this[0];{if(!!arguments.length){e=f.isFunction(a);return this.each(function(d){var g=f(this),h;if(this.nodeType===1){e?h=a.call(this,d,g.val()):h=a,h==null?h="":typeof h=="number"?h+="":f.isArray(h)&&(h=f.map(h,function(a){return a==null?"":a+""})),c=f.valHooks[this.nodeName.toLowerCase()]||f.valHooks[this.type];if(!c||!("set"in c)||c.set(this,h,"value")===b)this.value=h}})}if(g){c=f.valHooks[g.nodeName.toLowerCase()]||f.valHooks[g.type];if(c&&"get"in c&&(d=c.get(g,"value"))!==b)return d;d=g.value;return typeof d=="string"?d.replace(q,""):d==null?"":d}}}}),f.extend({valHooks:{option:{get:function(a){var b=a.attributes.value;return!b||b.specified?a.value:a.text}},select:{get:function(a){var b,c,d,e,g=a.selectedIndex,h=[],i=a.options,j=a.type==="select-one";if(g<0)return null;c=j?g:0,d=j?g+1:i.length;for(;c<d;c++){e=i[c];if(e.selected&&(f.support.optDisabled?!e.disabled:e.getAttribute("disabled")===null)&&(!e.parentNode.disabled||!f.nodeName(e.parentNode,"optgroup"))){b=f(e).val();if(j)return b;h.push(b)}}if(j&&!h.length&&i.length)return f(i[g]).val();return h},set:function(a,b){var c=f.makeArray(b);f(a).find("option").each(function(){this.selected=f.inArray(f(this).val(),c)>=0}),c.length||(a.selectedIndex=-1);return c}}},attrFn:{val:!0,css:!0,html:!0,text:!0,data:!0,width:!0,height:!0,offset:!0},attr:function(a,c,d,e){var g,h,i,j=a.nodeType;if(!!a&&j!==3&&j!==8&&j!==2){if(e&&c in f.attrFn)return f(a)[c](d);if(typeof a.getAttribute=="undefined")return f.prop(a,c,d);i=j!==1||!f.isXMLDoc(a),i&&(c=c.toLowerCase(),h=f.attrHooks[c]||(u.test(c)?x:w));if(d!==b){if(d===null){f.removeAttr(a,c);return}if(h&&"set"in h&&i&&(g=h.set(a,d,c))!==b)return g;a.setAttribute(c,""+d);return d}if(h&&"get"in h&&i&&(g=h.get(a,c))!==null)return g;g=a.getAttribute(c);return g===null?b:g}},removeAttr:function(a,b){var c,d,e,g,h=0;if(b&&a.nodeType===1){d=b.toLowerCase().split(p),g=d.length;for(;h<g;h++)e=d[h],e&&(c=f.propFix[e]||e,f.attr(a,e,""),a.removeAttribute(v?e:c),u.test(e)&&c in a&&(a[c]=!1))}},attrHooks:{type:{set:function(a,b){if(r.test(a.nodeName)&&a.parentNode)f.error("type property can't be changed");else if(!f.support.radioValue&&b==="radio"&&f.nodeName(a,"input")){var c=a.value;a.setAttribute("type",b),c&&(a.value=c);return b}}},value:{get:function(a,b){if(w&&f.nodeName(a,"button"))return w.get(a,b);return b in a?a.value:null},set:function(a,b,c){if(w&&f.nodeName(a,"button"))return w.set(a,b,c);a.value=b}}},propFix:{tabindex:"tabIndex",readonly:"readOnly","for":"htmlFor","class":"className",maxlength:"maxLength",cellspacing:"cellSpacing",cellpadding:"cellPadding",rowspan:"rowSpan",colspan:"colSpan",usemap:"useMap",frameborder:"frameBorder",contenteditable:"contentEditable"},prop:function(a,c,d){var e,g,h,i=a.nodeType;if(!!a&&i!==3&&i!==8&&i!==2){h=i!==1||!f.isXMLDoc(a),h&&(c=f.propFix[c]||c,g=f.propHooks[c]);return d!==b?g&&"set"in g&&(e=g.set(a,d,c))!==b?e:a[c]=d:g&&"get"in g&&(e=g.get(a,c))!==null?e:a[c]}},propHooks:{tabIndex:{get:function(a){var c=a.getAttributeNode("tabindex");return c&&c.specified?parseInt(c.value,10):s.test(a.nodeName)||t.test(a.nodeName)&&a.href?0:b}}}}),f.attrHooks.tabindex=f.propHooks.tabIndex,x={get:function(a,c){var d,e=f.prop(a,c);return e===!0||typeof e!="boolean"&&(d=a.getAttributeNode(c))&&d.nodeValue!==!1?c.toLowerCase():b},set:function(a,b,c){var d;b===!1?f.removeAttr(a,c):(d=f.propFix[c]||c,d in a&&(a[d]=!0),a.setAttribute(c,c.toLowerCase()));return c}},v||(y={name:!0,id:!0},w=f.valHooks.button={get:function(a,c){var d;d=a.getAttributeNode(c);return d&&(y[c]?d.nodeValue!=="":d.specified)?d.nodeValue:b},set:function(a,b,d){var e=a.getAttributeNode(d);e||(e=c.createAttribute(d),a.setAttributeNode(e));return e.nodeValue=b+""}},f.attrHooks.tabindex.set=w.set,f.each(["width","height"],function(a,b){f.attrHooks[b]=f.extend(f.attrHooks[b],{set:function(a,c){if(c===""){a.setAttribute(b,"auto");return c}}})}),f.attrHooks.contenteditable={get:w.get,set:function(a,b,c){b===""&&(b="false"),w.set(a,b,c)}}),f.support.hrefNormalized||f.each(["href","src","width","height"],function(a,c){f.attrHooks[c]=f.extend(f.attrHooks[c],{get:function(a){var d=a.getAttribute(c,2);return d===null?b:d}})}),f.support.style||(f.attrHooks.style={get:function(a){return a.style.cssText.toLowerCase()||b},set:function(a,b){return a.style.cssText=""+b}}),f.support.optSelected||(f.propHooks.selected=f.extend(f.propHooks.selected,{get:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex);return null}})),f.support.enctype||(f.propFix.enctype="encoding"),f.support.checkOn||f.each(["radio","checkbox"],function(){f.valHooks[this]={get:function(a){return a.getAttribute("value")===null?"on":a.value}}}),f.each(["radio","checkbox"],function(){f.valHooks[this]=f.extend(f.valHooks[this],{set:function(a,b){if(f.isArray(b))return a.checked=f.inArray(f(a).val(),b)>=0}})});var z=/^(?:textarea|input|select)$/i,A=/^([^\.]*)?(?:\.(.+))?$/,B=/\bhover(\.\S+)?\b/,C=/^key/,D=/^(?:mouse|contextmenu)|click/,E=/^(?:focusinfocus|focusoutblur)$/,F=/^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/,G=function(a){var b=F.exec(a);b&&(b[1]=(b[1]||"").toLowerCase(),b[3]=b[3]&&new RegExp("(?:^|\\s)"+b[3]+"(?:\\s|$)"));return b},H=function(a,b){var c=a.attributes||{};return(!b[1]||a.nodeName.toLowerCase()===b[1])&&(!b[2]||(c.id||{}).value===b[2])&&(!b[3]||b[3].test((c["class"]||{}).value))},I=function(a){return f.event.special.hover?a:a.replace(B,"mouseenter$1 mouseleave$1")};
	f.event={add:function(a,c,d,e,g){var h,i,j,k,l,m,n,o,p,q,r,s;if(!(a.nodeType===3||a.nodeType===8||!c||!d||!(h=f._data(a)))){d.handler&&(p=d,d=p.handler),d.guid||(d.guid=f.guid++),j=h.events,j||(h.events=j={}),i=h.handle,i||(h.handle=i=function(a){return typeof f!="undefined"&&(!a||f.event.triggered!==a.type)?f.event.dispatch.apply(i.elem,arguments):b},i.elem=a),c=f.trim(I(c)).split(" ");for(k=0;k<c.length;k++){l=A.exec(c[k])||[],m=l[1],n=(l[2]||"").split(".").sort(),s=f.event.special[m]||{},m=(g?s.delegateType:s.bindType)||m,s=f.event.special[m]||{},o=f.extend({type:m,origType:l[1],data:e,handler:d,guid:d.guid,selector:g,quick:G(g),namespace:n.join(".")},p),r=j[m];if(!r){r=j[m]=[],r.delegateCount=0;if(!s.setup||s.setup.call(a,e,n,i)===!1)a.addEventListener?a.addEventListener(m,i,!1):a.attachEvent&&a.attachEvent("on"+m,i)}s.add&&(s.add.call(a,o),o.handler.guid||(o.handler.guid=d.guid)),g?r.splice(r.delegateCount++,0,o):r.push(o),f.event.global[m]=!0}a=null}},global:{},remove:function(a,b,c,d,e){var g=f.hasData(a)&&f._data(a),h,i,j,k,l,m,n,o,p,q,r,s;if(!!g&&!!(o=g.events)){b=f.trim(I(b||"")).split(" ");for(h=0;h<b.length;h++){i=A.exec(b[h])||[],j=k=i[1],l=i[2];if(!j){for(j in o)f.event.remove(a,j+b[h],c,d,!0);continue}p=f.event.special[j]||{},j=(d?p.delegateType:p.bindType)||j,r=o[j]||[],m=r.length,l=l?new RegExp("(^|\\.)"+l.split(".").sort().join("\\.(?:.*\\.)?")+"(\\.|$)"):null;for(n=0;n<r.length;n++)s=r[n],(e||k===s.origType)&&(!c||c.guid===s.guid)&&(!l||l.test(s.namespace))&&(!d||d===s.selector||d==="**"&&s.selector)&&(r.splice(n--,1),s.selector&&r.delegateCount--,p.remove&&p.remove.call(a,s));r.length===0&&m!==r.length&&((!p.teardown||p.teardown.call(a,l)===!1)&&f.removeEvent(a,j,g.handle),delete o[j])}f.isEmptyObject(o)&&(q=g.handle,q&&(q.elem=null),f.removeData(a,["events","handle"],!0))}},customEvent:{getData:!0,setData:!0,changeData:!0},trigger:function(c,d,e,g){if(!e||e.nodeType!==3&&e.nodeType!==8){var h=c.type||c,i=[],j,k,l,m,n,o,p,q,r,s;if(E.test(h+f.event.triggered))return;h.indexOf("!")>=0&&(h=h.slice(0,-1),k=!0),h.indexOf(".")>=0&&(i=h.split("."),h=i.shift(),i.sort());if((!e||f.event.customEvent[h])&&!f.event.global[h])return;c=typeof c=="object"?c[f.expando]?c:new f.Event(h,c):new f.Event(h),c.type=h,c.isTrigger=!0,c.exclusive=k,c.namespace=i.join("."),c.namespace_re=c.namespace?new RegExp("(^|\\.)"+i.join("\\.(?:.*\\.)?")+"(\\.|$)"):null,o=h.indexOf(":")<0?"on"+h:"";if(!e){j=f.cache;for(l in j)j[l].events&&j[l].events[h]&&f.event.trigger(c,d,j[l].handle.elem,!0);return}c.result=b,c.target||(c.target=e),d=d!=null?f.makeArray(d):[],d.unshift(c),p=f.event.special[h]||{};if(p.trigger&&p.trigger.apply(e,d)===!1)return;r=[[e,p.bindType||h]];if(!g&&!p.noBubble&&!f.isWindow(e)){s=p.delegateType||h,m=E.test(s+h)?e:e.parentNode,n=null;for(;m;m=m.parentNode)r.push([m,s]),n=m;n&&n===e.ownerDocument&&r.push([n.defaultView||n.parentWindow||a,s])}for(l=0;l<r.length&&!c.isPropagationStopped();l++)m=r[l][0],c.type=r[l][1],q=(f._data(m,"events")||{})[c.type]&&f._data(m,"handle"),q&&q.apply(m,d),q=o&&m[o],q&&f.acceptData(m)&&q.apply(m,d)===!1&&c.preventDefault();c.type=h,!g&&!c.isDefaultPrevented()&&(!p._default||p._default.apply(e.ownerDocument,d)===!1)&&(h!=="click"||!f.nodeName(e,"a"))&&f.acceptData(e)&&o&&e[h]&&(h!=="focus"&&h!=="blur"||c.target.offsetWidth!==0)&&!f.isWindow(e)&&(n=e[o],n&&(e[o]=null),f.event.triggered=h,e[h](),f.event.triggered=b,n&&(e[o]=n));return c.result}},dispatch:function(c){c=f.event.fix(c||a.event);var d=(f._data(this,"events")||{})[c.type]||[],e=d.delegateCount,g=[].slice.call(arguments,0),h=!c.exclusive&&!c.namespace,i=[],j,k,l,m,n,o,p,q,r,s,t;g[0]=c,c.delegateTarget=this;if(e&&!c.target.disabled&&(!c.button||c.type!=="click")){m=f(this),m.context=this.ownerDocument||this;for(l=c.target;l!=this;l=l.parentNode||this){o={},q=[],m[0]=l;for(j=0;j<e;j++)r=d[j],s=r.selector,o[s]===b&&(o[s]=r.quick?H(l,r.quick):m.is(s)),o[s]&&q.push(r);q.length&&i.push({elem:l,matches:q})}}d.length>e&&i.push({elem:this,matches:d.slice(e)});for(j=0;j<i.length&&!c.isPropagationStopped();j++){p=i[j],c.currentTarget=p.elem;for(k=0;k<p.matches.length&&!c.isImmediatePropagationStopped();k++){r=p.matches[k];if(h||!c.namespace&&!r.namespace||c.namespace_re&&c.namespace_re.test(r.namespace))c.data=r.data,c.handleObj=r,n=((f.event.special[r.origType]||{}).handle||r.handler).apply(p.elem,g),n!==b&&(c.result=n,n===!1&&(c.preventDefault(),c.stopPropagation()))}}return c.result},props:"attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){a.which==null&&(a.which=b.charCode!=null?b.charCode:b.keyCode);return a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,d){var e,f,g,h=d.button,i=d.fromElement;a.pageX==null&&d.clientX!=null&&(e=a.target.ownerDocument||c,f=e.documentElement,g=e.body,a.pageX=d.clientX+(f&&f.scrollLeft||g&&g.scrollLeft||0)-(f&&f.clientLeft||g&&g.clientLeft||0),a.pageY=d.clientY+(f&&f.scrollTop||g&&g.scrollTop||0)-(f&&f.clientTop||g&&g.clientTop||0)),!a.relatedTarget&&i&&(a.relatedTarget=i===a.target?d.toElement:i),!a.which&&h!==b&&(a.which=h&1?1:h&2?3:h&4?2:0);return a}},fix:function(a){if(a[f.expando])return a;var d,e,g=a,h=f.event.fixHooks[a.type]||{},i=h.props?this.props.concat(h.props):this.props;a=f.Event(g);for(d=i.length;d;)e=i[--d],a[e]=g[e];a.target||(a.target=g.srcElement||c),a.target.nodeType===3&&(a.target=a.target.parentNode),a.metaKey===b&&(a.metaKey=a.ctrlKey);return h.filter?h.filter(a,g):a},special:{ready:{setup:f.bindReady},load:{noBubble:!0},focus:{delegateType:"focusin"},blur:{delegateType:"focusout"},beforeunload:{setup:function(a,b,c){f.isWindow(this)&&(this.onbeforeunload=c)},teardown:function(a,b){this.onbeforeunload===b&&(this.onbeforeunload=null)}}},simulate:function(a,b,c,d){var e=f.extend(new f.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?f.event.trigger(e,null,b):f.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},f.event.handle=f.event.dispatch,f.removeEvent=c.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)}:function(a,b,c){a.detachEvent&&a.detachEvent("on"+b,c)},f.Event=function(a,b){if(!(this instanceof f.Event))return new f.Event(a,b);a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||a.returnValue===!1||a.getPreventDefault&&a.getPreventDefault()?K:J):this.type=a,b&&f.extend(this,b),this.timeStamp=a&&a.timeStamp||f.now(),this[f.expando]=!0},f.Event.prototype={preventDefault:function(){this.isDefaultPrevented=K;var a=this.originalEvent;!a||(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){this.isPropagationStopped=K;var a=this.originalEvent;!a||(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=K,this.stopPropagation()},isDefaultPrevented:J,isPropagationStopped:J,isImmediatePropagationStopped:J},f.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(a,b){f.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c=this,d=a.relatedTarget,e=a.handleObj,g=e.selector,h;if(!d||d!==c&&!f.contains(c,d))a.type=e.origType,h=e.handler.apply(this,arguments),a.type=b;return h}}}),f.support.submitBubbles||(f.event.special.submit={setup:function(){if(f.nodeName(this,"form"))return!1;f.event.add(this,"click._submit keypress._submit",function(a){var c=a.target,d=f.nodeName(c,"input")||f.nodeName(c,"button")?c.form:b;d&&!d._submit_attached&&(f.event.add(d,"submit._submit",function(a){this.parentNode&&!a.isTrigger&&f.event.simulate("submit",this.parentNode,a,!0)}),d._submit_attached=!0)})},teardown:function(){if(f.nodeName(this,"form"))return!1;f.event.remove(this,"._submit")}}),f.support.changeBubbles||(f.event.special.change={setup:function(){if(z.test(this.nodeName)){if(this.type==="checkbox"||this.type==="radio")f.event.add(this,"propertychange._change",function(a){a.originalEvent.propertyName==="checked"&&(this._just_changed=!0)}),f.event.add(this,"click._change",function(a){this._just_changed&&!a.isTrigger&&(this._just_changed=!1,f.event.simulate("change",this,a,!0))});return!1}f.event.add(this,"beforeactivate._change",function(a){var b=a.target;z.test(b.nodeName)&&!b._change_attached&&(f.event.add(b,"change._change",function(a){this.parentNode&&!a.isSimulated&&!a.isTrigger&&f.event.simulate("change",this.parentNode,a,!0)}),b._change_attached=!0)})},handle:function(a){var b=a.target;if(this!==b||a.isSimulated||a.isTrigger||b.type!=="radio"&&b.type!=="checkbox")return a.handleObj.handler.apply(this,arguments)},teardown:function(){f.event.remove(this,"._change");return z.test(this.nodeName)}}),f.support.focusinBubbles||f.each({focus:"focusin",blur:"focusout"},function(a,b){var d=0,e=function(a){f.event.simulate(b,a.target,f.event.fix(a),!0)};f.event.special[b]={setup:function(){d++===0&&c.addEventListener(a,e,!0)},teardown:function(){--d===0&&c.removeEventListener(a,e,!0)}}}),f.fn.extend({on:function(a,c,d,e,g){var h,i;if(typeof a=="object"){typeof c!="string"&&(d=c,c=b);for(i in a)this.on(i,c,d,a[i],g);return this}d==null&&e==null?(e=c,d=c=b):e==null&&(typeof c=="string"?(e=d,d=b):(e=d,d=c,c=b));if(e===!1)e=J;else if(!e)return this;g===1&&(h=e,e=function(a){f().off(a);return h.apply(this,arguments)},e.guid=h.guid||(h.guid=f.guid++));return this.each(function(){f.event.add(this,a,e,d,c)})},one:function(a,b,c,d){return this.on.call(this,a,b,c,d,1)},off:function(a,c,d){if(a&&a.preventDefault&&a.handleObj){var e=a.handleObj;f(a.delegateTarget).off(e.namespace?e.type+"."+e.namespace:e.type,e.selector,e.handler);return this}if(typeof a=="object"){for(var g in a)this.off(g,c,a[g]);return this}if(c===!1||typeof c=="function")d=c,c=b;d===!1&&(d=J);return this.each(function(){f.event.remove(this,a,d,c)})},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},live:function(a,b,c){f(this.context).on(a,this.selector,b,c);return this},die:function(a,b){f(this.context).off(a,this.selector||"**",b);return this},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return arguments.length==1?this.off(a,"**"):this.off(b,a,c)},trigger:function(a,b){return this.each(function(){f.event.trigger(a,b,this)})},triggerHandler:function(a,b){if(this[0])return f.event.trigger(a,b,this[0],!0)},toggle:function(a){var b=arguments,c=a.guid||f.guid++,d=0,e=function(c){var e=(f._data(this,"lastToggle"+a.guid)||0)%d;f._data(this,"lastToggle"+a.guid,e+1),c.preventDefault();return b[e].apply(this,arguments)||!1};e.guid=c;while(d<b.length)b[d++].guid=c;return this.click(e)},hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}}),f.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){f.fn[b]=function(a,c){c==null&&(c=a,a=null);return arguments.length>0?this.on(b,null,a,c):this.trigger(b)},f.attrFn&&(f.attrFn[b]=!0),C.test(b)&&(f.event.fixHooks[b]=f.event.keyHooks),D.test(b)&&(f.event.fixHooks[b]=f.event.mouseHooks)}),function(){function x(a,b,c,e,f,g){for(var h=0,i=e.length;h<i;h++){var j=e[h];if(j){var k=!1;j=j[a];while(j){if(j[d]===c){k=e[j.sizset];break}if(j.nodeType===1){g||(j[d]=c,j.sizset=h);if(typeof b!="string"){if(j===b){k=!0;break}}else if(m.filter(b,[j]).length>0){k=j;break}}j=j[a]}e[h]=k}}}function w(a,b,c,e,f,g){for(var h=0,i=e.length;h<i;h++){var j=e[h];if(j){var k=!1;j=j[a];while(j){if(j[d]===c){k=e[j.sizset];break}j.nodeType===1&&!g&&(j[d]=c,j.sizset=h);if(j.nodeName.toLowerCase()===b){k=j;break}j=j[a]}e[h]=k}}}var a=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,d="sizcache"+(Math.random()+"").replace(".",""),e=0,g=Object.prototype.toString,h=!1,i=!0,j=/\\/g,k=/\r\n/g,l=/\W/;[0,0].sort(function(){i=!1;return 0});var m=function(b,d,e,f){e=e||[],d=d||c;var h=d;if(d.nodeType!==1&&d.nodeType!==9)return[];if(!b||typeof b!="string")return e;var i,j,k,l,n,q,r,t,u=!0,v=m.isXML(d),w=[],x=b;do{a.exec(""),i=a.exec(x);if(i){x=i[3],w.push(i[1]);if(i[2]){l=i[3];break}}}while(i);if(w.length>1&&p.exec(b))if(w.length===2&&o.relative[w[0]])j=y(w[0]+w[1],d,f);else{j=o.relative[w[0]]?[d]:m(w.shift(),d);while(w.length)b=w.shift(),o.relative[b]&&(b+=w.shift()),j=y(b,j,f)}else{!f&&w.length>1&&d.nodeType===9&&!v&&o.match.ID.test(w[0])&&!o.match.ID.test(w[w.length-1])&&(n=m.find(w.shift(),d,v),d=n.expr?m.filter(n.expr,n.set)[0]:n.set[0]);if(d){n=f?{expr:w.pop(),set:s(f)}:m.find(w.pop(),w.length===1&&(w[0]==="~"||w[0]==="+")&&d.parentNode?d.parentNode:d,v),j=n.expr?m.filter(n.expr,n.set):n.set,w.length>0?k=s(j):u=!1;while(w.length)q=w.pop(),r=q,o.relative[q]?r=w.pop():q="",r==null&&(r=d),o.relative[q](k,r,v)}else k=w=[]}k||(k=j),k||m.error(q||b);if(g.call(k)==="[object Array]")if(!u)e.push.apply(e,k);else if(d&&d.nodeType===1)for(t=0;k[t]!=null;t++)k[t]&&(k[t]===!0||k[t].nodeType===1&&m.contains(d,k[t]))&&e.push(j[t]);else for(t=0;k[t]!=null;t++)k[t]&&k[t].nodeType===1&&e.push(j[t]);else s(k,e);l&&(m(l,h,e,f),m.uniqueSort(e));return e};m.uniqueSort=function(a){if(u){h=i,a.sort(u);if(h)for(var b=1;b<a.length;b++)a[b]===a[b-1]&&a.splice(b--,1)}return a},m.matches=function(a,b){return m(a,null,null,b)},m.matchesSelector=function(a,b){return m(b,null,null,[a]).length>0},m.find=function(a,b,c){var d,e,f,g,h,i;if(!a)return[];for(e=0,f=o.order.length;e<f;e++){h=o.order[e];if(g=o.leftMatch[h].exec(a)){i=g[1],g.splice(1,1);if(i.substr(i.length-1)!=="\\"){g[1]=(g[1]||"").replace(j,""),d=o.find[h](g,b,c);if(d!=null){a=a.replace(o.match[h],"");break}}}}d||(d=typeof b.getElementsByTagName!="undefined"?b.getElementsByTagName("*"):[]);return{set:d,expr:a}},m.filter=function(a,c,d,e){var f,g,h,i,j,k,l,n,p,q=a,r=[],s=c,t=c&&c[0]&&m.isXML(c[0]);while(a&&c.length){for(h in o.filter)if((f=o.leftMatch[h].exec(a))!=null&&f[2]){k=o.filter[h],l=f[1],g=!1,f.splice(1,1);if(l.substr(l.length-1)==="\\")continue;s===r&&(r=[]);if(o.preFilter[h]){f=o.preFilter[h](f,s,d,r,e,t);if(!f)g=i=!0;else if(f===!0)continue}if(f)for(n=0;(j=s[n])!=null;n++)j&&(i=k(j,f,n,s),p=e^i,d&&i!=null?p?g=!0:s[n]=!1:p&&(r.push(j),g=!0));if(i!==b){d||(s=r),a=a.replace(o.match[h],"");if(!g)return[];break}}if(a===q)if(g==null)m.error(a);else break;q=a}return s},m.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)};var n=m.getText=function(a){var b,c,d=a.nodeType,e="";if(d){if(d===1||d===9){if(typeof a.textContent=="string")return a.textContent;if(typeof a.innerText=="string")return a.innerText.replace(k,"");for(a=a.firstChild;a;a=a.nextSibling)e+=n(a)}else if(d===3||d===4)return a.nodeValue}else for(b=0;c=a[b];b++)c.nodeType!==8&&(e+=n(c));return e},o=m.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(a){return a.getAttribute("href")},type:function(a){return a.getAttribute("type")}},relative:{"+":function(a,b){var c=typeof b=="string",d=c&&!l.test(b),e=c&&!d;d&&(b=b.toLowerCase());for(var f=0,g=a.length,h;f<g;f++)if(h=a[f]){while((h=h.previousSibling)&&h.nodeType!==1);a[f]=e||h&&h.nodeName.toLowerCase()===b?h||!1:h===b}e&&m.filter(b,a,!0)},">":function(a,b){var c,d=typeof b=="string",e=0,f=a.length;if(d&&!l.test(b)){b=b.toLowerCase();for(;e<f;e++){c=a[e];if(c){var g=c.parentNode;a[e]=g.nodeName.toLowerCase()===b?g:!1}}}else{for(;e<f;e++)c=a[e],c&&(a[e]=d?c.parentNode:c.parentNode===b);d&&m.filter(b,a,!0)}},"":function(a,b,c){var d,f=e++,g=x;typeof b=="string"&&!l.test(b)&&(b=b.toLowerCase(),d=b,g=w),g("parentNode",b,f,a,d,c)},"~":function(a,b,c){var d,f=e++,g=x;typeof b=="string"&&!l.test(b)&&(b=b.toLowerCase(),d=b,g=w),g("previousSibling",b,f,a,d,c)}},find:{ID:function(a,b,c){if(typeof b.getElementById!="undefined"&&!c){var d=b.getElementById(a[1]);return d&&d.parentNode?[d]:[]}},NAME:function(a,b){if(typeof b.getElementsByName!="undefined"){var c=[],d=b.getElementsByName(a[1]);for(var e=0,f=d.length;e<f;e++)d[e].getAttribute("name")===a[1]&&c.push(d[e]);return c.length===0?null:c}},TAG:function(a,b){if(typeof b.getElementsByTagName!="undefined")return b.getElementsByTagName(a[1])}},preFilter:{CLASS:function(a,b,c,d,e,f){a=" "+a[1].replace(j,"")+" ";if(f)return a;for(var g=0,h;(h=b[g])!=null;g++)h&&(e^(h.className&&(" "+h.className+" ").replace(/[\t\n\r]/g," ").indexOf(a)>=0)?c||d.push(h):c&&(b[g]=!1));return!1},ID:function(a){return a[1].replace(j,"")},TAG:function(a,b){return a[1].replace(j,"").toLowerCase()},CHILD:function(a){if(a[1]==="nth"){a[2]||m.error(a[0]),a[2]=a[2].replace(/^\+|\s*/g,"");var b=/(-?)(\d*)(?:n([+\-]?\d*))?/.exec(a[2]==="even"&&"2n"||a[2]==="odd"&&"2n+1"||!/\D/.test(a[2])&&"0n+"+a[2]||a[2]);a[2]=b[1]+(b[2]||1)-0,a[3]=b[3]-0}else a[2]&&m.error(a[0]);a[0]=e++;return a},ATTR:function(a,b,c,d,e,f){var g=a[1]=a[1].replace(j,"");!f&&o.attrMap[g]&&(a[1]=o.attrMap[g]),a[4]=(a[4]||a[5]||"").replace(j,""),a[2]==="~="&&(a[4]=" "+a[4]+" ");return a},PSEUDO:function(b,c,d,e,f){if(b[1]==="not")if((a.exec(b[3])||"").length>1||/^\w/.test(b[3]))b[3]=m(b[3],null,null,c);else{var g=m.filter(b[3],c,d,!0^f);d||e.push.apply(e,g);return!1}else if(o.match.POS.test(b[0])||o.match.CHILD.test(b[0]))return!0;return b},POS:function(a){a.unshift(!0);return a}},filters:{enabled:function(a){return a.disabled===!1&&a.type!=="hidden"},disabled:function(a){return a.disabled===!0},checked:function(a){return a.checked===!0},selected:function(a){a.parentNode&&a.parentNode.selectedIndex;return a.selected===!0},parent:function(a){return!!a.firstChild},empty:function(a){return!a.firstChild},has:function(a,b,c){return!!m(c[3],a).length},header:function(a){return/h\d/i.test(a.nodeName)},text:function(a){var b=a.getAttribute("type"),c=a.type;return a.nodeName.toLowerCase()==="input"&&"text"===c&&(b===c||b===null)},radio:function(a){return a.nodeName.toLowerCase()==="input"&&"radio"===a.type},checkbox:function(a){return a.nodeName.toLowerCase()==="input"&&"checkbox"===a.type},file:function(a){return a.nodeName.toLowerCase()==="input"&&"file"===a.type},password:function(a){return a.nodeName.toLowerCase()==="input"&&"password"===a.type},submit:function(a){var b=a.nodeName.toLowerCase();return(b==="input"||b==="button")&&"submit"===a.type},image:function(a){return a.nodeName.toLowerCase()==="input"&&"image"===a.type},reset:function(a){var b=a.nodeName.toLowerCase();return(b==="input"||b==="button")&&"reset"===a.type},button:function(a){var b=a.nodeName.toLowerCase();return b==="input"&&"button"===a.type||b==="button"},input:function(a){return/input|select|textarea|button/i.test(a.nodeName)},focus:function(a){return a===a.ownerDocument.activeElement}},setFilters:{first:function(a,b){return b===0},last:function(a,b,c,d){return b===d.length-1},even:function(a,b){return b%2===0},odd:function(a,b){return b%2===1},lt:function(a,b,c){return b<c[3]-0},gt:function(a,b,c){return b>c[3]-0},nth:function(a,b,c){return c[3]-0===b},eq:function(a,b,c){return c[3]-0===b}},filter:{PSEUDO:function(a,b,c,d){var e=b[1],f=o.filters[e];if(f)return f(a,c,b,d);if(e==="contains")return(a.textContent||a.innerText||n([a])||"").indexOf(b[3])>=0;if(e==="not"){var g=b[3];for(var h=0,i=g.length;h<i;h++)if(g[h]===a)return!1;return!0}m.error(e)},CHILD:function(a,b){var c,e,f,g,h,i,j,k=b[1],l=a;switch(k){case"only":case"first":while(l=l.previousSibling)if(l.nodeType===1)return!1;if(k==="first")return!0;l=a;case"last":while(l=l.nextSibling)if(l.nodeType===1)return!1;return!0;case"nth":c=b[2],e=b[3];if(c===1&&e===0)return!0;f=b[0],g=a.parentNode;if(g&&(g[d]!==f||!a.nodeIndex)){i=0;for(l=g.firstChild;l;l=l.nextSibling)l.nodeType===1&&(l.nodeIndex=++i);g[d]=f}j=a.nodeIndex-e;return c===0?j===0:j%c===0&&j/c>=0}},ID:function(a,b){return a.nodeType===1&&a.getAttribute("id")===b},TAG:function(a,b){return b==="*"&&a.nodeType===1||!!a.nodeName&&a.nodeName.toLowerCase()===b},CLASS:function(a,b){return(" "+(a.className||a.getAttribute("class"))+" ").indexOf(b)>-1},ATTR:function(a,b){var c=b[1],d=m.attr?m.attr(a,c):o.attrHandle[c]?o.attrHandle[c](a):a[c]!=null?a[c]:a.getAttribute(c),e=d+"",f=b[2],g=b[4];return d==null?f==="!=":!f&&m.attr?d!=null:f==="="?e===g:f==="*="?e.indexOf(g)>=0:f==="~="?(" "+e+" ").indexOf(g)>=0:g?f==="!="?e!==g:f==="^="?e.indexOf(g)===0:f==="$="?e.substr(e.length-g.length)===g:f==="|="?e===g||e.substr(0,g.length+1)===g+"-":!1:e&&d!==!1},POS:function(a,b,c,d){var e=b[2],f=o.setFilters[e];if(f)return f(a,c,b,d)}}},p=o.match.POS,q=function(a,b){return"\\"+(b-0+1)};for(var r in o.match)o.match[r]=new RegExp(o.match[r].source+/(?![^\[]*\])(?![^\(]*\))/.source),o.leftMatch[r]=new RegExp(/(^(?:.|\r|\n)*?)/.source+o.match[r].source.replace(/\\(\d+)/g,q));var s=function(a,b){a=Array.prototype.slice.call(a,0);if(b){b.push.apply(b,a);return b}return a};try{Array.prototype.slice.call(c.documentElement.childNodes,0)[0].nodeType}catch(t){s=function(a,b){var c=0,d=b||[];if(g.call(a)==="[object Array]")Array.prototype.push.apply(d,a);else if(typeof a.length=="number")for(var e=a.length;c<e;c++)d.push(a[c]);else for(;a[c];c++)d.push(a[c]);return d}}var u,v;c.documentElement.compareDocumentPosition?u=function(a,b){if(a===b){h=!0;return 0}if(!a.compareDocumentPosition||!b.compareDocumentPosition)return a.compareDocumentPosition?-1:1;return a.compareDocumentPosition(b)&4?-1:1}:(u=function(a,b){if(a===b){h=!0;return 0}if(a.sourceIndex&&b.sourceIndex)return a.sourceIndex-b.sourceIndex;var c,d,e=[],f=[],g=a.parentNode,i=b.parentNode,j=g;if(g===i)return v(a,b);if(!g)return-1;if(!i)return 1;while(j)e.unshift(j),j=j.parentNode;j=i;while(j)f.unshift(j),j=j.parentNode;c=e.length,d=f.length;for(var k=0;k<c&&k<d;k++)if(e[k]!==f[k])return v(e[k],f[k]);return k===c?v(a,f[k],-1):v(e[k],b,1)},v=function(a,b,c){if(a===b)return c;var d=a.nextSibling;while(d){if(d===b)return-1;d=d.nextSibling}return 1}),function(){var a=c.createElement("div"),d="script"+(new Date).getTime(),e=c.documentElement;a.innerHTML="<a name='"+d+"'/>",e.insertBefore(a,e.firstChild),c.getElementById(d)&&(o.find.ID=function(a,c,d){if(typeof c.getElementById!="undefined"&&!d){var e=c.getElementById(a[1]);return e?e.id===a[1]||typeof e.getAttributeNode!="undefined"&&e.getAttributeNode("id").nodeValue===a[1]?[e]:b:[]}},o.filter.ID=function(a,b){var c=typeof a.getAttributeNode!="undefined"&&a.getAttributeNode("id");return a.nodeType===1&&c&&c.nodeValue===b}),e.removeChild(a),e=a=null}(),function(){var a=c.createElement("div");a.appendChild(c.createComment("")),a.getElementsByTagName("*").length>0&&(o.find.TAG=function(a,b){var c=b.getElementsByTagName(a[1]);if(a[1]==="*"){var d=[];for(var e=0;c[e];e++)c[e].nodeType===1&&d.push(c[e]);c=d}return c}),a.innerHTML="<a href='#'></a>",a.firstChild&&typeof a.firstChild.getAttribute!="undefined"&&a.firstChild.getAttribute("href")!=="#"&&(o.attrHandle.href=function(a){return a.getAttribute("href",2)}),a=null}(),c.querySelectorAll&&function(){var a=m,b=c.createElement("div"),d="__sizzle__";b.innerHTML="<p class='TEST'></p>";if(!b.querySelectorAll||b.querySelectorAll(".TEST").length!==0){m=function(b,e,f,g){e=e||c;if(!g&&!m.isXML(e)){var h=/^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(b);if(h&&(e.nodeType===1||e.nodeType===9)){if(h[1])return s(e.getElementsByTagName(b),f);if(h[2]&&o.find.CLASS&&e.getElementsByClassName)return s(e.getElementsByClassName(h[2]),f)}if(e.nodeType===9){if(b==="body"&&e.body)return s([e.body],f);if(h&&h[3]){var i=e.getElementById(h[3]);if(!i||!i.parentNode)return s([],f);if(i.id===h[3])return s([i],f)}try{return s(e.querySelectorAll(b),f)}catch(j){}}else if(e.nodeType===1&&e.nodeName.toLowerCase()!=="object"){var k=e,l=e.getAttribute("id"),n=l||d,p=e.parentNode,q=/^\s*[+~]/.test(b);l?n=n.replace(/'/g,"\\$&"):e.setAttribute("id",n),q&&p&&(e=e.parentNode);try{if(!q||p)return s(e.querySelectorAll("[id='"+n+"'] "+b),f)}catch(r){}finally{l||k.removeAttribute("id")}}}return a(b,e,f,g)};for(var e in a)m[e]=a[e];b=null}}(),function(){var a=c.documentElement,b=a.matchesSelector||a.mozMatchesSelector||a.webkitMatchesSelector||a.msMatchesSelector;if(b){var d=!b.call(c.createElement("div"),"div"),e=!1;try{b.call(c.documentElement,"[test!='']:sizzle")}catch(f){e=!0}m.matchesSelector=function(a,c){c=c.replace(/\=\s*([^'"\]]*)\s*\]/g,"='$1']");if(!m.isXML(a))try{if(e||!o.match.PSEUDO.test(c)&&!/!=/.test(c)){var f=b.call(a,c);if(f||!d||a.document&&a.document.nodeType!==11)return f}}catch(g){}return m(c,null,null,[a]).length>0}}}(),function(){var a=c.createElement("div");a.innerHTML="<div class='test e'></div><div class='test'></div>";if(!!a.getElementsByClassName&&a.getElementsByClassName("e").length!==0){a.lastChild.className="e";if(a.getElementsByClassName("e").length===1)return;o.order.splice(1,0,"CLASS"),o.find.CLASS=function(a,b,c){if(typeof b.getElementsByClassName!="undefined"&&!c)return b.getElementsByClassName(a[1])},a=null}}(),c.documentElement.contains?m.contains=function(a,b){return a!==b&&(a.contains?a.contains(b):!0)}:c.documentElement.compareDocumentPosition?m.contains=function(a,b){return!!(a.compareDocumentPosition(b)&16)}:m.contains=function(){return!1},m.isXML=function(a){var b=(a?a.ownerDocument||a:0).documentElement;return b?b.nodeName!=="HTML":!1};var y=function(a,b,c){var d,e=[],f="",g=b.nodeType?[b]:b;while(d=o.match.PSEUDO.exec(a))f+=d[0],a=a.replace(o.match.PSEUDO,"");a=o.relative[a]?a+"*":a;for(var h=0,i=g.length;h<i;h++)m(a,g[h],e,c);return m.filter(f,e)};m.attr=f.attr,m.selectors.attrMap={},f.find=m,f.expr=m.selectors,f.expr[":"]=f.expr.filters,f.unique=m.uniqueSort,f.text=m.getText,f.isXMLDoc=m.isXML,f.contains=m.contains}();var L=/Until$/,M=/^(?:parents|prevUntil|prevAll)/,N=/,/,O=/^.[^:#\[\.,]*$/,P=Array.prototype.slice,Q=f.expr.match.POS,R={children:!0,contents:!0,next:!0,prev:!0};f.fn.extend({find:function(a){var b=this,c,d;if(typeof a!="string")return f(a).filter(function(){for(c=0,d=b.length;c<d;c++)if(f.contains(b[c],this))return!0});var e=this.pushStack("","find",a),g,h,i;for(c=0,d=this.length;c<d;c++){g=e.length,f.find(a,this[c],e);if(c>0)for(h=g;h<e.length;h++)for(i=0;i<g;i++)if(e[i]===e[h]){e.splice(h--,1);break}}return e},has:function(a){var b=f(a);return this.filter(function(){for(var a=0,c=b.length;a<c;a++)if(f.contains(this,b[a]))return!0})},not:function(a){return this.pushStack(T(this,a,!1),"not",a)},filter:function(a){return this.pushStack(T(this,a,!0),"filter",a)},is:function(a){return!!a&&(typeof a=="string"?Q.test(a)?f(a,this.context).index(this[0])>=0:f.filter(a,this).length>0:this.filter(a).length>0)},closest:function(a,b){var c=[],d,e,g=this[0];if(f.isArray(a)){var h=1;while(g&&g.ownerDocument&&g!==b){for(d=0;d<a.length;d++)f(g).is(a[d])&&c.push({selector:a[d],elem:g,level:h});g=g.parentNode,h++}return c}var i=Q.test(a)||typeof a!="string"?f(a,b||this.context):0;for(d=0,e=this.length;d<e;d++){g=this[d];while(g){if(i?i.index(g)>-1:f.find.matchesSelector(g,a)){c.push(g);break}g=g.parentNode;if(!g||!g.ownerDocument||g===b||g.nodeType===11)break}}c=c.length>1?f.unique(c):c;return this.pushStack(c,"closest",a)},index:function(a){if(!a)return this[0]&&this[0].parentNode?this.prevAll().length:-1;if(typeof a=="string")return f.inArray(this[0],f(a));return f.inArray(a.jquery?a[0]:a,this)},add:function(a,b){var c=typeof a=="string"?f(a,b):f.makeArray(a&&a.nodeType?[a]:a),d=f.merge(this.get(),c);return this.pushStack(S(c[0])||S(d[0])?d:f.unique(d))},andSelf:function(){return this.add(this.prevObject)}}),f.each({parent:function(a){var b=a.parentNode;return b&&b.nodeType!==11?b:null},parents:function(a){return f.dir(a,"parentNode")},parentsUntil:function(a,b,c){return f.dir(a,"parentNode",c)},next:function(a){return f.nth(a,2,"nextSibling")},prev:function(a){return f.nth(a,2,"previousSibling")},nextAll:function(a){return f.dir(a,"nextSibling")},prevAll:function(a){return f.dir(a,"previousSibling")},nextUntil:function(a,b,c){return f.dir(a,"nextSibling",c)},prevUntil:function(a,b,c){return f.dir(a,"previousSibling",c)},siblings:function(a){return f.sibling(a.parentNode.firstChild,a)},children:function(a){return f.sibling(a.firstChild)},contents:function(a){return f.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:f.makeArray(a.childNodes)}},function(a,b){f.fn[a]=function(c,d){var e=f.map(this,b,c);L.test(a)||(d=c),d&&typeof d=="string"&&(e=f.filter(d,e)),e=this.length>1&&!R[a]?f.unique(e):e,(this.length>1||N.test(d))&&M.test(a)&&(e=e.reverse());return this.pushStack(e,a,P.call(arguments).join(","))}}),f.extend({filter:function(a,b,c){c&&(a=":not("+a+")");return b.length===1?f.find.matchesSelector(b[0],a)?[b[0]]:[]:f.find.matches(a,b)},dir:function(a,c,d){var e=[],g=a[c];while(g&&g.nodeType!==9&&(d===b||g.nodeType!==1||!f(g).is(d)))g.nodeType===1&&e.push(g),g=g[c];return e},nth:function(a,b,c,d){b=b||1;var e=0;for(;a;a=a[c])if(a.nodeType===1&&++e===b)break;return a},sibling:function(a,b){var c=[];for(;a;a=a.nextSibling)a.nodeType===1&&a!==b&&c.push(a);return c}});var V="abbr|article|aside|audio|canvas|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",W=/ jQuery\d+="(?:\d+|null)"/g,X=/^\s+/,Y=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,Z=/<([\w:]+)/,$=/<tbody/i,_=/<|&#?\w+;/,ba=/<(?:script|style)/i,bb=/<(?:script|object|embed|option|style)/i,bc=new RegExp("<(?:"+V+")","i"),bd=/checked\s*(?:[^=]|=\s*.checked.)/i,be=/\/(java|ecma)script/i,bf=/^\s*<!(?:\[CDATA\[|\-\-)/,bg={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],area:[1,"<map>","</map>"],_default:[0,"",""]},bh=U(c);bg.optgroup=bg.option,bg.tbody=bg.tfoot=bg.colgroup=bg.caption=bg.thead,bg.th=bg.td,f.support.htmlSerialize||(bg._default=[1,"div<div>","</div>"]),f.fn.extend({text:function(a){if(f.isFunction(a))return this.each(function(b){var c=f(this);c.text(a.call(this,b,c.text()))});if(typeof a!="object"&&a!==b)return this.empty().append((this[0]&&this[0].ownerDocument||c).createTextNode(a));return f.text(this)},wrapAll:function(a){if(f.isFunction(a))return this.each(function(b){f(this).wrapAll(a.call(this,b))});if(this[0]){var b=f(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&a.firstChild.nodeType===1)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){if(f.isFunction(a))return this.each(function(b){f(this).wrapInner(a.call(this,b))});return this.each(function(){var b=f(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=f.isFunction(a);return this.each(function(c){f(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){f.nodeName(this,"body")||f(this).replaceWith(this.childNodes)}).end()},append:function(){return this.domManip(arguments,!0,function(a){this.nodeType===1&&this.appendChild(a)})},prepend:function(){return this.domManip(arguments,!0,function(a){this.nodeType===1&&this.insertBefore(a,this.firstChild)})},before:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,!1,function(a){this.parentNode.insertBefore(a,this)});if(arguments.length){var a=f.clean(arguments);a.push.apply(a,this.toArray());return this.pushStack(a,"before",arguments)}},after:function(){if(this[0]&&this[0].parentNode)return this.domManip(arguments,!1,function(a){this.parentNode.insertBefore(a,this.nextSibling)});if(arguments.length){var a=this.pushStack(this,"after",arguments);a.push.apply(a,f.clean(arguments));return a}},remove:function(a,b){for(var c=0,d;(d=this[c])!=null;c++)if(!a||f.filter(a,[d]).length)!b&&d.nodeType===1&&(f.cleanData(d.getElementsByTagName("*")),f.cleanData([d])),d.parentNode&&d.parentNode.removeChild(d);return this},empty:function()
	{for(var a=0,b;(b=this[a])!=null;a++){b.nodeType===1&&f.cleanData(b.getElementsByTagName("*"));while(b.firstChild)b.removeChild(b.firstChild)}return this},clone:function(a,b){a=a==null?!1:a,b=b==null?a:b;return this.map(function(){return f.clone(this,a,b)})},html:function(a){if(a===b)return this[0]&&this[0].nodeType===1?this[0].innerHTML.replace(W,""):null;if(typeof a=="string"&&!ba.test(a)&&(f.support.leadingWhitespace||!X.test(a))&&!bg[(Z.exec(a)||["",""])[1].toLowerCase()]){a=a.replace(Y,"<$1></$2>");try{for(var c=0,d=this.length;c<d;c++)this[c].nodeType===1&&(f.cleanData(this[c].getElementsByTagName("*")),this[c].innerHTML=a)}catch(e){this.empty().append(a)}}else f.isFunction(a)?this.each(function(b){var c=f(this);c.html(a.call(this,b,c.html()))}):this.empty().append(a);return this},replaceWith:function(a){if(this[0]&&this[0].parentNode){if(f.isFunction(a))return this.each(function(b){var c=f(this),d=c.html();c.replaceWith(a.call(this,b,d))});typeof a!="string"&&(a=f(a).detach());return this.each(function(){var b=this.nextSibling,c=this.parentNode;f(this).remove(),b?f(b).before(a):f(c).append(a)})}return this.length?this.pushStack(f(f.isFunction(a)?a():a),"replaceWith",a):this},detach:function(a){return this.remove(a,!0)},domManip:function(a,c,d){var e,g,h,i,j=a[0],k=[];if(!f.support.checkClone&&arguments.length===3&&typeof j=="string"&&bd.test(j))return this.each(function(){f(this).domManip(a,c,d,!0)});if(f.isFunction(j))return this.each(function(e){var g=f(this);a[0]=j.call(this,e,c?g.html():b),g.domManip(a,c,d)});if(this[0]){i=j&&j.parentNode,f.support.parentNode&&i&&i.nodeType===11&&i.childNodes.length===this.length?e={fragment:i}:e=f.buildFragment(a,this,k),h=e.fragment,h.childNodes.length===1?g=h=h.firstChild:g=h.firstChild;if(g){c=c&&f.nodeName(g,"tr");for(var l=0,m=this.length,n=m-1;l<m;l++)d.call(c?bi(this[l],g):this[l],e.cacheable||m>1&&l<n?f.clone(h,!0,!0):h)}k.length&&f.each(k,bp)}return this}}),f.buildFragment=function(a,b,d){var e,g,h,i,j=a[0];b&&b[0]&&(i=b[0].ownerDocument||b[0]),i.createDocumentFragment||(i=c),a.length===1&&typeof j=="string"&&j.length<512&&i===c&&j.charAt(0)==="<"&&!bb.test(j)&&(f.support.checkClone||!bd.test(j))&&(f.support.html5Clone||!bc.test(j))&&(g=!0,h=f.fragments[j],h&&h!==1&&(e=h)),e||(e=i.createDocumentFragment(),f.clean(a,i,e,d)),g&&(f.fragments[j]=h?e:1);return{fragment:e,cacheable:g}},f.fragments={},f.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){f.fn[a]=function(c){var d=[],e=f(c),g=this.length===1&&this[0].parentNode;if(g&&g.nodeType===11&&g.childNodes.length===1&&e.length===1){e[b](this[0]);return this}for(var h=0,i=e.length;h<i;h++){var j=(h>0?this.clone(!0):this).get();f(e[h])[b](j),d=d.concat(j)}return this.pushStack(d,a,e.selector)}}),f.extend({clone:function(a,b,c){var d,e,g,h=f.support.html5Clone||!bc.test("<"+a.nodeName)?a.cloneNode(!0):bo(a);if((!f.support.noCloneEvent||!f.support.noCloneChecked)&&(a.nodeType===1||a.nodeType===11)&&!f.isXMLDoc(a)){bk(a,h),d=bl(a),e=bl(h);for(g=0;d[g];++g)e[g]&&bk(d[g],e[g])}if(b){bj(a,h);if(c){d=bl(a),e=bl(h);for(g=0;d[g];++g)bj(d[g],e[g])}}d=e=null;return h},clean:function(a,b,d,e){var g;b=b||c,typeof b.createElement=="undefined"&&(b=b.ownerDocument||b[0]&&b[0].ownerDocument||c);var h=[],i;for(var j=0,k;(k=a[j])!=null;j++){typeof k=="number"&&(k+="");if(!k)continue;if(typeof k=="string")if(!_.test(k))k=b.createTextNode(k);else{k=k.replace(Y,"<$1></$2>");var l=(Z.exec(k)||["",""])[1].toLowerCase(),m=bg[l]||bg._default,n=m[0],o=b.createElement("div");b===c?bh.appendChild(o):U(b).appendChild(o),o.innerHTML=m[1]+k+m[2];while(n--)o=o.lastChild;if(!f.support.tbody){var p=$.test(k),q=l==="table"&&!p?o.firstChild&&o.firstChild.childNodes:m[1]==="<table>"&&!p?o.childNodes:[];for(i=q.length-1;i>=0;--i)f.nodeName(q[i],"tbody")&&!q[i].childNodes.length&&q[i].parentNode.removeChild(q[i])}!f.support.leadingWhitespace&&X.test(k)&&o.insertBefore(b.createTextNode(X.exec(k)[0]),o.firstChild),k=o.childNodes}var r;if(!f.support.appendChecked)if(k[0]&&typeof (r=k.length)=="number")for(i=0;i<r;i++)bn(k[i]);else bn(k);k.nodeType?h.push(k):h=f.merge(h,k)}if(d){g=function(a){return!a.type||be.test(a.type)};for(j=0;h[j];j++)if(e&&f.nodeName(h[j],"script")&&(!h[j].type||h[j].type.toLowerCase()==="text/javascript"))e.push(h[j].parentNode?h[j].parentNode.removeChild(h[j]):h[j]);else{if(h[j].nodeType===1){var s=f.grep(h[j].getElementsByTagName("script"),g);h.splice.apply(h,[j+1,0].concat(s))}d.appendChild(h[j])}}return h},cleanData:function(a){var b,c,d=f.cache,e=f.event.special,g=f.support.deleteExpando;for(var h=0,i;(i=a[h])!=null;h++){if(i.nodeName&&f.noData[i.nodeName.toLowerCase()])continue;c=i[f.expando];if(c){b=d[c];if(b&&b.events){for(var j in b.events)e[j]?f.event.remove(i,j):f.removeEvent(i,j,b.handle);b.handle&&(b.handle.elem=null)}g?delete i[f.expando]:i.removeAttribute&&i.removeAttribute(f.expando),delete d[c]}}}});var bq=/alpha\([^)]*\)/i,br=/opacity=([^)]*)/,bs=/([A-Z]|^ms)/g,bt=/^-?\d+(?:px)?$/i,bu=/^-?\d/,bv=/^([\-+])=([\-+.\de]+)/,bw={position:"absolute",visibility:"hidden",display:"block"},bx=["Left","Right"],by=["Top","Bottom"],bz,bA,bB;f.fn.css=function(a,c){if(arguments.length===2&&c===b)return this;return f.access(this,a,c,!0,function(a,c,d){return d!==b?f.style(a,c,d):f.css(a,c)})},f.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=bz(a,"opacity","opacity");return c===""?"1":c}return a.style.opacity}}},cssNumber:{fillOpacity:!0,fontWeight:!0,lineHeight:!0,opacity:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":f.support.cssFloat?"cssFloat":"styleFloat"},style:function(a,c,d,e){if(!!a&&a.nodeType!==3&&a.nodeType!==8&&!!a.style){var g,h,i=f.camelCase(c),j=a.style,k=f.cssHooks[i];c=f.cssProps[i]||i;if(d===b){if(k&&"get"in k&&(g=k.get(a,!1,e))!==b)return g;return j[c]}h=typeof d,h==="string"&&(g=bv.exec(d))&&(d=+(g[1]+1)*+g[2]+parseFloat(f.css(a,c)),h="number");if(d==null||h==="number"&&isNaN(d))return;h==="number"&&!f.cssNumber[i]&&(d+="px");if(!k||!("set"in k)||(d=k.set(a,d))!==b)try{j[c]=d}catch(l){}}},css:function(a,c,d){var e,g;c=f.camelCase(c),g=f.cssHooks[c],c=f.cssProps[c]||c,c==="cssFloat"&&(c="float");if(g&&"get"in g&&(e=g.get(a,!0,d))!==b)return e;if(bz)return bz(a,c)},swap:function(a,b,c){var d={};for(var e in b)d[e]=a.style[e],a.style[e]=b[e];c.call(a);for(e in b)a.style[e]=d[e]}}),f.curCSS=f.css,f.each(["height","width"],function(a,b){f.cssHooks[b]={get:function(a,c,d){var e;if(c){if(a.offsetWidth!==0)return bC(a,b,d);f.swap(a,bw,function(){e=bC(a,b,d)});return e}},set:function(a,b){if(!bt.test(b))return b;b=parseFloat(b);if(b>=0)return b+"px"}}}),f.support.opacity||(f.cssHooks.opacity={get:function(a,b){return br.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?parseFloat(RegExp.$1)/100+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=f.isNumeric(b)?"alpha(opacity="+b*100+")":"",g=d&&d.filter||c.filter||"";c.zoom=1;if(b>=1&&f.trim(g.replace(bq,""))===""){c.removeAttribute("filter");if(d&&!d.filter)return}c.filter=bq.test(g)?g.replace(bq,e):g+" "+e}}),f(function(){f.support.reliableMarginRight||(f.cssHooks.marginRight={get:function(a,b){var c;f.swap(a,{display:"inline-block"},function(){b?c=bz(a,"margin-right","marginRight"):c=a.style.marginRight});return c}})}),c.defaultView&&c.defaultView.getComputedStyle&&(bA=function(a,b){var c,d,e;b=b.replace(bs,"-$1").toLowerCase(),(d=a.ownerDocument.defaultView)&&(e=d.getComputedStyle(a,null))&&(c=e.getPropertyValue(b),c===""&&!f.contains(a.ownerDocument.documentElement,a)&&(c=f.style(a,b)));return c}),c.documentElement.currentStyle&&(bB=function(a,b){var c,d,e,f=a.currentStyle&&a.currentStyle[b],g=a.style;f===null&&g&&(e=g[b])&&(f=e),!bt.test(f)&&bu.test(f)&&(c=g.left,d=a.runtimeStyle&&a.runtimeStyle.left,d&&(a.runtimeStyle.left=a.currentStyle.left),g.left=b==="fontSize"?"1em":f||0,f=g.pixelLeft+"px",g.left=c,d&&(a.runtimeStyle.left=d));return f===""?"auto":f}),bz=bA||bB,f.expr&&f.expr.filters&&(f.expr.filters.hidden=function(a){var b=a.offsetWidth,c=a.offsetHeight;return b===0&&c===0||!f.support.reliableHiddenOffsets&&(a.style&&a.style.display||f.css(a,"display"))==="none"},f.expr.filters.visible=function(a){return!f.expr.filters.hidden(a)});var bD=/%20/g,bE=/\[\]$/,bF=/\r?\n/g,bG=/#.*$/,bH=/^(.*?):[ \t]*([^\r\n]*)\r?$/mg,bI=/^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,bJ=/^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,bK=/^(?:GET|HEAD)$/,bL=/^\/\//,bM=/\?/,bN=/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,bO=/^(?:select|textarea)/i,bP=/\s+/,bQ=/([?&])_=[^&]*/,bR=/^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/,bS=f.fn.load,bT={},bU={},bV,bW,bX=["*/"]+["*"];try{bV=e.href}catch(bY){bV=c.createElement("a"),bV.href="",bV=bV.href}bW=bR.exec(bV.toLowerCase())||[],f.fn.extend({load:function(a,c,d){if(typeof a!="string"&&bS)return bS.apply(this,arguments);if(!this.length)return this;var e=a.indexOf(" ");if(e>=0){var g=a.slice(e,a.length);a=a.slice(0,e)}var h="GET";c&&(f.isFunction(c)?(d=c,c=b):typeof c=="object"&&(c=f.param(c,f.ajaxSettings.traditional),h="POST"));var i=this;f.ajax({url:a,type:h,dataType:"html",data:c,complete:function(a,b,c){c=a.responseText,a.isResolved()&&(a.done(function(a){c=a}),i.html(g?f("<div>").append(c.replace(bN,"")).find(g):c)),d&&i.each(d,[c,b,a])}});return this},serialize:function(){return f.param(this.serializeArray())},serializeArray:function(){return this.map(function(){return this.elements?f.makeArray(this.elements):this}).filter(function(){return this.name&&!this.disabled&&(this.checked||bO.test(this.nodeName)||bI.test(this.type))}).map(function(a,b){var c=f(this).val();return c==null?null:f.isArray(c)?f.map(c,function(a,c){return{name:b.name,value:a.replace(bF,"\r\n")}}):{name:b.name,value:c.replace(bF,"\r\n")}}).get()}}),f.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "),function(a,b){f.fn[b]=function(a){return this.on(b,a)}}),f.each(["get","post"],function(a,c){f[c]=function(a,d,e,g){f.isFunction(d)&&(g=g||e,e=d,d=b);return f.ajax({type:c,url:a,data:d,success:e,dataType:g})}}),f.extend({getScript:function(a,c){return f.get(a,b,c,"script")},getJSON:function(a,b,c){return f.get(a,b,c,"json")},ajaxSetup:function(a,b){b?b_(a,f.ajaxSettings):(b=a,a=f.ajaxSettings),b_(a,b);return a},ajaxSettings:{url:bV,isLocal:bJ.test(bW[1]),global:!0,type:"GET",contentType:"application/x-www-form-urlencoded",processData:!0,async:!0,accepts:{xml:"application/xml, text/xml",html:"text/html",text:"text/plain",json:"application/json, text/javascript","*":bX},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText"},converters:{"* text":a.String,"text html":!0,"text json":f.parseJSON,"text xml":f.parseXML},flatOptions:{context:!0,url:!0}},ajaxPrefilter:bZ(bT),ajaxTransport:bZ(bU),ajax:function(a,c){function w(a,c,l,m){if(s!==2){s=2,q&&clearTimeout(q),p=b,n=m||"",v.readyState=a>0?4:0;var o,r,u,w=c,x=l?cb(d,v,l):b,y,z;if(a>=200&&a<300||a===304){if(d.ifModified){if(y=v.getResponseHeader("Last-Modified"))f.lastModified[k]=y;if(z=v.getResponseHeader("Etag"))f.etag[k]=z}if(a===304)w="notmodified",o=!0;else try{r=cc(d,x),w="success",o=!0}catch(A){w="parsererror",u=A}}else{u=w;if(!w||a)w="error",a<0&&(a=0)}v.status=a,v.statusText=""+(c||w),o?h.resolveWith(e,[r,w,v]):h.rejectWith(e,[v,w,u]),v.statusCode(j),j=b,t&&g.trigger("ajax"+(o?"Success":"Error"),[v,d,o?r:u]),i.fireWith(e,[v,w]),t&&(g.trigger("ajaxComplete",[v,d]),--f.active||f.event.trigger("ajaxStop"))}}typeof a=="object"&&(c=a,a=b),c=c||{};var d=f.ajaxSetup({},c),e=d.context||d,g=e!==d&&(e.nodeType||e instanceof f)?f(e):f.event,h=f.Deferred(),i=f.Callbacks("once memory"),j=d.statusCode||{},k,l={},m={},n,o,p,q,r,s=0,t,u,v={readyState:0,setRequestHeader:function(a,b){if(!s){var c=a.toLowerCase();a=m[c]=m[c]||a,l[a]=b}return this},getAllResponseHeaders:function(){return s===2?n:null},getResponseHeader:function(a){var c;if(s===2){if(!o){o={};while(c=bH.exec(n))o[c[1].toLowerCase()]=c[2]}c=o[a.toLowerCase()]}return c===b?null:c},overrideMimeType:function(a){s||(d.mimeType=a);return this},abort:function(a){a=a||"abort",p&&p.abort(a),w(0,a);return this}};h.promise(v),v.success=v.done,v.error=v.fail,v.complete=i.add,v.statusCode=function(a){if(a){var b;if(s<2)for(b in a)j[b]=[j[b],a[b]];else b=a[v.status],v.then(b,b)}return this},d.url=((a||d.url)+"").replace(bG,"").replace(bL,bW[1]+"//"),d.dataTypes=f.trim(d.dataType||"*").toLowerCase().split(bP),d.crossDomain==null&&(r=bR.exec(d.url.toLowerCase()),d.crossDomain=!(!r||r[1]==bW[1]&&r[2]==bW[2]&&(r[3]||(r[1]==="http:"?80:443))==(bW[3]||(bW[1]==="http:"?80:443)))),d.data&&d.processData&&typeof d.data!="string"&&(d.data=f.param(d.data,d.traditional)),b$(bT,d,c,v);if(s===2)return!1;t=d.global,d.type=d.type.toUpperCase(),d.hasContent=!bK.test(d.type),t&&f.active++===0&&f.event.trigger("ajaxStart");if(!d.hasContent){d.data&&(d.url+=(bM.test(d.url)?"&":"?")+d.data,delete d.data),k=d.url;if(d.cache===!1){var x=f.now(),y=d.url.replace(bQ,"$1_="+x);d.url=y+(y===d.url?(bM.test(d.url)?"&":"?")+"_="+x:"")}}(d.data&&d.hasContent&&d.contentType!==!1||c.contentType)&&v.setRequestHeader("Content-Type",d.contentType),d.ifModified&&(k=k||d.url,f.lastModified[k]&&v.setRequestHeader("If-Modified-Since",f.lastModified[k]),f.etag[k]&&v.setRequestHeader("If-None-Match",f.etag[k])),v.setRequestHeader("Accept",d.dataTypes[0]&&d.accepts[d.dataTypes[0]]?d.accepts[d.dataTypes[0]]+(d.dataTypes[0]!=="*"?", "+bX+"; q=0.01":""):d.accepts["*"]);for(u in d.headers)v.setRequestHeader(u,d.headers[u]);if(d.beforeSend&&(d.beforeSend.call(e,v,d)===!1||s===2)){v.abort();return!1}for(u in{success:1,error:1,complete:1})v[u](d[u]);p=b$(bU,d,c,v);if(!p)w(-1,"No Transport");else{v.readyState=1,t&&g.trigger("ajaxSend",[v,d]),d.async&&d.timeout>0&&(q=setTimeout(function(){v.abort("timeout")},d.timeout));try{s=1,p.send(l,w)}catch(z){if(s<2)w(-1,z);else throw z}}return v},param:function(a,c){var d=[],e=function(a,b){b=f.isFunction(b)?b():b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};c===b&&(c=f.ajaxSettings.traditional);if(f.isArray(a)||a.jquery&&!f.isPlainObject(a))f.each(a,function(){e(this.name,this.value)});else for(var g in a)ca(g,a[g],c,e);return d.join("&").replace(bD,"+")}}),f.extend({active:0,lastModified:{},etag:{}});var cd=f.now(),ce=/(\=)\?(&|$)|\?\?/i;f.ajaxSetup({jsonp:"callback",jsonpCallback:function(){return f.expando+"_"+cd++}}),f.ajaxPrefilter("json jsonp",function(b,c,d){var e=b.contentType==="application/x-www-form-urlencoded"&&typeof b.data=="string";if(b.dataTypes[0]==="jsonp"||b.jsonp!==!1&&(ce.test(b.url)||e&&ce.test(b.data))){var g,h=b.jsonpCallback=f.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,i=a[h],j=b.url,k=b.data,l="$1"+h+"$2";b.jsonp!==!1&&(j=j.replace(ce,l),b.url===j&&(e&&(k=k.replace(ce,l)),b.data===k&&(j+=(/\?/.test(j)?"&":"?")+b.jsonp+"="+h))),b.url=j,b.data=k,a[h]=function(a){g=[a]},d.always(function(){a[h]=i,g&&f.isFunction(i)&&a[h](g[0])}),b.converters["script json"]=function(){g||f.error(h+" was not called");return g[0]},b.dataTypes[0]="json";return"script"}}),f.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/javascript|ecmascript/},converters:{"text script":function(a){f.globalEval(a);return a}}}),f.ajaxPrefilter("script",function(a){a.cache===b&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),f.ajaxTransport("script",function(a){if(a.crossDomain){var d,e=c.head||c.getElementsByTagName("head")[0]||c.documentElement;return{send:function(f,g){d=c.createElement("script"),d.async="async",a.scriptCharset&&(d.charset=a.scriptCharset),d.src=a.url,d.onload=d.onreadystatechange=function(a,c){if(c||!d.readyState||/loaded|complete/.test(d.readyState))d.onload=d.onreadystatechange=null,e&&d.parentNode&&e.removeChild(d),d=b,c||g(200,"success")},e.insertBefore(d,e.firstChild)},abort:function(){d&&d.onload(0,1)}}}});var cf=a.ActiveXObject?function(){for(var a in ch)ch[a](0,1)}:!1,cg=0,ch;f.ajaxSettings.xhr=a.ActiveXObject?function(){return!this.isLocal&&ci()||cj()}:ci,function(a){f.extend(f.support,{ajax:!!a,cors:!!a&&"withCredentials"in a})}(f.ajaxSettings.xhr()),f.support.ajax&&f.ajaxTransport(function(c){if(!c.crossDomain||f.support.cors){var d;return{send:function(e,g){var h=c.xhr(),i,j;c.username?h.open(c.type,c.url,c.async,c.username,c.password):h.open(c.type,c.url,c.async);if(c.xhrFields)for(j in c.xhrFields)h[j]=c.xhrFields[j];c.mimeType&&h.overrideMimeType&&h.overrideMimeType(c.mimeType),!c.crossDomain&&!e["X-Requested-With"]&&(e["X-Requested-With"]="XMLHttpRequest");try{for(j in e)h.setRequestHeader(j,e[j])}catch(k){}h.send(c.hasContent&&c.data||null),d=function(a,e){var j,k,l,m,n;try{if(d&&(e||h.readyState===4)){d=b,i&&(h.onreadystatechange=f.noop,cf&&delete ch[i]);if(e)h.readyState!==4&&h.abort();else{j=h.status,l=h.getAllResponseHeaders(),m={},n=h.responseXML,n&&n.documentElement&&(m.xml=n),m.text=h.responseText;try{k=h.statusText}catch(o){k=""}!j&&c.isLocal&&!c.crossDomain?j=m.text?200:404:j===1223&&(j=204)}}}catch(p){e||g(-1,p)}m&&g(j,k,m,l)},!c.async||h.readyState===4?d():(i=++cg,cf&&(ch||(ch={},f(a).unload(cf)),ch[i]=d),h.onreadystatechange=d)},abort:function(){d&&d(0,1)}}}});var ck={},cl,cm,cn=/^(?:toggle|show|hide)$/,co=/^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i,cp,cq=[["height","marginTop","marginBottom","paddingTop","paddingBottom"],["width","marginLeft","marginRight","paddingLeft","paddingRight"],["opacity"]],cr;f.fn.extend({show:function(a,b,c){var d,e;if(a||a===0)return this.animate(cu("show",3),a,b,c);for(var g=0,h=this.length;g<h;g++)d=this[g],d.style&&(e=d.style.display,!f._data(d,"olddisplay")&&e==="none"&&(e=d.style.display=""),e===""&&f.css(d,"display")==="none"&&f._data(d,"olddisplay",cv(d.nodeName)));for(g=0;g<h;g++){d=this[g];if(d.style){e=d.style.display;if(e===""||e==="none")d.style.display=f._data(d,"olddisplay")||""}}return this},hide:function(a,b,c){if(a||a===0)return this.animate(cu("hide",3),a,b,c);var d,e,g=0,h=this.length;for(;g<h;g++)d=this[g],d.style&&(e=f.css(d,"display"),e!=="none"&&!f._data(d,"olddisplay")&&f._data(d,"olddisplay",e));for(g=0;g<h;g++)this[g].style&&(this[g].style.display="none");return this},_toggle:f.fn.toggle,toggle:function(a,b,c){var d=typeof a=="boolean";f.isFunction(a)&&f.isFunction(b)?this._toggle.apply(this,arguments):a==null||d?this.each(function(){var b=d?a:f(this).is(":hidden");f(this)[b?"show":"hide"]()}):this.animate(cu("toggle",3),a,b,c);return this},fadeTo:function(a,b,c,d){return this.filter(":hidden").css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){function g(){e.queue===!1&&f._mark(this);var b=f.extend({},e),c=this.nodeType===1,d=c&&f(this).is(":hidden"),g,h,i,j,k,l,m,n,o;b.animatedProperties={};for(i in a){g=f.camelCase(i),i!==g&&(a[g]=a[i],delete a[i]),h=a[g],f.isArray(h)?(b.animatedProperties[g]=h[1],h=a[g]=h[0]):b.animatedProperties[g]=b.specialEasing&&b.specialEasing[g]||b.easing||"swing";if(h==="hide"&&d||h==="show"&&!d)return b.complete.call(this);c&&(g==="height"||g==="width")&&(b.overflow=[this.style.overflow,this.style.overflowX,this.style.overflowY],f.css(this,"display")==="inline"&&f.css(this,"float")==="none"&&(!f.support.inlineBlockNeedsLayout||cv(this.nodeName)==="inline"?this.style.display="inline-block":this.style.zoom=1))}b.overflow!=null&&(this.style.overflow="hidden");for(i in a)j=new f.fx(this,b,i),h=a[i],cn.test(h)?(o=f._data(this,"toggle"+i)||(h==="toggle"?d?"show":"hide":0),o?(f._data(this,"toggle"+i,o==="show"?"hide":"show"),j[o]()):j[h]()):(k=co.exec(h),l=j.cur(),k?(m=parseFloat(k[2]),n=k[3]||(f.cssNumber[i]?"":"px"),n!=="px"&&(f.style(this,i,(m||1)+n),l=(m||1)/j.cur()*l,f.style(this,i,l+n)),k[1]&&(m=(k[1]==="-="?-1:1)*m+l),j.custom(l,m,n)):j.custom(l,h,""));return!0}var e=f.speed(b,c,d);if(f.isEmptyObject(a))return this.each(e.complete,[!1]);a=f.extend({},a);return e.queue===!1?this.each(g):this.queue(e.queue,g)},stop:function(a,c,d){typeof a!="string"&&(d=c,c=a,a=b),c&&a!==!1&&this.queue(a||"fx",[]);return this.each(function(){function h(a,b,c){var e=b[c];f.removeData(a,c,!0),e.stop(d)}var b,c=!1,e=f.timers,g=f._data(this);d||f._unmark(!0,this);if(a==null)for(b in g)g[b]&&g[b].stop&&b.indexOf(".run")===b.length-4&&h(this,g,b);else g[b=a+".run"]&&g[b].stop&&h(this,g,b);for(b=e.length;b--;)e[b].elem===this&&(a==null||e[b].queue===a)&&(d?e[b](!0):e[b].saveState(),c=!0,e.splice(b,1));(!d||!c)&&f.dequeue(this,a)})}}),f.each({slideDown:cu("show",1),slideUp:cu("hide",1),slideToggle:cu("toggle",1),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){f.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),f.extend({speed:function(a,b,c){var d=a&&typeof a=="object"?f.extend({},a):{complete:c||!c&&b||f.isFunction(a)&&a,duration:a,easing:c&&b||b&&!f.isFunction(b)&&b};d.duration=f.fx.off?0:typeof d.duration=="number"?d.duration:d.duration in f.fx.speeds?f.fx.speeds[d.duration]:f.fx.speeds._default;if(d.queue==null||d.queue===!0)d.queue="fx";d.old=d.complete,d.complete=function(a){f.isFunction(d.old)&&d.old.call(this),d.queue?f.dequeue(this,d.queue):a!==!1&&f._unmark(this)};return d},easing:{linear:function(a,b,c,d){return c+d*a},swing:function(a,b,c,d){return(-Math.cos(a*Math.PI)/2+.5)*d+c}},timers:[],fx:function(a,b,c){this.options=b,this.elem=a,this.prop=c,b.orig=b.orig||{}}}),f.fx.prototype={update:function(){this.options.step&&this.options.step.call(this.elem,this.now,this),(f.fx.step[this.prop]||f.fx.step._default)(this)},cur:function(){if(this.elem[this.prop]!=null&&(!this.elem.style||this.elem.style[this.prop]==null))return this.elem[this.prop];var a,b=f.css(this.elem,this.prop);return isNaN(a=parseFloat(b))?!b||b==="auto"?0:b:a},custom:function(a,c,d){function h(a){return e.step(a)}var e=this,g=f.fx;this.startTime=cr||cs(),this.end=c,this.now=this.start=a,this.pos=this.state=0,this.unit=d||this.unit||(f.cssNumber[this.prop]?"":"px"),h.queue=this.options.queue,h.elem=this.elem,h.saveState=function(){e.options.hide&&f._data(e.elem,"fxshow"+e.prop)===b&&f._data(e.elem,"fxshow"+e.prop,e.start)},h()&&f.timers.push(h)&&!cp&&(cp=setInterval(g.tick,g.interval))},show:function(){var a=f._data(this.elem,"fxshow"+this.prop);this.options.orig[this.prop]=a||f.style(this.elem,this.prop),this.options.show=!0,a!==b?this.custom(this.cur(),a):this.custom(this.prop==="width"||this.prop==="height"?1:0,this.cur()),f(this.elem).show()},hide:function(){this.options.orig[this.prop]=f._data(this.elem,"fxshow"+this.prop)||f.style(this.elem,this.prop),this.options.hide=!0,this.custom(this.cur(),0)},step:function(a){var b,c,d,e=cr||cs(),g=!0,h=this.elem,i=this.options;if(a||e>=i.duration+this.startTime){this.now=this.end,this.pos=this.state=1,this.update(),i.animatedProperties[this.prop]=!0;for(b in i.animatedProperties)i.animatedProperties[b]!==!0&&(g=!1);if(g){i.overflow!=null&&!f.support.shrinkWrapBlocks&&f.each(["","X","Y"],function(a,b){h.style["overflow"+b]=i.overflow[a]}),i.hide&&f(h).hide();if(i.hide||i.show)for(b in i.animatedProperties)f.style(h,b,i.orig[b]),f.removeData(h,"fxshow"+b,!0),f.removeData(h,"toggle"+b,!0);d=i.complete,d&&(i.complete=!1,d.call(h))}return!1}i.duration==Infinity?this.now=e:(c=e-this.startTime,this.state=c/i.duration,this.pos=f.easing[i.animatedProperties[this.prop]](this.state,c,0,1,i.duration),this.now=this.start+(this.end-this.start)*this.pos),this.update();return!0}},f.extend(f.fx,{tick:function(){var a,b=f.timers,c=0;for(;c<b.length;c++)a=b[c],!a()&&b[c]===a&&b.splice(c--,1);b.length||f.fx.stop()},interval:13,stop:function(){clearInterval(cp),cp=null},speeds:{slow:600,fast:200,_default:400},step:{opacity:function(a){f.style(a.elem,"opacity",a.now)},_default:function(a){a.elem.style&&a.elem.style[a.prop]!=null?a.elem.style[a.prop]=a.now+a.unit:a.elem[a.prop]=a.now}}}),f.each(["width","height"],function(a,b){f.fx.step[b]=function(a){f.style(a.elem,b,Math.max(0,a.now)+a.unit)}}),f.expr&&f.expr.filters&&(f.expr.filters.animated=function(a){return f.grep(f.timers,function(b){return a===b.elem}).length});var cw=/^t(?:able|d|h)$/i,cx=/^(?:body|html)$/i;"getBoundingClientRect"in c.documentElement?f.fn.offset=function(a){var b=this[0],c;if(a)return this.each(function(b){f.offset.setOffset(this,a,b)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return f.offset.bodyOffset(b);try{c=b.getBoundingClientRect()}catch(d){}var e=b.ownerDocument,g=e.documentElement;if(!c||!f.contains(g,b))return c?{top:c.top,left:c.left}:{top:0,left:0};var h=e.body,i=cy(e),j=g.clientTop||h.clientTop||0,k=g.clientLeft||h.clientLeft||0,l=i.pageYOffset||f.support.boxModel&&g.scrollTop||h.scrollTop,m=i.pageXOffset||f.support.boxModel&&g.scrollLeft||h.scrollLeft,n=c.top+l-j,o=c.left+m-k;return{top:n,left:o}}:f.fn.offset=function(a){var b=this[0];if(a)return this.each(function(b){f.offset.setOffset(this,a,b)});if(!b||!b.ownerDocument)return null;if(b===b.ownerDocument.body)return f.offset.bodyOffset(b);var c,d=b.offsetParent,e=b,g=b.ownerDocument,h=g.documentElement,i=g.body,j=g.defaultView,k=j?j.getComputedStyle(b,null):b.currentStyle,l=b.offsetTop,m=b.offsetLeft;while((b=b.parentNode)&&b!==i&&b!==h){if(f.support.fixedPosition&&k.position==="fixed")break;c=j?j.getComputedStyle(b,null):b.currentStyle,l-=b.scrollTop,m-=b.scrollLeft,b===d&&(l+=b.offsetTop,m+=b.offsetLeft,f.support.doesNotAddBorder&&(!f.support.doesAddBorderForTableAndCells||!cw.test(b.nodeName))&&(l+=parseFloat(c.borderTopWidth)||0,m+=parseFloat(c.borderLeftWidth)||0),e=d,d=b.offsetParent),f.support.subtractsBorderForOverflowNotVisible&&c.overflow!=="visible"&&(l+=parseFloat(c.borderTopWidth)||0,m+=parseFloat(c.borderLeftWidth)||0),k=c}if(k.position==="relative"||k.position==="static")l+=i.offsetTop,m+=i.offsetLeft;f.support.fixedPosition&&k.position==="fixed"&&(l+=Math.max(h.scrollTop,i.scrollTop),m+=Math.max(h.scrollLeft,i.scrollLeft));return{top:l,left:m}},f.offset={bodyOffset:function(a){var b=a.offsetTop,c=a.offsetLeft;f.support.doesNotIncludeMarginInBodyOffset&&(b+=parseFloat(f.css(a,"marginTop"))||0,c+=parseFloat(f.css(a,"marginLeft"))||0);return{top:b,left:c}},setOffset:function(a,b,c){var d=f.css(a,"position");d==="static"&&(a.style.position="relative");var e=f(a),g=e.offset(),h=f.css(a,"top"),i=f.css(a,"left"),j=(d==="absolute"||d==="fixed")&&f.inArray("auto",[h,i])>-1,k={},l={},m,n;j?(l=e.position(),m=l.top,n=l.left):(m=parseFloat(h)||0,n=parseFloat(i)||0),f.isFunction(b)&&(b=b.call(a,c,g)),b.top!=null&&(k.top=b.top-g.top+m),b.left!=null&&(k.left=b.left-g.left+n),"using"in b?b.using.call(a,k):e.css(k)}},f.fn.extend({position:function(){if(!this[0])return null;var a=this[0],b=this.offsetParent(),c=this.offset(),d=cx.test(b[0].nodeName)?{top:0,left:0}:b.offset();c.top-=parseFloat(f.css(a,"marginTop"))||0,c.left-=parseFloat(f.css(a,"marginLeft"))||0,d.top+=parseFloat(f.css(b[0],"borderTopWidth"))||0,d.left+=parseFloat(f.css(b[0],"borderLeftWidth"))||0;return{top:c.top-d.top,left:c.left-d.left}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||c.body;while(a&&!cx.test(a.nodeName)&&f.css(a,"position")==="static")a=a.offsetParent;return a})}}),f.each(["Left","Top"],function(a,c){var d="scroll"+c;f.fn[d]=function(c){var e,g;if(c===b){e=this[0];if(!e)return null;g=cy(e);return g?"pageXOffset"in g?g[a?"pageYOffset":"pageXOffset"]:f.support.boxModel&&g.document.documentElement[d]||g.document.body[d]:e[d]}return this.each(function(){g=cy(this),g?g.scrollTo(a?f(g).scrollLeft():c,a?c:f(g).scrollTop()):this[d]=c})}}),f.each(["Height","Width"],function(a,c){var d=c.toLowerCase();f.fn["inner"+c]=function(){var a=this[0];return a?a.style?parseFloat(f.css(a,d,"padding")):this[d]():null},f.fn["outer"+c]=function(a){var b=this[0];return b?b.style?parseFloat(f.css(b,d,a?"margin":"border")):this[d]():null},f.fn[d]=function(a){var e=this[0];if(!e)return a==null?null:this;if(f.isFunction(a))return this.each(function(b){var c=f(this);c[d](a.call(this,b,c[d]()))});if(f.isWindow(e)){var g=e.document.documentElement["client"+c],h=e.document.body;return e.document.compatMode==="CSS1Compat"&&g||h&&h["client"+c]||g}if(e.nodeType===9)return Math.max(e.documentElement["client"+c],e.body["scroll"+c],e.documentElement["scroll"+c],e.body["offset"+c],e.documentElement["offset"+c]);if(a===b){var i=f.css(e,d),j=parseFloat(i);return f.isNumeric(j)?j:i}return this.css(d,typeof a=="string"?a:a+"px")}}),a.jQuery=a.$=f,typeof define=="function"&&define.amd&&define.amd.jQuery&&define("jquery",[],function(){return f})})(window);/*
 * JavaScript Logger - Lightweight Wrapper for Console Logging
 * Paul Irish
 * paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
 *
 */
window.log=function(){log.history=log.history||[];log.history.push(arguments);if(this.console && window.location.search.indexOf("showlog") > -1){console.log(Array.prototype.slice.call(arguments))}};
window.debug=function(){debug.history=log.history||[];debug.history.push(arguments);if(this.console && window.location.search.indexOf("showdebug") > -1){console.log(Array.prototype.slice.call(arguments))}};
/*
 * jQueryMX from JavaScriptMVC v3.0.5
 * jquery.class, jquery.controller, jquery destroyed event and jquery string helpers
 */
(function(g){var j=false,n=/xyz/.test(function(){})?/\b_super\b/:/.*/,m=function(a,c,d){d=d||a;for(var b in a)d[b]=typeof a[b]=="function"&&typeof c[b]=="function"&&n.test(a[b])?function(h,i){return function(){var e=this._super,f;this._super=c[h];f=i.apply(this,arguments);this._super=e;return f}}(b,a[b]):a[b]};jQuery.Class=function(){arguments.length&&jQuery.Class.extend.apply(jQuery.Class,arguments)};g.extend(g.Class,{callback:function(a){var c=jQuery.makeArray(arguments),d;a=c.shift();jQuery.isArray(a)||
(a=[a]);d=this;return function(){for(var b=c.concat(jQuery.makeArray(arguments)),h,i=a.length,e=0,f;e<i;e++)if(f=a[e]){if((h=typeof f=="string")&&d._set_called)d.called=f;b=(h?d[f]:f).apply(d,b||[]);if(e<i-1)b=!jQuery.isArray(b)||b._use_call?[b]:b}return b}},getObject:function(a,c){c=c||window;a=a?a.split(/\./):[];for(var d=0;d<a.length;d++)c=c[a[d]]||(c[a[d]]={});return c},newInstance:function(){var a=this.rawInstance(),c;if(a.setup)c=a.setup.apply(a,arguments);if(a.init)a.init.apply(a,g.isArray(c)?
	c:arguments);return a},setup:function(a){this.defaults=g.extend(true,{},a.defaults,this.defaults);return arguments},rawInstance:function(){j=true;var a=new this;j=false;return a},extend:function(a,c,d){function b(){if(!j)return this.constructor!==b&&arguments.length?arguments.callee.extend.apply(arguments.callee,arguments):this.Class.newInstance.apply(this.Class,arguments)}if(typeof a!="string"){d=c;c=a;a=null}if(!d){d=c;c=null}d=d||{};var h=this,i=this.prototype,e,f,k,l;j=true;l=new this;j=false;
	m(d,i,l);for(e in this)if(this.hasOwnProperty(e)&&g.inArray(e,["prototype","defaults","getObject"])==-1)b[e]=this[e];m(c,this,b);if(a){k=a.split(/\./);f=k.pop();k=i=g.Class.getObject(k.join("."));i[f]=b}g.extend(b,{prototype:l,namespace:k,shortName:f,constructor:b,fullName:a});b.prototype.Class=b.prototype.constructor=b;h=b.setup.apply(b,[h].concat(g.makeArray(arguments)));if(b.init)b.init.apply(b,h||[]);return b}});jQuery.Class.prototype.callback=jQuery.Class.callback})(jQuery);
(function(h){var c={undHash:/_|-/,colons:/::/,words:/([A-Z]+)([A-Z][a-z])/g,lowerUpper:/([a-z\d])([A-Z])/g,dash:/([a-z\d])([A-Z])/g,replacer:/\{([^\}]+)\}/g},i=function(a,b,e){b=b||window;a=a?a.split(/\./):[];for(var f,d=0;d<a.length-1&&b;d++)b=b[a[d]];f=b[a[d]];e&&delete b[a[d]];return f},g=h.String={strip:function(a){return a.replace(/^\s+/,"").replace(/\s+$/,"")},capitalize:function(a){return a.charAt(0).toUpperCase()+a.substr(1)},endsWith:function(a,b){var e=a.length-b.length;return e>=0&&a.lastIndexOf(b)===
	e},camelize:function(a){a=a.split(c.undHash);var b=1;for(a[0]=a[0].charAt(0).toLowerCase()+a[0].substr(1);b<a.length;b++)a[b]=g.capitalize(a[b]);return a.join("")},classize:function(a){a=a.split(c.undHash);for(var b=0;b<a.length;b++)a[b]=g.capitalize(a[b]);return a.join("")},niceName:function(a){a=a.split(c.undHash);for(var b=0;b<a.length;b++)a[b]=g.capitalize(a[b]);return a.join(" ")},underscore:function(a){return a.replace(c.colons,"/").replace(c.words,"$1_$2").replace(c.lowerUpper,"$1_$2").replace(c.dash,
	"_").toLowerCase()},sub:function(a,b,e){return a.replace(c.replacer,function(f,d){return i(d,b,e).toString()})}}})(jQuery);

(function(a){var e=jQuery.cleanData;a.cleanData=function(b){for(var c=0,d;(d=b[c])!==undefined;c++)a(d).triggerHandler("destroyed");e(b)}})(jQuery);
(function(d){var n=function(a,b,c){var e;if(b.indexOf(">")===0){b=b.substr(1);e=function(f){if(f.target===a)c.apply(this,arguments);else f.handled=null}}d(a).bind(b,e||c);return function(){d(a).unbind(b,e||c);a=b=c=e=null}},o=function(a,b,c,e){d(a).delegate(b,c,e);return function(){d(a).undelegate(b,c,e);a=c=e=b=null}},i=function(a,b,c,e){return e?o(a,e,b,c):n(a,b,c)},h=function(a){return function(){return a.apply(null,[d(this)].concat(Array.prototype.slice.call(arguments,0)))}},p=/\./g,q=/_?controllers?/ig,
	l=function(a){return d.String.underscore(a.replace("jQuery.","").replace(p,"_").replace(q,""))},r=/[^\w]/,s=/^(>?default\.)|(>)/,m=/\{([^\}]+)\}/g,t=/^(?:(.*?)\s)?([\w\.\:>]+)$/,j;d.Class.extend("jQuery.Controller",{init:function(){if(!(!this.shortName||this.fullName=="jQuery.Controller")){this._fullName=l(this.fullName);this._shortName=l(this.shortName);var a=this,b=this.pluginName||this._fullName,c;d.fn[b]||(d.fn[b]=function(e){var f=d.makeArray(arguments),u=typeof e=="string"&&d.isFunction(a.prototype[e]),
	v=f[0];this.each(function(){var g=d.data(this,"controllers");if(g=g&&g[b])u?g[v].apply(g,f.slice(1)):g.update.apply(g,f);else a.newInstance.apply(a,[this].concat(f))});return this});this.actions={};for(c in this.prototype)if(d.isFunction(this.prototype[c]))if(this._isAction(c))this.actions[c]=this._getAction(c);this.onDocument&&new a(document.documentElement)}},hookup:function(a){return new this(a)},_isAction:function(a){if(r.test(a))return true;else{a=a.replace(s,"");return d.inArray(a,this.listensTo)>
	-1||d.event.special[a]||d.Controller.processors[a]}},_getAction:function(a,b){m.lastIndex=0;if(!b&&m.test(a))return null;a=(b?d.String.sub(a,b):a).match(t);return{processor:this.processors[a[2]]||j,parts:a}},processors:{},listensTo:[],defaults:{}},{setup:function(a,b){var c,e=this.Class;a=a.jquery?a[0]:a;this.element=d(a).addClass(e._fullName);(d.data(a,"controllers")||d.data(a,"controllers",{}))[e._fullName]=this;this._bindings=[];this.options=d.extend(d.extend(true,{},e.defaults),b);for(c in e.actions){b=
	e.actions[c]||e._getAction(c,this.options);this._bindings.push(b.processor(a,b.parts[2],b.parts[1],this.callback(c),this))}this.called="init";var f=h(this.callback("destroy"));this.element.bind("destroyed",f);this._bindings.push(function(){f.removed=true;d(a).unbind("destroyed",f)});return this.element},bind:function(a,b,c){if(typeof a=="string"){c=b;b=a;a=this.element}return this._binder(a,b,c)},_binder:function(a,b,c,e){if(typeof c=="string")c=h(this.callback(c));this._bindings.push(i(a,b,c,e));
	return this._bindings.length},delegate:function(a,b,c,e){if(typeof a=="string"){e=c;c=b;b=a;a=this.element}return this._binder(a,c,e,b)},update:function(a){d.extend(this.options,a)},destroy:function(){if(this._destroyed)throw this.Class.shortName+" controller instance has been deleted";var a=this,b=this.Class._fullName;this._destroyed=true;this.element.removeClass(b);d.each(this._bindings,function(e,f){d.isFunction(f)&&f(a.element[0])});delete this._actions;var c=this.element.data("controllers");
	c&&c[b]&&delete c[b];d(this).triggerHandler("destroyed");this.element=null},find:function(a){return this.element.find(a)},_set_called:true});j=function(a,b,c,e,f){f=f.Class;if(f.onDocument&&!/^Main(Controller)?$/.test(f.shortName))c=c?"#"+f._shortName+" "+c:"#"+f._shortName;return i(a,b,h(e),c)};var k=d.Controller.processors,w=function(a,b,c,e){return i(window,b.replace(/window/,""),h(e))};d.each("change click contextmenu dblclick keydown keyup keypress mousedown mousemove mouseout mouseover mouseup reset windowresize resize windowscroll scroll select submit dblclick focusin focusout load unload ready hashchange mouseenter mouseleave".split(" "),
	function(a,b){k[b]=j});d.each(["windowresize","windowscroll","load","ready","unload","hashchange"],function(a,b){k[b]=w});k.ready=function(a,b,c,e){d(h(e))};d.fn.mixin=function(){var a=d.makeArray(arguments);return this.each(function(){for(var b=0;b<a.length;b++)new a[b](this)})};var x=function(a,b){for(var c=0;c<b.length;c++)if(typeof b[c]=="string"?a.Class._shortName==b[c]:a instanceof b[c])return true;return false};d.fn.controllers=function(){var a=d.makeArray(arguments),b=[],c;this.each(function(){if(c=
	d.data(this,"controllers"))for(var e in c){var f=c[e];if(!a.length||x(f,a))b.push(f)}});return b};d.fn.controller=function(){return this.controllers.apply(this,arguments)[0]}})(jQuery);

/*
 * JSizes - JQuery plugin v0.33
 *
 * Licensed under the revised BSD License.
 * Copyright 2008-2010 Bram Stein
 * All rights reserved.
 */
(function(b){var a=function(c){return parseInt(c,10)||0};b.each(["min","max"],function(d,c){b.fn[c+"Size"]=function(g){var f,e;if(g){if(g.width!==undefined){this.css(c+"-width",g.width)}if(g.height!==undefined){this.css(c+"-height",g.height)}return this}else{f=this.css(c+"-width");e=this.css(c+"-height");return{width:(c==="max"&&(f===undefined||f==="none"||a(f)===-1)&&Number.MAX_VALUE)||a(f),height:(c==="max"&&(e===undefined||e==="none"||a(e)===-1)&&Number.MAX_VALUE)||a(e)}}}});b.fn.isVisible=function(){return this.is(":visible")};b.each(["border","margin","padding"],function(d,c){b.fn[c]=function(e){if(e){if(e.top!==undefined){this.css(c+"-top"+(c==="border"?"-width":""),e.top)}if(e.bottom!==undefined){this.css(c+"-bottom"+(c==="border"?"-width":""),e.bottom)}if(e.left!==undefined){this.css(c+"-left"+(c==="border"?"-width":""),e.left)}if(e.right!==undefined){this.css(c+"-right"+(c==="border"?"-width":""),e.right)}return this}else{return{top:a(this.css(c+"-top"+(c==="border"?"-width":""))),bottom:a(this.css(c+"-bottom"+(c==="border"?"-width":""))),left:a(this.css(c+"-left"+(c==="border"?"-width":""))),right:a(this.css(c+"-right"+(c==="border"?"-width":"")))}}}})})(jQuery);
/**
 * hoverIntent r5 // 2007.03.27 // jQuery 1.1.2+
 * <http://cherne.net/brian/resources/jquery.hoverIntent.html>
 *
 * @param  f  onMouseOver function || An object with configuration options
 * @param  g  onMouseOut function  || Nothing (use configuration options object)
 * @author    Brian Cherne <brian@cherne.net>
 (function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);
 */

/*
 * jqModal - Minimalist Modaling with jQuery
 *   (http://dev.iceburg.net/jquery/jqModal/)
 *
 * Copyright (c) 2007,2008 Brice Burgess <bhb@iceburg.net>
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 * 
 * $Version: 03/01/2009 +r14
 */
(function($) {
	$.fn.jqm=function(o){
		var p={
			overlay: 25,
			overlayClass: 'jqmOverlay',
			closeClass: 'jqmClose',
			trigger: '.jqModal',
			ajax: F,
			ajaxText: '',
			target: F,
			modal: F,
			toTop: F,
			intWidth: '',
			onShow: F,
			onHide: F,
			onLoad: F,
			buttonHide: '',
			closescript: '',
			openscript: '',
			openscript2: ''
		};
		return this.each(function(){if(this._jqm)return H[this._jqm].c=$.extend({},H[this._jqm].c,o);s++;this._jqm=s;
			H[s]={c:$.extend(p,$.jqm.params,o),a:F,w:$(this).addClass('jqmID'+s),s:s};
			if(p.trigger)$(this).jqmAddTrigger(p.trigger);
		});};
	function scrollLock(e) {
		var a=e.data.pos;
		window.scrollTo(a[0],a[1]);
		return false;
	}
	$.fn.jqmAddClose=function(e){return hs(this,e,'jqmHide');};
	$.fn.jqmAddTrigger=function(e){return hs(this,e,'jqmShow');};
	$.fn.jqmShow=function(t){return this.each(function(){t=t||window.event;$.jqm.open(this._jqm,t);});};
	$.fn.jqmHide=function(t){return this.each(function(){t=t||window.event;$.jqm.close(this._jqm,t);});};

	$.jqm = {
		hash:{},
		open:function(s,t){

			var h=H[s],c=h.c,cc='.'+c.closeClass,z=(parseInt(h.w.css('z-index'))),z=(z>0)?z:3000,o=$('<div></div>').css({height:'100%',width:'100%',position:'fixed',left:0,top:0,'z-index':z-1,opacity:c.overlay/100});if(h.a)return F;h.t=t;h.a=true;h.w.css('z-index',z);
			if(c.openscript !== '') { eval(c.openscript); }
			/*if(c.modal) {if(!A[0])L('bind');A.push(s);}
			 else if(c.overlay > 0)h.w.jqmAddClose(o);
			 else*/
			o.addClass(c.overlayClass);
			h.w.jqmAddClose(o);
			//o=F;

			// Grey out the buttons
			if (c.buttonHide!=='') { var buttonList = new Array(); buttonList = c.buttonHide.split(','); for (i=0; i<buttonList.length; i++) { document.getElementById(buttonList[i].toString()).src = "/images/en/us/btn/" + buttonList[i].toString() + "_grey.png"; } }

			h.o=(o)?o.addClass(c.overlayClass).prependTo('body'):F;
			if(Sym.Global.IEVersion == 6){$('html,body').css({height:'100%',width:'100%'});if(o){o=o.css({position:'absolute'})[0];for(var y in {Top:1,Left:1})o.style.setExpression(y.toLowerCase(),"(_=(document.documentElement.scroll"+y+" || document.body.scroll"+y+"))+'px'");}}

			/* added for interstitial IE 7 issue on clp d64v1_3p3s_nav2_rol */
			if ( $.browser.msie ) {
				IEVersion= parseInt($.browser.version, 10);
			}

			if(Sym.Global.IEVersion == 6 || Sym.Global.IEVersion == 7) {
				$("#d64v1_3p3s_nav2_rol .jqmOverlay").css('z-index', "0");
				$("#d64v1_3p3s_nav2_rol #showCommerceArea").css('z-index','10');
			}

			if(c.ajax) {var r=c.target||h.w,u=c.ajax,r=(typeof r == 'string')?$(r,h.w):$(r),u=(u.substr(0,1) == '@')?$(t).attr(u.substring(1)):u;
				r.html(c.ajaxText).load(u,function(){if(c.onLoad)c.onLoad.call(this,h);if(cc)h.w.jqmAddClose($(cc,h.w));e(h);});}
			else if(cc)h.w.jqmAddClose($(cc,h.w));
			if(Sym.Global.IEVersion == 6) { var ddList = new Array(); ddList = document.getElementsByTagName("select"); for (i=0;i<ddList.length;i++) { ddList[i].style.visibility='hidden'; } }
			if(c.toTop&&h.o)h.w.before('<span id="jqmP'+h.w[0]._jqm+'"></span>').insertAfter(h.o);
			if(c.openscript2 !== ''){eval(c.openscript2);}
			(c.onShow)?c.onShow(h):h.w.show();e(h);
			//if(!c.ajax){alert(h.w.css('width'));var popWidth=0;popWidth+=(parseInt(h.w.css('width'))/2*(-1));h.w.css('margin-left',popWidth);} else
			if(c.intWidth !== ''){h.w.css('margin-left',parseInt(c.intWidth)/2*(-1));}
			return F;
		},
		close:function(s){
			var h=H[s];if(!h.a)return F;h.a=F;
			$('.error', h.w).removeClass('error');
			// Color in the buttons
			if (h.c.buttonHide!=='') { var buttonList = new Array(); buttonList = h.c.buttonHide.split(','); for (i=0; i<buttonList.length; i++) { document.getElementById(buttonList[i].toString()).src = "/images/en/us/btn/" + buttonList[i].toString() + ".png"; } }
			if(A[0]){A.pop();if(!A[0])L('unbind');}
			if(h.c.toTop&&h.o)$('#jqmP'+h.w[0]._jqm).after(h.w).remove();
			if(h.c.onHide)h.c.onHide(h);else{h.w.hide();if(h.o)h.o.remove();}
			if(Sym.Global.IEVersion == 6) { var ddList = new Array(); ddList = document.getElementsByTagName("select"); for (i=0;i<ddList.length;i++) { ddList[i].style.visibility='visible'; } }
			if(h.c.closescript != '') { eval(h.c.closescript); }
			return F;
		},
		params:{}};
	var s=0,H=$.jqm.hash,A=[],F=false,
		i=$('<iframe src="javascript:false;document.write(\'\');" class="jqm"></iframe>').css({opacity:0}),
		e=function(h){if(Sym.Global.IEVersion == 6)if(h.o)h.o.html('<p style="width:100%;height:100%"/>').prepend(i);else if(!$('iframe.jqm',h.w)[0])h.w.prepend(i); f(h);},
		f=function(h){try{}catch(_){}},
		L=function(t){$()[t]("keypress",m)[t]("keydown",m)[t]("mousedown",m);},
		m=function(e){var h=H[A[A.length-1]],r=(!$(e.target).parents('.jqmID'+h.s)[0]);if(r)f(h);return !r;},
		hs=function(w,t,c){
			return w.each(function(){
				var s=this._jqm;
				$(t).each(function() {
					if (!$(this).hasClass('jqmOverlay')) {
						if (!this[c]) {
							this[c] = [];
							$(this).click(function(){
								for (var i in {
									jqmShow: 1,
									jqmHide: 1
								})
									for (var s in this[i])
										if (H[this[i][s]])
											H[this[i][s]].w[i](this);
								return F;
							});
						}
						this[c].push(s);
					}
				});
			});
		};
})(jQuery);

/*
 * jqDnR - Minimalistic Drag'n'Resize for jQuery.
 *
 * Copyright (c) 2007 Brice Burgess <bhb@iceburg.net>, http://www.iceburg.net
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *  
 * $Version: 2007.08.19 +r2
 */
(function($){
	$.fn.jqDrag=function(h){return i(this,h,'d');};
	$.fn.jqResize=function(h){return i(this,h,'r');};
	$.jqDnR={dnr:{},e:0,
		drag:function(v){
			if(M.k == 'd')E.css({left:M.X+v.pageX-M.pX,top:M.Y+v.pageY-M.pY});
			else E.css({width:Math.max(v.pageX-M.pX+M.W,0),height:Math.max(v.pageY-M.pY+M.H,0)});
			return false;},
		stop:function(){$().unbind('mousemove',J.drag).unbind('mouseup',J.stop);}
	};
	var J=$.jqDnR,M=J.dnr,E=J.e,
		i=function(e,h,k){return e.each(function(){h=(h)?$(h,e):e;
			h.bind('mousedown',{e:e,k:k},function(v){var d=v.data,p={};E=d.e;
				// attempt utilization of dimensions plugin to fix IE issues
				if(E.css('position') != 'relative'){try{E.position(p);}catch(e){}}
				M={X:p.left||f('left')||0,Y:p.top||f('top')||0,W:f('width')||E[0].scrollWidth||0,H:f('height')||E[0].scrollHeight||0,pX:v.pageX,pY:v.pageY,k:d.k,o:E.css('opacity')};
				$().mousemove($.jqDnR.drag).mouseup($.jqDnR.stop);
				return false;
			});
		});},
		f=function(k){return parseInt(E.css(k))||false;};
})(jQuery);

/*
 * jQuery pub/sub plugin by Peter Higgins (dante@dojotoolkit.org)
 * Loosely based on Dojo publish/subscribe API, limited in scope. Rewritten blindly.
 * Original is (c) Dojo Foundation 2004-2010. Released under either AFL or new BSD, see:
 * http://dojofoundation.org/license for more information.
 */
(function(d){

	// the topic/subscription hash
	var cache = {};

	d.publish = function(/* String */topic, /* Array? */args){
		// summary: 
		//		Publish some data on a named topic.
		// topic: String
		//		The channel to publish on
		// args: Array?
		//		The data to publish. Each array item is converted into an ordered
		//		arguments on the subscribed functions. 
		//
		// example:
		//		Publish stuff on '/some/topic'. Anything subscribed will be called
		//		with a function signature like: function(a,b,c){ ... }
		//
		//	|		$.publish("/some/topic", ["a","b","c"]);
		cache[topic] && d.each(cache[topic], function(){
			this.apply(d, args || []);
		});
	};

	d.subscribe = function(/* String */topic, /* Function */callback){
		// summary:
		//		Register a callback on a named topic.
		// topic: String
		//		The channel to subscribe to
		// callback: Function
		//		The handler event. Anytime something is $.publish'ed on a 
		//		subscribed channel, the callback will be called with the
		//		published array as ordered arguments.
		//
		// returns: Array
		//		A handle which can be used to unsubscribe this particular subscription.
		//	
		// example:
		//	|	$.subscribe("/some/topic", function(a, b, c){ /* handle data */ });
		//
		if(!cache[topic]){
			cache[topic] = [];
		}
		cache[topic].push(callback);
		return [topic, callback]; // Array
	};

	d.unsubscribe = function(/* Array */handle){
		// summary:
		//		Disconnect a subscribed function for a topic.
		// handle: Array
		//		The return value from a $.subscribe call.
		// example:
		//	|	var handle = $.subscribe("/something", function(){});
		//	|	$.unsubscribe(handle);

		var t = handle[0];
		cache[t] && d.each(cache[t], function(idx){
			if(this == handle[1]){
				cache[t].splice(idx, 1);
			}
		});
	};
})(jQuery);

/*
 * jScrollPane - v2.0.0beta11 - 2011-07-04
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2010 Kelvin Luck
 * Dual licensed under the MIT and GPL licenses.
 */
(function(b,a,c){b.fn.jScrollPane=function(e){function d(D,O){var az,Q=this,Y,ak,v,am,T,Z,y,q,aA,aF,av,i,I,h,j,aa,U,aq,X,t,A,ar,af,an,G,l,au,ay,x,aw,aI,f,L,aj=true,P=true,aH=false,k=false,ap=D.clone(false,false).empty(),ac=b.fn.mwheelIntent?"mwheelIntent.jsp":"mousewheel.jsp";aI=D.css("paddingTop")+" "+D.css("paddingRight")+" "+D.css("paddingBottom")+" "+D.css("paddingLeft");f=(parseInt(D.css("paddingLeft"),10)||0)+(parseInt(D.css("paddingRight"),10)||0);function at(aR){var aM,aO,aN,aK,aJ,aQ,aP=false,aL=false;az=aR;if(Y===c){aJ=D.scrollTop();aQ=D.scrollLeft();D.css({overflow:"hidden",padding:0});ak=D.innerWidth()+f;v=D.innerHeight();D.width(ak);Y=b('<div class="jspPane" />').css("padding",aI).append(D.children());am=b('<div class="jspContainer" />').css({width:ak+"px",height:v+"px"}).append(Y).appendTo(D)}else{D.css("width","");aP=az.stickToBottom&&K();aL=az.stickToRight&&B();aK=D.innerWidth()+f!=ak||D.outerHeight()!=v;if(aK){ak=D.innerWidth()+f;v=D.innerHeight();am.css({width:ak+"px",height:v+"px"})}if(!aK&&L==T&&Y.outerHeight()==Z){D.width(ak);return}L=T;Y.css("width","");D.width(ak);am.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()}Y.css("overflow","auto");if(aR.contentWidth){T=aR.contentWidth}else{T=Y[0].scrollWidth}Z=Y[0].scrollHeight;Y.css("overflow","");y=T/ak;q=Z/v;aA=q>1;aF=y>1;if(!(aF||aA)){D.removeClass("jspScrollable");Y.css({top:0,width:am.width()-f});n();E();R();w();ai()}else{D.addClass("jspScrollable");aM=az.maintainPosition&&(I||aa);if(aM){aO=aD();aN=aB()}aG();z();F();if(aM){N(aL?(T-ak):aO,false);M(aP?(Z-v):aN,false)}J();ag();ao();if(az.enableKeyboardNavigation){S()}if(az.clickOnTrack){p()}C();if(az.hijackInternalLinks){m()}}if(az.autoReinitialise&&!aw){aw=setInterval(function(){at(az)},az.autoReinitialiseDelay)}else{if(!az.autoReinitialise&&aw){clearInterval(aw)}}aJ&&D.scrollTop(0)&&M(aJ,false);aQ&&D.scrollLeft(0)&&N(aQ,false);D.trigger("jsp-initialised",[aF||aA])}function aG(){if(aA){am.append(b('<div class="jspVerticalBar" />').append(b('<div class="jspCap jspCapTop" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragTop" />'),b('<div class="jspDragBottom" />'))),b('<div class="jspCap jspCapBottom" />')));U=am.find(">.jspVerticalBar");aq=U.find(">.jspTrack");av=aq.find(">.jspDrag");if(az.showArrows){ar=b('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp",aE(0,-1)).bind("click.jsp",aC);af=b('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp",aE(0,1)).bind("click.jsp",aC);if(az.arrowScrollOnHover){ar.bind("mouseover.jsp",aE(0,-1,ar));af.bind("mouseover.jsp",aE(0,1,af))}al(aq,az.verticalArrowPositions,ar,af)}t=v;am.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function(){t-=b(this).outerHeight()});av.hover(function(){av.addClass("jspHover")},function(){av.removeClass("jspHover")}).bind("mousedown.jsp",function(aJ){b("html").bind("dragstart.jsp selectstart.jsp",aC);av.addClass("jspActive");var s=aJ.pageY-av.position().top;b("html").bind("mousemove.jsp",function(aK){V(aK.pageY-s,false)}).bind("mouseup.jsp mouseleave.jsp",ax);return false});o()}}function o(){aq.height(t+"px");I=0;X=az.verticalGutter+aq.outerWidth();Y.width(ak-X-f);try{if(U.position().left===0){Y.css("margin-left",X+"px")}}catch(s){}}function z(){if(aF){am.append(b('<div class="jspHorizontalBar" />').append(b('<div class="jspCap jspCapLeft" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragLeft" />'),b('<div class="jspDragRight" />'))),b('<div class="jspCap jspCapRight" />')));an=am.find(">.jspHorizontalBar");G=an.find(">.jspTrack");h=G.find(">.jspDrag");if(az.showArrows){ay=b('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp",aE(-1,0)).bind("click.jsp",aC);x=b('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp",aE(1,0)).bind("click.jsp",aC);
	if(az.arrowScrollOnHover){ay.bind("mouseover.jsp",aE(-1,0,ay));x.bind("mouseover.jsp",aE(1,0,x))}al(G,az.horizontalArrowPositions,ay,x)}h.hover(function(){h.addClass("jspHover")},function(){h.removeClass("jspHover")}).bind("mousedown.jsp",function(aJ){b("html").bind("dragstart.jsp selectstart.jsp",aC);h.addClass("jspActive");var s=aJ.pageX-h.position().left;b("html").bind("mousemove.jsp",function(aK){W(aK.pageX-s,false)}).bind("mouseup.jsp mouseleave.jsp",ax);return false});l=am.innerWidth();ah()}}function ah(){am.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function(){l-=b(this).outerWidth()});G.width(l+"px");aa=0}function F(){if(aF&&aA){var aJ=G.outerHeight(),s=aq.outerWidth();t-=aJ;b(an).find(">.jspCap:visible,>.jspArrow").each(function(){l+=b(this).outerWidth()});l-=s;v-=s;ak-=aJ;G.parent().append(b('<div class="jspCorner" />').css("width",aJ+"px"));o();ah()}if(aF){Y.width((am.outerWidth()-f)+"px")}Z=Y.outerHeight();q=Z/v;if(aF){au=Math.ceil(1/y*l);if(au>az.horizontalDragMaxWidth){au=az.horizontalDragMaxWidth}else{if(au<az.horizontalDragMinWidth){au=az.horizontalDragMinWidth}}h.width(au+"px");j=l-au;ae(aa)}if(aA){A=Math.ceil(1/q*t);if(A>az.verticalDragMaxHeight){A=az.verticalDragMaxHeight}else{if(A<az.verticalDragMinHeight){A=az.verticalDragMinHeight}}av.height(A+"px");i=t-A;ad(I)}}function al(aK,aM,aJ,s){var aO="before",aL="after",aN;if(aM=="os"){aM=/Mac/.test(navigator.platform)?"after":"split"}if(aM==aO){aL=aM}else{if(aM==aL){aO=aM;aN=aJ;aJ=s;s=aN}}aK[aO](aJ)[aL](s)}function aE(aJ,s,aK){return function(){H(aJ,s,this,aK);this.blur();return false}}function H(aM,aL,aP,aO){aP=b(aP).addClass("jspActive");var aN,aK,aJ=true,s=function(){if(aM!==0){Q.scrollByX(aM*az.arrowButtonSpeed)}if(aL!==0){Q.scrollByY(aL*az.arrowButtonSpeed)}aK=setTimeout(s,aJ?az.initialDelay:az.arrowRepeatFreq);aJ=false};s();aN=aO?"mouseout.jsp":"mouseup.jsp";aO=aO||b("html");aO.bind(aN,function(){aP.removeClass("jspActive");aK&&clearTimeout(aK);aK=null;aO.unbind(aN)})}function p(){w();if(aA){aq.bind("mousedown.jsp",function(aO){if(aO.originalTarget===c||aO.originalTarget==aO.currentTarget){var aM=b(this),aP=aM.offset(),aN=aO.pageY-aP.top-I,aK,aJ=true,s=function(){var aS=aM.offset(),aT=aO.pageY-aS.top-A/2,aQ=v*az.scrollPagePercent,aR=i*aQ/(Z-v);if(aN<0){if(I-aR>aT){Q.scrollByY(-aQ)}else{V(aT)}}else{if(aN>0){if(I+aR<aT){Q.scrollByY(aQ)}else{V(aT)}}else{aL();return}}aK=setTimeout(s,aJ?az.initialDelay:az.trackClickRepeatFreq);aJ=false},aL=function(){aK&&clearTimeout(aK);aK=null;b(document).unbind("mouseup.jsp",aL)};s();b(document).bind("mouseup.jsp",aL);return false}})}if(aF){G.bind("mousedown.jsp",function(aO){if(aO.originalTarget===c||aO.originalTarget==aO.currentTarget){var aM=b(this),aP=aM.offset(),aN=aO.pageX-aP.left-aa,aK,aJ=true,s=function(){var aS=aM.offset(),aT=aO.pageX-aS.left-au/2,aQ=ak*az.scrollPagePercent,aR=j*aQ/(T-ak);if(aN<0){if(aa-aR>aT){Q.scrollByX(-aQ)}else{W(aT)}}else{if(aN>0){if(aa+aR<aT){Q.scrollByX(aQ)}else{W(aT)}}else{aL();return}}aK=setTimeout(s,aJ?az.initialDelay:az.trackClickRepeatFreq);aJ=false},aL=function(){aK&&clearTimeout(aK);aK=null;b(document).unbind("mouseup.jsp",aL)};s();b(document).bind("mouseup.jsp",aL);return false}})}}function w(){if(G){G.unbind("mousedown.jsp")}if(aq){aq.unbind("mousedown.jsp")}}function ax(){b("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp");if(av){av.removeClass("jspActive")}if(h){h.removeClass("jspActive")}}function V(s,aJ){if(!aA){return}if(s<0){s=0}else{if(s>i){s=i}}if(aJ===c){aJ=az.animateScroll}if(aJ){Q.animate(av,"top",s,ad)}else{av.css("top",s);ad(s)}}function ad(aJ){if(aJ===c){aJ=av.position().top}am.scrollTop(0);I=aJ;var aM=I===0,aK=I==i,aL=aJ/i,s=-aL*(Z-v);if(aj!=aM||aH!=aK){aj=aM;aH=aK;D.trigger("jsp-arrow-change",[aj,aH,P,k])}u(aM,aK);Y.css("top",s);D.trigger("jsp-scroll-y",[-s,aM,aK]).trigger("scroll")}function W(aJ,s){if(!aF){return}if(aJ<0){aJ=0}else{if(aJ>j){aJ=j}}if(s===c){s=az.animateScroll}if(s){Q.animate(h,"left",aJ,ae)
}else{h.css("left",aJ);ae(aJ)}}function ae(aJ){if(aJ===c){aJ=h.position().left}am.scrollTop(0);aa=aJ;var aM=aa===0,aL=aa==j,aK=aJ/j,s=-aK*(T-ak);if(P!=aM||k!=aL){P=aM;k=aL;D.trigger("jsp-arrow-change",[aj,aH,P,k])}r(aM,aL);Y.css("left",s);D.trigger("jsp-scroll-x",[-s,aM,aL]).trigger("scroll")}function u(aJ,s){if(az.showArrows){ar[aJ?"addClass":"removeClass"]("jspDisabled");af[s?"addClass":"removeClass"]("jspDisabled")}}function r(aJ,s){if(az.showArrows){ay[aJ?"addClass":"removeClass"]("jspDisabled");x[s?"addClass":"removeClass"]("jspDisabled")}}function M(s,aJ){var aK=s/(Z-v);V(aK*i,aJ)}function N(aJ,s){var aK=aJ/(T-ak);W(aK*j,s)}function ab(aW,aR,aK){var aO,aL,aM,s=0,aV=0,aJ,aQ,aP,aT,aS,aU;try{aO=b(aW)}catch(aN){return}aL=aO.outerHeight();aM=aO.outerWidth();am.scrollTop(0);am.scrollLeft(0);while(!aO.is(".jspPane")){s+=aO.position().top;aV+=aO.position().left;aO=aO.offsetParent();if(/^body|html$/i.test(aO[0].nodeName)){return}}aJ=aB();aP=aJ+v;if(s<aJ||aR){aS=s-az.verticalGutter}else{if(s+aL>aP){aS=s-v+aL+az.verticalGutter}}if(aS){M(aS,aK)}aQ=aD();aT=aQ+ak;if(aV<aQ||aR){aU=aV-az.horizontalGutter}else{if(aV+aM>aT){aU=aV-ak+aM+az.horizontalGutter}}if(aU){N(aU,aK)}}function aD(){return -Y.position().left}function aB(){return -Y.position().top}function K(){var s=Z-v;return(s>20)&&(s-aB()<10)}function B(){var s=T-ak;return(s>20)&&(s-aD()<10)}function ag(){am.unbind(ac).bind(ac,function(aM,aN,aL,aJ){var aK=aa,s=I;Q.scrollBy(aL*az.mouseWheelSpeed,-aJ*az.mouseWheelSpeed,false);return aK==aa&&s==I})}function n(){am.unbind(ac)}function aC(){return false}function J(){Y.find(":input,a").unbind("focus.jsp").bind("focus.jsp",function(s){ab(s.target,false)})}function E(){Y.find(":input,a").unbind("focus.jsp")}function S(){var s,aJ,aL=[];aF&&aL.push(an[0]);aA&&aL.push(U[0]);Y.focus(function(){D.focus()});D.attr("tabindex",0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp",function(aO){if(aO.target!==this&&!(aL.length&&b(aO.target).closest(aL).length)){return}var aN=aa,aM=I;switch(aO.keyCode){case 40:case 38:case 34:case 32:case 33:case 39:case 37:s=aO.keyCode;aK();break;case 35:M(Z-v);s=null;break;case 36:M(0);s=null;break}aJ=aO.keyCode==s&&aN!=aa||aM!=I;return !aJ}).bind("keypress.jsp",function(aM){if(aM.keyCode==s){aK()}return !aJ});if(az.hideFocus){D.css("outline","none");if("hideFocus" in am[0]){D.attr("hideFocus",true)}}else{D.css("outline","");if("hideFocus" in am[0]){D.attr("hideFocus",false)}}function aK(){var aN=aa,aM=I;switch(s){case 40:Q.scrollByY(az.keyboardSpeed,false);break;case 38:Q.scrollByY(-az.keyboardSpeed,false);break;case 34:case 32:Q.scrollByY(v*az.scrollPagePercent,false);break;case 33:Q.scrollByY(-v*az.scrollPagePercent,false);break;case 39:Q.scrollByX(az.keyboardSpeed,false);break;case 37:Q.scrollByX(-az.keyboardSpeed,false);break}aJ=aN!=aa||aM!=I;return aJ}}function R(){D.attr("tabindex","-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp")}function C(){if(location.hash&&location.hash.length>1){var aL,aJ,aK=escape(location.hash);try{aL=b(aK)}catch(s){return}if(aL.length&&Y.find(aK)){if(am.scrollTop()===0){aJ=setInterval(function(){if(am.scrollTop()>0){ab(aK,true);b(document).scrollTop(am.position().top);clearInterval(aJ)}},50)}else{ab(aK,true);b(document).scrollTop(am.position().top)}}}}function ai(){b("a.jspHijack").unbind("click.jsp-hijack").removeClass("jspHijack")}function m(){ai();b("a[href^=#]").addClass("jspHijack").bind("click.jsp-hijack",function(){var s=this.href.split("#"),aJ;if(s.length>1){aJ=s[1];if(aJ.length>0&&Y.find("#"+aJ).length>0){ab("#"+aJ,true);return false}}})}function ao(){var aK,aJ,aM,aL,aN,s=false;am.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp",function(aO){var aP=aO.originalEvent.touches[0];aK=aD();aJ=aB();aM=aP.pageX;aL=aP.pageY;aN=false;s=true}).bind("touchmove.jsp",function(aR){if(!s){return}var aQ=aR.originalEvent.touches[0],aP=aa,aO=I;Q.scrollTo(aK+aM-aQ.pageX,aJ+aL-aQ.pageY);aN=aN||Math.abs(aM-aQ.pageX)>5||Math.abs(aL-aQ.pageY)>5;
	return aP==aa&&aO==I}).bind("touchend.jsp",function(aO){s=false}).bind("click.jsp-touchclick",function(aO){if(aN){aN=false;return false}})}function g(){var s=aB(),aJ=aD();D.removeClass("jspScrollable").unbind(".jsp");D.replaceWith(ap.append(Y.children()));ap.scrollTop(s);ap.scrollLeft(aJ)}b.extend(Q,{reinitialise:function(aJ){aJ=b.extend({},az,aJ);at(aJ)},scrollToElement:function(aK,aJ,s){ab(aK,aJ,s)},scrollTo:function(aK,s,aJ){N(aK,aJ);M(s,aJ)},scrollToX:function(aJ,s){N(aJ,s)},scrollToY:function(s,aJ){M(s,aJ)},scrollToPercentX:function(aJ,s){N(aJ*(T-ak),s)},scrollToPercentY:function(aJ,s){M(aJ*(Z-v),s)},scrollBy:function(aJ,s,aK){Q.scrollByX(aJ,aK);Q.scrollByY(s,aK)},scrollByX:function(s,aK){var aJ=aD()+Math[s<0?"floor":"ceil"](s),aL=aJ/(T-ak);W(aL*j,aK)},scrollByY:function(s,aK){var aJ=aB()+Math[s<0?"floor":"ceil"](s),aL=aJ/(Z-v);V(aL*i,aK)},positionDragX:function(s,aJ){W(s,aJ)},positionDragY:function(aJ,s){V(aJ,s)},animate:function(aJ,aM,s,aL){var aK={};aK[aM]=s;aJ.animate(aK,{duration:az.animateDuration,easing:az.animateEase,queue:false,step:aL})},getContentPositionX:function(){return aD()},getContentPositionY:function(){return aB()},getContentWidth:function(){return T},getContentHeight:function(){return Z},getPercentScrolledX:function(){return aD()/(T-ak)},getPercentScrolledY:function(){return aB()/(Z-v)},getIsScrollableH:function(){return aF},getIsScrollableV:function(){return aA},getContentPane:function(){return Y},scrollToBottom:function(s){V(i,s)},hijackInternalLinks:function(){m()},destroy:function(){g()}});at(O)}e=b.extend({},b.fn.jScrollPane.defaults,e);b.each(["mouseWheelSpeed","arrowButtonSpeed","trackClickSpeed","keyboardSpeed"],function(){e[this]=e[this]||e.speed});return this.each(function(){var f=b(this),g=f.data("jsp");if(g){g.reinitialise(e)}else{g=new d(f,e);f.data("jsp",g)}})};b.fn.jScrollPane.defaults={showArrows:false,maintainPosition:true,stickToBottom:false,stickToRight:false,clickOnTrack:true,autoReinitialise:false,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,contentWidth:c,animateScroll:false,animateDuration:300,animateEase:"linear",hijackInternalLinks:false,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:0,arrowButtonSpeed:0,arrowRepeatFreq:50,arrowScrollOnHover:false,trackClickSpeed:0,trackClickRepeatFreq:70,verticalArrowPositions:"split",horizontalArrowPositions:"split",enableKeyboardNavigation:true,hideFocus:false,keyboardSpeed:0,initialDelay:300,speed:30,scrollPagePercent:0.8}})(jQuery,this);



/*!
 * jQuery UI 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI
 */
(function(c,j){function k(a,b){var d=a.nodeName.toLowerCase();if("area"===d){b=a.parentNode;d=b.name;if(!a.href||!d||b.nodeName.toLowerCase()!=="map")return false;a=c("img[usemap=#"+d+"]")[0];return!!a&&l(a)}return(/input|select|textarea|button|object/.test(d)?!a.disabled:"a"==d?a.href||b:b)&&l(a)}function l(a){return!c(a).parents().andSelf().filter(function(){return c.curCSS(this,"visibility")==="hidden"||c.expr.filters.hidden(this)}).length}c.ui=c.ui||{};if(!c.ui.version){c.extend(c.ui,{version:"1.8.16",
	keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91}});c.fn.extend({propAttr:c.fn.prop||c.fn.attr,_focus:c.fn.focus,focus:function(a,b){return typeof a==="number"?this.each(function(){var d=
	this;setTimeout(function(){c(d).focus();b&&b.call(d)},a)}):this._focus.apply(this,arguments)},scrollParent:function(){var a;a=c.browser.msie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(c.curCSS(this,"position",1))&&/(auto|scroll)/.test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(c.curCSS(this,
	"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0);return/fixed/.test(this.css("position"))||!a.length?c(document):a},zIndex:function(a){if(a!==j)return this.css("zIndex",a);if(this.length){a=c(this[0]);for(var b;a.length&&a[0]!==document;){b=a.css("position");if(b==="absolute"||b==="relative"||b==="fixed"){b=parseInt(a.css("zIndex"),10);if(!isNaN(b)&&b!==0)return b}a=a.parent()}}return 0},disableSelection:function(){return this.bind((c.support.selectstart?"selectstart":
	"mousedown")+".ui-disableSelection",function(a){a.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}});c.each(["Width","Height"],function(a,b){function d(f,g,m,n){c.each(e,function(){g-=parseFloat(c.curCSS(f,"padding"+this,true))||0;if(m)g-=parseFloat(c.curCSS(f,"border"+this+"Width",true))||0;if(n)g-=parseFloat(c.curCSS(f,"margin"+this,true))||0});return g}var e=b==="Width"?["Left","Right"]:["Top","Bottom"],h=b.toLowerCase(),i={innerWidth:c.fn.innerWidth,innerHeight:c.fn.innerHeight,
	outerWidth:c.fn.outerWidth,outerHeight:c.fn.outerHeight};c.fn["inner"+b]=function(f){if(f===j)return i["inner"+b].call(this);return this.each(function(){c(this).css(h,d(this,f)+"px")})};c.fn["outer"+b]=function(f,g){if(typeof f!=="number")return i["outer"+b].call(this,f);return this.each(function(){c(this).css(h,d(this,f,true,g)+"px")})}});c.extend(c.expr[":"],{data:function(a,b,d){return!!c.data(a,d[3])},focusable:function(a){return k(a,!isNaN(c.attr(a,"tabindex")))},tabbable:function(a){var b=c.attr(a,
	"tabindex"),d=isNaN(b);return(d||b>=0)&&k(a,!d)}});c(function(){var a=document.body,b=a.appendChild(b=document.createElement("div"));c.extend(b.style,{minHeight:"100px",height:"auto",padding:0,borderWidth:0});c.support.minHeight=b.offsetHeight===100;c.support.selectstart="onselectstart"in b;a.removeChild(b).style.display="none"});c.extend(c.ui,{plugin:{add:function(a,b,d){a=c.ui[a].prototype;for(var e in d){a.plugins[e]=a.plugins[e]||[];a.plugins[e].push([b,d[e]])}},call:function(a,b,d){if((b=a.plugins[b])&&
	a.element[0].parentNode)for(var e=0;e<b.length;e++)a.options[b[e][0]]&&b[e][1].apply(a.element,d)}},contains:function(a,b){return document.compareDocumentPosition?a.compareDocumentPosition(b)&16:a!==b&&a.contains(b)},hasScroll:function(a,b){if(c(a).css("overflow")==="hidden")return false;b=b&&b==="left"?"scrollLeft":"scrollTop";var d=false;if(a[b]>0)return true;a[b]=1;d=a[b]>0;a[b]=0;return d},isOverAxis:function(a,b,d){return a>b&&a<b+d},isOver:function(a,b,d,e,h,i){return c.ui.isOverAxis(a,d,h)&&
	c.ui.isOverAxis(b,e,i)}})}})(jQuery);
;/*!
 * jQuery UI Widget 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Widget
 */
(function(b,j){if(b.cleanData){var k=b.cleanData;b.cleanData=function(a){for(var c=0,d;(d=a[c])!=null;c++)try{b(d).triggerHandler("remove")}catch(e){}k(a)}}else{var l=b.fn.remove;b.fn.remove=function(a,c){return this.each(function(){if(!c)if(!a||b.filter(a,[this]).length)b("*",this).add([this]).each(function(){try{b(this).triggerHandler("remove")}catch(d){}});return l.call(b(this),a,c)})}}b.widget=function(a,c,d){var e=a.split(".")[0],f;a=a.split(".")[1];f=e+"-"+a;if(!d){d=c;c=b.Widget}b.expr[":"][f]=
	function(h){return!!b.data(h,a)};b[e]=b[e]||{};b[e][a]=function(h,g){arguments.length&&this._createWidget(h,g)};c=new c;c.options=b.extend(true,{},c.options);b[e][a].prototype=b.extend(true,c,{namespace:e,widgetName:a,widgetEventPrefix:b[e][a].prototype.widgetEventPrefix||a,widgetBaseClass:f},d);b.widget.bridge(a,b[e][a])};b.widget.bridge=function(a,c){b.fn[a]=function(d){var e=typeof d==="string",f=Array.prototype.slice.call(arguments,1),h=this;d=!e&&f.length?b.extend.apply(null,[true,d].concat(f)):
	d;if(e&&d.charAt(0)==="_")return h;e?this.each(function(){var g=b.data(this,a),i=g&&b.isFunction(g[d])?g[d].apply(g,f):g;if(i!==g&&i!==j){h=i;return false}}):this.each(function(){var g=b.data(this,a);g?g.option(d||{})._init():b.data(this,a,new c(d,this))});return h}};b.Widget=function(a,c){arguments.length&&this._createWidget(a,c)};b.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",options:{disabled:false},_createWidget:function(a,c){b.data(c,this.widgetName,this);this.element=b(c);this.options=
	b.extend(true,{},this.options,this._getCreateOptions(),a);var d=this;this.element.bind("remove."+this.widgetName,function(){d.destroy()});this._create();this._trigger("create");this._init()},_getCreateOptions:function(){return b.metadata&&b.metadata.get(this.element[0])[this.widgetName]},_create:function(){},_init:function(){},destroy:function(){this.element.unbind("."+this.widgetName).removeData(this.widgetName);this.widget().unbind("."+this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass+
	"-disabled ui-state-disabled")},widget:function(){return this.element},option:function(a,c){var d=a;if(arguments.length===0)return b.extend({},this.options);if(typeof a==="string"){if(c===j)return this.options[a];d={};d[a]=c}this._setOptions(d);return this},_setOptions:function(a){var c=this;b.each(a,function(d,e){c._setOption(d,e)});return this},_setOption:function(a,c){this.options[a]=c;if(a==="disabled")this.widget()[c?"addClass":"removeClass"](this.widgetBaseClass+"-disabled ui-state-disabled").attr("aria-disabled",
	c);return this},enable:function(){return this._setOption("disabled",false)},disable:function(){return this._setOption("disabled",true)},_trigger:function(a,c,d){var e=this.options[a];c=b.Event(c);c.type=(a===this.widgetEventPrefix?a:this.widgetEventPrefix+a).toLowerCase();d=d||{};if(c.originalEvent){a=b.event.props.length;for(var f;a;){f=b.event.props[--a];c[f]=c.originalEvent[f]}}this.element.trigger(c,d);return!(b.isFunction(e)&&e.call(this.element[0],c,d)===false||c.isDefaultPrevented())}}})(jQuery);
;/*!
 * jQuery UI Mouse 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Mouse
 *
 * Depends:
 *	jquery.ui.widget.js
 */
(function(b){var d=false;b(document).mouseup(function(){d=false});b.widget("ui.mouse",{options:{cancel:":input,option",distance:1,delay:0},_mouseInit:function(){var a=this;this.element.bind("mousedown."+this.widgetName,function(c){return a._mouseDown(c)}).bind("click."+this.widgetName,function(c){if(true===b.data(c.target,a.widgetName+".preventClickEvent")){b.removeData(c.target,a.widgetName+".preventClickEvent");c.stopImmediatePropagation();return false}});this.started=false},_mouseDestroy:function(){this.element.unbind("."+
	this.widgetName)},_mouseDown:function(a){if(!d){this._mouseStarted&&this._mouseUp(a);this._mouseDownEvent=a;var c=this,f=a.which==1,g=typeof this.options.cancel=="string"&&a.target.nodeName?b(a.target).closest(this.options.cancel).length:false;if(!f||g||!this._mouseCapture(a))return true;this.mouseDelayMet=!this.options.delay;if(!this.mouseDelayMet)this._mouseDelayTimer=setTimeout(function(){c.mouseDelayMet=true},this.options.delay);if(this._mouseDistanceMet(a)&&this._mouseDelayMet(a)){this._mouseStarted=
	this._mouseStart(a)!==false;if(!this._mouseStarted){a.preventDefault();return true}}true===b.data(a.target,this.widgetName+".preventClickEvent")&&b.removeData(a.target,this.widgetName+".preventClickEvent");this._mouseMoveDelegate=function(e){return c._mouseMove(e)};this._mouseUpDelegate=function(e){return c._mouseUp(e)};b(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate);a.preventDefault();return d=true}},_mouseMove:function(a){if(b.browser.msie&&
	!(document.documentMode>=9)&&!a.button)return this._mouseUp(a);if(this._mouseStarted){this._mouseDrag(a);return a.preventDefault()}if(this._mouseDistanceMet(a)&&this._mouseDelayMet(a))(this._mouseStarted=this._mouseStart(this._mouseDownEvent,a)!==false)?this._mouseDrag(a):this._mouseUp(a);return!this._mouseStarted},_mouseUp:function(a){b(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate);if(this._mouseStarted){this._mouseStarted=
	false;a.target==this._mouseDownEvent.target&&b.data(a.target,this.widgetName+".preventClickEvent",true);this._mouseStop(a)}return false},_mouseDistanceMet:function(a){return Math.max(Math.abs(this._mouseDownEvent.pageX-a.pageX),Math.abs(this._mouseDownEvent.pageY-a.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return true}})})(jQuery);
;/*
 * jQuery UI Slider 1.8.16
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Slider
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.mouse.js
 *	jquery.ui.widget.js
 */
(function(d){d.widget("ui.slider",d.ui.mouse,{widgetEventPrefix:"slide",options:{animate:false,distance:0,max:100,min:0,orientation:"horizontal",range:false,step:1,value:0,values:null},_create:function(){var a=this,b=this.options,c=this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),f=b.values&&b.values.length||1,e=[];this._mouseSliding=this._keySliding=false;this._animateOff=true;this._handleIndex=null;this._detectOrientation();this._mouseInit();this.element.addClass("ui-slider ui-slider-"+
	this.orientation+" ui-widget ui-widget-content ui-corner-all"+(b.disabled?" ui-slider-disabled ui-disabled":""));this.range=d([]);if(b.range){if(b.range===true){if(!b.values)b.values=[this._valueMin(),this._valueMin()];if(b.values.length&&b.values.length!==2)b.values=[b.values[0],b.values[0]]}this.range=d("<div></div>").appendTo(this.element).addClass("ui-slider-range ui-widget-header"+(b.range==="min"||b.range==="max"?" ui-slider-range-"+b.range:""))}for(var j=c.length;j<f;j+=1)e.push("<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>");
	this.handles=c.add(d(e.join("")).appendTo(a.element));this.handle=this.handles.eq(0);this.handles.add(this.range).filter("a").click(function(g){g.preventDefault()}).hover(function(){b.disabled||d(this).addClass("ui-state-hover")},function(){d(this).removeClass("ui-state-hover")}).focus(function(){if(b.disabled)d(this).blur();else{d(".ui-slider .ui-state-focus").removeClass("ui-state-focus");d(this).addClass("ui-state-focus")}}).blur(function(){d(this).removeClass("ui-state-focus")});this.handles.each(function(g){d(this).data("index.ui-slider-handle",
		g)});this.handles.keydown(function(g){var k=true,l=d(this).data("index.ui-slider-handle"),i,h,m;if(!a.options.disabled){switch(g.keyCode){case d.ui.keyCode.HOME:case d.ui.keyCode.END:case d.ui.keyCode.PAGE_UP:case d.ui.keyCode.PAGE_DOWN:case d.ui.keyCode.UP:case d.ui.keyCode.RIGHT:case d.ui.keyCode.DOWN:case d.ui.keyCode.LEFT:k=false;if(!a._keySliding){a._keySliding=true;d(this).addClass("ui-state-active");i=a._start(g,l);if(i===false)return}break}m=a.options.step;i=a.options.values&&a.options.values.length?
		(h=a.values(l)):(h=a.value());switch(g.keyCode){case d.ui.keyCode.HOME:h=a._valueMin();break;case d.ui.keyCode.END:h=a._valueMax();break;case d.ui.keyCode.PAGE_UP:h=a._trimAlignValue(i+(a._valueMax()-a._valueMin())/5);break;case d.ui.keyCode.PAGE_DOWN:h=a._trimAlignValue(i-(a._valueMax()-a._valueMin())/5);break;case d.ui.keyCode.UP:case d.ui.keyCode.RIGHT:if(i===a._valueMax())return;h=a._trimAlignValue(i+m);break;case d.ui.keyCode.DOWN:case d.ui.keyCode.LEFT:if(i===a._valueMin())return;h=a._trimAlignValue(i-
		m);break}a._slide(g,l,h);return k}}).keyup(function(g){var k=d(this).data("index.ui-slider-handle");if(a._keySliding){a._keySliding=false;a._stop(g,k);a._change(g,k);d(this).removeClass("ui-state-active")}});this._refreshValue();this._animateOff=false},destroy:function(){this.handles.remove();this.range.remove();this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-slider-disabled ui-widget ui-widget-content ui-corner-all").removeData("slider").unbind(".slider");this._mouseDestroy();
	return this},_mouseCapture:function(a){var b=this.options,c,f,e,j,g;if(b.disabled)return false;this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()};this.elementOffset=this.element.offset();c=this._normValueFromMouse({x:a.pageX,y:a.pageY});f=this._valueMax()-this._valueMin()+1;j=this;this.handles.each(function(k){var l=Math.abs(c-j.values(k));if(f>l){f=l;e=d(this);g=k}});if(b.range===true&&this.values(1)===b.min){g+=1;e=d(this.handles[g])}if(this._start(a,g)===false)return false;
	this._mouseSliding=true;j._handleIndex=g;e.addClass("ui-state-active").focus();b=e.offset();this._clickOffset=!d(a.target).parents().andSelf().is(".ui-slider-handle")?{left:0,top:0}:{left:a.pageX-b.left-e.width()/2,top:a.pageY-b.top-e.height()/2-(parseInt(e.css("borderTopWidth"),10)||0)-(parseInt(e.css("borderBottomWidth"),10)||0)+(parseInt(e.css("marginTop"),10)||0)};this.handles.hasClass("ui-state-hover")||this._slide(a,g,c);return this._animateOff=true},_mouseStart:function(){return true},_mouseDrag:function(a){var b=
	this._normValueFromMouse({x:a.pageX,y:a.pageY});this._slide(a,this._handleIndex,b);return false},_mouseStop:function(a){this.handles.removeClass("ui-state-active");this._mouseSliding=false;this._stop(a,this._handleIndex);this._change(a,this._handleIndex);this._clickOffset=this._handleIndex=null;return this._animateOff=false},_detectOrientation:function(){this.orientation=this.options.orientation==="vertical"?"vertical":"horizontal"},_normValueFromMouse:function(a){var b;if(this.orientation==="horizontal"){b=
	this.elementSize.width;a=a.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)}else{b=this.elementSize.height;a=a.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)}b=a/b;if(b>1)b=1;if(b<0)b=0;if(this.orientation==="vertical")b=1-b;a=this._valueMax()-this._valueMin();return this._trimAlignValue(this._valueMin()+b*a)},_start:function(a,b){var c={handle:this.handles[b],value:this.value()};if(this.options.values&&this.options.values.length){c.value=this.values(b);
	c.values=this.values()}return this._trigger("start",a,c)},_slide:function(a,b,c){var f;if(this.options.values&&this.options.values.length){f=this.values(b?0:1);if(this.options.values.length===2&&this.options.range===true&&(b===0&&c>f||b===1&&c<f))c=f;if(c!==this.values(b)){f=this.values();f[b]=c;a=this._trigger("slide",a,{handle:this.handles[b],value:c,values:f});this.values(b?0:1);a!==false&&this.values(b,c,true)}}else if(c!==this.value()){a=this._trigger("slide",a,{handle:this.handles[b],value:c});
	a!==false&&this.value(c)}},_stop:function(a,b){var c={handle:this.handles[b],value:this.value()};if(this.options.values&&this.options.values.length){c.value=this.values(b);c.values=this.values()}this._trigger("stop",a,c)},_change:function(a,b){if(!this._keySliding&&!this._mouseSliding){var c={handle:this.handles[b],value:this.value()};if(this.options.values&&this.options.values.length){c.value=this.values(b);c.values=this.values()}this._trigger("change",a,c)}},value:function(a){if(arguments.length){this.options.value=
	this._trimAlignValue(a);this._refreshValue();this._change(null,0)}else return this._value()},values:function(a,b){var c,f,e;if(arguments.length>1){this.options.values[a]=this._trimAlignValue(b);this._refreshValue();this._change(null,a)}else if(arguments.length)if(d.isArray(arguments[0])){c=this.options.values;f=arguments[0];for(e=0;e<c.length;e+=1){c[e]=this._trimAlignValue(f[e]);this._change(null,e)}this._refreshValue()}else return this.options.values&&this.options.values.length?this._values(a):
	this.value();else return this._values()},_setOption:function(a,b){var c,f=0;if(d.isArray(this.options.values))f=this.options.values.length;d.Widget.prototype._setOption.apply(this,arguments);switch(a){case "disabled":if(b){this.handles.filter(".ui-state-focus").blur();this.handles.removeClass("ui-state-hover");this.handles.propAttr("disabled",true);this.element.addClass("ui-disabled")}else{this.handles.propAttr("disabled",false);this.element.removeClass("ui-disabled")}break;case "orientation":this._detectOrientation();
	this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation);this._refreshValue();break;case "value":this._animateOff=true;this._refreshValue();this._change(null,0);this._animateOff=false;break;case "values":this._animateOff=true;this._refreshValue();for(c=0;c<f;c+=1)this._change(null,c);this._animateOff=false;break}},_value:function(){var a=this.options.value;return a=this._trimAlignValue(a)},_values:function(a){var b,c;if(arguments.length){b=this.options.values[a];
	return b=this._trimAlignValue(b)}else{b=this.options.values.slice();for(c=0;c<b.length;c+=1)b[c]=this._trimAlignValue(b[c]);return b}},_trimAlignValue:function(a){if(a<=this._valueMin())return this._valueMin();if(a>=this._valueMax())return this._valueMax();var b=this.options.step>0?this.options.step:1,c=(a-this._valueMin())%b;a=a-c;if(Math.abs(c)*2>=b)a+=c>0?b:-b;return parseFloat(a.toFixed(5))},_valueMin:function(){return this.options.min},_valueMax:function(){return this.options.max},_refreshValue:function(){var a=
	this.options.range,b=this.options,c=this,f=!this._animateOff?b.animate:false,e,j={},g,k,l,i;if(this.options.values&&this.options.values.length)this.handles.each(function(h){e=(c.values(h)-c._valueMin())/(c._valueMax()-c._valueMin())*100;j[c.orientation==="horizontal"?"left":"bottom"]=e+"%";d(this).stop(1,1)[f?"animate":"css"](j,b.animate);if(c.options.range===true)if(c.orientation==="horizontal"){if(h===0)c.range.stop(1,1)[f?"animate":"css"]({left:e+"%"},b.animate);if(h===1)c.range[f?"animate":"css"]({width:e-
	g+"%"},{queue:false,duration:b.animate})}else{if(h===0)c.range.stop(1,1)[f?"animate":"css"]({bottom:e+"%"},b.animate);if(h===1)c.range[f?"animate":"css"]({height:e-g+"%"},{queue:false,duration:b.animate})}g=e});else{k=this.value();l=this._valueMin();i=this._valueMax();e=i!==l?(k-l)/(i-l)*100:0;j[c.orientation==="horizontal"?"left":"bottom"]=e+"%";this.handle.stop(1,1)[f?"animate":"css"](j,b.animate);if(a==="min"&&this.orientation==="horizontal")this.range.stop(1,1)[f?"animate":"css"]({width:e+"%"},
	b.animate);if(a==="max"&&this.orientation==="horizontal")this.range[f?"animate":"css"]({width:100-e+"%"},{queue:false,duration:b.animate});if(a==="min"&&this.orientation==="vertical")this.range.stop(1,1)[f?"animate":"css"]({height:e+"%"},b.animate);if(a==="max"&&this.orientation==="vertical")this.range[f?"animate":"css"]({height:100-e+"%"},{queue:false,duration:b.animate})}}});d.extend(d.ui.slider,{version:"1.8.16"})})(jQuery);
;

/*!
 * jQuery postMessage - v0.5 - 9/11/2009
 * http://benalman.com/projects/jquery-postmessage-plugin/
 * 
 * Copyright (c) 2009 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

// Script: jQuery postMessage: Cross-domain scripting goodness
//
// *Version: 0.5, Last updated: 9/11/2009*
// 
// Project Home - http://benalman.com/projects/jquery-postmessage-plugin/
// GitHub       - http://github.com/cowboy/jquery-postmessage/
// Source       - http://github.com/cowboy/jquery-postmessage/raw/master/jquery.ba-postmessage.js
// (Minified)   - http://github.com/cowboy/jquery-postmessage/raw/master/jquery.ba-postmessage.min.js (0.9kb)
// 
// About: License
// 
// Copyright (c) 2009 "Cowboy" Ben Alman,
// Dual licensed under the MIT and GPL licenses.
// http://benalman.com/about/license/
// 
// About: Examples
// 
// This working example, complete with fully commented code, illustrates one
// way in which this plugin can be used.
// 
// Iframe resizing - http://benalman.com/code/projects/jquery-postmessage/examples/iframe/
// 
// About: Support and Testing
// 
// Information about what version or versions of jQuery this plugin has been
// tested with and what browsers it has been tested in.
// 
// jQuery Versions - 1.3.2
// Browsers Tested - Internet Explorer 6-8, Firefox 3, Safari 3-4, Chrome, Opera 9.
// 
// About: Release History
// 
// 0.5 - (9/11/2009) Improved cache-busting
// 0.4 - (8/25/2009) Initial release

(function($){
	'$:nomunge'; // Used by YUI compressor.

	// A few vars used in non-awesome browsers.
	var interval_id,
		last_hash,
		cache_bust = 1,

	// A var used in awesome browsers.
		rm_callback,

	// A few convenient shortcuts.
		window = this,
		FALSE = !1,

	// Reused internal strings.
		postMessage = 'postMessage',
		addEventListener = 'addEventListener',

		p_receiveMessage,

	// I couldn't get window.postMessage to actually work in Opera 9.64!
		has_postMessage = window[postMessage]; // && !$.browser.opera;

	// Method: jQuery.postMessage
	// 
	// This method will call window.postMessage if available, setting the
	// targetOrigin parameter to the base of the target_url parameter for maximum
	// security in browsers that support it. If window.postMessage is not available,
	// the target window's location.hash will be used to pass the message. If an
	// object is passed as the message param, it will be serialized into a string
	// using the jQuery.param method.
	// 
	// Usage:
	// 
	// > jQuery.postMessage( message, target_url [, target ] );
	// 
	// Arguments:
	// 
	//  message - (String) A message to be passed to the other frame.
	//  message - (Object) An object to be serialized into a params string, using
	//    the jQuery.param method.
	//  target_url - (String) The URL of the other frame this window is
	//    attempting to communicate with. This must be the exact URL (including
	//    any query string) of the other window for this script to work in
	//    browsers that don't support window.postMessage.
	//  target - (Object) A reference to the other frame this window is
	//    attempting to communicate with. If omitted, defaults to `parent`.
	// 
	// Returns:
	// 
	//  Nothing.

	$[postMessage] = function( message, target_url, target ) {
		if ( !target_url ) { return; }

		// Serialize the message if not a string. Note that this is the only real
		// jQuery dependency for this script. If removed, this script could be
		// written as very basic JavaScript.
		message = typeof message === 'string' ? message : $.param( message );

		// Default to parent if unspecified.
		target = target || parent;

		if ( has_postMessage ) {
			// The browser supports window.postMessage, so call it with a targetOrigin
			// set appropriately, based on the target_url parameter.
			target[postMessage]( message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1' ) );

		} else if ( target_url ) {
			// The browser does not support window.postMessage, so set the location
			// of the target to target_url#message. A bit ugly, but it works! A cache
			// bust parameter is added to ensure that repeat messages trigger the
			// callback.
			target.location = target_url.replace( /#.*$/, '' ) + '#' + (+new Date) + (cache_bust++) + '&' + message;
		}
	};

	// Method: jQuery.receiveMessage
	// 
	// Register a single callback for either a window.postMessage call, if
	// supported, or if unsupported, for any change in the current window
	// location.hash. If window.postMessage is supported and source_origin is
	// specified, the source window will be checked against this for maximum
	// security. If window.postMessage is unsupported, a polling loop will be
	// started to watch for changes to the location.hash.
	// 
	// Note that for simplicity's sake, only a single callback can be registered
	// at one time. Passing no params will unbind this event (or stop the polling
	// loop), and calling this method a second time with another callback will
	// unbind the event (or stop the polling loop) first, before binding the new
	// callback.
	// 
	// Also note that if window.postMessage is available, the optional
	// source_origin param will be used to test the event.origin property. From
	// the MDC window.postMessage docs: This string is the concatenation of the
	// protocol and "://", the host name if one exists, and ":" followed by a port
	// number if a port is present and differs from the default port for the given
	// protocol. Examples of typical origins are https://example.org (implying
	// port 443), http://example.net (implying port 80), and http://example.com:8080.
	// 
	// Usage:
	// 
	// > jQuery.receiveMessage( callback [, source_origin ] [, delay ] );
	// 
	// Arguments:
	// 
	//  callback - (Function) This callback will execute whenever a <jQuery.postMessage>
	//    message is received, provided the source_origin matches. If callback is
	//    omitted, any existing receiveMessage event bind or polling loop will be
	//    canceled.
	//  source_origin - (String) If window.postMessage is available and this value
	//    is not equal to the event.origin property, the callback will not be
	//    called.
	//  source_origin - (Function) If window.postMessage is available and this
	//    function returns false when passed the event.origin property, the
	//    callback will not be called.
	//  delay - (Number) An optional zero-or-greater delay in milliseconds at
	//    which the polling loop will execute (for browser that don't support
	//    window.postMessage). If omitted, defaults to 100.
	// 
	// Returns:
	// 
	//  Nothing!

	$.receiveMessage = p_receiveMessage = function( callback, source_origin, delay ) {
		if ( has_postMessage ) {
			// Since the browser supports window.postMessage, the callback will be
			// bound to the actual event associated with window.postMessage.

			if ( callback ) {
				// Unbind an existing callback if it exists.
				rm_callback && p_receiveMessage();

				// Bind the callback. A reference to the callback is stored for ease of
				// unbinding.
				rm_callback = function(e) {
					if ( ( typeof source_origin === 'string' && e.origin !== source_origin )
						|| ( $.isFunction( source_origin ) && source_origin( e.origin ) === FALSE ) ) {
						return FALSE;
					}
					callback( e );
				};
			}

			if ( window[addEventListener] ) {
				window[ callback ? addEventListener : 'removeEventListener' ]( 'message', rm_callback, FALSE );
			} else {
				window[ callback ? 'attachEvent' : 'detachEvent' ]( 'onmessage', rm_callback );
			}

		} else {
			// Since the browser sucks, a polling loop will be started, and the
			// callback will be called whenever the location.hash changes.

			interval_id && clearInterval( interval_id );
			interval_id = null;

			if ( callback ) {
				delay = typeof source_origin === 'number'
					? source_origin
					: typeof delay === 'number'
					? delay
					: 100;

				interval_id = setInterval(function(){
					var hash = document.location.hash,
						re = /^#?\d+&/;
					if ( hash !== last_hash && re.test( hash ) ) {
						last_hash = hash;
						callback({ data: hash.replace( re, '' ) });
					}
				}, delay );
			}
		}
	};

})(jQuery);
/**
 Cookie Parser js for reading the Key and getting the Value from the Cookie.Somya test
 **/
if( typeof Sym === "undefined" ) {
	var Sym = {};
}
Sym.CookieParse = {

	readSubCookie : function(key){
		try{

			var cookieArray = new Array();
			cookieArray[0] = "es";
			cookieArray[1] = "tp";
			cookieArray[2] = "pr";

			for(var i=0;i < cookieArray.length;i++){

				var cookieValHEx = Sym.CookieParse.readCookie(cookieArray[i]);
				if(cookieValHEx == null){
					continue;
				}
				var cookieVal = Sym.CookieParse.hexDecode(cookieValHEx);

				var subCookie=cookieVal.split('|');

				for(var j=0; j < subCookie.length;j++){
					var subCookieKeyValPair=subCookie[j];
					var subCookieVal = subCookieKeyValPair.split('=');
					if(subCookieVal[0].replace(/^\s+|\s+$/g,"") == key.replace(/^\s+|\s+$/g,"")){
						return unescape(subCookieVal[1]);
					}

				}

			}

		}catch(e){}
	},


	//Decodes Hex(base16) formated data
	hexDecode : function(data){
		var b16_digits = '0123456789abcdef';
		var b16_map = new Array();
		for (var i=0; i<256; i++) {
			b16_map[b16_digits.charAt(i >> 4) + b16_digits.charAt(i & 15)] = String.fromCharCode(i);
		}
		if (!data.match(/^[a-f0-9]*$/i)) return false;// return false if input data is not a valid Hex string

		if (data.length % 2) data = '0'+data;

		var result = new Array();
		var j=0;
		for (var i=0; i<data.length; i+=2) {
			result[j++] = b16_map[data.substr(i,2)];
		}

		return result.join('');
	},


	readCookie : function(name){
		try{
			var name=name+"=";
			var ca=document.cookie.split(';');
			for(var i=0;i < ca.length;i++){
				var c=ca[i];
				while(c.charAt(0)==' '){c=c.substring(1,c.length);}
				if(c.indexOf(name)==0){return unescape(c.substring(name.length,c.length));}
			}
			return null;
		}catch(e){}
	}
};/*
 * @page index hybrid
 * @tag index
 * 
 * # Post Desert Inn Documentation
 * This documentation is for the file that came out of the Desert Inn JavaScript Re-architecture project. For informationabout the Desert Inn Project and what that entailed please see the documentation at:
 *
 *  * [![link][2]][1]
 * [1]: http://buy-static.norton.com/sharepoint/eCX/Documentation/rearchDocs/docs.html
 * [2]: images/LinkIcon.gif
 *
 * ## hybrid.js
 *
 * This is the documentation for the hybrid.js file. 
 * Currently, this is the main file for our Symantec eStore JavaScript library. While this is the main file we also include other files which have their own references and documentations. Most all of these are rolled up into StroeGlobal_hybri.js and serverd concatinated and minimized to the browser. Very few other files are served 'on their own'. 
 *
 * The files included in StoreGlobal_hyrid.js are:
 *
 * * jQuery
 * * hyplugins.js
 * * mbox.js
 * * wicket_evet.js
 * * wicket_ajax.js
 * * hybrid.js (this file) 
 */

// Separator for documentation auto generation engine Otherwise it doesnt separate the index from the Sym pages
var updated =  "6/4/13";
/*
 * @page Sym 1. Sym Namespace
 * @parent index
 *
 * The Sym global namespace. Here we just declare the namespace to use later on.
 * We have oue own namespace because of the different javascript files that our page serves up. some are from third party vendors and others are libraries such as wicket, that are used by the back-end for their dynamic replacing. 
 */
if( typeof Sym === "undefined" ) {
	var Sym = {};
}
/*
 * @attribute Sym.hybridVersion
 * @parent Sym
 *
 * ## version
 *
 * Current Version -> 1.0 
 * This is just a variable that references the version of the hybrid.js file.
 *
 */

Sym.hybridVersion = "1.15";

/*
 * @attribute Sym.popupVars
 * @parent Sym
 *
 * ## popup window variables
 *
 * These are the URL's for the differnt jqModal ajax requests, for each individual pop-up window. Some of these are duplicated
 *
 */

Sym.popupVars = {
	thankYou : '/estore/thankYouError',
	interstitial : '/estore/interstitial',
	download : '/estore/download',
	IBEditPayment : '/estore/mf/editOnlineBank?showError=true',
	IBPaymentError : '/estore/orderError',
	onlinePaymentEdit : '/estore/mf/editOnlinePayment?showError=true',
	onlineOrderError : '/estore/orderError',
	directDebitEdit : '/estore/editDirectDebit?showError=true',
	taxOverlay : '/estore/taxoverlay',
	signIn : '/estore/mf/signIn',
	mfHelp : '/estore/mf/help',
	rcHelp : '/estore/rc/help',
	cvvHelp : '/estore/mf/whatvisa',
	learnMore : '/estore/mf/learnMorePage',
	EULA : '/estore/mf/eulaPopup',
	OBTDownload : '/estore/download',
	OBTEditError : '/estore/mf/editOnlineBank?showError=true',
	editCustomer : '/estore/editCustomer',
	editDirectDebit : '/estore/mf/editDirectDebit',
	editOnlineBank : '/estore/mf/editOnlineBank?showError=true',
	progressIndicator : '/estore/progressIndicator',
	editBillingAddress : '/estore/mf/editBillingAddress',
	editShippingAddress : '/estore/mf/editShippingAddress',
	editOnlinePayment : '/estore/mf/editOnlinePayment', /* Added the below for eASD Sprint11 */
	editPaymentDigitalWallet: '/estore/mf/editPaymentDigitalWallet',
	digitalWalletError : '/estore/orderError'
};


/*
 * @class Sym.CONST
 * @parent Sym
 * 
 * ## Constants
 *
 * This contains the global constants in our JS. Currently this only holds the IFRAME_SRC variable which is the Iframe Src.
 */

Sym.CONST = {
	/*
	 * URL of the blank HTML files used in all of the IE6 ActiveX hacks
	 */
	IFRAME_SRC: window.location.protocol + "//buy-static.norton.com/estore/script/common/blank.html"
};

/*
 * @class Sym.Global
 * @parent Sym
 * @tag constants, global, IE, Taxonomy, browser
 * 
 * ## Global Variables
 *
 * Here we have a collection of global variables that are set by Sym.Util.getSymGlobal. We set these by reading a collection of swpans on the page. These spans are enclised in a div with a class of "nodisplay" 
 */
Sym.Global = {
	/*
	 * @attribute Sym.Global.IEVersion
	 * The current version of IE (null if non-IE)
	 */
	IEVersion: null,
	/*
	 * @attribute Sym.Global.Language
	 * The current language of the site as reported by the velocity variable $context.getLanguage()
	 */
	Language: "",
	/*
	 * @attribute Sym.Global.Country
	 * The current country of the site as reported by the velocity variable $context.getCountry()
	 */
	Country: "",
	/*
	 * @attribute Sym.Global.Site
	 * The current site of the site as reported by the velocity variable $context.getSite()
	 */
	Site: "",
	/*
	 * @attribute Sym.Global.Vendor
	 * The current vendor of the site as reported by the velocity variable $context.getVendor()
	 */
	Vendor: "",
	/*
	 * @attribute Sym.Global.SubChannel
	 * The current sub channel of the site as reported by the velocity variable $context.getSubchannel()
	 */
	SubChannel: "",
	/*
	 * @attribute Sym.Global.URLParams
	 * An object containing the name value pairs for the current URL querystring
	 */
	URLParams: {},
	/*
	 * @attribute Sym.Global.beforeLoad
	 * Timestamp that page started to load
	 */
	beforeLoad: (new Date()).getTime()
};

/*
 * @class Sym.Util
 * @parent Sym
 * 
 * ## Utilities
 *
 * This is a collection of utilitiy functions that get called throiught our site. There are only two of these functions left as we removed most all of thgem in our cleanup project.
 *
 * Whats left is:
 *
 * * getSymGlobal - which finds the taxonomy statics and returns the value back.
 * * switchClass - which just adds a class to a given element. Here for backwards compatibility.
 */
Sym.Util = {
	/*
	 * Retrieves a value from an element. These elements are populated with Velocity code that give the JavaScript access to the current context (language, country, etc)
	 *
	 * @param {String} sgId Id of element that contains value
	 * @return {String} the text that is inside of the element
	 */
	getSymGlobal : function(sgId){
		var sgEl = $('#' + sgId);
		if (sgEl.length != 0) {
			return sgEl.text();
		} else {
			return '';
		}
	},
	/*
	 * Add a class to an object
	 * 
	 * @param {Object} obj element to add class to
	 * @param {String} className class to add
	 */
	switchClass: function(obj, className){
		log('Sym.Util.switchClass', arguments);
		$(obj).addClass(className);
	},
	/**
	 * This function used by sliders. It gets current quantity from URL
	 **/
	getCurrentQtyFromURL: function(type) {
		var currentURL = window.location.href;
		var currentQty = 0;
		var match = currentURL.replace(new RegExp('(.*)\/' + type + '\/', 'i'), '');
		if (match === currentURL) {
			currentQty = -1;
			return currentQty;
		}
		match = match.replace (new RegExp('\/(.*)', 'i'), '');
		if (match != '') {
			currentQty = parseInt(match);
		}
		return currentQty;
	}
};

/*
 * @class Sym.MessageBubble
 * @parent Sym
 * 
 * Creates a small floating div that contains a message. It always has an arrow pointing to the left and is always positioned
 * so the arrow points to the left-most edge of whatever triggered it.
 * @image ../../documentjs/jmvcdoc/images/MessageBubbleUI.png
 */
$.Controller.extend("Sym.MessageBubble",
	/* @Static */
	{
		/**
		 * The default values for constructing a MessageBubble
		 *
		 * *	closeClass: "mbClose",
		 * *	closeHoverClass: "bblTopRtCrnrRo",
		 * *	layoutOffsetX: 12,
		 * *	layoutOffsetY: -18,
		 * *	message: "",
		 * *	messageContainerId: "hintTipLayer",
		 * *	offsetY: 0,
		 * *	offsetX: 0,
		 * *	width: 280
		 */
		defaults : {
			closeClass: "mbClose",
			closeHoverClass: "bblTopRtCrnrRo",
			layoutOffsetX: 12,
			layoutOffsetY: -18,
			message: "",
			messageContainerId: "hintTipLayer",
			offsetY: 0,
			offsetX: 0,
			width: 280
		}
	},
	/*
	 * @prototype
	 */
	{

		/*
		 * initalizes message bubble
		 * @param {Object} jQuery object 
		 * @param {Object} options 
		 */
		init : function(el, options){
			this.messageContainer = $("#" + this.options.messageContainerId);
			if (this.messageContainer.length == 0) {
				$('body').append('<div id="' + this.options.messageContainerId + '"></div>');
			}
			this.messageContainer = $("#" + this.options.messageContainerId);

			this.subscribe();

		},

		/*
		 * Creates and positions MessageBubble
		 *
		 * @param {integer} offsetX the x-offset that should be applied when positioning the MessageBubble
		 * @param {integer} offsetY the y-offset that should be applied when positioning the MessageBubble
		 */
		show : function(offsetX, offsetY){
			this.create();
			var elPosition = this.element.offset();
			elPosition.left += (typeof offsetX !== "undefined") ? offsetX : 0;
			elPosition.top += (typeof offsetY !== "undefined") ? offsetY : 0;

			this.messageContainer.css({
				left: elPosition.left + this.options.layoutOffsetX + this.element.width() + 'px',
				top: elPosition.top + this.options.layoutOffsetY + 'px',
				visibility: 'visible'
			});
			//stop propogation
			return false;
		},

		/*
		 * sets the css to hide the bubble
		 *
		 */

		hide : function() {
			this.messageContainer
				.html('')
				.css({
					left: '-9999px',
					top: '',
					visibility: 'hidden'
				});
		},

		/*
		 * Writes the HTML for the bubble, and appends click events for the close box
		 *
		 */
		create: function() {
			var tmpl = [];
			tmpl.push('<div class="bblLftArrow"></div>');
			tmpl.push('<table cellpadding="0" cellspacing="0" width="' + this.options.width + '" border="0"  >');
			tmpl.push('  <tr>');
			tmpl.push('    <td class="bblTopLftCrnr"><\/td>');
			tmpl.push('    <td class="bblTopGrad"><\/td>');
			tmpl.push('    <td class="bblTopRtCrnr hoverhand ' + this.options.closeClass + '"><\/td>');
			tmpl.push('  <\/tr>');
			tmpl.push('  <tr>');
			tmpl.push('    <td class="bblLftGrad"> <\/td>');
			tmpl.push('    <td>');
			tmpl.push('      <div class="divContentStyle">' + this.options.message + '<\/div>');
			tmpl.push('    <\/td>');
			tmpl.push('    <td class="bblRgtGrad"><\/td>');
			tmpl.push('  <\/tr>');
			tmpl.push('  <tr>');
			tmpl.push('    <td class="bblBtmLftCrnr"><\/td>');
			tmpl.push('    <td class="bblBtmGrad" ><\/td>');
			tmpl.push('    <td class="bblBtmRtCrnr"><\/td>');
			tmpl.push('  <\/tr>');
			tmpl.push('<\/table>');

			this.messageContainer.html(tmpl.join(''));

			var closeHoverClass = this.options.closeHoverClass;
			this.messageContainer
				.delegate("." + this.options.closeClass, "click", $.proxy(this.hide, this))
				.delegate("." + this.options.closeClass, "hover", function() {
					$(this).toggleClass(closeHoverClass);
				});
		},
		/*
		 * removes all the actions we applied for this bubble
		 */
		destroy : function(){
			this.messageContainer
				.undelegate("." + this.options.closeClass, "click", $.proxy(this.hide, this))
				.undelegate("." + this.options.closeClass, "hover", function() {
					$(this).toggleClass(closeHoverClass);
				});
			this.messageContainer.remove();
			this._super();
		},
		/*
		 * listen for a MessageBubble hide or hideAll messages that will hide a specifc or every MessageBubble (and extended controllers)
		 *
		 */
		subscribe: function() {
			$.subscribe("/MessageBubble/hideAll", $.proxy(function() {
				this.hide();
			}, this));
			$.subscribe("/MessageBubble/hide", $.proxy(function() {
				this.hide();
			}, this));
		},
		/*
		 * The click event. We assign the click event to the elemnt which this has been applied to.
		 */
		"click" : function(el, ev){
			ev.preventDefault();
			this.show();
		}

	});

/*
 * @parent Sym
 * @class Sym.ErrorBubble
 * @inherits Sym.MessageBubble
 */
Sym.MessageBubble.extend("Sym.ErrorBubble",
	{
		/*
		 * @parent ErrorBubble
		 * The defaults for the error bubble
		 *
		 * * closeClass: "ebClose",
		 * * closeHoverClass: "bblTopRtCrnrRo",	
		 * * iframeId: "errorBubbleIframe",
		 * * iframeTipId: "errorBubbleTipIframe",
		 * * layoutOffsetX: 12,
		 * * layoutOffsetY: -18,
		 * * message: "",
		 * * messageContainerId: "errorBubbleLayer",
		 * * offsetY: 0,
		 * * offsetX: 0,
		 * * width: 200
		 */
		defaults : {
			closeClass: "ebClose",
			closeHoverClass: "bblTopRtCrnrRo",
			iframeId: "errorBubbleIframe",
			iframeTipId: "errorBubbleTipIframe",
			layoutOffsetX: 12,
			layoutOffsetY: -18,
			message: "",
			messageContainerId: "errorBubbleLayer",
			offsetY: 0,
			offsetX: 0,
			width: 200
		}
	},
	/* @Prototype */
	{
		/*
		 * initalizes error bubble\
		 * A big difference between MessageBubble and ErrorBubble is that we have to include an iFrame to go under the bubble to allow for activeX bleeding in form elements 
		 * @param {Object} jQuery object 
		 * @param {Object} options 
		 */
		init : function(el, options){
			this._super(el, options);

			//create iframes for IE6
			if(Sym.Global.IEVersion == 6) {
				this.iframe = $("#" + this.options.iframeId);
				this.iframeTip = $("#" + this.options.iframeTipId);

				if (this.iframe.length == 0) {
					this.iframe = $('body').append('<iframe id="' + this.options.iframeId + '" src="' + Sym.CONST.IFRAME_SRC + '" frameborder="0" scrolling="no"></iframe>');
				}

				if (this.iframeTip.length == 0) {
					$('body').append('<iframe id="' + this.options.iframeTipId + '" src="' + Sym.CONST.IFRAME_SRC + '" frameborder="0" scrolling="no"></iframe>');
				}

				this.iframe = $("#" + this.options.iframeId);
				this.iframeTip = $("#" + this.options.iframeTipId);
			}

			//select and checkbox border fix
			if(this.element.is('input[type="checkbox"]')) {
				this.element.wrap("<div class='cbFixError'></div>");
			}

			if(this.element.is('select')) {
				this.element.wrap("<div class='ddFixError'></div>").width(this.element.width() + 2);
			}
		},
		/*
		 * Creates and positions ErrorBubble and the iframe
		 *
		 * @param {integer} offsetX the x-offset that should be applied when positioning the MessageBubble
		 * @param {integer} offsetY the y-offset that should be applied when positioning the MessageBubble
		 */
		show: function(offsetX, offsetY) {

			var containerPosition = this.messageContainer.offset();
			var elPosition = this.element.offset();

			this.create();

			offsetX = (typeof offsetX !== "undefined") ? offsetX : 0;
			offsetY = (typeof offsetY !== "undefined") ? offsetY : 0;

			if(this.element.attr("type") == "checkbox") {
				offsetX += 35;
			} else {
				offsetX += 5;
			}

			if(Sym.Global.IEVersion == 6) {
				offsetX += 15;
			}

			this.messageContainer.css({
				left: elPosition.left + this.options.layoutOffsetX + this.element.width() + 'px',
				top: elPosition.top + this.options.layoutOffsetY + 'px',
				visibility: 'visible'
			});

			if(Sym.Global.IEVersion == 6) {
				this.iframe
					.css({
						left: containerPosition.left + 3 + 'px',
						top: containerPosition.top + 3 + 'px',
						"z-index": this.messageContainer.css("z-index") - 1,
						visibility: 'visible'
					})
					.width(this.messageContainer.width() - 12)
					.height(this.messageContainer.height() - 12);

				this.iframeTip
					.css({
						left: containerPosition.left - 13 + 'px',
						top: containerPosition.top + 22 + 'px',
						"z-index": this.messageContainer.css("z-index") - 1,
						visibility: 'visible'
					})
					.width(this.messageContainer.width() - 15)
					.height(this.messageContainer.height() - 14);
			}

			//send Tealeaf error bubbles
			var e = {
				elementID: this.element[0].id,
				errorMessage: this.options.message
			};
			if (typeof TeaLeaf !== "undefined") {
				TeaLeaf.Event.tlAddCustomEvent("ErrorBubble", e)
			}

			//stop propogation
			return false;
		},
		/*
		 * Hides the error bubble
		 */
		hide : function() {
			if(Sym.Global.IEVersion == 6) {
				this.iframe.css({
					left: '-9999px',
					top: '',
					visibility: 'hidden'
				});
				this.iframeTip.css({
					left: '-9999px',
					top: '',
					visibility: 'hidden'
				});
			}

			this._super();
		},
		/*
		 * Kills the iframe (if IE6) then calls it's super destroy
		 */
		destroy : function(){
			if(Sym.Global.IEVersion == 6) {
				this.iframe.remove();
				this.iframeTip.remove();
			}

			this._super();
		},
		/*
		 * listen for a MessageBubble hideAll or an ErrorBubble hide message that will hide all MessageBubbles or a specifc ErrorBubble (and extended controllers).
		 */
		subscribe: function() {
			$.subscribe("/MessageBubble/hideAll", $.proxy(function() {
				this.hide();
			}, this));

			$.subscribe("/ErrorBubble/hide", $.proxy(function() {
				this.hide();
			}, this));
		},

		"click" : function(el, ev){},

		"focusin" : function(el, ev){
			ev.preventDefault();
			this.show();
		}
	});
/*
 * @class Sym.EventScrapper
 * @parent Sym
 * "Scrapes" inline JavaScript events from DOM elements and attaches them via jQuery
 */
$.Controller.extend("Sym.EventScraper",
	/* @Static */
	{
		/*
		 * The default values for the event Srcaper 
		 *
		 * * event: "", - such as onFocus
		 * * eventType: "",	- such as showErrorBubble	
		 * * plugin: "", - such as sym_error_bubble
		 * * pluginParams: "", - Most of these signatures tht we used have the elemnt in one position and the message in the second. However, sometimes they are swapped. so, this is a map for that. suchas {1:"message"}
		 * * selector: null - The selectors you want it to run on. In ErrorBubble's case its usually "input, select".
		 */
		defaults : {
			event: "",
			eventType: "",
			plugin: "",
			pluginParams: "",
			selector: null
		}
	},
	/* @Prototype */
	{
		/*
		 * Initializes the event scraper
		 * 
		 * @param {Object} el Element to scrape events from
		 * @param {Object} [options] 
		 */
		init: function(el, options) {
			if(this.options.selector !== null) {
				var thiz = this;
				$("*", this.element).filter((this.options.selector !== null) ? this.options.selector : "*").each(
					function(){
						if(typeof $(this).attr !== "undefined"
							&&	typeof $(this).attr(thiz.options.event) !== "undefined"
							&&	$(this).attr(thiz.options.event).toString().indexOf(thiz.options.eventType) != -1) {
							$.proxy(thiz.scrape($(this)), thiz);
							$(this).attr(thiz.options.event, "");
						}
					});
			} else {
				if(
					typeof this.element.attr !== "undefined"
						&& typeof this.element.attr(this.options.event) !== "undefined"
						&& this.element.attr(this.options.event).toString().indexOf(this.options.eventType) != -1
					) {
					this.scrape(this.element);
					this.element.attr(this.options.event, "");
				}
			}
		},

		/*
		 * Parses out the function(s) that have been scraped
		 *
		 * @param {Object} el Element to scrape events from 
		 */
		scrape: function(el) {
			var args = [];
			//get the inline function call
			var eventCall = el.attr(this.options.event).toString();
			//separate separate function calls
			var functions = eventCall.split(";");
			var thiz = this;
			$.each(functions, function(fidx) {
				//make sure we only process the function we want
				if(functions[fidx].indexOf(thiz.options.eventType) != -1) {
					//chrome and IE see this string as a full function
					functions[fidx] = functions[fidx].substring(functions[fidx].indexOf(thiz.options.eventType), functions[fidx].length -1);
					//separate by ', so we don't split text that has commas in it
					var eventComponents = functions[fidx].split("',");
					$.each(eventComponents, function(idx){
						var curComponent = eventComponents[idx];
						if(idx == 0) {
							curComponent = curComponent.replace(thiz.options.eventType + "(", "");
						} else if(idx == eventComponents.length - 1) {
							curComponent = $.String.strip(curComponent.replace(/^\s*'/, ""));
							curComponent = $.String.strip(curComponent.replace(/\)$/, ""));
							curComponent = $.String.strip(curComponent.replace(/'$/, ""));
							curComponent = $.String.strip(curComponent.replace(/\"$/, ""));
						}
						if(curComponent.charAt(0) === "'" || curComponent.charAt(0) === "\"") {
							curComponent = curComponent.substring(1, curComponent.length);
						}
						args.push(curComponent);
					});
					pluginArgs = {};
					$.each(args, function(idx){
						if(typeof thiz.options.pluginParams[idx] !== "undefined") {
							pluginArgs[thiz.options.pluginParams[idx]] = args[idx];
						}
					});
					el[thiz.options.plugin](pluginArgs);
				}
			});
		}
	});

/*
 * @class Sym.Interstitial
 * @parent Sym
 *
 * The interstitial controller. This is the controller that manages:
 *
 * * The cart Interstitial
 * * Help Interstitial
 * * CVV
 * * spinner
 * * Download
 * * Edit payment and profile
 * * VAT
 * * And many  more
 *
 */
$.Controller.extend("Sym.Interstitial",
	/* @Static */
	{
		/*
		 * The default values to make an interstitial
		 *
		 * * overlay: 25,
		 * * overlayClass: 'jqmOverlay',
		 * * closeClass: 'jqmClose',
		 * * trigger: '.jqModal',
		 * * ajax: false,
		 * * ajaxText: '',
		 * * iframeId: 'iframeInterstitialId',
		 * * target:false,
		 * * modal: false,
		 * * toTop: false,
		 * * onShow: false,
		 * * onHide: false,
		 * * onLoad: false,
		 * * position: false,
		 * * center: false,
		 * * positionEdit: false
		 */
		defaults:{
			overlay: 25,
			overlayClass: 'jqmOverlay',
			closeClass: 'jqmClose',
			trigger: '.jqModal',
			ajax: false,
			ajaxText: '',
			iframeId: 'iframeInterstitialId',
			target:false,
			modal: false,
			toTop: false,
			onShow: false,
			onHide: false,
			onLoad: false,
			position: false,
			center: false,
			positionEdit: false
		},

		init : function(el, options){
			//save the onLoad callback		
			var onLoadCallback = this.options.onLoad;
			//save this for use inside onLoad
			var thiz = this;
			//create iframes for IE6
			if ($.browser.msie && $.browser.version.substr(0,1)<7)
			{
				this.iframe = $("#" + this.options.iframeId);

				if (this.iframe.length == 0) {
					this.iframe = $('body').append('<iframe id="' + this.options.iframeId + '" src="' + Sym.CONST.IFRAME_SRC + '"  frameborder="0" scrolling="no" style="display:none;"></iframe>');
					$("body").data("iframeInterstitialId", this.options.iframeId);
				}

				this.iframe = $("#" + this.options.iframeId);
			}
			//override the onLoad callback to call this.position, this.positionEdit or this.center after it has executed 			
			this.options.onLoad = function(hash) {
				//call saved version of onLoad			
				if(onLoadCallback) {
					onLoadCallback(hash);
				}
				//call position, positionEdit or center (or none of them)
				if(thiz.options.position) {
					$.proxy(thiz.position(thiz.options.position.trigger, thiz.options.position.window, thiz.options.position.arrowPos1, thiz.options.position.arrowPos2), thiz);
				}
				if(thiz.options.positionEdit) {
					$.proxy(thiz.positionEdit(thiz.options.positionEdit.triggerID, thiz.options.positionEdit.windowID), thiz);
				}
				if(thiz.options.center) {
					$.proxy(thiz.center(thiz.options.center.window, thiz.options.center.offsetX, thiz.options.center.offsetY), thiz);
				}
				//tell evetyone we have loaded an interstitial and give them the hash passed into regular jqm callbacks
				$.publish("/Interstitial/" + thiz.element.attr('id') +  "/loaded", [hash]);
			};
			this.element.jqm(this.options);
		},

		show: function(){
			this.element.jqmShow();
		},

		hide: function(){
			this.element.jqmHide();
		},
		drag: function(){
			this.element.jqDrag();
		},
		position: function(triggerID, windowID, arrowPos1, arrowPos2){
			debug("position", arguments);
			//--Constants--//
			var TBL_SHADOW_RIGHT = TBL_SHADOW_BOTTOM = 7;
			var TBL_SHADOW_LEFT = TBL_SHADOW_TOP = 3;
			//--Variables--//
			if(typeof triggerID === "string") {
				var trigger = $('#' + triggerID);
			} else {
				var trigger = triggerID;
				var triggerID = trigger.attr('id');
			}
			debug("Trigger:", trigger);
			var popupWindow = $('#' + windowID);
			debug("Window:", popupWindow);
			//headerID for popups varies depending on the windowID
			var headerID = "";
			switch(windowID) {
				case "learnMoreAr":
					headerID = "LearnMoreDoubleHeaderBubble";
					break;
				case "eulaWindow":
					headerID = "EulaDoubleHeaderBubble";
					break;
				case "secCodeGeneric":
					headerID = "SecCodeDoubleHeaderBubble";
					break;
				case "helpInterstitial":
					headerID = "helpBubble";
					break;
				default:
					headerID = "DoubleHeaderBubble";
					break;
			}
			debug('headerID:', headerID);
			//Get table
			var tbl = $('#' + headerID + ' table:visible', '#' + windowID).eq(0);
			debug('tbl = #' + headerID + ' table:visible' + ',#' + windowID, tbl);
			debug(tbl);
			//Window Positioning
			var windowLeft = windowTop = 0;
			//Table Dimensions
			var tblWidth = tbl.width();
			var tblHeight = tbl.height();
			//Arrow
			var arrow;
			var arrowLeft = arrowTop = arrowHeight = arrowWidth = arrowXOffset = arrowYOffset = 0;
			var arrowClass = "";
			//Trigger Positioning
			var triggerLeft = Math.round(trigger.offset().left);
			var triggerTop = Math.round(trigger.offset().top);
			//Remove jqmodal negative margin	
			popupWindow.css('margin', 0);
			//Remove Arrow if there is one
			popupWindow.find(".bblLftArrowSml").remove();
			popupWindow.find(".bblLftArrow").remove();
			popupWindow.find(".bblRgtArrowSml").remove();
			popupWindow.find(".bblDwnArrow").remove();
			popupWindow.find(".bblRgtArrow").remove();
			//--Begin Positioning--//
			//arrowPos1
			if (arrowPos1 == "right") {
				debug("--right--");
				arrowClass = "bblRgtArrowSml";
				debug('arrowClass = ' + arrowClass);
				tbl.after('<div class="' + arrowClass + '" style="position:absolute"/>');
				arrow = $('#' + windowID + ' .' + arrowClass);
				arrowHeight = arrow.height();
				arrowWidth = arrow.width();
				arrowXOffset = Math.ceil(arrowWidth / 2);
				arrowYOffset = Math.ceil(arrowHeight / 2);
				arrowLeft = tblWidth - TBL_SHADOW_RIGHT;
				if(this.element.attr('id') == "helpInterstitial") {
					arrowLeft = arrowLeft - 3;
				}
				windowLeft = triggerLeft - tblWidth - (arrowWidth - TBL_SHADOW_RIGHT - 1);
				debug('arrowLeft(' + arrowLeft + ') = ' + tblWidth + '-' + TBL_SHADOW_RIGHT);
				debug('windowLeft(' + windowLeft + ') = ' + triggerLeft + '-' + tblWidth + '- (' + arrowWidth + '-' + TBL_SHADOW_RIGHT + '- 1)');
			}
			else if (arrowPos1 == "bottom") {
				debug("--bottom--");
				arrowClass = "bblDwnArrow";
				debug('arrowClass = ' + arrowClass);
				tbl.after('<div class="' + arrowClass + '" style="position:absolute"/>');
				arrow = $('#' + windowID + ' .' + arrowClass);
				arrowHeight = arrow.height();
				arrowWidth = arrow.width();
				arrowXOffset = Math.ceil(arrowWidth / 2);
				arrowYOffset = Math.ceil(arrowHeight / 2);
				arrowTop = tblHeight - TBL_SHADOW_BOTTOM;
				windowTop = triggerTop - tblHeight - arrowYOffset;
				debug('arrowTop(' + arrowTop + ') = ' + tblHeight + '-' + TBL_SHADOW_BOTTOM);
				debug('windowTop(' + windowTop + ') = ' + triggerTop + '-' + tblHeight + '-' + arrowYOffset);
			}
			else if (arrowPos1 == "left") {
				debug("--left--");
				arrowClass = "bblLftArrowSml";
				debug('arrowClass = ' + arrowClass);
				tbl.after('<div class="' + arrowClass + '" style="position:absolute"/>');
				arrow = $('#' + windowID + ' .' + arrowClass);
				arrowHeight = arrow.height();
				arrowWidth = arrow.width();
				arrowXOffset = Math.ceil(arrowWidth / 2);
				arrowYOffset = Math.ceil(arrowHeight / 2);
				arrowLeft = 0 - arrowWidth + TBL_SHADOW_TOP + 2;
				windowLeft = triggerLeft + trigger.width() + arrowWidth - TBL_SHADOW_LEFT;
				debug('arrowLeft(' + arrowLeft + ') = ' + 0 + '-' + arrowWidth + '+' + 2);
				debug('windowLeft(' + windowLeft + ') = ' + triggerLeft + '+' + trigger.width() + '+' + arrowWidth + '-' + TBL_SHADOW_LEFT);
			}
			//no top, because it isn't required for any window
			//arrowPos2
			if (arrowPos2 == "top") {
				debug("--top--");
				arrowTop = TBL_SHADOW_TOP + arrowYOffset + 20;
				windowTop = triggerTop - TBL_SHADOW_TOP - arrowYOffset - 3 - 20;
				debug('arrowTop(' + arrowTop + ') = ' + TBL_SHADOW_TOP + '+' + arrowYOffset);
				debug('windowTop(' + windowTop + ') = ' + triggerTop + '-' + TBL_SHADOW_TOP + '-' + arrowYOffset + '- 3');
			}
			else if (arrowPos2 == "middle") {
				debug("--middle--");
				arrowTop = TBL_SHADOW_TOP + Math.ceil(tblHeight / 2);
				windowTop = triggerTop - TBL_SHADOW_TOP - Math.ceil(tblHeight / 2);
				debug('arrowTop(' + arrowTop + ') = ' + TBL_SHADOW_TOP + '+' + Math.ceil(tblHeight / 2));
				debug('windowTop(' + windowTop + ') = ' + triggerTop + '-' + TBL_SHADOW_TOP + '-' + Math.ceil(tblHeight / 2));
			}
			else if (arrowPos2 == "bottom") {
				debug("--bottom--");
				arrowTop = tblHeight - TBL_SHADOW_BOTTOM - arrowHeight - arrowYOffset;
				windowTop = triggerTop - tblHeight + TBL_SHADOW_BOTTOM + arrowYOffset + arrowHeight - 3;
				debug('arrowTop(' + arrowTop + ') = ' + tblHeight + '-' + TBL_SHADOW_BOTTOM + '-' + arrowHeight + '-' + arrowYOffset);
				debug('windowTop(' + windowTop + ') = ' + triggerTop + '-' + tblHeight + '+' + TBL_SHADOW_BOTTOM + '+' + arrowYOffset + '+' + arrowHeight + '- 3');
			}
			else if (arrowPos2 == "left") {
				debug("--left--");
				arrowLeft = TBL_SHADOW_LEFT + arrowXOffset;
				windowLeft = triggerLeft - TBL_SHADOW_LEFT - arrowXOffset;
				debug('arrowLeft(' + arrowLeft + ') = ' + +'+' + arrowXOffset);
				debug('windowLeft(' + windowLeft + ') = ' + triggerLeft + '-' + TBL_SHADOW_LEFT + '-' + arrowXOffset);
			}
			else if (arrowPos2 == "right") {
				debug("--right--");
				arrowLeft = tblWidth - arrowWidth - TBL_SHADOW_RIGHT - arrowXOffset;
				windowLeft = triggerLeft - tblWidth + arrowWidth + TBL_SHADOW_RIGHT;
				debug('arrowLeft(' + arrowLeft + ') = ' + tblWidth + '-' + arrowWidth + '-' + TBL_SHADOW_RIGHT + '-' + arrowXOffset);
				debug('windowLeft(' + windowLeft + ') = ' + triggerLeft + '-' + tblWidth + '+' + arrowWidth + '+' + TBL_SHADOW_RIGHT);
			}
			//No bottom because bottommiddle and topmiddle aren't valid positionings		
			arrow.css('left', arrowLeft + 'px');
			arrow.css('top', arrowTop + 'px');
			popupWindow.css('left', windowLeft + 'px');
			popupWindow.css('top', windowTop + 'px');
			// for IE6 IFrame positioning
			if ($.browser.msie && $.browser.version.substr(0,1)<7)
			{
				this.iframe
					.css({
						left: this.element.offset().left,
						top: this.element.offset().top,
						"z-index": '999',
						display: '',
						background:'green',
						width:this.element.width(),
						height:this.element.height(),
						position:'absolute'
					})
				$(".jqmClose").bind("click", function(){
					$("#" + $("body").data("iframeInterstitialId")).hide();
				});
			}
		},

		positionEdit: function(triggerID, windowID) {
			log('ESTORE.Misc.positionEditPopup', arguments);
			//Cache some frequently used DOM objects
			var trigger = $('#' + triggerID);
			var window = $('#' + windowID);
			var table = $('table', window).eq(0);
			var boundry = $('.divSolidWhiteBack');
			//Define values for positioning
			var leftBoundry = boundry.offset().left;
			var rightBoundry = boundry.offset().left + boundry.width();
			var triggerLeft = Math.round(trigger.offset().left);
			var triggerTop = Math.round(trigger.offset().top);
			var tableWidth = table.width();
			var tableHeight = table.height();
			var tableShadowRight = 7;
			var tableShadowLeft = 3;
			var tableShadowTop = 3;
			var tableShadowBottom = 7;
			var arrow = null;
			var arrowOffset = 45;
			var arrowWidth = 26;
			var arrowHeight = 23;
			var arrowClass = "bblRgtArrowSml";
			//IE fix
			$('#editBubble').width(tableWidth + 10);
			//Remove arrow if there was one
			$('.bblRgtArrowSml, .bblLftArrowSml', window).remove();
			//Positioning Try #1 (right arrow, popup will be on left hand side of the trigger link)      
			windowLeft = triggerLeft - tableWidth - arrowWidth + 13;
			arrowLeft = tableWidth - tableShadowRight - 2;
			arrowTop = tableShadowTop + arrowOffset;
			windowTop = triggerTop - arrowTop - (arrowHeight / 2) + 5;
			//Check if we are within divSolidWhiteBack
			if(windowLeft < leftBoundry) {
				//save old values
				var oldWindowLeft = windowLeft;
				var oldArrowClass = arrowClass;
				var oldArrowLeft = arrowLeft;
				//compute how far we are off
				var overage = leftBoundry - windowLeft;
				//compute new positioning
				windowLeft = windowLeft + tableWidth + arrowWidth + trigger.width();
				arrowClass = "bblLftArrowSml";
				arrowLeft = 0 - arrowWidth + tableShadowLeft + 2;
				//If we are over on both sides, check which one is least over
				if((windowLeft + tableWidth + arrowWidth) > rightBoundry){
					overage = overage - ((windowLeft + tableWidth + arrowWidth) - rightBoundry);
					//if overage is negative, the default positioning is least off so we want the old values back
					if(overage < 0) {
						windowLeft = oldWindowLeft;
						arrowClass = oldArrowClass;
						arrowLeft = oldArrowLeft;
					}
					//otherwise, we keep the new values
				}
			}
			//Add arrow to popup
			window.prepend('<div class="' + arrowClass + '" style="position:absolute; z-index: 3000;"/>');
			arrow = $('.' + arrowClass, window);
			//Remove any pre-existing formatting
			window.css('margin-left', 0);
			arrow.css('margin-left', 0);
			arrow.css('margin-top', 0);
			//Apply positioning
			arrow.css('left', arrowLeft + 'px');
			arrow.css('top', arrowTop + 'px');
			window.css('left', windowLeft + 'px');
			window.css('top', windowTop + 'px');
		},

		center: function(window, offsetX, offsetY) {
			log('ESTORE.Misc.centerInterstitial', arguments);
			var windowWidth = $(window).width();
			var interstitial = $(window);
			if(interstitial.length > 0) {
				var interstitialWidth = interstitial.width();
				var left = Math.round((windowWidth/2) - (interstitialWidth/2));
				if(offsetX != 0) {
					left += offsetX;
				}
				interstitial.css('margin-left', 0);
				interstitial.css('left', left);
				if(offsetY != 0) {
					interstitial.css('top', offsetY);
				}
			}
		},

		destroy : function(){
			this._super();
		}
	});

/*
 * @class Sym.Button
 */
$.Controller.extend("Sym.Button",
	/* @Static */
	{
		defaults : {
			hoverClass: 'sym_button_hover',
			hoverSuffix: '_ro',
			disableClass: 'sym_button_disabled',
			disableSuffix: '_grey'
		}
	},
	/* @Prototype */
	{
		init : function(el, options){
			this.setButtonSrc(this.element.attr('src'));

			this.subscribe();
		},

		changeType : function(type, suffix){
			if(this.element.hasClass(type)){
				this.src = this.filename + this.ext;
			} else {
				this.src = this.filename + suffix + this.ext;
			}

			this.element.toggleClass(type);
			this.element.attr('src', this.src);
		},

		setButtonSrc: function(url) {
			this.element.attr('src', url);
			this.src = url;
			this.filename = this.src.substr(0, this.src.lastIndexOf('.'));
			this.ext = this.src.substr(this.src.lastIndexOf('.'), this.src.length);
		},

		hover : function(){
			this.changeType(this.options.hoverClass, this.options.hoverSuffix);
		},

		disable : function(){
			this.changeType(this.options.disableClass, this.options.disableSuffix);
		},

		destroy : function(){
			this.element
				.removeClass(this.options.hoverClass)
				.removeClass(this.options.disableClass);

			this._super();
		},

		subscribe : function() {
			//listen for a Button/{id}/disable message that will disable a button with id = {id}
			$.subscribe("/Button/" + this.element.attr('id') + "/disable", $.proxy(function() {
				this.disable();
			}, this));
		},

		"mouseenter" : function(el, ev){
			log('enter');
			this.hover();
		},

		"mouseleave" : function(el, ev){
			log('leave');
			this.hover();
		}
	});

/**
 * @class
 * CSS3 button
 * Allows "buttons" to be enabled and disabled
 */
$.Controller.extend("Sym.mButton",
	/* @static */
	{
		defaults : {
			disabledClass: "disabledButton"
		}
	},
	/* @prototype */
	{
		/**
		 * Store the href and onClick attributes of a button
		 *
		 * @param {Object} el jQuery object
		 * @param {Object} [options=this.static.deafults] Object containing controller options
		 */
		init : function(el, options){

			/* Saves the href and onClick of a button */
			this.href = this.element.attr("href");
			this.onClick = this.element.attr("onClick"); //onClick = string representation of onClick attribute
			//TODO: Do we need to unbind all other events and then rebind them?

			this.subscribe();

			//If this button is replaced, destroy this instance
			var thiz = this;
			this.element.one("wicketAjaxReplace", $.proxy(function() {
				thiz.destroy()
			}, thiz));

		},

		/**
		 * determine if the button enabled. true for enabled, false for disabled
		 */
		isEnabled : function () {
			return !this.element.hasClass(this.options.disabledClass);
		},

		/**
		 * Restore href and onClick attributes of a button
		 * Remove the disabled class
		 */
		enable : function() {

			this.element.attr("onclick", this.onClick)
			this.element.attr("href", this.href);
			this.element.removeClass(this.options.disabledClass);

		},

		/**
		 * Remove href and onClick attributes
		 * Add the disabled class
		 */
		disable : function() {

			this.element.removeAttr("href");
			this.element.removeAttr("onclick");
			this.element.addClass(this.options.disabledClass);

		},

		/**
		 * Listen for disableAll and enableAll messages
		 */
		subscribe : function() {

			/* listen for a Button/disableAll message that will disable all buttons */
			$.subscribe("/Button/disableAll", $.proxy(function() {
				this.disable();
			}, this));

			/* listen for a Button/enableAll message that will enable all buttons */
			$.subscribe("/Button/enableAll", $.proxy(function() {
				this.enable();
			}, this));

		}
	});

/*
 * @class Sym.Button
 */
$.Controller.extend("Sym.HeaderDropDown",
	/* @Static */
	{
		defaults : {
			linkClass: "dropDown"
		}
	},
	/* @Prototype */
	{
		init : function(el, options){

			//remove any inline onmouseover/onmouseout from wicket
			this.element.attr('onMouseout', "");
			this.element.attr('onMouseover', "");

			this.link = $('.dropDownLink', this.element);
			this.pane = $('.dropDownPane', this.element);
			this.border = $('.dropDownTopBorder', this.element);
		},

		show: function(){



			this.link
				.addClass(this.options.linkClass + "ON")
				.removeClass(this.options.linkClass);
			//position everything
			this.pane.css({
				top: "33px",
				visibility: "visible"
			});
			this.border.margin({left: (this.link.width() + this.link.padding().left + this.link.padding().right + this.link.margin().left + this.link.margin().right) - 1});

			if (Sym.Global.IEVersion == 6) {
				this.iframe = $("#" + this.options.iframeId);
				if (this.iframe.length == 0) {
					this.iframe = $('body').append('<iframe id="csDropDownIframe" src="' + Sym.CONST.IFRAME_SRC + '" frameborder="0" scrolling="no" style="z-index:400; position:absolute; top: -1000px; left: -9999px; visibility: hidden;"></iframe>');
				}
				var pos = this.pane.offset();
				this.iframe.css({
					left: pos.left + "px",
					width: this.pane.width() + this.pane.padding().left + this.pane.padding().right + this.pane.margin().left + this.pane.margin().right + "px",
					top: pos.top + "px",
					height: this.pane.width() + this.pane.padding().top + this.pane.padding().bottom + this.pane.margin().top + this.pane.margin().bottom + "px",
					visibility: "visible"
				});
			}
		},

		hide: function() {
			log('hide');
			this.link
				.removeClass(this.options.linkClass + "ON")
				.addClass(this.options.linkClass);
			this.pane.css("visibility", "hidden");
			if (Sym.Global.IEVersion == 6) {
				this.iframe.css("visibility", "hidden");
			}
		},

		destroy : function(){
			this.iframe.remove();
			this._super();
		},

		"mouseover" : function(el, ev){

			this.show();
		},

		"mouseout" : function(el, ev){

			this.hide();
		}
	});


/* ESTORE Stuff Here */

Sym.LiveChat = {
	send:function (){
		if( typeof lpAddVars !== "undefined" ){
			if (typeof window.pagename !== "undefined") {

				if (window.pagename.indexOf("interstitial_cart") > 0) {
					// For interstitial Page - send only iproducts and offered_sku_desc
					if (typeof window.iproducts !== "undefined") {
						lpAddVars('page', 'iproducts', window.iproducts);
						debug(window.iproducts);
					}

					if (typeof window.offered_sku_desc_int !== "undefined") {
						lpAddVars('page', 'offered_sku_desc_int', window.offered_sku_desc_int);
						debug(window.offered_sku_desc_int);
					}
					return;
				}


				if (window.pagename.indexOf(":cart:cart") == -1) {
					// For Cart Page - do not  reset  iproducts and offered_sku_desc only on pages other than cart

					debug("lpAddVars: iproducts");
					if (typeof window.iproducts !== "undefined") {
						lpAddVars('page', 'iproducts', window.iproducts);
						debug(window.iproducts);
					}
					else {
						// Reset this variable
						lpAddVars('page', 'iproducts', "na");
						debug("na");
					}

					debug("lpAddVars: offered_sku_desc_int");
					if (typeof window.offered_sku_desc_int !== "undefined") {
						lpAddVars('page', 'offered_sku_desc_int', window.offered_sku_desc_int);
						debug(window.offered_sku_desc_int);
					}
					else {
						lpAddVars('page', 'offered_sku_desc_int', "na");
						debug("na");
					}

				}
			}

			debug("lpAddVars: traffic_source");
			if(typeof window.traffic_source !== "undefined"){
				lpAddVars('session','traffic_source',window.traffic_source);
				debug(window.traffic_source);
			}
			debug("lpAddVars: current_subchannel");
			if(typeof window.current_subchannel !== "undefined"){
				lpAddVars('session','current_subchannel',window.current_subchannel);
				debug(window.current_subchannel);
			}
			debug("lpAddVars: language");
			if(typeof window.language !== "undefined"){
				lpAddVars('session','language',window.language);
				debug(window.language);
			}
			debug("lpAddVars: site_id");
			if(typeof window.site_id !== "undefined") {
				lpAddVars('session','site_id',window.site_id);
				debug(window.site_id);
			}
			debug("lpAddVars: country");
			if(typeof window.country !== "undefined") {
				lpAddVars('session','country',window.country);
				debug(window.country);
			}
			debug("lpAddVars: pagename");
			if(typeof window.pagename !== "undefined"){
				lpAddVars('page','pagename',window.pagename);
				debug(window.pagename);
			}
			debug("lpAddVars: incoming_productSKU");
			if(typeof window.incoming_productSKU !== "undefined"){
				lpAddVars('session','incoming_productSKU',window.incoming_productSKU);
				debug(window.incoming_productSKU);
			}
			debug("lpAddVars: nss_security_vendor");
			if(typeof window.nss_security_vendor !== "undefined") {
				lpAddVars('session','nss_security_vendor',window.nss_security_vendor);
				debug(window.nss_security_vendor);
			}
			debug("lpAddVars: number_of_renewals");
			if(typeof window.number_of_renewals !== "undefined") {
				lpAddVars('session','number_of_renewals',window.number_of_renewals);
				debug(window.number_of_renewals);
			}
			debug("lpAddVars: session_guid");
			if(typeof window.session_guid !== "undefined"){
				lpAddVars('session','session_guid', window.session_guid);
				debug(window.session_guid);
			}
			debug("lpAddVars: total_cart_value");
			if(typeof window.total_cart_value !== "undefined"){
				lpAddVars('page','total_cart_value',window.total_cart_value);
				debug(window.total_cart_value);
			}
			debug("lpAddVars: ucproduct");
			if(typeof window.ucproducts !== "undefined") {
				lpAddVars('page', 'ucproduct', window.ucproducts);
				debug(window.ucproducts);
			} else {
				lpAddVars('page', 'ucproduct', "na");
				debug("na");
			}
			debug("lpAddVars: total_order_value");
			if(typeof window.total_order_value !== "undefined") {
				lpAddVars('page', 'total_order_value', window.total_order_value);
				debug(window.total_order_value);
			}
			debug("lpAddVars: offered_sku_desc");
			if(typeof window.offered_sku_desc !== "undefined"){
				lpAddVars('page','offered_sku_desc',window.offered_sku_desc);
				debug(window.offered_sku_desc);
			} else {
				lpAddVars('page','offered_sku_desc','na');
				debug('na');
			}
			debug("lpAddVars: incoming_sku_desc");
			if(typeof window.incoming_sku_desc !== "undefined") {
				lpAddVars('page', 'incoming_sku_desc', window.incoming_sku_desc);
				debug(window.incoming_sku_desc);
			}
			debug("lpAddVars: purchaseid");
			if(typeof window.purchaseid !== "undefined") {
				lpAddVars('page','purchaseid', window.purchaseid);
				debug(window.purchaseid);
			}
			debug("lpAddVars: internal_search_keywords");
			if(typeof window.internal_search_keywords !== "undefined") {
				lpAddVars('page', 'internal_search_keywords', window.internal_search_keywords);
				debug(window.internal_search_keywords);
			}
			debug("lpAddVars: internal_search_num_results");
			if(typeof window.internal_search_num_results !== "undefined") {
				lpAddVars('page', 'internal_search_num_results', window.internal_search_num_results);
				debug(window.internal_search_num_results);
			}
			debug("lpAddVars: om_affiliate_id_param");
			if(typeof window.om_affiliate_id_param !== "undefined") {
				lpAddVars('session', 'om_affiliate_id_param', window.om_affiliate_id_param);
				debug(window.om_affiliate_id_param);
			}
			debug("lpAddVars: om_sem_site_param");
			if(typeof window.om_sem_site_param !== "undefined") {
				lpAddVars('session', 'om_sem_site_param', window.om_sem_site_param);
				debug(window.om_sem_site_param);
			}
			debug("lpAddVars: om_sem_kw_param");
			if(typeof window.om_sem_kw_param !== "undefined") {
				lpAddVars('session', 'om_sem_kw_param', window.om_sem_kw_param);
				debug(window.om_sem_kw_param);
			}
			debug("lpAddVars: partner");
			if(typeof window.partner !== "undefined") {
				lpAddVars('session', 'partner', window.partner);
				debug(window.traffic_source);
			}
			// See function sendErrortoLivePersonChat for billingerror variable.
			debug("lpAddVars: billingerror");
			lpAddVars('page','billingerror',false);
			debug(false);
		}
	}
}

//TODO: Create WicketSpinner Controller
Sym.WicketSpinner = {
	/**
	 * Shows a mask and a spinner over the element with the specified id
	 *
	 * @param String targetId - id of target element
	 */
	show: function(targetId){
		log('Sym.WicketSpinner.show', arguments);
		if ($('#wicket_spinner_' + targetId).length == 0) {
			$('body').append("<div class='wicket-spinner' style='cursor: not-allowed; z-index: 6000;' id='wicket_spinner_" + targetId + "'><img src='//buy-static.norton.com/estore/images/en/Non-Product/Misc/arw_spin_40x40.gif'/></div>");
		}
		$('#wicket_spinner_' + targetId).sym_interstitial({
			modal: true,
			overlay: 0
		}).sym_interstitial('show');
	},
	/**
	 * Hides the mask
	 *
	 * @param String targetId - id of target element
	 */
	hide: function(targetId){
		log('Sym.WicketSpinner.hide', arguments);
		$('#wicket_spinner_' + targetId).sym_interstitial('hide');
	}
};

//TODO: Find a new place for expand/collapse
Sym.LeftNav = {
	/**
	 * Collapse the Category List
	 *
	 * @param Object divId - id of div to collapse
	 */
	collapseCategoryList: function(divId){
		log('Sym.LeftNav.collapseCategoryList', arguments);
		$("#"+divId).css({'visibility' : 'hidden', 'display' : 'none'});
	},
	/**
	 * Expand the Category List
	 *
	 * @param Object divId - id of div to expand
	 */
	expandCategoryList: function(divId){
		log('Sym.LeftNav.expandCategoryList', arguments);
		$("#"+divId).css({'visibility' : 'visible', 'display' : 'block'});
	},
	//TODO: Create Flyout Controller/Use a good flyout plugin for this
	Flyout: {
		FOInit: false,
		obj: null,
		iframeObj: null,
		/**
		 * Initialize the Left Nav Flyout
		 */
		init: function(){
			log('Sym.LeftNav.Flyout.init', arguments);
			if (Sym.Global.IEVersion == 6) {
				Sym.LeftNav.Flyout.iframeObj = $('#flyoutIframe');
			}
			Sym.LeftNav.Flyout.obj = $('#flyoutNav');
			if (Sym.Global.IEVersion == 6) {
				if ($(Sym.LeftNav.Flyout.iframeObj).length == 0) {
					$('body').prepend('<iframe id="flyoutIframe" src="' + Sym.CONST.IFRAME_SRC + '" frameborder="0" scrolling="no"></iframe>');
					Sym.LeftNav.Flyout.iframeObj = $('#flyoutIframe');
				}
			}
			if ($(Sym.LeftNav.Flyout.obj).length == 0) {
				var content = $('#lftNavHflyoutMenu ul').html();
				$('body').prepend('<div id="flyoutNav"><ul>' + content + '</ul></div>');
				Sym.LeftNav.Flyout.obj = $('#flyoutNav');
			}
			$('#lftNavHflyoutMenu ul').remove();
			Sym.LeftNav.Flyout.FOInit = true;
		},
		/**
		 * Initialize the Left Nav Flyout Events
		 */
		eventInit: function(){
			log('Sym.LeftNav.Flyout.eventInit', arguments);
			Sym.LeftNav.Flyout.init();
			$('#flyoutNav li').hover(function(){
				$(this).addClass('iehover');
			}, function(){
				$(this).removeClass('iehover');
			});
			$('#lftNavHflyoutMenu li, #flyoutNav').hover(function(){
				Sym.LeftNav.Flyout.show();
			}, function(){
				Sym.LeftNav.Flyout.hide();
			});
		},
		/**
		 * Show the Left Nav Flyout
		 */
		show: function(){
			log('Sym.LeftNav.Flyout.show', arguments);
			if (!Sym.LeftNav.Flyout.FOInit) {
				Sym.LeftNav.Flyout.init();
			}
			$('#lftNavHflyoutMenu li').addClass('iehover');
			Sym.LeftNav.Flyout.obj.css('left', $('#lftNavHflyoutMenu li a.parent').offset().left + $('#lftNavHflyoutMenu').width());
			Sym.LeftNav.Flyout.obj.css('top', $('#lftNavHflyoutMenu li a.parent').offset().top);
			Sym.LeftNav.Flyout.obj.css('display', 'block');
		},
		/**
		 * Hide the Left Nav Flyout
		 */
		hide: function(){
			log('Sym.LeftNav.Flyout.hide', arguments);
			if (!Sym.LeftNav.Flyout.FOInit) {
				Sym.LeftNav.Flyout.init();
			}
			$('#lftNavHflyoutMenu li').removeClass('iehover');
			Sym.LeftNav.Flyout.obj.css('display', 'none');
		}
	}
};

Sym.CLP = {
	UpdateLink: {
		/**
		 * Sets image link, more info and buy now link depending on year selected
		 */
		subFrm: function(thiz, colIndex, yr, val){
			log('Sym.CLP.UpdateLink.subFrm', arguments);
			var $inputs = $('#prod' + val + ' :input:checked');
			var ImgRow = $("#staticOptionTable").find("tr").filter(':nth-child(2)');
			var ImgCell = ImgRow.find("td").filter(':nth-child(' + colIndex + ')')

			var linkRow = $("#staticOptionTable").find("tr").filter(':nth-child(3)');
			var linkCell = linkRow.find("td").filter(':nth-child(' + colIndex + ')')

			var btnRow = $("#staticOptionTable").find("tr").filter(':nth-child(4)');
			var btnCell = btnRow.find("td").filter(':nth-child(' + colIndex + ')')

			$inputs.each(function(){

				if (yr == 2) {
					ImgCell.find("div.ProductImage").filter(':nth-child(2)').css("display", "none");
					ImgCell.find("div.ProductImage").filter(':nth-child(1)').css("display", "");
					linkCell.find("a.moreInfo").filter(':nth-child(2)').css("display", "none");
					linkCell.find("a.moreInfo").filter(':nth-child(1)').css("display", "");

					btnCell.find("div.divBuyActionLinkRadio").filter(':nth-child(2)').css("display", "none");
					btnCell.find("div.divBuyActionLinkRadio").filter(':nth-child(1)').css("display", "");

				}
				else {
					ImgCell.find("div.ProductImage").filter(':nth-child(2)').css("display", "");
					ImgCell.find("div.ProductImage").filter(':nth-child(1)').css("display", "none");
					linkCell.find("a.moreInfo").filter(':nth-child(2)').css("display", "");
					linkCell.find("a.moreInfo").filter(':nth-child(1)').css("display", "none");

					btnCell.find("div.divBuyActionLinkRadio").filter(':nth-child(2)').css("display", "");
					btnCell.find("div.divBuyActionLinkRadio").filter(':nth-child(1)').css("display", "none");

				}
			});
		}
	},
	TwoCol: {
		/**
		 * Sets image link, more info and buy now link depending on year selected
		 */
		subFrm: function(val, thiz){
			log('sym.CLP.TwoCol.subFrm', arguments);
			var $inputs = $('#prod' + val + ' :input:checked');
			var tmp = $(this);
			$inputs.each(function(){
				par = $(this).parent()
				yr = "2";
				if ($(par).html().indexOf("1 Year") > -1) {
					yr = "1";
				}
				ref = eval('twoCol.prod' + val + '_' + yr + 'year');
				$(thiz).parent().find("a").attr("href", ref);
			});
		},
		prod0_2year: $("#SObuyProduct_0_01").find("a").attr('href'), //"http://buy.norton.com/ps?ctry=US&lang=en&selSKU=20032918&tppc=76101b0f310d3a8acc6269d51c3f7f2d",
		prod0_1year: $("#SObuyProduct_0_11").find("a").attr('href'), //"http://buy.norton.com/ps?ctry=US&lang=en&selSKU=20032932&tppc=76101b0f310d3a8acc6269d51c3f7f2d",
		prod1_2year: $("#SObuyProduct_1_01").find("a").attr('href'), //"http://buy.norton.com/ps?ctry=US&lang=en&selSKU=20032950&tppc=76101b0f310d3a8acc6269d51c3f7f2d",
		prod1_1year: $("#SObuyProduct_1_11").find("a").attr('href'), //"http://buy.norton.com/ps?ctry=US&lang=en&selSKU=20032905&tppc=76101b0f310d3a8acc6269d51c3f7f2d",
		prod2_2year: $("#SObuyProduct_2_01").find("a").attr('href'), //"http://buy.norton.com/ps?ctry=US&lang=en&selSKU=20004532&tppc=76101b0f310d3a8acc6269d51c3f7f2d",
		prod2_1year: $("#SObuyProduct_2_11").find("a").attr('href') //"http://buy.norton.com/ps?ctry=US&lang=en&selSKU=20004233&tppc=76101b0f310d3a8acc6269d51c3f7f2d"
	}
};

//TODO: What calls this? Can this be added into a TeaLeaf Controller?
Sym.CollectAndSendTT = {
	/**
	 * This combs the order confirmation page and grabs some key values to be sent back to test and target
	 */
	getData: function(pageName){
		log('Sym.CollectAndSendTT.getData', arguments);
		var orderNum = $(".orderNumberConfirmKey").html();
		var prods = new Array();
		var row = $('#tblCart').find('tr').each(function(){
			;
			if ($(this).find('.spanProdTitle')) {
				var txt = $(this).find('.spanProdTitle').find('strong').html();
				if (txt != null) {
					prods.push(txt);
				}
			}
			if ($(this).find('.bundleProdTitle')) {
				var txt = $(this).find('.bundleProdTitle').find('strong').html();
				if (txt != null) {
					prods.push(txt);
				}
			}
		});
		var originalTotal = $('#tblCart').find('.big').html();
		var prodIds = "|" + originalTotal + "|" + prods.join(";");
		if(typeof window.mboxCreate == 'function') {
			mboxCreate(pageName, 'productPurchasedId=' + prodIds, 'orderTotal=' + window.total_order_value, 'orderId=' + orderNum);
		}
	}
};

//TODO: Optimize this code? -> Look at BP Page
Sym.PaymentOptions = {
	showHide : function(){
		log('Sym.PaymentOptions.showHide', arguments);
		var ccVal = $('#orderPayment\\.cardType :selected').text();

		Sym.PaymentOptions.hideAll();
		if (ccVal == 'AMEX' || ccVal == 'Amex' || ccVal == 'American Express')      {
			Sym.PaymentOptions.amex();
		} else if (ccVal == 'Maestro'){
			Sym.PaymentOptions.maestro();

		}
	},

	amex : function(){
		log('Sym.PaymentOptions.amex', arguments);
		var blk = $("#orderPayment\\.cardType").position();
		$('#divAmexMsg').css('position', 'absolute');
		$('#divAmexMsg').css('top', blk.top - 11);
		$('#divAmexMsg').css('left', blk.left + 155);

		$('#divAmexMsg').css('display', 'block');
	},

	maestro : function(){
		log('Sym.PaymentOptions.maestro', arguments);
		var blk = $("#orderPayment\\.cardType").position();
		$('#divMaestroMsg').css('position', 'absolute');
		$('#divMaestroMsg').css('top', blk.top - 11);
		$('#divMaestroMsg').css('left', blk.left + 155);

		$('#divMaestroMsg').css('display', 'block');
	},

	hideAll : function(){
		log('Sym.PaymentOptions.hideAll', arguments);
		$('#divAmexMsg').css('display', 'none');
		$('#divMaestroMsg').css('display', 'none');
	}
};

Sym.Misc = {
	// Find page load time
	//TODO: Is this only an internally used function, can we add this to a TeaLeaf Controller?
	pageloadingtime: function() {
		log('Sym.Misc.pageloadingtime', arguments);
		afterload = (new Date()).getTime();
		secondes = Math.round(((afterload-Sym.Global.beforeLoad)/1000)*Math.pow(10,3))/Math.pow(10,3);
		return secondes;
	},
	/**
	 * Collapse a product column on the compare products popup
	 *
	 * @param Object obj - arrow object
	 */
	collapseCol: function(obj){
		log('Sym.Misc.collapseCol', arguments);
		var $td = $(obj).parent();
		var $tr = $td.parent();
		var $table = $tr.parent();
		var col = 0;
		var col2 = 0;
		var counter = 0;
		//sets the column index to toggle
		$tr.find('td').each(function(){
			if ($(this).html() === $td.html()) {
				col2 = col;
			}
			col++;
		});
		$table.find('tr').each(function(){
			if (counter == 1) {
				$(this).find('td:eq(' + col2 + ')').each(function(){

					if ($(this).find('div').is('.mainProdImg')) {
						$(".mainProdImg").parent().css("height", $("#div_nr_ProdImg").parent().height()); //this set height of the cell, to avoid dislocation of buy now div.
						$(this).find('.mainProdImg').prevAll().toggle();
						$(this).find('.mainProdImg').nextAll().toggle().filter(".spanTaxesMsg").toggle();
						return true;
					}
					else {
						$(this).children().toggle();
					}
				});
			}
			else {
				var tempcol2 = col2 + 1;
				$(this).find('td:eq(' + tempcol2 + ')').each(function(){
					$(this).find('div').each(function(){
						if ($(this).attr('class') == "col1")
							$(this).removeClass('col1').addClass('col0');
						else
							$(this).removeClass('col0').addClass('col1');
					});
					$(this).children().toggle();
				});
			}
			counter++;
		});
	},
	/**
	 * Toggle Shipping Address Area
	 * Still used on BP Page, but functionality is gone right?
	 */
	//TODO: Verify that we need the shipping inputs anymore, remove if this feature is gone
	toggleShippingAddress: function(){
		log('Sym.Misc.toggleShippingAddress', arguments);
		if (document.getElementById("divShippingAddress").style.display == "block") {
			document.getElementById("divShippingAddress").style.display = "none";
			document.getElementById("divShippingPnl").style.display = "none";
			document.getElementById("isShippingSame").checked = true;
		}
		else {
			document.getElementById("divShippingAddress").style.display = "block";
			document.getElementById("divShippingPnl").style.display = "block";
			document.getElementById("isShippingSame").checked = false;
		}
		if ($(document).getUrlParam('id') == 5) {
			document.getElementById("divShippingPnl").style.display = "block";
		}
	},
	/**
	 * Show appropriate card inputs
	 *
	 * @param String creditcard - card type
	 */
	//TODO: Add class to all credit card inputs so we can optimize the show/hide
	updateCreditCard: function(creditcard){
		log('Sym.Misc.updateCreditCard', arguments);
		var $maestro_input = $('#maestro_input');
		var $visa_input = $('#visa_input');
		var $cardSecurityCodeAmex = $('#cardSecurityCodeAmex');
		var $cardSecurityCodeVisa = $('#cardSecurityCodeVisa');
		var $cardSecurityCodeLabel = $('#cardSecurityCodeLabel');
		var $cardSecurityCodeInput = $('#cardSecurityCodeInput');
		if (creditcard == 'visa') {
			// Hide others
			$maestro_input.css('display', 'none');
			$cardSecurityCodeAmex.css('display', 'none');
			// Show the selected one
			$visa_input.css('display', 'block');
			$cardSecurityCodeVisa.css('display', 'inline');
			$cardSecurityCodeLabel.css('display', 'inline');
			$cardSecurityCodeInput.css('display', 'inline');
		}
		else if (creditcard == 'amex') {
			// Hide others
			$maestro_input.css('display', 'none');
			$cardSecurityCodeVisa.css('display', 'none');
			// Show the selected one
			$visa_input.css('display', 'block');
			$cardSecurityCodeAmex.css('display', 'inline');
			$cardSecurityCodeLabel.css('display', 'inline');
			$cardSecurityCodeInput.css('display', 'inline');
		}
		else if (creditcard == 'maestro') {
			// Hide others
			$visa_input.css('display', 'none');
			$cardSecurityCodeAmex.css('display', 'none');
			$cardSecurityCodeVisa.css('display', 'none');
			$cardSecurityCodeLabel.css('display', 'none');
			$cardSecurityCodeInput.css('display', 'none');
			// Show the selected one
			$maestro_input.css('display', 'none');
		}
	},
	/**
	 * Find position of object on screen
	 *
	 * @param Object obj - object to find position of (Native or jQuery)
	 */
	//TODO: Remove this call in each function with the offset() function
	findPos: function(obj){
		log('Sym.Misc.findPos', arguments);
		if( typeof(obj) !== "undefined" ) {
			return [$(obj).offset().left, $(obj).offset().top];
		}
		return [0, 0];
	},
	/**
	 * Clears Coupon Field
	 *
	 * @param Object: obj - input field
	 */
	checkCoupon: function(obj){
		log('Sym.Misc.checkCoupon', arguments);
		if (obj.value == "optional") {
			$("input.inputCartValues").val("");
		}
	},
	//TODO: Create a Hideable Controller (shows something, hides it on a certain event)
	loadingPageAnimation: {
		/**
		 * Show loading page animation
		 */
		show: function(){
			log('Sym.Misc.loadingPageAnimation.show', arguments);
			$('.loadingPageOverlay').show();
		},
		/**
		 * Hide loading page animation
		 */
		hide: function(){
			log('Sym.Misc.loadingPageAnimation.hide', arguments);
			$('.loadingPageOverlay').hide();
		}
	},
	/**
	 * Show tabbed content
	 *
	 * String tabid - id of the tab
	 * String divid - id of the div to show
	 * String id - id of tooltip (optional)
	 */
	//TODO: Create Tabs Controller
	showTab: function(tabid, divid, id){
		log('Sym.Misc.showTab', arguments);
		$.publish("/MessageBubble/hideAll");
		$('.divTabs').css('display', 'none');
		$('.activeTab').attr('class', 'inactiveTab');
		$('#' + tabid).attr('class', 'activeTab');
		$('#' + divid).css('display', 'block');

		// For SPARK-37100 hiding checkbox for Boleto in Boleto tab 
		if(('body.BR').length>0){
			if($('#tabBank').hasClass("activeTab") && $('.paymentBoletoMessage').length>0){

				$('.positionFutureCheckBox').hide();
			}
			else
			{
				$('.positionFutureCheckBox').css("display","block");
			}
		}
	},
	/**
	 * Show tabbed content
	 *
	 * String tab - id of the tab
	 * String name - name of current panel
	 */
	//TODO: Roll into Tabs Controller
	showTabbedPanel: function(tab, name){
		log('Sym.Misc.showTabbedPanel', arguments);
		var panels = new Array('pnlKeyFeatures', 'pnlSysReq');
		$.publish("/MessageBubble/hideAll");
		$('.activeTab').attr('class', 'inactiveTab');
		$('#' + tab).attr('class', 'activeTab');
		if (name == panels[0]) {
			$('#' + panels[0]).css('display', 'block');
			$('#' + panels[1]).css('display', 'none');
		}
		else {
			$('#' + panels[1]).css('display', 'block');
			$('#' + panels[0]).css('display', 'none');
		}
	},
	/**
	 * Disables a link by hiding it and displaying a wait message
	 *
	 * @param Object obj - link object
	 */
	//TODO: Verify this on common\panel\OrderReviewCompleteOrderPanel.html and figure out if we need this
	disableLink: function(obj){
		log('Sym.Misc.disableLink', arguments);
		obj.style.display = "none";
		var waitMessageObj = document.all ? document.all["waitMessage"] : document.getElementById ? document.getElementById("waitMessage") : "";
		waitMessageObj.style.display = "block";
	},
	/**
	 * Toggles the visibility of the download popup
	 */
	//TODO: Put this in the Interstitial Controller?
	toggleDownload: function(){
		log('Sym.Misc.toggleDownload', arguments);
		if (document.getElementById('divContent').style.display == 'none') {
			document.getElementById('divContent').style.display = 'block';
		}
		else {
			document.getElementById('divContent').style.display = 'none';
		}
	},
	/**
	 * Minifies/Maximizes the download popup
	 */
	//TODO: Put this in the Interstitial Controller?
	minDownload: function(){
		log('Sym.Misc.minDownload', arguments);
		var $overlay = $('.jqmOverlay');
		if ($overlay.css('display') == 'block') {
			$overlay.css('display', 'none');
		}
		else {
			$overlay.css('display', 'block');
		}
	},
	openPopup: function(url, width, height, toolbar, menubar, scrollbars, resizeable, location, directories, status, x, y){
		log('Sym.Misc.openPopup', arguments);
		popupWindow = window.open(url, '', 'width=' + width + ',height=' + height + 'toolbar=' + toolbar + ',menubar=' + menubar + ',scrollbars=' + scrollbars + ',resizable=' + resizeable + ',location=' + location + ',directories=' + directories + ',status=' + status + ',left=' + x + ',top=' + y);
		popupWindow.focus();
		return false;
	}
};

Sym.Input = {
	/**
	 * Disable the context menu of an element
	 *
	 * @param String id - id of element
	 */
	disableContextMenu: function(id){
		log('Sym.Input.disableContextMenu', arguments);
		$('#' + id).bind("contextmenu", function(e){
			return false;
		});
	},
	/**
	 * Disable paste functionality inside an element
	 *
	 * @param (Object) event - JS event
	 */
	disablePaste: function(event){
		log('Sym.Input.disablePaste', arguments);
		if (event.keyCode == 45) {
			ctrlPressed = false;
			return false;
		}
		if (event.ctrlKey == 1 && event.altKey == 1) {
			return true;
		}
		else if (event.ctrlKey == 1 && event.keyCode > 48) {
			ctrlPressed = false;
			return false;
		}
	},
	/**
	 * Check for optional value
	 *
	 * @param Object input - input we are checking
	 */
	//TODO: Check common\panel\CustomerProfileInfoPanel.html and see if we can remove this
	checkOpt: function(input){
		log('Sym.Input.checkOpt', arguments);
		if (input.value == ' optional') {
			javascript: input.value = '';
		}
		input.style.color = '#666';
	},
	clear: function(inputBox){
		log('ESTORE.Input.clear', arguments);
		if ($.trim(inputBox.defaultValue) == $.trim(inputBox.value)) {
			inputBox.value = "";
		}
	},
	/**
	 * Set value of an input box if it is empty
	 *
	 * @param Object inputBox - input we are clearing
	 * @param String inputDefault - default string to be populated into input
	 */
	write: function(inputBox, inputDefault){
		log('ESTORE.Input.write', arguments);
		if (inputBox.value == "") {
			inputBox.value = inputDefault;
		}
	}
};

Sym.Interstitial = {
	/**
	 * Clear Error bubbles
	 */
	//TODO: Look at the EditPanels on OR page to optimize/remove this, can this move to the Interstitial Controller
	onKeyPressCancel: function(){
		log('Sym.Interstitial.onKeyPressCancel', arguments);
		if(event.keyCode==13){
			$(this).trigger('click');
		}
	},
	/**
	 * Clear Error bubbles
	 */
	//TODO: Look at the EditPanels on OR page to optimize/remove this, can this move to the Interstitial Controller
	onKeyPressUpdate: function(event){
		console.log ('Sym.Interstitial.onKeyPressUpdate', arguments);
		if(event.keyCode!= null && event.keyCode!= "undefined")
		{
			if(event.keyCode==13){
				$(this).trigger('click');
			}
		}
	},
	/* SITE-10290: Add interstitial function for TnT */
	//TODO: Create a pub/sub for TnT
	ttCartInterstitial: function(){
		log('Sym.Interstitial.ttCartInterstitial', arguments);
		//Empty function for TnT to use to manipulate Cart interstitial
	},
	/**
	 * Show the cart interstitial
	 */
	//TODO: Move this into the Cart page

	/**
	 * Show the download interstitial
	 */
	//TODO: Move this into the OC pages
	DownloadInterstitial: function(){
		log('Sym.Interstitial.DownloadInterstitial', arguments);
		var open = function(hash){
			hash.w.fadeIn('2000', function(){
				hash.o.show();
			});
		};
		$('#downloadInterstitial').sym_interstitial({
			ajax: Sym.popupVars.download,
			modal: true,
			trigger: false,
			onShow: open
		}).sym_interstitial('show');
	},
	/**
	 * Show the IB Payment Details Edit Popup
	 */
	//TODO: Move this into the OR page
	IBPaymentDetailsEditPopup: function(){
		log('Sym.Interstitial.IBPaymentDetailsEditPopup', arguments);
		$('#editOnlineBank').sym_interstitial({
			ajax: Sym.popupVars.IBEditPayment,
			modal: true,
			positionEdit: {
				triggerID: "edit_onlineBank",
				windowID: "editOnlineBank"
			},
			trigger: false
		}).sym_interstitial('show');
	},
	/**
	 * Show the IB Payment Error Popup
	 */
	//TODO: Move this into the OR page
	IBPaymentDetailsErrorPopup: function(){
		log('Sym.Interstitial.IBPaymentDetailsErrorPopup', arguments);
		$('#showError').sym_interstitial({
			ajax: Sym.popupVars.IBPaymentError,
			modal: true,
			trigger: false
		}).sym_interstitial('show');
	},
	/**
	 * Show the Online Payment Details Edit Popup
	 */
	//TODO: Move this into the OR page
	OnlinePaymentPanelEditPopup: function(){

		log('Sym.Interstitial.OnlinePaymentPanelEditPopup', arguments);
		if($("#abbForm").length>0){
			$('#editPayment1').sym_interstitial({
				ajax: Sym.popupVars.onlinePaymentEdit,
				modal: true,
				center: {
					window: "editPayment1"
				},
				trigger: false
			}).sym_interstitial('show');
		}else{
			$('#editPayment1').sym_interstitial({
				ajax: Sym.popupVars.onlinePaymentEdit,
				modal: true,
				positionEdit: {
					triggerID: "edit_payment",
					windowID: "editPayment1"
				},
				trigger: false
			}).sym_interstitial('show');}

		if ($("#paymentPanelMainContainer").length === 1) {
			$(".bblRgtArrowSml").hide();
		}

	},


	/**
	 * Show the Online Payment Errot Popup
	 */
	//TODO: Move this into the OR page
	OnlinePaymentPanelErrorPopup: function(){
		log('Sym.Interstitial.OnlinePaymentPanelErrorPopup', arguments);
		$('#showError').sym_interstitial({
			ajax: Sym.popupVars.onlineOrderError,
			modal: true,
			trigger: false
		}).sym_interstitial('show');
	},
	/**
	 * Show the Online Direct Debit Details Edit Popup
	 */
	//TODO: Move this into the OR page
	OnlineDirectDebitPanelEditPopup: function(){
		log('Sym.Interstitial.OnlineDirectDebitPanelEditPopup', arguments);
		$('#editDirectDebit').sym_interstitial({
			ajax: Sym.popupVars.directDebitEdit,
			modal: true,
			positionEdit: {
				triggerID: "edit_directDebit",
				windowID: "editDirectDebit"
			},
			trigger: false
		}).sym_interstitial('show');
	},
	/**
	 * Show the Online Payment Details Edit Popup
	 */
	//TODO: Move this into the OR page
	DWPaymentDetailsEditPopup: function(){
		log('Sym.Interstitial.DWPaymentDetailsEditPopup', arguments);
		$('#editPayment1').sym_interstitial({
			ajax: Sym.popupVars.onlinePaymentEdit,
			modal: true,
			positionEdit: {
				triggerID: "edit_payment",
				windowID: "editPayment1"
			},
			trigger: false
		}).sym_interstitial('show');
	},
	/**
	 * Show the Online Payment Errot Popup
	 */
	//TODO: Move this into the OR page
	DWPaymentDetailsErrorPopup: function(){
		log('Sym.Interstitial.DWPaymentDetailsErrorPopup ', arguments);
		$('#showError').sym_interstitial({
			ajax: Sym.popupVars.digitalWalletError,
			modal: true,
			trigger: false
		}).sym_interstitial('show');
	}
};

//TODO: Create a CartNotifier Controller
Sym.CartCount = {
	shown: false,
	enabled: true,
	/**
	 * Disable the Cart Count functionality (Added for Static Site)
	 */
	disable: function(){
		log('Sym.CartCount.disable', arguments);
		Sym.CartCount.enabled = false;
	},
	/**
	 * Enable the Cart Count functionality (Added for Static Site)
	 */
	enable: function(){
		log('Sym.CartCount.enable', arguments);
		Sym.CartCount.enabled = true;
	},
	/**
	 * Show the Cart Count div and setup the hide
	 *
	 * @param int hideDelay - time (in ms) to wait until hiding the cartCount, 0 disables the hide
	 */
	show: function(hideDelay){
		log('Sym.CartCount.show', arguments);
		if (Sym.CartCount.enabled && !Sym.CartCount.shown) {
			$("#cartCount").animate({
				"top": "+=410px"
			}, 3000);
			Sym.CartCount.shown = true;
			if (typeof(hideDelay) === 'number' || hideDelay != 0) {
				setTimeout('Sym.CartCount.hide()', hideDelay);
			}
			$(".jqmClose ","#cartCount").live("click",function(){
				Sym.CartCount.hide();
			});
		}
	},
	/**
	 * Hide the Cart Count div
	 */
	hide: function(){
		log('Sym.CartCount.hide', arguments);
		$("#cartCount").animate({
			"top": "-=410px"
		}, 2000);
		Sym.CartCount.shown = false;
	}
};

/*
 *
 * @class Sym.CandyRack
 *
 * Sets up the size, positioning, and events for the candyRack 
 */

$.Controller.extend("Sym.CandyRack",

	{
		defaults : {
			crSize : 0,
			crProdSize : 0,
			lessThanSize : false
		}
	},

	{

		init : function(){
			log('CandyRack.init', arguments);
			this.setCRSize();
			this.initjScrollPane();
			$(".candyrack-container").show();
			this.posHandF();
			this.fixCart();

		},

		/*
		 * @function getGlobalCRSize
		 * 
		 * There is a global 'candyrackRows' at the top of the page.
		 * Here I go see if that global variable exists, and if so set my crSize value to it.
		 */
		getGlobalCRSize : function(){
			log('CandyRack.getGlobalCRSize', arguments);
			if (typeof(candyrackRows) != "undefined") {
				this.crSize = candyrackRows;
			}
		},

		/*
		 * 
		 * @function getActualCRSize
		 *
		 * If the global 'candyrackRows' is smaller than the actual number of products merchandised, then 
		 * I want to set my crSize value appropriately 
		 *
		 */
		getActualCRSize : function(){
			log('CandyRack.getActualCRSize', arguments);
			this.crProdSize = $(".candyrack-product").size();
			if (this.crProdSize <= this.crSize){
				this.lessThanSize = true;
				this.crSize = this.crProdSize;
			}
		},

		/*
		 * This checks the global value, compares it against the actual nunber of rows 
		 * and finally sets the actual size of the candyRack
		 *
		 */
		setCRSize : function(){
			this.getGlobalCRSize();
			this.getActualCRSize();
			$("#candyRackPane").css('height', 50*this.crSize +35);
		},

		/*
		 *
		 * @function initjScrollPane
		 *
		 * This function:
		 *
		 * * Initalizes jScrollPane for the element
		 * * Checks if 'lessThanSize' is true, and if so:
		 * * Adds some html for the side of the scroll pane.
		 *
		 */
		initjScrollPane : function(){
			log('CandyRack.initjScrollPane', arguments);
			if (this.lessThanSize) {
				var bottomOffset = 0;
				if (Sym.Global.IEVersion == 7){
					bottomOffset = 6;
				}

				var htm = [];


				htm.push('<div class="jspVerticalBar"><div class="jspCap jspCapTop"></div><a class="jspArrow jspArrowUp jspDisabled"></a><div class="jspTrack"></div><a class="jspArrow jspArrowDown jspDisabled"></a><div class="jspCap jspCapBottom"></div></div>');
				/*htm.push('<div class="jspCap jspCapTop"></div>');
				 htm.push('<a class="jspArrow jspArrowUp jspDisabled"></a>');
				 htm.push(<'<div class="jspTrack" style="height: 153px;">');
				 htm.push(<'<div class="jspDrag" style="height: 121px;">');
				 htm.push(<'<div class="jspDragTop"></div>');
				 htm.push(<'<div class="jspDragBottom"></div>');
				 htm.push(<'</div>');
				 htm.push(<'</div>');
				 htm.push(<'<a class="jspArrow jspArrowDown"></a>');
				 htm.push(<'<div class="jspCap jspCapBottom"></div>');
				 htm.push(<'</div>');
				 */

				$(".candyrack-container").append(htm.join(''));
				//$(".jspTrack").css("height", "153px");
				$(".jspTrack").css("height", "152px");
				$("#candyRackPane").css({"height": "155px", "width": "665px"});
				$(".canyrack-title").css("width", "663px");
				$(".canyrack-footer").css({"top": "169px", "width": "673px", "height": "14px", "border-bottom": "1px solid #DDDDDD"});
				//$(".canyrack-footer").css({"top": "143px", "width": "673px", "height": "14px", "border-bottom": "1px solid #DDDDDD"});
				$(".candyrack-container").css("overflow","hidden");
			} else {
				$(function(){
					$(".canyrack-footer").css({"top": "154px", "width": "669px", "height": "14px", "border-bottom": "1px solid #DDDDDD"});
					$("#candyRackPane").css({"height": "169px"});
					//$(".canyrack-title").css({"width":"639px"});
					$('#candyRackPane').jScrollPane({
						showArrows:true,
						scrollbarWidth:22,
						arrowSize:10
					});
					$("#jspHorizontalBar").hide();
				});
			}
		},

		/*
		 *
		 *@function posHandF
		 *
		 * The header and footer are separate peices of HTML that we need to reposition once the CR is rendered.
		 * The whole idea of the header and footer being different peices of HTML needs to be revisietd and is legacy from SP.
		 *
		 */
		posHandF : function(){
			log('CandyRack.posHandF', arguments);
			$(".canyrack-title").css({"position" : "absolute", "top" : "0px"});
			var heightOfCR = $(".candyrack-container").height() - 69;//68;		
			if (this.lessThanSize) {
				heightOfCR = heightOfCR + 1; + "px";
				heightOfCR = '169px';
			}else{
				heightOfCR = '153px';
			}
			$(".canyrack-footer").css({"position" : "absolute", "top" : heightOfCR});
		},

		/*
		 *
		 *@function fixCart
		 *
		 * The design of the cart changes IF the candyrack is present. This function, fixes that up.
		 *
		 */
		fixCart : function(){
			log('CandyRack.fixCart', arguments);
			if($("#candyRackPane").length > 0) {
				log('CandyRack.fixCart', arguments);
				debug($('.CR_no_border_top'));
				$('.CR_no_border_top')
					.addClass('no_border_top')
					.removeClass('CR_no_border_top')
					.removeClass('border_top');
				debug($('.CR_no_border_bottom'));
				$('.CR_no_border_bottom')
					.addClass('no_border_bottom')
					.removeClass('CR_no_border_bottom')
					.removeClass('border_bottom');
				debug($('.CR_no_border_right'));
				$('.CR_no_border_right')
					.addClass('no_border_right')
					.removeClass('CR_no_border_right')
					.removeClass('border_right');
			}
		},

		destroy : function(){
			log('CandyRack.destroy', arguments);
			this._super();
		},

		".candyrack-product mouseover" : function(el, ev){
			el.addClass("candyrack-product-hover");
		},

		".candyrack-product mouseout" : function(el, ev){
			el.removeClass("candyrack-product-hover");
		}

	});



/**
 * A single slider for membership
 * 	 options {
 *		min: the slider minimum value
 *		max: the slider maximum value
 *		step: the value of each step of the slider
 *		initValue: initial value of the slider	
 *		updateCheckout: true or false; true if update checkout button needed 
 *	 }
 **/
$.Controller.extend("Sym.SingleSlider",
	/* @static */
	{
		defaults : {
			messages: null
		}
	},
	/* @prototype */
	{
		init : function(el, options){
			log('ESTORE.SingleSlider.Init', options);

			//the slider intialization
			this.min = options.min; // float or int
			this.max = options.max; // float or int
			this.step = options.step; // float or int 
			this.initValue = options.initValue; //float or int
			this.mid = Math.ceil((this.max - this.min) / 2)+this.min;
			this.slider = this.element.find('.slider');
			this.amount = this.element.find('.amount');
			if (typeof options.updateAddLinkObj !== 'undefined') {
				this.updateAddLinkObj = options.updateAddLinkObj;
			}
			this.isUpdateCheckout = false;
			if (typeof options.isUpdateCheckout !== 'undefined') {
				this.isUpdateCheckout = options.isUpdateCheckout;
			}
			this.currentQty = this.initValue / this.step;
			var self = this;
			this.slider.slider({
				range: 'min',
				value: this.initValue,
				min: this.min,
				max: this.max,
				step: this.step,
				animate: true,
				slide: function(event, ui) {
					self.amount.html(ui.value);
				},
				change: function(event, ui) {
					var step = parseFloat(self.step);
					self.currentQty = parseFloat(ui.value) / step;
					self.updateButtons(true);
					if (typeof self.updateAddLinkObj !== 'undefined') {
						self.updateAddLink(self.updateAddLinkObj.add, self.updateAddLinkObj.type);
					}
				}
			});
			this.amount.html(this.slider.slider("option", "value"));
			// create the slider label
			$('.min',this.element).text(this.min);
			$('.mid',this.element).text(this.mid);
			$('.max',this.element).text(this.max);

			// init buttons
			this.updateButtons(false);
		},
		".inc click": function(el, event) { //incremental button event

			var sliderValue = this.slider.slider("option", "value"),
				newValue = sliderValue + this.step;
			if (newValue > this.max) {
				newValue = this.max;
			}
			this.slider.slider("option", "value", newValue);
			this.amount.html(newValue);
			this.currentQty = newValue / this.step;
			if(sliderValue != newValue)
			{
				this.updateButtons(true);
				if (typeof this.updateAddLinkObj !== 'undefined') {
					this.updateAddLink(this.updateAddLinkObj.add, this.updateAddLinkObj.type);
				}
			}
			event.preventDefault();
			event.stopPropagation();

			return false;
		},
		".dec click": function(el, event) { //decremental button event

			var sliderValue = this.slider.slider("option", "value"),
				newValue = sliderValue - this.step;
			if ((newValue) < this.min) {
				newValue = this.min;
			}
			this.slider.slider("option", "value", newValue);
			this.amount.html(newValue);
			this.currentQty = newValue / this.step;
			this.updateButtons(true);
			if (this.updateAddLinkObj !== 'undefined') {
				this.updateAddLink(this.updateAddLinkObj.add, this.updateAddLinkObj.type);
			}
			event.preventDefault();
			event.stopPropagation();

			return false;
		},
		updateButtons: function(update) {
			this.updateAddButtons(update);
			this.updateCheckoutButton(update);
		},
		/**
		 * updateAddButtons: initialize or update buttons 'Add Device' and 'Add Storage'
		 * update: true if updating buttons, false if initializing buttons
		 **/
		updateAddButtons: function(update) {
			var addButtonCtrs = $('.mStoreButton');
			if (addButtonCtrs.controllers().length === 0) {
				addButtonCtrs = $('.mStoreButton').sym_m_button();
			}
			if (addButtonCtrs !== undefined) {
				addButtonCtrs = addButtonCtrs.controllers();
			}
			if (addButtonCtrs === undefined) {
				return;
			}

			if (update === false) {
				for ( var i = 0, limit = addButtonCtrs.length; i < limit; i++) {
					if (addButtonCtrs[i].isEnabled() === true)
						addButtonCtrs[i].disable();
				}
				return;
			}

			for ( var i = 0, limit = addButtonCtrs.length; i < limit; i++) {
				if (addButtonCtrs[i].isEnabled() === false)
					addButtonCtrs[i].enable();
			}
			return;
		},
		/**
		 * initialize or update checkout button, 'Protect ME'
		 * update: true if updating buttons, false if initializing buttons
		 **/
		updateCheckoutButton: function(update) {
			var checkoutButtonCtr = $('.checkoutButton');
			if (checkoutButtonCtr !== undefined) {
				checkoutButtonCtr = checkoutButtonCtr.controller();
			}
			if (checkoutButtonCtr === undefined) {
				return;
			}
			if (update === false) {
				if (checkoutButtonCtr.isEnabled() === false) {
					checkoutButtonCtr.enable();
				}
				return;
			}
			if (checkoutButtonCtr.isEnabled() === true) {
				checkoutButtonCtr.disable();
			}
			return;
		},
		/* updateAddLink: update add button link
		 *   add: element
		 *   type: type (name) of name-value pair to update in the add link
		 *   currentQty: value of name-value pair to update in the add link	 
		 **/
		updateAddLink: function(add, type) {

			if (typeof add === 'undefined' || typeof type === 'undefined') {
				log('ESTORE.updateSliderAddLink', 'Arguments not defined');
				return;
			}
			if (add.hasClass('dualAdd')){
				// I need to do the RENEWAL button calculation at the time the button is clicked as opposed to as the slider is moved.
				// this just makes sure that happens.
				return;
			}
			var addHref = add.attr('href');
			if (addHref === undefined) {
				addHref = window.location.href; // not sure if it would help
			}
			var pattern = new RegExp('\/'+ type + '(\/|$)', 'i');
			var match = addHref.match(pattern);
			if (match === null) {
				log('ESTORE.updateAddLink', 'Wrong link found');
				return false;
			}
			pattern = new RegExp(match[0] + '([0-9]+)(.{0,1})([0-9]*)(\/|$)', 'i');
			addHref = addHref.replace(pattern, match[0] + this.currentQty + '\/');
			add.attr('href', addHref);
			return true;
		},
		destroy : function(){
			this._super();
		}
	});

/**
 * Slider overlay control  modified to work like the page
 **/
$.Controller.extend("Sym.sliderOverlay",
	/* @static */
	{
		defaults : {
			messages: null
		}
	},
	/* @prototype */
	{
		init: function(el, options) {
			var ajaxURL = options.ajaxURL;
			var sliderType = this.getSliderType(ajaxURL);
			if (sliderType === 'BAS') {
				this.initBAS(ajaxURL);
			} else if (sliderType === 'BMS') {
				this.initBMS(ajaxURL);
			} else {
				this.initDouble(ajaxURL);
			}
		},

		show: function(){
			$('.popupPanel').sym_interstitial('show');

			/* this code is to show/hide monthly/yearly details on edit overlay for monthly */
			var timer = setInterval(function(){
				// console.log('Inside....'+$("#monthlyCheck").length);
				if($("#monthlyCheck").length>0){
					if($("#monthlyCheck").html()=="true"){
						$("#infoYear").hide();
						$("#infoBottomYear").hide();
					}else{
						$("#infoMonth").hide();
						$("#infoBottomMonth").hide();
					}
				}
			},1000);

			setTimeout(function(){
				clearInterval(timer);
			}, 10000);

			/*commented out because I wasn't using it. Leaving it in incase I want to use it.
			 if (this.getSliderType(this.options.ajaxURL) === 'Double'){
			 LOADER.fire('DoubleSlidersOverlay');
			 }
			 */
		},

		close: function(hash){
			// breaking this error checking out into two if statements so I can read it easier.
			// first, does the checkout button exist? Second, does it have a controller? if so, make sure it's enabled.
			if ($(".checkout").length > -1){
				if($(".checkout").controller() !== undefined){
					$(".checkout").controller().enable();
				}
			}

			hash.w.fadeOut('2000',function(){
				if (hash.o) {
					//hash.o.remove();
					hash.o.hide();
				}
			});
		},

		/**
		 * determine slider types: BAS, BMS or Double from input href
		 **/
		getSliderType: function(href) {
			if (href === undefined || href === '')
				return null;
			var pattern = new RegExp('(BAS|BMS|Double)', 'i');
			var match = href.match(pattern);
			if (match === null)
				return null;
			return match[0];
		},

		initBAS: function(ajaxURL) {
			var showSlider = function() {
				if ($('.popupPanel').css('display') == 'none') {
					$('.popupPanel').show();
				}
				var type = 'currentNewSeatQty';
				var currentQty = Sym.Util.getCurrentQtyFromURL(type);
				if (currentQty === -1) {
					if (typeof window.currentRenewalSeatUnit_slot_1 !== "undefined") {
						currentQty = parseInt(window.currentSeatUnit_slot_1) + parseInt(window.currentRenewalSeatUnit_slot_1);
					} else {
						currentQty = parseInt(window.currentSeatUnit_slot_1);
					}

				}
				var step = parseInt(window.seatPerUnit_slot_1),
					min = parseInt(window.minSeatUnit_slot_1),
					max = parseInt(window.maxSeatUnit_slot_1);
				var initValue = currentQty * step;

				var options = {
					min: min,
					max: max,
					step: step,
					initValue: initValue,
					updateAddLinkObj: {
						add: $('#add'),
						type: type,
						updateAddLink: true
					},
					updateCheckout: false
				}
				$('.basSingleSlider .sliderWrapper').sym_single_slider(options);
			}
			//init the overlay
			if( $('.popupPanel').hasClass('sym_interstitial') ) {
				$('.popupPanel').sym_interstitial('destroy');
			}
			if( !$('.popupPanel').hasClass('sym_interstitial') ) {

				$('.popupPanel').sym_interstitial({
					ajax: ajaxURL,
					modal: true,
					onShow: showSlider,
					onLoad: showSlider,
					onHide: this.close
				});

			}
		},

		initBMS: function(ajaxURL) {
			var showSlider = function() {
				if ($('.popupPanel').css('display') == 'none') {
					$('.popupPanel').show();
				}
				var type = 'currentNewStorageQty';
				var currentQty = Sym.Util.getCurrentQtyFromURL(type);
				if (currentQty === -1) {
					if (typeof window.currentRenewalSeatUnit_slot_1 !== "undefined") {
						currentQty = parseFloat(window.currentStorageUnit_slot_1) + parseFloat(window.currentRenewalStorageUnit_slot_1);
					} else {
						currentQty = parseFloat(window.currentStorageUnit_slot_1);
					}
				}
				var step = parseFloat(window.storagePerUnit_slot_1),
					min = parseFloat(window.minStorageUnit_slot_1),
					max = parseFloat(window.maxStorageUnit_slot_1);
				var initValue = currentQty * step;
				var options = {
					min: min,
					max: max,
					step: step,
					initValue: initValue,
					updateAddLinkObj: {
						add: $('#add'),
						type: type,
						updateAddLink: true
					},
					updateCheckout: false
				}
				$('.bmsSingleSlider .sliderWrapper').sym_single_slider(options);
			}

			//init the overlay
			if( $('.popupPanel').hasClass('sym_interstitial') ) {
				$('.popupPanel').sym_interstitial('destroy');
			}
			if( !$('.popupPanel').hasClass('sym_interstitial') ) {

				$('.popupPanel').sym_interstitial({
					ajax: ajaxURL,
					modal: true,
					onShow: showSlider,
					onLoad: showSlider,
					onHide: this.close
				});

			}
		},

		initDouble: function(ajaxURL) {
			var showBASSlider = function() {
				if ($('.popupPanel').css('display') == 'none') {
					$('.popupPanel').show();
				}
				//BAS slider
				var type = 'currentNewSeatQty';
				var currentQty = Sym.Util.getCurrentQtyFromURL(type);
				if (currentQty === -1) {
					currentQty = parseInt(window.currentSeatUnit_slot_1 + window.currentRenewalSeatUnit_slot_1);
				}
				var step = parseInt(window.seatPerUnit_slot_4),
					min = parseInt(window.minSeatUnit_slot_1),
					max = parseInt(window.maxSeatUnit_slot_4),
					renewal = parseInt(window.seatPerUnit_slot_2) || 0;
				//shruti : changed the value according to seat per unit for renewal
				var initValue = parseInt (min + (window.currentSeatUnit_slot_1 * step) + (window.currentRenewalSeatUnit_slot_1 * renewal));
				var options = {
					min: min,
					max: max,
					step: step,
					initValue: initValue,
					updateAddLinkObj: {
						add: $('#add'),
						type: type,
						updateAddLink: true
					},
					updateCheckout: false
				}
				$('.basSliderPanel .sliderWrapper').sym_single_slider(options);
			}
			var showBMSSlider = function() {
				// BMS slider
				var type = 'currentNewStorageQty';
				var currentQty = Sym.Util.getCurrentQtyFromURL(type);
				if (currentQty === -1) {
					currentQty = parseFloat(window.currentStorageUnit_slot_1 + window.currentRenewalStorageUnit_slot_1);
				}
				var step = parseFloat(window.storagePerUnit_slot_5),
					min = parseFloat(window.minStorageUnit_slot_1),
					max = parseFloat(window.maxStorageUnit_slot_5),
					renewal = parseFloat(window.storagePerUnit_slot_3) || 0;
				//shruti : changed the value according to seat per unit for renewal	
				var initValue = parseFloat (min + (window.currentStorageUnit_slot_1 * step)+ (window.currentRenewalStorageUnit_slot_1 * renewal));
				var options = {
					min: min,
					max: max,
					step: step,
					initValue: initValue,
					updateAddLinkObj: {
						add: $('#add'),
						type: type,
						updateAddLink: true
					},
					updateCheckout: false
				}
				$('.bmsSliderPanel .sliderWrapper').sym_single_slider(options);
			}

			var showDoubleSliders = function() {
				//console.log('showingDouble Sliders')
				showBASSlider();
				showBMSSlider();
			}
			var loadedDoubleSliders = function(){
				showDoubleSliders();
				$('.dualAdd').click(function(){
					Sym.SliderActions.dblSliderOverlayUpdateBtn();
				})
			}
			//init the overlay
			if( $('.popupPanel').hasClass('sym_interstitial') ) {
				$('.popupPanel').sym_interstitial('destroy');
			}
			if( !$('.popupPanel').hasClass('sym_interstitial') ) {
				$('.popupPanel').sym_interstitial({
					ajax: ajaxURL,
					modal: true,
					onShow: showDoubleSliders,
					onLoad: loadedDoubleSliders,
					onHide: this.close
				});
			}
		}
	});

Sym.SliderActions = {

	dblSliderOverlayUpdateBtn: function(){
		/*
		 * The main purpose here is to calculate the renewall URL at the time the button is clicked.
		 * The single slider does a very similar calculation at slide time, however
		 * in the renewall slider, I have to calculate the qty of new vs. renewals.
		 * keeping track of what was the original amount was becoming a problem
		 */

		// Set-up some shortcuts to get the element I am talking about
		var add = $(".dualAdd");
		var addHref = add.attr('href');

		// do this for each slider, BAS and BMS

		var sliders = ["bas", "bms"];

		for (x in sliders){
			//console.log("current",$('.'+sliders[x]+'SliderPanel .sliderWrapper').controller());
			// get the values of each slider from it's controller object
			var currentQty = $('.'+sliders[x]+'SliderPanel .sliderWrapper').controller().currentQty;
			//console.log("current",$('.'+sliders[x]+'SliderPanel .sliderWrapper').controller().currentQty);
			//console.log("init",$('.'+sliders[x]+'SliderPanel .sliderWrapper').controller().initValue);
			//console.log("step",$('.'+sliders[x]+'SliderPanel .sliderWrapper').controller().step);
			var initVal = $('.'+sliders[x]+'SliderPanel .sliderWrapper').controller().initValue / $('.'+sliders[x]+'SliderPanel .sliderWrapper').controller().step;

			// the types, this is important for our URL modification below
			var renType = "currentRenewalStorageQty";
			var newType = "currentNewStorageQty";
			var globalRenewal = window.globalRenewalStorageUnit_slot_1 || 0;
			var currentNewQty = parseInt(window.currentStorageUnit_slot_1) || 0;
			var currentRenewalQty = parseInt(window.currentRenewalStorageUnit_slot_1) || 0;
			if (sliders[x] === "bas"){
				renType = "currentRenewalSeatQty";
				newType = "currentNewSeatQty";
				globalRenewal = window.globalRenewalSeatUnit_slot_1 || 0;
				currentNewQty = parseInt(window.currentSeatUnit_slot_1) || 0;
				currentRenewalQty = parseInt(window.currentRenewalSeatUnit_slot_1) || 0;
			}
			var diff = Math.abs(initVal - currentQty);
			//console.log(sliders[x], "init", initVal, "current", currentQty);
			//alert(currentQty);
			if(currentQty != initVal)
			{
				if (currentQty < initVal){ // decrease the renewal value
					currentNewQty = currentNewQty - diff;
					if(currentNewQty <= 0)
					{
						currentRenewalQty = currentRenewalQty + currentNewQty;
						currentNewQty  = 0;
					}
					var pattern = new RegExp('\/'+ renType + '(\/|$)', 'i');

					var match = addHref.match(pattern);
					if (match === null) {
						log('ESTORE.updateDualRenewallLink-'+sliders[x], 'Wrong link found');
						return false;
					}

					pattern = new RegExp(match[0] + '([0-9]+)(.{0,1})([0-9]*)(\/|$)', 'i');
					addHref = addHref.replace(pattern, match[0] + currentRenewalQty + '\/');
					add.attr('href', addHref);

					var pattern = new RegExp('\/'+ newType + '(\/|$)', 'i');
					var match = addHref.match(pattern);
					if (match === null) {
						log('ESTORE.updateDualNewLink1-'+sliders[x], 'Wrong link found');
						return false;
					}
					pattern = new RegExp(match[0] + '([0-9]+)(.{0,1})([0-9]*)(\/|$)', 'i');
					addHref = addHref.replace(pattern, match[0] + currentNewQty + '\/');
					add.attr('href', addHref);

				} else { // increase the new value
					var x = globalRenewal - currentRenewalQty;
					currentQty = diff - x;
					if(currentQty <= 0)
					{
						currentRenewalQty = currentRenewalQty + diff;
					}
					else
					{
						currentRenewalQty = currentRenewalQty + x;
						currentNewQty = currentNewQty + currentQty;
					}

					var pattern = new RegExp('\/'+ renType + '(\/|$)', 'i');

					var match = addHref.match(pattern);
					if (match === null) {
						log('ESTORE.updateDualRenewallLink-'+sliders[x], 'Wrong link found');
						return false;
					}

					pattern = new RegExp(match[0] + '([0-9]+)(.{0,1})([0-9]*)(\/|$)', 'i');
					addHref = addHref.replace(pattern, match[0] + currentRenewalQty + '\/');
					add.attr('href', addHref);

					var pattern = new RegExp('\/'+ newType + '(\/|$)', 'i');
					var match = addHref.match(pattern);
					if (match === null) {
						log('ESTORE.updateDualNewLink1-'+sliders[x], 'Wrong link found');
						return false;
					}
					pattern = new RegExp(match[0] + '([0-9]+)(.{0,1})([0-9]*)(\/|$)', 'i');
					addHref = addHref.replace(pattern, match[0] + currentNewQty + '\/');
					add.attr('href', addHref);
				}
			}
		}

		return true;
		//console.log($(".dualAdd").attr('href'))
		//return false;
	}
};



//TODO: Can this be rolled into the Interstitial controller or utilize it to do this?
Sym.TaxOverlay = {
	init : function(position1, position2) {
		log('Sym.TaxOverlay.init', arguments);
		var pos1 = typeof(position1) === "string" ? position1 : "bottom";
		var pos2 = typeof(position2) === "string" ? position2 : "right";
		$('.vattrigger').live('click', function(){
			$('#vatWindow').sym_interstitial({
				ajax: Sym.popupVars.taxOverlay,
				position: {
					trigger: $(this),
					window: "vatWindow",
					arrowPos1: pos1,
					arrowPos2: pos2
				},
				modal: true
			}).sym_interstitial('show');
			return false;
		});
	}
};

//TODO: Create an InputValidation Controller
Sym.BillingPageValidation = {
	/**
	 Test if e-mail entered by the user follows valid e-mail format
	 Valid e-mail format is alphanumeric@alphanumeric.alphaonly
	 **/
	enable : true,

	emailValidator: function() {
		log('Sym.BillingPageValidation.emailValidator', arguments);
		if (!Sym.BillingPageValidation.enable) {
			Sym.BillingPageValidation.enable = true;
			return;
		}
		if($("#email").val().length == 0) {
			// Empty email field do not show error message.
			$.publish("/ErrorBubble/hide");
			return;

		}
		var filter = /^([a-zA-Z0-9*!#$%&'*+-_.~{:}|?/\=])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9_-])+$/;
		if (!filter.test($("#email").val())) {
			$("#email").sym_error_bubble({
				message: $('#emailValidatorMsg').text()
			}).sym_error_bubble("show");
		} else {
			$.publish("/ErrorBubble/hide");
			$("#email").sym_error_bubble("destroy");
			$("#email").unbind("focusin");
		}
	},

	/**
	 Test if data entered into verifyemail field is valid
	 Data entered into verify email field should be same as email field
	 **/
	verifyemailValidator: function() {
		log('Sym.BillingPageValidation.verifyemailValidator', arguments);
		if (!Sym.BillingPageValidation.enable) {
			Sym.BillingPageValidation.enable = true;
			return;
		}
		if(($("#verifyEmail").val().length == 0) && ($("#email").val().length == 0)) {
			// Verify Email field is empty - do not irritate the user
			return;
		}
		// Perform case insensitive match. Most e-mail servers are case insensitive.
		if ($("#email").val().toLowerCase() != $("#verifyEmail").val().toLowerCase()) {
			$("#verifyEmail").sym_error_bubble({
				message: $('#verifyEmailValidatorMsg').text()
			}).sym_error_bubble("show");
		}
		else {
			$.publish("/ErrorBubble/hide");
			$("#verifyEmail").sym_error_bubble("destroy");
			$("#verifyEmail").unbind("focusin");
		}
	},

	/***
	 SelectCard function used by card type function
	 **/

	selectCard: function(cardType) {
		log('Sym.BillingPageValidation.selectCard', arguments);
		$("#orderPayment\\.cardType").val(cardType.toUpperCase());
		// Show the CVV code field.
		$("#orderPayment\\.cardType").change();

	},

	/***
	 Validate the password field.
	 **/
	passwordValidator: function() {
		log('Sym.BillingPageValidation.passwordValidator', arguments);
		if (!Sym.BillingPageValidation.enable) {
			Sym.BillingPageValidation.enable = true;
			return;
		}
		if($("#password").val().length == 0) {
			// Password field is empty - do not show the error message
			$.publish("/ErrorBubble/hide");
			return;
		}
		if ($("#password").val().length < 6){
			$("#password").sym_error_bubble({
				message: $('#pwdValidatorMsg').text()
			}).sym_error_bubble("show");
		} else
			$.publish("/ErrorBubble/hide");
	},

	/***
	 Validate the verify password field
	 Check performed - does the password and verify password match?
	 **/
	verifypasswordValidator: function() {
		log('Sym.BillingPageValidation.verifypasswordValidator', arguments);
		if (!Sym.BillingPageValidation.enable) {
			Sym.BillingPageValidation.enable = true;
			return;
		}

		if(($("#verifyPassword").val().length == 0) && ($("#password").val().length == 0)) {
			// Nothing is entered yet - do not show the error message
			$.publish("/ErrorBubble/hide");
			return;
		}

		if ($("#password").val() != $("#verifyPassword").val()) {
			$("#verifyPassword").sym_error_bubble({
				message: $('#verifyPwdValidatorMsg').text()
			}).sym_error_bubble("show");
		}
		else {
			$.publish("/ErrorBubble/hide");
		}

	},

	/* CPF_CNPJValidatorMsg */
	cpf_cnpjValidator: function() {
		log('Sym.BillingPageValidation.cpf_cnpjValidator', arguments);
		if (!Sym.BillingPageValidation.enable) {
			Sym.BillingPageValidation.enable = true;
			return;
		}



		var cnfRegex = /^[0-9]{3}\.[0-9]{3}\.[0-9]{3}\-[0-9]{2}$/;
		var cnpjRegex = /^[0-9]{2}\.[0-9]{3}\.[0-9]{3}\/[0-9]{4}\-[0-9]{2}$/;


		setTimeout(function(){
			if (!(cnfRegex.test($("#CPF_CNPJ").val()) || cnpjRegex.test($("#CPF_CNPJ").val()))){
				$("#CPF_CNPJ").sym_error_bubble({
					message: $('#CPF_CNPJValidatorMsg').text()
				}).sym_error_bubble("show");
			}

			else {
				$.publish("/ErrorBubble/hide");
				$("#CPF_CNPJ").sym_error_bubble("destroy");
				$("#CPF_CNPJ").unbind("focusin");
			}
		},1000);


	},
	/***
	 Guess the card type by reading the cardNumber
	 **/
	selectCardType: function() {
		log('Sym.BillingPageValidation.selectCardType', arguments);
		if( $("#orderPayment\\.cardType").val() != "error")
			return;

		cardNumber =  jQuery.trim( $("#orderPayment\\.cardNumber").val());
		cardNumberLength = cardNumber.length;
		firstDigitStr = cardNumber.slice(0,1);

		if (firstDigitStr == -1)
			return;

		firstDigit = parseInt(firstDigitStr);

		if(firstDigit == NaN)
			return;

		firstTwoDigitsStr = cardNumber.slice(0,2);

		if(firstTwoDigitsStr == -1)
			return;

		firstTwoDigits = parseInt(firstTwoDigitsStr);

		if (firstTwoDigits == NaN || firstTwoDigits == 0)
			return; // Test 'Fo2345678901234'

		firstThreeDigitsStr = cardNumber.slice(0,3);

		if(firstThreeDigitsStr == -1)
			return;

		firstThreeDigits = parseInt(firstThreeDigitsStr);

		if (firstThreeDigits == NaN || firstThreeDigits == 0)
			return; // Test 'Fo2345678901234'


		if (((cardNumberLength == 16) || (cardNumberLength == 18) || (cardNumberLength == 19) )&& (firstTwoDigits == 67)) {
			Sym.BillingPageValidation.selectCard("Maestro"); // Test '5612345678901234'
			return;
		}
		else if( (cardNumberLength == 15)  && ((firstTwoDigits == 34) || (firstTwoDigits == 37))) {
			Sym.BillingPageValidation.selectCard("Amex"); // Test '312345678901234'
			return;
		}
		else if( (cardNumberLength == 16) &&( firstDigit == 6)) {
			Sym.BillingPageValidation.selectCard("Discover"); // Test '6123456789012345'
			return;
		}
		else if( (cardNumberLength == 16)&&( firstDigit == 4)) {
			Sym.BillingPageValidation.selectCard("Visa"); // Test '4123456789012345'
			return;
		}
		else if( (cardNumberLength == 16) &&( firstDigit == 5)) {
			Sym.BillingPageValidation.selectCard("MasterCard"); // Test '5123456789012345'
			return;
		}
		else if ((cardNumberLength == 16) && (firstTwoDigits == 35)) {
			Sym.BillingPageValidation.selectCard("JCB"); // Test '3812345678901234'
			return;
		}
		else if ((cardNumberLength == 14) && (firstTwoDigits == 36)) {
			Sym.BillingPageValidation.selectCard("Diners"); // Test '36012345678901'
			return;
		}
		else if ((cardNumberLength == 16) && ((firstTwoDigits == 30) || (firstTwoDigits == 38)|| (firstTwoDigits == 39))) {
			Sym.BillingPageValidation.selectCard("Diners"); // Test '301234567890123456'
			return;
		}

	},
	/***
	 Validate Zipcode from Billing page
	 **/
	zipCodeValidator:function(){
		// just make sure the PAGE_COUNTRY exists
		var currentCountry = $("#billingAddress\\.country").val();
		log('Sym.BillingPageValidation.verifyZipCode', arguments);
		var zipCodeVal = $("#billingAddress\\.zipOrPostalCode").val();
		if(zipCodeVal.length == 0 && currentCountry != 'BR') {
			// Verify ZipCode field is empty - do not irritate the user
			return;
		}
		// Check the value using a regexp
		switch (currentCountry) {
			case "US":
				postalCodeRegex = /^([0-9]{5})(?:[-\s]{0,1}([0-9]{4}))?$/;
				break;
			case "CA":
				postalCodeRegex = /^([A-Za-z][0-9][A-Za-z])\s{0,1}([0-9][A-Za-z][0-9])$/;
				break;
			case "BR":
				postalCodeRegex = /^([0-9]){5}([-])([0-9]){3}$/;
				break;
			default :
				postalCodeRegex = /^.*$/;

		}
		setTimeout(function(){
			$(".shorter").removeClass('optional');
			if(!postalCodeRegex.test($("#billingAddress\\.zipOrPostalCode").val())){
				$("#billingAddress\\.zipOrPostalCode").sym_error_bubble({
					message: $('#verifyZipcodeValidatorMsg').text()
				}).sym_error_bubble("show");
			}  else {
				$.publish("/ErrorBubble/hide");
				$("#billingAddress\\.zipOrPostalCode").sym_error_bubble("destroy");
				$("#billingAddress\\.zipOrPostalCode").unbind('focusin');
			}
		},1000);
	}
};
var Wicket = {};

/*
 * Wicket.savedReplaceOuterHtml Saves the Wicket.replaceOuterHtml function so
 * we can override it
 */
var SavedWicketreplaceOuterHtml = Wicket.replaceOuterHtml;
/*
 * eCX version of the Wicket libarary function of the same name
 * Calls the Wicket library function (SavedWicketreplaceOuterHtml) and then
 * fires a wicketAjaxReplace event on the new version of the element that was
 * replaced
 * <h3>Example:</h3>
 * The element with an id of billingPaymentForm will execute this code after
 * being replaced by wicketAjax
 * @codestart
 * $("#billingPaymentForm").live("wicketAjaxReplace", function(ev){
 *   //Prevents child elements from running this function
 *   if(ev.target !== this) {
 *    return true;
 *   }
 *   // Event function code here
 * });  
 * @codeend
 * @param {DOMObject} element element to replace
 * @param {String} text the HTML returned from the server that should replace the element
 */
Wicket.replaceOuterHtml = function(element, text) {
	//call the wicket
	SavedWicketreplaceOuterHtml(element, text);
	//get new element
	var id = $(text).attr("id");
	if( typeof id !== "undefined" ){

		id = $(text).attr("id").replace('.', '\\.');
		var el = $("#" + id);
		//var el = $("#" + $(text).attr("id"));
		debug('wicketAjaxReplace', 'old id:', $(element).attr("id"), 'new id:', $(text).attr("id"), 'dom id:', id, 'el:', el);
		//trigger wicketAjaxReplace handler	
		el.trigger("wicketAjaxReplace");

	}
};

/*
 * Backwards Compatible Functions
 */
var ESTORE = {};

var followMeBubbles = {
	init : function() {

	}
}

function pageloadingtime() {
	return Sym.Misc.pageloadingtime();
}
function logDebug(msg){
	debug(msg);
}
function highlightOptions(thisObj, className){
}
function lowlightOptions(thisObj, className){
}

function showTab(tabid, divid, id){

	Sym.Misc.showTab(tabid, divid, id);
}

function showFirstErrorBubble(){

	$('.sym_error_bubble').eq(0).sym_error_bubble("show");
}

function resetErrorBubbles(){
	//Do nothing
}


var errorMsg;
function showErrorBubble(a,msg){
	errorMsg = msg;
}
function showTooltip() {
	//Do Nothing	
}
function positiontip() {
	//Do Nothing
}

function hideCSDropDown() {
	//Nothing
}

function showCSDropDown() {
	//Nothing
}

function hideCartCount() {
	//Nothing
}

function hideOpenErrorBubble(){
	$.publish("/ErrorBubble/hide");
}

function hideErrorBubble(){
	$.publish("/ErrorBubble/hide");
}

function openPopup(url, width, height, toolbar, menubar, scrollbars, resizeable, location, directories, status, x, y){
	Sym.Misc.openPopup(url, width, height, toolbar, menubar, scrollbars, resizeable, location, directories, status, x, y);
}

ESTORE.HintTip = {
	hide: function() {
		$.publish('/MessageBubble/hide');
	}
};

ESTORE.Misc = Sym.Misc;

ESTORE.PaymentOptions = Sym.PaymentOptions;

ESTORE.Input = Sym.Input;

ESTORE.CandyRack = {
	init : function(){}
};

var Mask = {
	show: function(targetId){
		Sym.WicketSpinner.show(targetId);
	},
	hide: function(targetId){
		Sym.WicketSpinner.hide(targetId);
	}
};

var updateLink = {
	subFrm: function(thiz, colIndex, yr, val){
		Sym.CLP.UpdateLink.subFrm(thiz, colIndex, yr, val);
	}
};
var twoCol = {
	subFrm: function(val, thiz){
		Sym.CLP.TwoCol.subFrm(val, thiz);
	}
};
var collectAndSendTT = {
	getData: function(pageName){
		Sym.CollectAndSendTT.getData(pageName);
	}
};

/******* STOP ADDING NEW HYBRID.JS CODE HERE *******/

/****************************************************************************
 * ONLOAD Events
 * Specifies the onload events that each page needs to function
 *
 * Each page/page type is an object inside ONLOAD with only one required function: onload
 * The id on the body element specifies what Object to use
 * More functions can be added which can be triggered by additional classes being
 * applied to the body element
 * Common.onload is always called
 * The Common.rc/Common.mf are called if a class of "mf/rc" is applied to the body element
 ***************************************************************************/
var ONLOAD = {
	Common: {
		onload: function(){
			//Populate Sym.Global variables
			Sym.Global.Language = Sym.Util.getSymGlobal('symGlobalLanguage');
			Sym.Global.Country = Sym.Util.getSymGlobal('symGlobalCountry');
			Sym.Global.Site = Sym.Util.getSymGlobal('symGlobalSite');
			Sym.Global.Vendor = Sym.Util.getSymGlobal('symGlobalVendor');
			Sym.Global.SubChannel = Sym.Util.getSymGlobal('symGlobalSubChannel');
			//Run through all query string parameters and store them in Sym.Global.URLParams
			var e,
				a = /\+/g,  // Regex for replacing addition symbol with a space
				r = /([^&=]+)=?([^&]*)/g,
				d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
				q = window.location.search.substring(1);
			while (e = r.exec(q)) {
				Sym.Global.URLParams[d(e[1])] = d(e[2]);
			}
			$('html').removeClass('no-js');
			//Where am I?
			log("You Are Here:", "Language:", Sym.Global.Language, "Country:", Sym.Global.Country, "Site:", Sym.Global.Site, "Vendor:", Sym.Global.Vendor, "SubChannel:", Sym.Global.SubChannel);
			//TODO: Rewrite as Controller
			Sym.LeftNav.Flyout.eventInit();
			//Left Nav Hover States
			//TODO: Move this to CSS
			$('.lftNavMainNav').hover(function(){
				$(this).addClass('lftNavOnHoverNoArws');
			}, function(){
				$(this).removeClass('lftNavOnHoverNoArws');
			});
			//Hides error bubble and clears value form the input box.
			if ($('#searchText').length > 0) {
				$('#searchText').focus(function(){
					$.publish("/ErrorBubble/hide");
					$('#searchText').attr('value', '');
				});
			}
			//Apply button controller
			$('.estore_button').sym_button();
			$('.mStoreButton').sym_m_button();
			//Apply dropdown controller

			/* Check if the two uls inside the country drop down have content */
			if( /\S+/.test($('#countryDropDown .localizationPaneCtryTxt ul').eq(0).html()) && /\S+/.test($('#countryDropDown .localizationPaneCtryTxt ul').eq(1).html()) ) {

				$('#countryDropDown').sym_header_drop_down({
					linkClass: "localizationCtry"
				});

			} else { //still have to remove the inline JS calls
				$('#countryDropDown').attr('onMouseout', "");
				$('#countryDropDown').attr('onMouseover', "");
			}
			$('#shoppingDropDown').sym_header_drop_down({
				linkClass: "localizationShop"
			});
			//scrape tooltip events
			//TODO: Make this more targetted to pages we know that have tolltips
			$('body').sym_event_scraper({
				event: "onClick",
				eventType: "showTooltip",
				plugin: "sym_message_bubble",
				pluginParams: {0:"message",1:"width"},
				selector: ".popupMarker, .moreInfoMarker, .optOutWhy"
			});
			//Print button hover
			$(".printOption").hover(function(){
				$(this).toggleClass('printOptionOnHover');
			});
			//Popup marker hover
			$('.popupMarker').hover(function(){
				$(this).toggleClass('popupMarkerOnhover');
			});
			//More info marker hover
			$('.moreInfoMarker').hover(function(){
				$(this).toggleClass('moreInfoMarkerOnhover');
			});

			if (typeof standardssourl != 'undefined') {
				debug("initing sign in "+standardssourl);
				//SSOClient.init({ssoBaseUrl:standardssourl});
			}


			//This code will populate the norton one link based on site.
			if ($('html[lang="en"]').length > 0) {
				if ($('body.US_SITE').length > 0) {
					$('a[href$="mawareCategoryPage"]').css('display', 'block');
				} else if ($('body.CA_SITE').length > 0) {
					$('a[href$="mawareCategoryPage"]').css('display', 'block');
				} else if ($('body.NZ_SITE').length > 0) {
					$('a[href$="mawareCategoryPage"]').css('display', 'block');
				} else if ($('body.AU_SITE').length > 0) {
					$('a[href$="mawareCategoryPage"]').css('display', 'block');
				} else if ($('body.GB_SITE').length > 0) {
					$('a[href$="mawareCategoryPage"]').css('display', 'block');
				} else {
					$('a[href$="mawareCategoryPage"]').css('display', 'none');
					$('a[href$="mawareCategoryPage"]').parent().removeClass("lftNavMainNav");
				}
			}
			else {
				$('a[href$="mawareCategoryPage"]').css('display', 'none');
				$('a[href$="mawareCategoryPage"]').parent().removeClass("lftNavMainNav");
			}
			//Adding Omniture 'click' event to the help link.
			$("#help").click(function(){
				var s=s_gi('symantecstorehho');
				s.tl(this,'o','hho_store_header_help');
			});
			$("#Help").click(function(){
				var s=s_gi('symantecstorehho');
				s.tl(this,'o','hho_store_header_help');
			});



		},

		seostatic: function(){
			log('ONLOAD.Common.seostatic');
			//Disables the CartCount so it doesn't show when empty, seo.js will enable CartCount if it is needed
			Sym.CartCount.disable();
			$("#social .facebook").click(function(){
				window.open('http://www.facebook.com/sharer.php?u='+window.location.href+'&t='+document.title);
			});
			$("#social .twitter").click(function(){
				window.open('http://twitter.com/home?status='+document.title+window.location.href);
			});
			$("#social .delicious").click(function(){
				window.open('http://del.icio.us/post?url='+window.location.href+'&title='+document.title);
			});
		},

		mf: function(){
			log('ONLOAD.Common.mf');
			//Cart Drop Down
			Sym.CartCount.show(10000);
			//Create Account Interstitial
			$('#interstitialCreateAccount').sym_interstitial({
				ajax: Sym.popupVars.signIn ,
				modal: true,
				position: {
					trigger: "signIn",
					window: "interstitialCreateAccount",
					arrowPos1: "right",
					arrowPos2: "top"
				},
				trigger: 'a.signTrigger'
			});
			//Help Interstitial
			$('#helpInterstitial').sym_interstitial({
				ajax: Sym.popupVars.mfHelp,
				modal: true,
				position: {
					trigger: "help",
					window: "helpInterstitial",
					arrowPos1: "right",
					arrowPos2: "top"
				},
				trigger: 'a.helpTrigger',
				onLoad: function(){LOADER.fire('Help');}
			});
		},
		rc: function(){
			log('ONLOAD.Common.rc');
			$('#helpInterstitial_RC').sym_interstitial({
				ajax: Sym.popupVars.rcHelp,
				modal: true,
				position: {
					trigger: "Help",
					window: "helpInterstitial_RC",
					arrowPos1: "right",
					arrowPos2: "top"
				},
				trigger: 'a.helpTrigger',
				onLoad: function(){LOADER.fire('Help');}
			});

		}
	},

	BillingPayment: {
		onload: function(){
			log('ONLOAD.BillingPayment.onload');

			$('#submitOrder').sym_interstitial({
				ajax: Sym.popupVars.progressIndicator,
				modal: true,
				center: {
					window: "submitOrder",
					offsetX: 0,
					offsetY: 450
				},
				trigger: 'a.submitOrder'
			});

			/* VAT Info tooltip */

			$('#vatMoreInfo').live('click', function(){

				if( !$(this).hasClass('sym_message_bubble') ){
					$(this).sym_message_bubble({
						message: $(this).data('message')
					});
				}

				$(this).sym_message_bubble('show');

			});

			//LV-24565 adding the resize event for the error bubble placements			
			$(window).resize(function(){
				if($("#errorBubbleLayer").css("visibility") == "visible")
				{
					$.publish("/MessageBubble/hideAll");
					showFirstErrorBubble();
				}
			});

			//CVV Code Popup			
			$('#cardSecurityCodeVisa:visible, #cardIssueNumberMaestro:visible').live('click', function(){
				$('#secCodeGeneric').sym_interstitial({
					ajax: Sym.popupVars.cvvHelp,
					modal: true,
					position: {
						trigger: $('.cardTrigger'),
						window: "secCodeGeneric",
						arrowPos1: "right",
						arrowPos2: "top"
					},
					onLoad: function() {
						if($('#cardIssueNumberMaestro').isVisible()) {
							$('#maestro', '#secCodeGeneric').show();
						} else if ($('#cardSecurityCodeVisa').isVisible()) {
							$('#visa', '#secCodeGeneric').show();
						}
					}
				}).sym_interstitial("show");
				$.publish("/MessageBubble/hideAll");
			});

			//Learn More Popup
			$('#learnMoreLinkAr').live('click', function(){
				$('#learnMoreAr').sym_interstitial({
					ajax: Sym.popupVars.learnMore,
					modal: true,
					position: {
						trigger: "learnMoreLinkAr",
						window: "learnMoreAr",
						arrowPos1: "right",
						arrowPos2: "middle"
					}
				}).sym_interstitial("show");
				$.publish("/MessageBubble/hideAll");
			});

			//EULA Popup			
			$('#eula').live('click', function(){
				$('#eulaWindow').sym_interstitial({
					ajax: Sym.popupVars.EULA,
					modal: true,
					position: {
						trigger: "eula",
						window: "eulaWindow",
						arrowPos1: "bottom",
						arrowPos2: "right"
					}
				}).sym_interstitial("show");
				$.publish("/MessageBubble/hideAll");
			});

			// On click event for Credit Card Images
			//LV-14156: Made selector more specific to avoid targeting 3DS image
			$("#divCreditCardWrapper img").live("click", function() {
				// Set the card type in credit card options
				$("#orderPayment\\.cardType").val($(this).attr("id").toUpperCase());
				$("#orderPayment\\.cardType").change();
			});

			$("#billingPaymentForm").live("wicketAjaxReplace", function(ev){
				//Don't run for descendants
				if(ev.target === this) {
					//Scrape error bubble events
					$(this).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input, select, h4.arRdbBnErrorMsg"
					});
					return false;
				}
			});

			$("#abbForm").live("wicketAjaxReplace", function(ev){
				//Don't run for descendants
				if(ev.target === this) {
					//Scrape error bubble events
					$(this).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input, select"
					});
					return false;
				}
			});

			// Hiding grey color of zipcode and state for Brazil 
			if($('body.BR').length > 0)
			{
				$('.state').removeClass('optional');
				$('.shorter').removeClass('optional');
			}

			/*
			 * Edit Payment Pop-up
			 * Here I add the call to run the event scraper on the billing info popup. 
			 * I have to run it twice because wicket is doing some odd things.
			 * It is replacing an element multiple times, first just the element is replaced then the tr which contains it is replaced. 
			 * I couldn't think of a way to optimize this call. And to be honest, I don't have much time left.
			 */

			var paymentLoad = function(hash){
				Sym.Misc.updateCreditCard($('select[name="cardType"] :selected').text());
				$("input,select",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"}
					});
					return false;
				});

				$("*",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input,select"
					});
					return false;
				});
			};

			var paymentHide = function(hash){
				$('.sym_error_bubble', hash.w).sym_error_bubble('destroy');
				hash.o.remove();
				hash.w.hide();
				$.publish("/ErrorBubble/hide");
			};
			if($("#abbForm").length>0){
				$('#editPayment1').sym_interstitial({
					ajax: Sym.popupVars.onlinePaymentEdit,
					modal: true,
					onLoad: paymentLoad,
					onHide: paymentHide,
					center:true,
					trigger: 'a.editPayment'
				});
			}else{
				$('#editPayment1').sym_interstitial({
					ajax: Sym.popupVars.editOnlinePayment,
					modal: true,
					onLoad: paymentLoad,
					onHide: paymentHide,
					positionEdit: {
						triggerID: "edit_payment",
						windowID: "editPayment"
					},
					trigger: 'a.editPayment'
				});
			}

			// Spark-33122 New changes
			$('.enrollCheckBox').live('change', function() {
				if (!$(this).prop('checked')) {
					$("body").append("<div class='modalOverlay'></div>");
					$('.autoRenewPopup').show();
				}
			});

			$(".confirmationButton,.no_subscription").live("click",function(){
				if($(this).hasClass('confirmationButton')){
					$(".enrollCheckBox").prop('checked','checked');
				}
				$(".modalOverlay").remove();
				$(".autoRenewPopup").hide();
			});


			/* SPARK-30917 ends */
			$('.disableCrtlClick').die("click");
			$('.disableCrtlClick').live("click",function(e) {
				if(e.ctrlKey  == true) {
					e.preventDefault();
				}
			});


			$("body").delegate("#email", "blur", Sym.BillingPageValidation.emailValidator);

			$("body").delegate("#verifyEmail", "blur", Sym.BillingPageValidation.verifyemailValidator);

			$("body").delegate("#password", "blur", Sym.BillingPageValidation.passwordValidator);

			$("body").delegate("#verifyPassword", "blur", Sym.BillingPageValidation.verifypasswordValidator);

			$("body").delegate("#orderPayment\\.cardNumber", "blur", Sym.BillingPageValidation.selectCardType);

			$("body").delegate("#orderPayment\\.cardNumber", "blur", Sym.BillingPageValidation.selectCardType);

			Sym.TaxOverlay.init('right', 'middle');

			$("body").delegate("#vattrigger", 'click', function(){	$('#vatWindow').sym_interstitial('show');});

			$("body").delegate("#billingAddress\\.zipOrPostalCode", "blur", Sym.BillingPageValidation.zipCodeValidator);

			$("body").delegate("#billingAddress\\.country", "change", Sym.BillingPageValidation.zipCodeValidator);
			$("body").delegate("#CPF_CNPJ", "blur", Sym.BillingPageValidation.cpf_cnpjValidator);

		}

	},

	BuyMoreStoragePage: {
		onload: function(){
			log('ONLOAD.BuyMoreStoragePage.onload');
			$('#tabKeyFeatures').click(function(){
				Sym.Misc.showTabbedPanel('tabKeyFeatures', 'pnlKeyFeatures');
			});
			$('#tabSysReq').click(function(){
				Sym.Misc.showTabbedPanel('tabSysReq', 'pnlSysReq');
			});
		}
	},

	CompareFeatures: {
		onload: function(){
			//make all wrappers the same height
			var maxHeight = 0;
			$('.wrapper').each(function(){
				if($(this).height() > maxHeight){
					maxHeight = $(this).height();
				}
			});
			$('.wrapper').height(maxHeight);
		}
	},

	CLP: {
		onload: function(){
			$('.SObuyNowChanger').click(function(){
				var split = $(this).val().split('_');
				var col = split[0];
				var row = split[1];
				logDebug('Change: ' + col + ', ' + row);
				$('#SObuyProduct_' + col + '_0').hide();
				$('#SObuyProduct_' + col + '_0_2').hide();
				$('#SObuyProduct_' + col + '_1').hide();
				$('#SObuyProduct_' + col + '_1_2').hide();
				$('#SObuyProduct_' + col + '_' + row).show();
				$('#SObuyProduct_' + col + '_' + row + '_2').show();
				logDebug('Show: #SObuyProduct_' + col + '_' + row);
				logDebug('Show: #SObuyProduct_' + col + '_' + row + '_2');
			});
			//Paid Search URL
			$.each($('body.ps .PSButton a'), function(){
				var url = window.location.toString();
				if (url.indexOf("?") > -1) {
					var $link = $(this);
					var link_url = $link.attr('href');
					var search_params = url.substr(url.indexOf('om_sem_cid'), url.length);
					if (link_url.indexOf("?") == -1) {
						link_url = link_url + '?' + search_params;
					}
					else {
						link_url = link_url + '&' + search_params;
					}
					//link_url = link_url + '?' + search_params;
					$link.attr('href', link_url);
				}
			});
		}
	},

	Compare: {
		onload: function(){
			log('ONLOAD.Compare.onload');
			if ($(".setTop").length > 0 && $(".spanPrice").length > 0) {
				$(".setTop").css("top", ($(".spanPrice").offset().top - 1)) //set top position of divs, Bug 9745, compare page pop up
			}
			if ($(".spanTaxesMsg").length > 0 && $("#div_nr_ProdImg").length > 0) {
				$(".spanTaxesMsg").css("left", ($("#div_nr_ProdImg").offset().left)) //sets left position of tax span, Bug 9745, compare page pop up
			}
		}
	},

	Help: {
		onload: function(){
			log('ONLOAD.Help.onload');
			var qCount = 0;
			$(".divFaq .divFaqQ").each(function(){
				if($(this).hasClass('divFaqQ')) {
					if (qCount == 0) {
						$(this).attr('id','hho_store_header_help_additionalcomputers');
					}
					else if (qCount == 1) {
						$(this).attr('id','hho_store_header_help_download');
					}
					else if (qCount == 2) {
						$(this).attr('id','hho_store_header_help_automaticinstall');
					}
					else if (qCount == 3) {
						$(this).attr('id','hho_store_header_help_invoice');
					}
					else if (qCount == 4) {
						$(this).attr('id','hho_store_header_help_ndi');
					}
				}
				qCount++;
				var answer = $(this).next(".divFaqA");
				$(this).click(function(){
					if (answer.hasClass("show")) {
						/* removed code for FAQ expansion and collapse	*/
						var s=s_gi('symantecstorehho');
						s.tl(this,'o',$(this).attr('id'));
					}
				});
			});
			$('#expandShopping').click(function(){
				var answer = $('#divShopping .divFaqA');
				if (answer.hasClass("show")) {
					answer.removeClass("show");
					$('.divFaqQ').removeClass("active");
					$('.divFaqQ').removeClass("divFaqQ_close");
					$('.divFaqQ').addClass("divFaqQ_open");
					$(this).html('Expand all in this section');
				}
				else {
					answer.addClass("show");
					$('.divFaqQ').addClass("active");
					$('.divFaqQ').removeClass("divFaqQ_open");
					$('.divFaqQ').addClass("divFaqQ_close");
					$(this).html('Collapse all in this section');
				}
			});
			$('#expandCheckout').click(function(){
				var answer = $('#divCheckout .divFaqA');
				if (answer.hasClass("show")) {
					answer.removeClass("show");
					$('#divCheckout .divFaqQ').removeClass("active");
					$('#divCheckout .divFaqQ').removeClass("divFaqQ_close");
					$('#divCheckout .divFaqQ').addClass("divFaqQ_open");
					$(this).html('Expand all in this section');
				}
				else {
					answer.addClass("show");
					$('#divCheckout .divFaqQ').addClass("active");
					$('#divCheckout .divFaqQ').removeClass("divFaqQ_open");
					$('#divCheckout .divFaqQ').addClass("divFaqQ_close");
					$(this).html('Collapse all in this section');
				}
			});
		}
	},

	NextStepsPBBankPage: {
		onload: function(){
			log('ONLOAD.NextStepsPBBankPage.onload');

			Sym.TaxOverlay.init('right', 'middle');
		},

		mf: function(){
			log('ONLOAD.NextStepsPBPhonePage.mf');
			//$('#hlpLinkOpenedatLoad').trigger('onclick');
		}
	},

	NextStepsPBCheckPage: {
		onload: function(){
			log('ONLOAD.NextStepsPBCheckPage.onload');

			Sym.TaxOverlay.init('right', 'middle');
		},

		mf: function(){
			log('ONLOAD.NextStepsPBCheckPage.mf');
			//$('#hlpLinkOpenedatLoad').trigger('onclick');
		}
	},

	NextStepsPBPhonePage: {
		onload: function(){
			log('ONLOAD.NextStepsPBPhonePage.onload');

			Sym.TaxOverlay.init('right', 'middle');
		},

		mf: function(){
			log('ONLOAD.NextStepsPBPhonePage.mf');
			//$('#hlpLinkOpenedatLoad').trigger('onclick');
		}
	},

	NortonLiveServices: {
		onload: function(){
			log('ONLOAD.NortonLiveServices.onload');
			var nortonOnShow = function(hash){
				hash.w.css({
					'top': $('.divSolidWhiteBack').position().top + 75,
					'left': $('.divSolidWhiteBack').position().left + 40,
					'margin-left': 0
				}).fadeIn('2000', function(){
						hash.o.show();
					});
			}
			$('#nortonLiveOverlay').sym_interstitial({
				modal: true,
				onShow: nortonOnShow,
				trigger: null
			});
			$('.imgProductBoxshot').click(function(){
				$('.popupLayer').hide();
				$('#popup' + ($(this).index('.imgProductBoxshot') + 1)).show();
				$('#nortonLiveOverlay').sym_interstitial('show');
			});
			$('.overlayTrigger').click(function(){
				$('.popupLayer').hide();
				$('#popup' + ($(this).index('.overlayTrigger') + 1)).show();
				$('#nortonLiveOverlay').sym_interstitial('show');
			});
		}
	},

	OBTConfirmation: {
		onload: function(){
			log('ONLOAD.OBTConfirmation.onload');

			$(".protated_data_message").each(function () {
				$(this).sym_message_bubble({
					message: $(this).data("message")
				});
			});

			//Download Interstitial
			$('#downloadInterstitial').sym_interstitial({
				ajax: Sym.popupVars.OBTDownload,
				modal: true,
				trigger: ".triggerDownload"
			});
			//Delete whatever boxes are empty
			$('.billingAddressInfoBox:empty').remove();
			$('.shippingAddressInfoBox:empty').remove();
			$('.onlinePaymentInfoBox:empty').remove();

			//Make height of all info boxes the same
			$('.edit_containerMf').height($('.edit_containerMf').height());

			Sym.TaxOverlay.init('right', 'middle');
		}
	},

	OptionPage: {
		onload: function(){
			log('ONLOAD.OptionPage.onload');
		},
		rc: function(){
			log('ONLOAD.OptionPage.rc');
			//Show/Hide Compare Table
			$('#divCompareLink').click(function(){
				var $$ = $(this);
				if ($$.hasClass('divCompareinactive')) {
					$$.removeClass('divCompareinactive').addClass('divCompareactive');
					$('.visibleCompareRow').show();
					$$.parent().addClass('tdBorderBottom');
				}
				else {
					$('.visibleCompareRow').hide();
					$$.removeClass('divCompareactive').addClass('divCompareinactive');
					$$.parent().removeClass('tdBorderBottom');
				}
			});
		}
	},
	/*Edited on 15/6/2011 SITE-9635*/
	nssOptions: {
		onload: function () {
			log("ONLOAD.nssOptions.onload")
		},
		rc: function () {
			log("ONLOAD.nssOptions.rc");
			$("#divCompareLink").click(function () {
				var b = $(this);
				if (b.hasClass("divCompareinactive")) {
					b.removeClass("divCompareinactive").addClass("divCompareactive");
					$(".visibleCompareRow").show();
					b.parent().addClass("tdBorderBottom")
				} else {
					$(".visibleCompareRow").hide();
					b.removeClass("divCompareactive").addClass("divCompareinactive");
					b.parent().removeClass("tdBorderBottom")
				}
			})
		}
	},
	OrderConfirmation: {
		onload: function(){
			log('ONLOAD.OrderConfirmation.onload');

			$(".protated_data_message").each(function () {
				$(this).sym_message_bubble({
					message: $(this).data("message")
				});
			});

			//Make height of all info boxes the same
			$('.edit_containerMf').height($('.edit_containerMf').height());

			//Delete whatever boxes are empty
			$('.billingAddressInfoBox:empty').remove();
			$('.shippingAddressInfoBox:empty').remove();
			$('.onlinePaymentInfoBox:empty').remove();
			//Edit Online Bank PopUp
			$('#editOnlineBank').sym_interstitial({
				ajax: Sym.popupVars.OBTEditError,
				modal: true,
				positionEdit: {
					triggerID: "edit_onlineBank",
					windowID: "editOnlineBank"
				},
				trigger: 'a.editOnlineBank'
			});
			//Tax overlay
			Sym.TaxOverlay.init("right", "middle");
			//Download Interstitial
			var open = function(hash){
				hash.w.fadeIn('2000', function(){
					hash.o.show();
				});
			};
			$('#downloadInterstitial').sym_interstitial({
				ajax: Sym.popupVars.download,
				modal: true,
				trigger: false,
				onShow: open
			});

		}
	},

	OrderReview: {
		onload: function(){
			log('ONLOAD.OrderReview.onload');

			$(".protated_data_message").each(function () {
				$(this).sym_message_bubble({
					message: $(this).data("message")
				});
			});

			$('.disableCrtlClick').die("click");
			$('.disableCrtlClick').live("click",function(e) {
				if(e.ctrlKey  == true) {
					e.preventDefault();
				}
			});

			//Customer Edit Popup 
			$('#editCustomer').sym_interstitial({
				ajax: Sym.popupVars.editCustomer,
				modal: true,
				positionEdit: {
					triggerID: "edit_customer",
					windowID: "editCustomer"
				},
				trigger: 'a.editCustomer'
			});



			/* Edit DirectDebit Popup 
			 * Here I add the call to run the event scraper on the billing info popup. 
			 * I have to run it twice because wicket is doing some odd things.
			 * It is replacing an element multiple times, first just the element is replaced then the tr which contains it is replaced. 
			 * I couldn't think of a way to optimize this call. And to be honest, I don't have much time left.
			 */
			var directDebitLoad = function(hash){
				Sym.Misc.updateCreditCard($('select[name="cardType"] :selected').text());
				$("input,select",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"}
					});
					return false;
				});


				$("*",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input,select"
					});
					return false;
				});
			};


			/* These functions were added to make the red pop up error bubbles work the credit card error payment panel*/

			var paymentCustomLoad = function(hash){
				$("input,select",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"}
					});
					return false;
				});

				$("*",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input,select"
					});
					return false;
				});
			};
			var paymentCustomHide = function(hash){
				$('.sym_error_bubble', hash.w).sym_error_bubble('destroy');
				hash.o.remove();
				hash.w.hide();
			};
			$('#editPayment1').sym_interstitial({
				ajax: Sym.popupVars.onlinePaymentEdit,
				onLoad: paymentCustomLoad,
				onHide: paymentCustomHide,
				modal: true,
				positionEdit: {
					triggerID: "edit_payment",
					windowID: "editPayment1"
				},
				trigger: 'a.editPayment1'
			});

			/*This code hides an arrow image that is not needed*/

			if ($("#paymentPanelMainContainer")) {
				$.subscribe('/Interstitial/editPayment1/loaded', function (hash) {
					$(".bblRgtArrowSml").hide();
				});
			}

			/* End payment panel*/

			var directDebitHide = function(hash){
				$('.sym_error_bubble', hash.w).sym_error_bubble('destroy');
				hash.o.remove();
				hash.w.hide();
			};
			$('#editDirectDebit').sym_interstitial({
				ajax: Sym.popupVars.editDirectDebit,
				modal: true,
				positionEdit: {
					triggerID: "edit_directDebit",
					windowID: "editDirectDebit"
				},
				onLoad: directDebitLoad,
				onHide: directDebitHide,
				trigger: 'a.editDirectDebit'
			});
			/* Edit OBT Popup 
			 * Here I add the call to run the event scraper on the billing info popup. 
			 * I have to run it twice because wicket is doing some odd things.
			 * It is replacing an element multiple times, first just the element is replaced then the tr which contains it is replaced. 
			 * I couldn't think of a way to optimize this call. And to be honest, I don't have much time left.
			 */
			var bankLoad = function(hash){
				$("input,select",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"}
					});
					return false;
				});

				$("*",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input,select"
					});
					return false;
				});
			};
			var bankHide = function(hash){
				$('.sym_error_bubble', hash.w).sym_error_bubble('destroy');
				hash.o.remove();
				hash.w.hide();
			};
			$('#editOnlineBank').sym_interstitial({
				ajax: Sym.popupVars.editOnlineBank,
				onLoad: bankLoad,
				onHide: bankHide,
				modal: true,
				positionEdit: {
					triggerID: "edit_onlineBank",
					windowID: "editOnlineBank"
				},
				trigger: 'a.editOnlineBank'
			});
			//Submit Order		    
			$('#submitOrder').sym_interstitial({
				ajax: Sym.popupVars.progressIndicator,
				modal: true,
				center: {
					window: "submitOrder",
					offsetX: 0,
					offsetY: 450
				},
				trigger: 'a.submitOrder'
			});
			/* Edit Billing Popup 
			 * Here I add the call to run the event scraper on the billing info popup. 
			 * I have to run it twice because wicket is doing some odd things.
			 * It is replacing an element multiple times, first just the element is replaced then the tr which contains it is replaced. 
			 * I couldn't think of a way to optimize this call. And to be honest, I don't have much time left.
			 */
			var billingLoad = function(hash){
				$("input,select",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"}
					});
					return false;
				});

				$("*",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input,select"
					});
					return false;
				});
			};
			var billingHide = function(hash){
				$('.sym_error_bubble', hash.w).sym_error_bubble('destroy');
				hash.o.remove();
				hash.w.hide();
			};
			$('#editBilling').sym_interstitial({
				ajax: Sym.popupVars.editBillingAddress,
				modal: true,
				onLoad: billingLoad,
				onHide: billingHide,
				positionEdit: {
					triggerID: "edit_billing",
					windowID: "editBilling"
				},
				trigger: 'a.editBilling'
			});
			//Edit Shipping Popup                     
			$('#editShipping').sym_interstitial({
				ajax: Sym.popupVars.editShippingAddress,
				modal: true,
				positionEdit: {
					triggerID: "edit_shipping",
					windowID: "editShipping"
				},
				trigger: 'a.editShipping'
			});

			/*
			 * Edit Payment Pop-up
			 * Here I add the call to run the event scraper on the billing info popup. 
			 * I have to run it twice because wicket is doing some odd things.
			 * It is replacing an element multiple times, first just the element is replaced then the tr which contains it is replaced. 
			 * I couldn't think of a way to optimize this call. And to be honest, I don't have much time left.
			 */

			var paymentLoad = function(hash){
				Sym.Misc.updateCreditCard($('select[name="cardType"] :selected').text());
				$("input,select",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"}
					});
					return false;
				});

				$("*",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input,select"
					});
					return false;
				});
			};

			var paymentHide = function(hash){
				$('.sym_error_bubble', hash.w).sym_error_bubble('destroy');
				hash.o.remove();
				hash.w.hide();
				$.publish("/ErrorBubble/hide");
			};

			$('#editPayment').sym_interstitial({
				//Checking for the DigitalWallet payment type. 
				ajax: $("#DWpaymentType").length ? Sym.popupVars.editPaymentDigitalWallet : Sym.popupVars.editOnlinePayment,
				modal: true,
				onLoad: paymentLoad,
				onHide: paymentHide,
				positionEdit: {
					triggerID: "edit_payment",
					windowID: "editPayment"
				},
				trigger: 'a.editPayment'
			});

			//Make height of all info boxes the same
			$('.edit_containerMf').height($('.edit_containerMf').height());

			//Delete whatever boxes are empty
			$('.billingAddressInfoBox:empty').remove();
			$('.shippingAddressInfoBox:empty').remove();
			$('.onlinePaymentInfoBox:empty').remove();

			// VAT Initialization
			Sym.TaxOverlay.init("right", "middle");

		}
	},

	ProductDetails: {
		onload: function(){
			log('ONLOAD.ProductDetails.onload');
			$('#tabKeyFeatures').click(function(){
				Sym.Misc.showTabbedPanel('tabKeyFeatures', 'pnlKeyFeatures');
			});
			$('#tabSysReq').click(function(){
				Sym.Misc.showTabbedPanel('tabSysReq', 'pnlSysReq');
			});
		}
	},

	ProductBundleDetails: {
		onload: function(){
			log('ONLOAD.ProductBundleDetails.onload');
			$('#tabKeyFeatures').click(function(){
				Sym.Misc.showTabbedPanel('tabKeyFeatures', 'pnlKeyFeatures');
			});
			$('#tabSysReq').click(function(){
				Sym.Misc.showTabbedPanel('tabSysReq', 'pnlSysReq');
			});
		}
	},

	ProductDetailsMoreInfo: {
		onload: function(){
			log('ONLOAD.ProductDetailsMoreInfo.onload');
			$('#tabKeyFeatures').click(function(){
				Sym.Misc.showTabbedPanel('tabKeyFeatures', 'pnlKeyFeatures');
			});
			$('#tabSysReq').click(function(){
				Sym.Misc.showTabbedPanel('tabSysReq', 'pnlSysReq');
			});
		}
	},

	MAwarePD: {
		onload: function(){
			log('ONLOAD.MAwarePD.onload');
			/*code added for monthly radio button show hide */
			$("#buy_year").hide();
			$(".year_entitle").hide();
			//code which tests if monthly option exists then select that by default onload
			if($("#radio_month")){
				$("#radio_month").attr('checked', true);
			}
			//merchandising for monthly is not setup then hide monthly radio button and show yearly button
			if($("#buy_month").length === 0)
			{
				$("#rdmonth").hide();
				$("#radio_year").hide();
				$("#buy_year").show();
				$(".year_entitle").show();
				$(".month_entitle").hide();
			}
			$('.btn_mon_year').change(function(e){
				var targ = e.target;
				if(targ.id == "radio_month"){
					$('#buy_month').show();
					$('#buy_year').hide();
					$(".year_entitle").hide();
					$(".month_entitle").show();
				}
				else if(targ.id == "radio_year"){
					$('#buy_month').hide();
					$('#buy_year').show();
					$(".year_entitle").show();
					$(".month_entitle").hide();
				}
			});
			$('#tabKeyFeatures').click(function(){
				Sym.Misc.showTabbedPanel('tabKeyFeatures', 'pnlKeyFeatures');
			});
			$('#tabSysReq').click(function(){
				Sym.Misc.showTabbedPanel('tabSysReq', 'pnlSysReq');
			});
		}
	},

	MAwarePDMI: {
		onload: function(){
			log('ONLOAD.MAwarePDMI.onload');
			$('#tabKeyFeatures').click(function(){
				Sym.Misc.showTabbedPanel('tabKeyFeatures', 'pnlKeyFeatures');
			});
			$('#tabSysReq').click(function(){
				Sym.Misc.showTabbedPanel('tabSysReq', 'pnlSysReq');
			});
		}
	},

	ReviewOrderPBBankPage: {
		onload: function(){
			log('ONLOAD.ReviewOrderPBBankPage.onload');
			/* Edit Billing Popup 
			 * Here I add the call to run the event scraper on the billing info popup. 
			 * I have to run it twice because wicket is doing some odd things.
			 * It is replacing an element multiple times, first just the element is replaced then the tr which contains it is replaced. 
			 * I couldn't think of a way to optimize this call. And to be honest, I don't have much time left.
			 */

			$(".protated_data_message").each(function () {
				$(this).sym_message_bubble({
					message: $(this).data("message")
				});
			});

			var billingLoad = function(hash){
				$("input,select",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"}
					});
					return false;
				});

				$("*",hash.w).live("wicketAjaxReplace", function(ev){
					$(ev.target).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input,select"
					});
					return false;
				});
			};
			var billingHide = function(hash){
				$('.sym_error_bubble', hash.w).sym_error_bubble('destroy');
				hash.o.remove();
				hash.w.hide();
			};
			$('#editBilling').sym_interstitial({
				ajax: Sym.popupVars.editBillingAddress,
				modal: true,
				onLoad: billingLoad,
				onHide: billingHide,
				positionEdit: {
					triggerID: "edit_billing",
					windowID: "editBilling"
				},
				trigger: 'a.editBilling'
			});
			//Submit Order		    
			$('#submitOrder').sym_interstitial({
				ajax: Sym.popupVars.progressIndicator,
				modal: true,
				center: {
					window: "submitOrder",
					offsetX: 0,
					offsetY: 450
				},
				trigger: 'a.submitOrder'
			});
			$('#editShipping').sym_interstitial({
				ajax: Sym.popupVars.editShippingAddress,
				modal: true,
				positionEdit: {
					triggerID: "edit_shipping",
					windowID: "editShipping"
				},
				trigger: 'a.editShipping'
			});

			Sym.TaxOverlay.init('right', 'middle');
		}
	},

	ShoppingCart: {
		onload: function(){
			log("ONLOAD.ShoppingCart.onload");

			$(".protated_data_message").each(function () {
				$(this).sym_message_bubble({
					message: $(this).data("message")
				});
			});


			Sym.TaxOverlay.init("right", "middle");
			//CandyRack
			//Sym.CandyRack.init();
			$(".candyrack-container").sym_candy_rack();
			//Interstitial			
			var load = function(hash){
				var interstitialType;
				if ($('#upsell_interstitial_wrapper').length) {
					interstitialType = 'upsell';
				}
				else if ($('.xInter').length) {
					interstitialType = 'xsell';
				}
				var pageType;
				if ($('body').hasClass('mf')) {
					pageType = 'mf';
				}
				else {
					pageType = 'rc';
				}
				var topOffset;
				var leftOffset = 300;
				var cartIntBG;
				if (interstitialType == 'upsell') {
					if (pageType == 'rc') {
						topOffset = -25;
					}
					else {
						topOffset = -30;
					}
					cartIntBG = 'none';
				}
				else {
					if (interstitialType == 'xsell') {
						topOffset = -5;
						cartIntBG = 'white';
					}
				}
				hash.w.css('top', $('.divSolidWhiteBack').position().top + topOffset);
				hash.w.css('left', $('.divSolidWhiteBack').position().left + leftOffset);
				$('#cartIcvcvcnterstitial').css('background', cartIntBG);
				Sym.Interstitial.ttCartInterstitial();
			};
			var open = function(hash){
				hash.w.fadeIn('2000', function(){
					hash.o.show();
				});
				$.publish("/Button/but_addtocart/disable");
				$.publish("/Button/button_continue/disable");
			};
			var hide = function(hash){
				$.publish("/Button/but_addtocart/disable");
				$.publish("/Button/button_continue/disable");
				hash.o.remove();
				hash.w.hide();
			};
			$('#cartIcvcvcnterstitial').sym_interstitial({
				ajax: Sym.popupVars.interstitial,
				modal: true,
				trigger: false,
				onShow: open,
				onLoad: load,
				onHide: hide
			});

			$(".shopingCartTableForm").live("wicketAjaxReplace", function(ev){
				//Don't run for descendants
				if(ev.target === this) {
					//Scrape error bubble events
					$(this).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"},
						selector: "input"
					});
					return false;
				}
			});

			$(".applyCoupon input").live("wicketAjaxReplace", function(ev){
				//Don't run for descendants
				if(ev.target === this) {
					//Scrape error bubble events
					$(this).sym_event_scraper({
						event: "onFocus",
						eventType: "showErrorBubble",
						plugin: "sym_error_bubble",
						pluginParams: {1:"message"}
					});
					return false;
				}
			});

			// Fixing SPARK-37231. Validation for quantity field on cart page

			$( ".inputQty" ).live( "change", function() {

				setTimeout(function(){

					if($('.error').length>0){
						$( ".inputQty" ).trigger( "focus" );
						$(".inputQty").sym_error_bubble({
							message: errorMsg
						}).sym_error_bubble("show");

					}else{
						$(".inputQty").sym_error_bubble("destroy");
						$.publish("/ErrorBubble/hide");
					}

				},1500);
			});


			/**
			 *	.editLink clicked to show the slider overlay:
			 *    - BAS slider overlay,
			 *    - BMS slider overlay,
			 *    - Double slider overlay
			 **/
			var editLink = $('.editLink');
			if (editLink.length > 0) {
				var options = {
					ajaxURL: editLink.attr('href')
				};
				editLink.attr('href', '#');
				//options.ajaxURL = '/w/ProjectA/HTML/common/page/MawareDoubleSlidersOverlay.html';
				var sliderOverlayCtl = $('.popupPanel').sym_slider_overlay(options);
				if (sliderOverlayCtl !== undefined) {
					sliderOverlayCtl = sliderOverlayCtl.controller();
				}
				editLink.live("click", function(){
					sliderOverlayCtl.show();
					if ($.browser.msie && parseInt($.browser.version) == 7) {
						$("body").append($("#popupPanel"));
					}
				});
			}

			//For RC flow Breadcrumb adjustments
			$('<em> &gt; </em>').appendTo('.renewalNavItem span').last(".renewalNavItem span").text("");

			//For Credit card alignments
			$("#divCreditCardWrapper a:eq(4)").css({"padding-right":"0px"});
			$("#divCreditCardWrapper a:eq(3)").css({"padding-right":"0px"});
		}
	},

	SignIn: {
		onload: function(){
			log('ONLOAD.SignIn.onload');
			function switchSignInLayer(){
				$.publish("/ErrorBubble/hide");
				if (document.getElementById("forgotPasswordLayer").style.display == 'none') {
					document.getElementById("signInLayer").style.display = 'none';
					document.getElementById("forgotPasswordLayer").style.display = 'block';
				}
				else {
					document.getElementById("forgotPasswordLayer").style.display = 'none';
					document.getElementById("signInLayer").style.display = 'block';
				}
			}
			function switchCreateAccountLayer(){
				$.publish("/ErrorBubble/hide");
				if (document.getElementById("createNortonAccount").style.display == 'none') {
					document.getElementById("signInLayer").style.display = 'none';
					document.getElementById("forgotPasswordLayer").style.display = 'none';
					document.getElementById("createNortonAccount").style.display = 'block';
				}
				else {
					document.getElementById("forgotPasswordLayer").style.display = 'none';
					document.getElementById("signInLayer").style.display = 'none';
					document.getElementById("createNortonAccount").style.display = 'none';
				}
			}
			function switchToSignIn(){
				$.publish("/ErrorBubble/hide");
				if (document.getElementById("signInLayer") != null) {
					document.getElementById("signInLayer").style.display = 'block';
				}
				else {
					document.getElementById("loginRetryLayer").style.display = 'block';
				}
				document.getElementById("forgotPasswordLayer").style.display = 'none';
				document.getElementById("createNortonAccount").style.display = 'none';
			}
		}
	},

	SignInPageDummy: {
		onload: function(){
			log('ONLOAD.SignInPageDummy.onload');
			function showForgotPasswordLayer(){
				document.getElementById("forgotPasswordLayer").style.display = 'block';
				document.getElementById("signInLayer").style.display = 'none';
			}
			function showCreateAccountLayer(){
				document.getElementById("createNortonAccount").style.display = 'block';
				document.getElementById("signInLayer").style.display = 'none';
			}
		}
	},

	StorePage: {
		onload: function(){
			log('ONLOAD.StorePage.onload');
			Sym.Misc.loadingPageAnimation.hide();
		}
	},

	ThankYouPage: {
		onload: function(){
			log('ONLOAD.ThankYouPage.onload');
			//Thank you popup
			var open = function(hash){
				hash.w.fadeIn('2000', function(){
					hash.o.show();
				});
			};
			$('#thankYouInterstitial').sym_interstitial({
				ajax: Sym.popupVars.thankYou,
				modal: true,
				trigger: false,
				intWidth: "500",
				onShow: open
			}).sym_interstitial('show').sym_interstitial('drag');
		}
	},

	UpgradeCenterPage: {
		onload: function(){
			log('ONLOAD.UpgradeCenterPage.onload');
			var surveyInit = function(hash){
				$('#surveyWindow .yes').click(function(){
					Sym.Misc.openPopup('https://www.surveymonkey.com/s/GX63939', '750', '750', 'no', 'no', 'no', 'no', 'no', 'no', 'no', '100', '200');
				});
			};

			function popupSurvey(){
				$('#surveyWindow').sym_interstitial({
					/*ajax: '/estore/mf/whatvisa?action=90',*/
					onLoad: surveyInit,
					trigger: false,
					modal: true
				}).sym_interstitial('show');
			}
		}
	},

	OptionsRenewal: {
		onload: function() {
			// BAS Slider	
			//var currentNewQty = parseInt(window.currentSeatUnit_slot_1);
			//var currentRenewalQty = parseInt(window.currentRenewalSeatUnit_slot_1);


			//console.log('currentNewQty', currentNewQty);
			//console.log('currentRenewalQty', currentRenewalQty);

			var step = parseInt(window.seatPerUnit_slot_4),
				min = parseInt(window.minSeatUnit_slot_1),
				max = parseInt(window.maxSeatUnit_slot_4),
				renewal = parseInt(window.seatPerUnit_slot_2) || 0;
			//console.log('setUpStepbas', step);		
			var options = {
				min: min,
				max: max,
				step: step,
				currentRenewalQty: window.currentRenewalSeatUnit_slot_1,
				currentNewQty: window.currentSeatUnit_slot_1,
				//initValue : parseInt(window.minSeatUnit_slot_1 + (window.currentSeatUnit_slot_1 * window.seatPerUnit_slot_4) + (window.currentRenewalSeatUnit_slot_1 * step)),
				initValue : parseInt(min + (window.currentSeatUnit_slot_1 * step) + (window.currentRenewalSeatUnit_slot_1 * renewal)),
				updateAddLinkObj: {
					add: $('#addBAS'),
					type: 'SeatQty',
					updateAddLink: true
				},
				updateCheckout: true
			}
			//console.log("basOptions", options);
			$('.basSliderPanel .sliderWrapper').sym_single_slider(options);

			// BMS Slider
			//var currentNewQty = parseInt(window.currentStorageUnit_slot_1);
			//var currentRenewalQty = parseInt(window.currentRenewalStorageUnit_slot_1);

			//console.log('currentNewQty', currentNewQty);
			//console.log('currentRenewalQty', currentRenewalQty);

			//console.log('setUpCurrentRbms', currentRenewalQty);
			var step = parseFloat(window.storagePerUnit_slot_5),
				min = parseFloat(window.minStorageUnit_slot_1),
				max = parseFloat(window.maxStorageUnit_slot_5),
				renewal = parseFloat(window.storagePerUnit_slot_3) || 0;
			//console.log('setUpStepbms', step);	
			var options = {
				min: min,
				max: max,
				step: step,
				currentRenewalQty: window.currentRenewalStorageUnit_slot_1,
				currentNewQty: parseInt(window.currentStorageUnit_slot_1),
				initValue : parseFloat(min + (window.currentStorageUnit_slot_1 * step)+ (window.currentRenewalStorageUnit_slot_1 * renewal)),
				updateAddLinkObj: {
					add: $('#addBMS'),
					type: 'StorageQty',
					updateAddLink: true
				},
				updateCheckout: true
			}
			$('.bmsSliderPanel .sliderWrapper').sym_single_slider(options);
			$('.dualAdd').click(function(){
				Sym.SliderActions.dblSliderOverlayUpdateBtn();
			});

			//Added code for eV2 Monthly
			/* On load it will check the radio link. Depending on radio link do monthly/yearly information visible and hide*/

			var maware_RadioLink = $("#maware_RadioLink").attr("href");

			if(maware_RadioLink != undefined  ){
				if(maware_RadioLink.search("monthly/false") != -1){
					$("#radio_month").attr("checked","checked");
					$("#memYear").hide();
					$("#infoYear").hide();
					$("#infoBottomYear").hide();
				}
				else if(maware_RadioLink.search("monthly/true") != -1){
					$("#radio_year").attr("checked","checked");
					$("#memMonth").hide();
					$("#infoMonth").hide();
					$("#infoBottomMonth").hide();
				}
			}
			/* Code for marchandising if radio link is blank check for monthly and yearly price */
			else if(maware_RadioLink == "" || maware_RadioLink == undefined  ){
				if( $("#radio_monthly").html() == "") {
					$("#memMonth").hide();
					$("#infoMonth").hide();
					$("#infoBottomMonth").hide();
					$("#desc_year").show();
					$("#rdmonth").hide();
					$("#radio_year").hide();
				}
				else if( $("#radio_yearly").html() == ""){
					$("#memYear").hide();
					$("#infoYear").hide();
					$("#infoBottomYear").hide();
					$("#desc_month").show();
					$("#rdyear").hide();
					$("#radio_month").hide();
				}
			}

			/* On radio button change set the radiolink href value to window href. Page will be reload */
			$('.btn_mon_year').change(function(e){
				window.location.href=$("#maware_RadioLink").attr("href");
			});
		}
	},

	DoubleSlidersOverlay: {
		onload: function(){
		}
	},

	MawareRenewalProduct: {
		onload: function(){
			log('ONLOAD.MawareRenewalProduct.onload');

			/**
			 *	.editProduct clicked to show the slider overlay:
			 *    - BAS slider overlay,
			 *    - BMS slider overlay,
			 *    - Double slider overlay
			 */
			var editLink = $('.editLink');
			var options = {
				ajaxURL: editLink.attr('href')
			};
			editLink.attr('href', '#');
			//options.ajaxURL = '/w/ProjectA/HTML/common/page/MawareDoubleSlidersOverlay.html';
			var sliderOverlayCtl = $('.popupPanel').sym_slider_overlay(options);
			if (sliderOverlayCtl !== undefined) {
				sliderOverlayCtl = sliderOverlayCtl.controller();
			}
			editLink.live("click", function(){
				sliderOverlayCtl.show();
			});
		}
	}

};

/******* DO NOT EDIT THIS UNLESS YOU KNOW WHAT YOU ARE DOING *******/

/****************************************************************************
 * LOADER
 * Fires the onload functions specified by a page's HTML markup on document.ready
 *
 * id on body element specifies what page object to use
 * classes on body specify what functions within the page object to invoke
 * The onload function of a page object is always invoked
 ***************************************************************************/
LOADER = {
	/**
	 * Invoke a function from our site's namespace
	 * Will always call onload function then any other functions specified by the body classes
	 *
	 * @param String func - Name of page object (in namespace)
	 * @param String funcname - Name of function (optional)
	 * @param Array args - Arguments to the function (optional)
	 */
	fire: function(func, funcname, args){
		var namespace = ONLOAD;
		//If no function name is provided, use onload
		funcname = (funcname === undefined) ? 'onload' : funcname;
		//Check that the function exists and call it
		if (func !== '' && namespace[func] && typeof namespace[func][funcname] == 'function') {
			namespace[func][funcname](args);
		}
	},
	/**
	 * Initializtion function
	 * Common is always called
	 * Body id determines the page name
	 * Body classes determine what functions to call (onload is always called)
	 */
	loadEvents: function(){
		//Always execute Common		
		LOADER.fire('Common');
		var pageName = document.body.id;
		//Fire the common static, mf or rc function if static, mf or rc classes are present 		
		$.each(document.body.className.split(/\s+/), function(i, classnm){
			if (classnm == "seostatic") {
				LOADER.fire('Common', 'seostatic');
			}
			else if (classnm == "rc") {
				LOADER.fire('Common', 'rc');
			}
			else if (classnm == "mf") {
				LOADER.fire('Common', 'mf');
			}
		});
		//Fires the onload function		
		LOADER.fire(pageName);
		//Then fire all other onloads
		$.each(document.body.className.split(/\s+/), function(i, classnm){
			LOADER.fire(pageName, classnm);
		});
	}
}
//Get the ball rolling
$(document).ready(LOADER.loadEvents);

// Added for eASDSprint4 The goal here is to re-factor this into the larger namespace

function getCookie(c_name)
{
	try{
		var name=c_name+"=";
		var cookies	=  document.cookie.split(';');

		var cookieHex;
		for(var i=0;i < cookies.length;i++){
			var c=cookies[i];
			while(c.charAt(0)==' '){
				c=c.substring(1,c.length)
			};
			if(c.indexOf(name)==0){
				cookieHex =  unescape(c.substring(name.length,c.length));

			}

		}


		var cookieString = hexToString(cookieHex);


		return cookieString;

	}catch(e){

	}
}

// Code to handle cookie string obtained from above method

function updateCookie(cookieString,cid,eid) {

	//cid:123,234|eid:aaa,bbb


	var cidString = "";
	var eidString = "";
	var cidKey = "";
	var eidKey = "";
	var cidValue = "";
	var eidValue = "";
	var strings = cookieString.split("|");
	var finalString = "";
	var cookieName = "tt";

	if(strings != null)
	{

		// if order of cid and eid changed
		var temp1 = strings[0];
		var temp2 = strings[1];



		if(temp1.indexOf('CID') == -1  && temp1.indexOf('cid') == -1) {

			eidString = temp1;
			cidString = temp2;
		}

		if(temp1.indexOf('EID') == -1  && temp1.indexOf('eid') == -1) {

			cidString = temp1;
			eidString = temp2;
		}


	}

	// CID 
	if(cidString != null)
	{
		var cidStringToken = cidString.split(":");
		cidKey = cidStringToken[0]; //cid
		cidValue = cidStringToken[1]; //123,234

		if(cidValue !=null)
		{
			cidValue = cidValue.concat(',').concat(cid);

		}
	}else {

		cidKey = "CID";
		cidValue = cid;
	}

	//EID			
	if(eidString != null)
	{
		var eidStringToken = eidString.split(":");

		eidKey = eidStringToken[0]; //eid
		eidValue = eidStringToken[1]; //aaa,bbb				
		if(eidValue !=null)
		{
			eidValue = eidValue.concat(',').concat(eid);
		}

	}else {

		eidKey = "EID";
		eidValue = eid;
	}

	finalString = cidKey.concat(':').concat(cidValue).concat('|').concat(eidKey).concat(':').concat(eidValue);


	return finalString;
}


function encodeToHex(str){

	var text2='';
	for (i=0;i<str.length;i++)
		text2 += str.charCodeAt(i).toString(16);

	return text2;
}



function setCookie(name,value,days){
	try{
		value = encodeToHex(value);
		var expires = "" ;
		var path = "/";
		var domain = "norton.com";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
		}

		var cookie_string = name + "=" + escape ( value ) + expires;

		if ( path )
			cookie_string += "; path=" + escape ( path );

		if ( domain )
			cookie_string += "; domain=" + escape ( domain );

		document.cookie = cookie_string;

	}catch(e){}
}



function execute(c_name,cid,eid) {

	var exdays= 14;
	var cookieString = getCookie(c_name);

	if(cookieString != null)
	{
		//hexadicmal convertor required to convert cookie							
		var finalCookie = updateCookie(cookieString,cid,eid);

		setCookie(c_name,finalCookie,exdays);

	}
	else
	{
		var new_Cookie = 'CID'.concat(':').concat(cid).concat('|').concat('EID').concat(':').concat(eid);
		setCookie(c_name,unescape(new_Cookie),exdays);


	}

}

function hexToString(data)
{

	if(data !=null)
	{

		var b16_digits = '0123456789abcdef';
		var b16_map = new Array();
		for (var i=0; i<256; i++) {
			b16_map[b16_digits.charAt(i >> 4) + b16_digits.charAt(i & 15)] = String.fromCharCode(i);
		}
		if (!data.match(/^[a-f0-9]*$/i)) return false;// return false if input data is not a valid Hex string

		if (data.length % 2) data = '0'+data;

		var result = new Array();
		var j=0;
		for (var i=0; i<data.length; i+=2) {
			result[j++] = b16_map[data.substr(i,2)];
		}

		return result.join('');
	}

	return null;
}


function emeaPopupCheck(){
	$.publish("/ErrorBubble/hide");
	$('.autoRenewPopup').show();
	$('body').append('<div class="modalOverlay"></div>');
}
$( document ).ready(function() {
	if($("body.NSB").length > 0){
		setTPCookie();
	}
	function setTPCookie(){
		var cookie = getCookie("tp");
		var array = cookie.split('|')
		var newCookie ='';
		for(var x=0;x<array.length;x++)
		{

			if($.trim(array[x]).indexOf("IPS=") == 0)
			{
				array[x] = "IPS="
			}
			newCookie +=(x==0)?array[x]:"|"+array[x];

		}
		setCookie("tp",newCookie,0);
	}
});


/*!
 * jQuery postMessage - v0.5 - 9/11/2009
 * http://benalman.com/projects/jquery-postmessage-plugin/
 * 
 * Copyright (c) 2009 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

// Script: jQuery postMessage: Cross-domain scripting goodness
//
// *Version: 0.5, Last updated: 9/11/2009*
// 
// Project Home - http://benalman.com/projects/jquery-postmessage-plugin/
// GitHub       - http://github.com/cowboy/jquery-postmessage/
// Source       - http://github.com/cowboy/jquery-postmessage/raw/master/jquery.ba-postmessage.js
// (Minified)   - http://github.com/cowboy/jquery-postmessage/raw/master/jquery.ba-postmessage.min.js (0.9kb)
// 
// About: License
// 
// Copyright (c) 2009 "Cowboy" Ben Alman,
// Dual licensed under the MIT and GPL licenses.
// http://benalman.com/about/license/
// 
// About: Examples
// 
// This working example, complete with fully commented code, illustrates one
// way in which this plugin can be used.
// 
// Iframe resizing - http://benalman.com/code/projects/jquery-postmessage/examples/iframe/
// 
// About: Support and Testing
// 
// Information about what version or versions of jQuery this plugin has been
// tested with and what browsers it has been tested in.
// 
// jQuery Versions - 1.3.2
// Browsers Tested - Internet Explorer 6-8, Firefox 3, Safari 3-4, Chrome, Opera 9.
// 
// About: Release History
// 
// 0.5 - (9/11/2009) Improved cache-busting
// 0.4 - (8/25/2009) Initial release

(function($){
	'$:nomunge'; // Used by YUI compressor.

	// A few vars used in non-awesome browsers.
	var interval_id,
		last_hash,
		cache_bust = 1,

	// A var used in awesome browsers.
		rm_callback,

	// A few convenient shortcuts.
		window = this,
		FALSE = !1,

	// Reused internal strings.
		postMessage = 'postMessage',
		addEventListener = 'addEventListener',

		p_receiveMessage,

	// I couldn't get window.postMessage to actually work in Opera 9.64!
		has_postMessage = window[postMessage]; // && !$.browser.opera;

	// Method: jQuery.postMessage
	// 
	// This method will call window.postMessage if available, setting the
	// targetOrigin parameter to the base of the target_url parameter for maximum
	// security in browsers that support it. If window.postMessage is not available,
	// the target window's location.hash will be used to pass the message. If an
	// object is passed as the message param, it will be serialized into a string
	// using the jQuery.param method.
	// 
	// Usage:
	// 
	// > jQuery.postMessage( message, target_url [, target ] );
	// 
	// Arguments:
	// 
	//  message - (String) A message to be passed to the other frame.
	//  message - (Object) An object to be serialized into a params string, using
	//    the jQuery.param method.
	//  target_url - (String) The URL of the other frame this window is
	//    attempting to communicate with. This must be the exact URL (including
	//    any query string) of the other window for this script to work in
	//    browsers that don't support window.postMessage.
	//  target - (Object) A reference to the other frame this window is
	//    attempting to communicate with. If omitted, defaults to `parent`.
	// 
	// Returns:
	// 
	//  Nothing.

	$[postMessage] = function( message, target_url, target ) {
		if ( !target_url ) { return; }

		// Serialize the message if not a string. Note that this is the only real
		// jQuery dependency for this script. If removed, this script could be
		// written as very basic JavaScript.
		message = typeof message === 'string' ? message : $.param( message );

		// Default to parent if unspecified.
		target = target || parent;

		if ( has_postMessage ) {
			// The browser supports window.postMessage, so call it with a targetOrigin
			// set appropriately, based on the target_url parameter.
			target[postMessage]( message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1' ) );

		} else if ( target_url ) {
			// The browser does not support window.postMessage, so set the location
			// of the target to target_url#message. A bit ugly, but it works! A cache
			// bust parameter is added to ensure that repeat messages trigger the
			// callback.
			target.location = target_url.replace( /#.*$/, '' ) + '#' + (+new Date) + (cache_bust++) + '&' + message;
		}
	};

	// Method: jQuery.receiveMessage
	// 
	// Register a single callback for either a window.postMessage call, if
	// supported, or if unsupported, for any change in the current window
	// location.hash. If window.postMessage is supported and source_origin is
	// specified, the source window will be checked against this for maximum
	// security. If window.postMessage is unsupported, a polling loop will be
	// started to watch for changes to the location.hash.
	// 
	// Note that for simplicity's sake, only a single callback can be registered
	// at one time. Passing no params will unbind this event (or stop the polling
	// loop), and calling this method a second time with another callback will
	// unbind the event (or stop the polling loop) first, before binding the new
	// callback.
	// 
	// Also note that if window.postMessage is available, the optional
	// source_origin param will be used to test the event.origin property. From
	// the MDC window.postMessage docs: This string is the concatenation of the
	// protocol and "://", the host name if one exists, and ":" followed by a port
	// number if a port is present and differs from the default port for the given
	// protocol. Examples of typical origins are https://example.org (implying
	// port 443), http://example.net (implying port 80), and http://example.com:8080.
	// 
	// Usage:
	// 
	// > jQuery.receiveMessage( callback [, source_origin ] [, delay ] );
	// 
	// Arguments:
	// 
	//  callback - (Function) This callback will execute whenever a <jQuery.postMessage>
	//    message is received, provided the source_origin matches. If callback is
	//    omitted, any existing receiveMessage event bind or polling loop will be
	//    canceled.
	//  source_origin - (String) If window.postMessage is available and this value
	//    is not equal to the event.origin property, the callback will not be
	//    called.
	//  source_origin - (Function) If window.postMessage is available and this
	//    function returns false when passed the event.origin property, the
	//    callback will not be called.
	//  delay - (Number) An optional zero-or-greater delay in milliseconds at
	//    which the polling loop will execute (for browser that don't support
	//    window.postMessage). If omitted, defaults to 100.
	// 
	// Returns:
	// 
	//  Nothing!

	$.receiveMessage = p_receiveMessage = function( callback, source_origin, delay ) {
		if ( has_postMessage ) {
			// Since the browser supports window.postMessage, the callback will be
			// bound to the actual event associated with window.postMessage.

			if ( callback ) {
				// Unbind an existing callback if it exists.
				rm_callback && p_receiveMessage();

				// Bind the callback. A reference to the callback is stored for ease of
				// unbinding.
				rm_callback = function(e) {
					if ( ( typeof source_origin === 'string' && e.origin !== source_origin )
						|| ( $.isFunction( source_origin ) && source_origin( e.origin ) === FALSE ) ) {
						return FALSE;
					}
					callback( e );
				};
			}

			if ( window[addEventListener] ) {
				window[ callback ? addEventListener : 'removeEventListener' ]( 'message', rm_callback, FALSE );
			} else {
				window[ callback ? 'attachEvent' : 'detachEvent' ]( 'onmessage', rm_callback );
			}

		} else {
			// Since the browser sucks, a polling loop will be started, and the
			// callback will be called whenever the location.hash changes.

			interval_id && clearInterval( interval_id );
			interval_id = null;

			if ( callback ) {
				delay = typeof source_origin === 'number'
					? source_origin
					: typeof delay === 'number'
					? delay
					: 100;

				interval_id = setInterval(function(){
					var hash = document.location.hash,
						re = /^#?\d+&/;
					if ( hash !== last_hash && re.test( hash ) ) {
						last_hash = hash;
						callback({ data: hash.replace( re, '' ) });
					}
				}, delay );
			}
		}
	};

})(jQuery);